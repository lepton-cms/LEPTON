# LEPTON
Repository for cms LEPTON.<br />
We are sure, LEPTON fits all your needs.


## Documentation
Please use the documentation to stay informed about modifications of LEPTON CMS, these informations may be helpful.<br />
Documentation and tipps how to use the cms can be found on the [LEPTON Documentation site][1].<br />
Core-Documentation (php documenter) can be found on the [core docs][2].


## Download
You can download all available packages [on our homepage][3] in the download area.<br />
Please don't use the [repository][4] for productive installations, because this is a developers SVN only. <br />


## Addons
A lot of Addons (extensions) can be found on the [Addons-Repository][10].


## Issues
Report all issues from current stable release to [issue tracker][5] in the svn.<br />
Please have a look on [our forum][6] and also on the [closed issues][7] before you open an new issue.


## Support
You can find many helpful informations in the [docs][1].<br />
If you need some help feel free to post on [our forum][6]. Please keep in mind that you have to register to get access.


## License
LEPTON Core and all addons included in the delivered package are free and (mostly) released under the [GNU General Public License][8].<br />

For more information about LEPTON project visit [our homepage][9]. <br />

Copyright (C) 2010-2025 [LEPTON CMS Project][9].

[1]: https://doc.lepton-cms.org
[2]: https://doc.lepton-cms.org/documentation/LEPTON/html/index.html
[3]: https://lepton-cms.org/english/download.php
[4]: https://gitlab.com/lepton-cms/LEPTON
[5]: https://gitlab.com/lepton-cms/LEPTON/issues
[6]: https://forum.lepton-cms.org
[7]: https://gitlab.com/lepton-cms/LEPTON/issues?scope=all&utf8=%E2%9C%93&state=closed
[8]: https://gnu.org/licenses/gpl-3.0.txt
[9]: https://lepton-cms.org
[10]: https://lepton-cms.com