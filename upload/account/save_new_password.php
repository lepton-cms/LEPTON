<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// Required page details
$page_id = 0;
$page_description = '';
$page_keywords = '';
define('PAGE_ID', 0);
define('ROOT_PARENT', 0);
define('PARENT', 0);
define('LEVEL', 0);
define('PAGE_TITLE', $MENU['FORGOT']);
define('MENU_TITLE', $MENU['FORGOT']);
define('VISIBILITY', 'public');

// Set the page content 
if(isset($_POST['hash']) && ($_POST['hash'] != "") ) 
{
	$confirm = intval($_POST['hash']);
} 
else
{
	header('Location: '.LEPTON_URL.'/index.php');
	exit(0);
}

// check time and hash
if (intval($_POST['r_time']) > ($confirm + 3600)) 
{
	die(LEPTON_tools::display($MESSAGE['FORGOT_CONFIRM_OLD'],'div','ui red message'));	
}

if(isset($_POST['signup']) && ($_POST['signup'] == "1") ) 
{
	$signup = true;
} 
else 
{
	$signup = false;
}

if(isset($_POST['email']) && ($_POST['email'] != "") ) 
{
	$user_email = $_POST['email'];
	
	if (false === LEPTON_handle::checkEmailChars($user_email) ) 
	{
		header('Location: '.LEPTON_URL.'/index.php');
		exit(0);
	}
} 
else 
{
	header('Location: '.LEPTON_URL.'/index.php');
	exit(0);
}

if(isset($_POST['new_password']) && ($_POST['new_password'] != "") ) 
{
	$new_password = $_POST['new_password'];
} 
else 
{
	$new_password = NULL; 
}

if(isset($_POST['new_password2']) && ($_POST['new_password2'] != "") ) 
{
	$new_password2 = $_POST['new_password2'];
} 
else 
{
	$new_password2 = NULL; 
}

//	get user data
$user = [];
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."users` WHERE login_ip = '".$confirm."' AND email = '".$user_email."' ",
	true,
	$user,
	false
);

	
if(!empty($user)) 
{
	if( $new_password != $new_password2 )
	{		
		$_SESSION["new_password_message"]= LEPTON_tools::display($MESSAGE['PREFERENCES_PASSWORD_MATCH'],'div','ui red message');		
	} 
	else 
	{	
		// check if password matches requirements
		if(strlen($new_password)< AUTH_MIN_PASS_LENGTH) 
		{
			$_SESSION["new_password_message"]= LEPTON_tools::display($MESSAGE['LOGIN_PASSWORD_TOO_SHORT'],'div','ui red message');	
		} 
		elseif (strlen($new_password) > AUTH_MAX_PASS_LENGTH ) 
		{
			$_SESSION["new_password_message"]= LEPTON_tools::display($MESSAGE['LOGIN_PASSWORD_TOO_LONG'],'div','ui red message');			
		} 
		else 
		{
			// save into database
			$fields = array(
				'login_ip'	=>	$_SERVER['REMOTE_ADDR'],
				'password'	=>	password_hash( $new_password, PASSWORD_DEFAULT),
				'last_reset'=>	intval($_POST['r_time'])
			);
			
			$database->build_and_execute( 'UPDATE', TABLE_PREFIX."users", $fields,"login_ip= '".$confirm."'");
																	
			$_SESSION["new_password_message"] = LEPTON_tools::display($MESSAGE['PREFERENCES_PASSWORD_CHANGED'],'div','ui positive message');
			
			//send confirmation link to email
			$mail = LEPTON_mailer::getInstance();
			$mail->CharSet = DEFAULT_CHARSET;	
			//Set who the message is to be sent from
			$mail->setFrom(SERVER_EMAIL);
			//Set who the message is to be sent to
			$mail->addAddress($user['email']);
			//Set the subject line
			$mail->Subject = $MESSAGE['SIGNUP2_SUBJECT_LOGIN_INFO'];
			//Switch to TEXT messages
			$mail->IsHTML(true);
			$mail->Body = sprintf($MESSAGE['FORGOT_PASSWORD_SUCCESS'],$user['username']);					
			
			if (!$mail->send()) 
			{
				$_SESSION["new_password_message"] = "Mailer Error: " . $mail->ErrorInfo;
			} 
			else 
			{
				$message = $MESSAGE['FORGOT_PASSWORD_SUCCESS'];
			}
		}
	}


	if(!isset($_SESSION["new_password_message"])) 
	{
		define('PAGE_CONTENT', LEPTON_PATH.'/index.php');
	} 
	else 
	{
		define('PAGE_CONTENT', LEPTON_PATH.'/account/new_password_form.php');
	}

	// Set auto authentication to false
	$auto_auth = false;
}
else
{
	die(LEPTON_tools::display($MESSAGE['SIGNUP_NO_EMAIL'],'div','ui red message'));
}

require(LEPTON_PATH.'/index.php');
