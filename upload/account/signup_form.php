<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// [1] See if there exists a frontend template file or use the fallback
if (LEPTON_handle::require_alternative('login/signup_form.php', 'frontend'))
{
    return 0;
}
else
{
	$oTWIG = lib_twig_box::getInstance();
	
	// Register path to make sure twig is looking in this module template folder first
	$oTWIG->registerPath(LEPTON_PATH."/account/templates/");

	// Additional hash
	$hash = sha1( microtime().$_SERVER['HTTP_USER_AGENT'] );
	$_SESSION['wb_apf_hash'] = $hash;

	// Getting the captcha.
	ob_start();
	captcha_control::getInstance()->call_captcha("all", "", (int)$section_id);
	$captcha = ob_get_clean();

	$submitted_when = time();
	$_SESSION['submitted_when'] = $submitted_when;

	unset($_SESSION['result_message']);

	$data = [
		'TEMPLATE_DIR'	=>	TEMPLATE_DIR,
		'SIGNUP_URL'	=>	SIGNUP_URL,
		'LOGOUT_URL'	=>	LOGOUT_URL,
		'FORGOT_URL'	=>	FORGOT_URL,
		'ENABLED_CAPTCHA'	=>	ENABLED_CAPTCHA, 
		'ENABLED_ASP'	=>	ENABLED_ASP, 
		'CALL_CAPTCHA'	=>	$captcha,
		'HASH'			=>	$hash, 
		'submitted_when'=> $submitted_when,
        'error_messages'=> account::getInstance()->getErrorMesssage()
	];
	
	echo $oTWIG->render("signup_form.lte", $data);	
}
