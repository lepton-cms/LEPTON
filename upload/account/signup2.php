<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file



// [1] See if there exists a frontend template file or use the fallback
if (LEPTON_handle::require_alternative('login/signup2.php', 'frontend'))
{
    return 0;
}
else
{
	// Get details entered
	$signup_id = FRONTEND_SIGNUP;
	$active = 1;
	$username = LEPTON_core::getValue('username','username','post') ?? "";	
	$display_name = LEPTON_core::getValue('display_name','username','post') ?? "";
	$mail_to = LEPTON_core::getValue('email','email','post');

	// Check values
	if($signup_id == 0) 
	{
		account::print_error(account::USERS_NO_GROUP);
	}

	if(is_null($username == null)) 
	{
		account::print_error(account::USERS_NAME_INVALID_CHARS);
	}

	if(is_null($mail_to)) 
	{
		account::print_error(account::USERS_INVALID_EMAIL);		
	}

	// Captcha
	if (ENABLED_CAPTCHA)
	{		
		if (isset($_POST['captcha']))
		{
			// Check for a mismatch
			if ($_POST['captcha'] != $_SESSION['captcha'.$_SESSION['captcha_id']])
			{
				account::print_error(account::MOD_FORM_INCORRECT_CAPTCHA);
			}
		} 
		else 
		{
			account::print_error(account::MOD_FORM_INCORRECT_CAPTCHA_MISSING);
		}
	}
	if (isset($_SESSION['captcha'.$_SESSION['captcha_id']]))
	{
		unset($_SESSION['captcha'.$_SESSION['captcha_id']]); 
	}

	// Delete not confirmed entries. older than 1 hour
	$database->simple_query("DELETE from `".TABLE_PREFIX."users` where (`password` = 'unconfirmed signup') AND (`login_ip` < ( UNIX_TIMESTAMP() - 3600 ))");

	// Check if username already exists
	$results = $database->get_one("SELECT user_id FROM ".TABLE_PREFIX."users WHERE username = '".$username."'");
	if(!is_null($results)) 
	{
		account::print_error(account::USERS_USERNAME_TAKEN);
	}

	// Check if the email already exists
	$results = $database->get_one("SELECT user_id FROM ".TABLE_PREFIX."users WHERE email = '".$mail_to."'");
	if(!is_null($results)) 
	{
		account::print_error(account::USERS_EMAIL_TAKEN);
	}

	// insert new user and send confirmation link, create hash
	$confirm_hash = time();

	// create confirmation link
	$enter_pw_link = LEPTON_URL.'/account/new_password.php?hash='.$confirm_hash.'&signup=1';
	$language = DEFAULT_LANGUAGE;

	//save into database
	$fields = [
		'login_ip'	=>	$confirm_hash,
		'groups_id'	=>	$signup_id,
		'active'	=>	$active,
		'username'	=>	$username,
		'language'	=>	$language,
		'password'	=>	'unconfirmed signup',
		'display_name'	=>	$display_name,
		'email'		=>	$mail_to	
		];
	$database->build_and_execute( 
		'INSERT', 
		TABLE_PREFIX."users", 
		$fields
	);
												

	//send confirmation link to email, Create a new PHPMailer instance
	$mail = LEPTON_mailer::getInstance();
	$mail->CharSet = DEFAULT_CHARSET;	
	//Set who the message is to be sent from
	$mail->setFrom(SERVER_EMAIL);
	//Set who the message is to be sent to
	$mail->addAddress($mail_to);					
	//Set the subject line
	$mail->Subject = $MESSAGE['SIGNUP2_SUBJECT_LOGIN_INFO'];
	//Switch to TEXT messages
	$mail->IsHTML(true);
	// Replace placeholders from language variable with values
	$values = [
		"\n"	=> "<br />",
		'{LOGIN_DISPLAY_NAME}'	 =>  $display_name,
		'{LOGIN_WEBSITE_TITLE}'	 =>  WEBSITE_TITLE,
		'{ENTER_PW_LINK}'	 	=>  $enter_pw_link
	];
	$mail_message = str_replace( array_keys($values), array_values($values),$MESSAGE['SIGNUP2_BODY_LOGIN_INFO']);
	$mail->Body = $mail_message;	

	//send the message, check for errors
	if (!$mail->send()) 
	{
		$message = "Mailer Error: " . $mail->ErrorInfo;
		$database->simple_query("DELETE FROM ".TABLE_PREFIX."users WHERE username = '".$username."' ");
		account::print_error(account::FORGOT_PASS_CANNOT_EMAIL);
	}

	// Send info to admin
	$mail = LEPTON_mailer::getInstance();

	$mail->CharSet = DEFAULT_CHARSET;	
	//Set who the message is to be sent from
	$mail->setFrom(SERVER_EMAIL);
	//Set who the message is to be sent to
	$mail->addAddress(SERVER_EMAIL);					
	//Set the subject line
	$mail->Subject = $MESSAGE['SIGNUP2_ADMIN_SUBJECT'];
	//Switch to TEXT messages
	$mail->IsHTML(true);
	// Replace placeholders from language variable with values
	$values = array(
		"\n"	=> "<br />",
		'{LOGIN_NAME}'	 =>  $display_name,
		'{LOGIN_ID}'	 =>  $database->get_one('SELECT LAST_INSERT_ID()'),
		'{LOGIN_EMAIL}'	 =>  $mail_to,
		'{LOGIN_IP}'	 =>  $_SERVER['REMOTE_ADDR'],
		'{SIGNUP_DATE}'	 =>  date("Y.m.d H:i:s")	
	);
	$mail_message = str_replace( array_keys($values), array_values($values),$MESSAGE['SIGNUP2_ADMIN_INFO']);
	$mail->Body = $mail_message;		

	//send the message, check for errors
	$mail->send();	

	echo LEPTON_tools::display($MESSAGE['FORGOT_PASS_PASSWORD_RESET'],'div','ui signup2 message');
}