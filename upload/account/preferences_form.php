<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// see if there exists a frontend template file or use the fallback
if(LEPTON_handle::require_alternative('login/preferences_form.php', 'frontend')) 
{
	return 0;
}
else
{
	// get all languages
	$languages = [];
	$database->execute_query(
		'SELECT `directory`,`name` FROM `'.TABLE_PREFIX.'addons` WHERE `type` = "language" ORDER BY `directory`',
		true,
		$languages,
		true
	);

	// default timezone
	$timezone_table = LEPTON_date::get_timezones();
	$timezone = [];
	$current_timezone_string = LEPTON_core::getValue('timezone_string','string_clean','session');
	foreach ($timezone_table as $title)
	{
		$timezone[] = array(
			'TIMEZONE_NAME' => $title,
			'TIMEZONE_SELECTED' => ($current_timezone_string == $title) ? ' selected="selected"' : ''
		);
	}

	// date format
	$date_format = [];
	$user_time = true;

	$DATE_FORMATS = LEPTON_date::get_dateformats();

	foreach($DATE_FORMATS AS $format => $title) 
	{	
		if($format == "system_default")
		{
			continue;	
		}
		
		if(DATE_FORMAT == $format && !isset($_SESSION['USE_DEFAULT_DATE_FORMAT'])) 
		{
			$sel = "selected='selected'";
		}
		elseif($format == 'system_default' && isset($_SESSION['USE_DEFAULT_DATE_FORMAT'])) 
		{
			$sel = "selected='selected'";
		} 
		else 
		{
			$sel = '';	
		}
		
		$date_format[] = array(
			'DATE_FORMAT_VALUE'	=>	$format,
			'DATE_FORMAT_TITLE'	=>	$title.( $format === DEFAULT_DATE_FORMAT ? " (system default)" : "" ),
			'DATE_FORMAT_SELECTED' => $sel
		);

	}

	// time format
	$time_format = [];

	$TIME_FORMATS = LEPTON_date::get_timeformats();
	foreach($TIME_FORMATS AS $format => $title) 
	{

		if($format == 'system_default') 
		{
			continue;
		}

		if(TIME_FORMAT == $format && !isset($_SESSION['USE_DEFAULT_TIME_FORMAT'])) 
		{
			$sel = "selected='selected'";	
		} 
		elseif($format == 'system_default' && isset($_SESSION['USE_DEFAULT_TIME_FORMAT'])) 
		{
			$sel = "selected='selected'";
		} 
		else 
		{
			$sel = '';
		}			
		
		$time_format[] = array(
			'TIME_FORMAT_VALUE'	=>	$format,
			'TIME_FORMAT_TITLE'	=>	$title.( $format === DEFAULT_TIME_FORMAT ? " (system default)" : "" ),
			'TIME_FORMAT_SELECTED' => $sel
		);
	}

	//	Build an access-preferences-form secure hash
	LEPTON_handle::register("random_string");
	$hash = sha1( microtime().$_SERVER['HTTP_USER_AGENT'].random_string(32) );
	$_SESSION['wb_apf_hash'] = $hash;

	// 	Delete any "result_message" if there is one.
	if( true === isset($_SESSION['result_message']) )
	{
		unset($_SESSION['result_message']);	
	}

	$data = [
		'TEMPLATE_DIR' 				=>	TEMPLATE_DIR,
		'PREFERENCES_URL'			=>	PREFERENCES_URL,
		'LOGOUT_URL'				=>	LOGOUT_URL,
		'DISPLAY_NAME'				=>	LEPTON_core::getValue('DISPLAY_NAME','string_clean','session'),
		'GET_EMAIL'					=>	LEPTON_core::getValue('EMAIL','email','session'),
		'USER_ID'					=>	(isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : '-1'),
		'r_time'					=>	time(),
		'HASH'						=>	$hash,
		'RESULT_MESSAGE'			=> (isset($_SESSION['result_message'])) ? $_SESSION['result_message'] : "",
		'languages'	=> $languages,
		'user_language'	=> LANGUAGE,
		'timezone'	=> $timezone,
		'date_format' => $date_format,
		'time_format' => $time_format	
	];

	$oTWIG = lib_twig_box::getInstance();

	// register path to make sure twig is looking in this module template folder first
	$oTWIG->registerPath( LEPTON_PATH."/account/templates/" );

	echo $oTWIG->render(
		"preferences_form.lte",
		$data
	);
}
