<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure file


// see if there exists a frontend template file or use the fallback
if(LEPTON_handle::require_alternative('login/login_form.php', 'frontend')) 
{
	return 0;
}
else
{
	$oTWIG = lib_twig_box::getInstance();

    // register path to make sure twig is looking in this module template folder first
    $oTWIG->registerPath( LEPTON_PATH."/account/templates/" );

    $thisApp = LEPTON_login::getInstance();
    
    //Building a secure-hash
    $hash = sha1( microtime().$_SERVER['HTTP_USER_AGENT'] );
    $_SESSION['wb_apf_hash'] = $hash;

    //	we want different hashes for the two fields
	$salt = sha1(microtime());

	if(TFA === 'local')
	{
		$login_url = LEPTON_URL.'/account/login.php' ;
	} 
	else 
	{
		$login_url = LOGIN_URL;
	}

    $data = [
		'fe_signup'		=>	intval(FRONTEND_SIGNUP),
		'SIGNUP_URL'	=>	SIGNUP_URL,
        'LOGIN_URL'		=>	$login_url,
        'LOGOUT_URL'	=>	LOGOUT_URL,
        'FORGOT_URL'	=>	FORGOT_URL,
        'MESSAGE'		=>	$thisApp->message,
        'signup_message'=> (isset($_SESSION["signup_message"]) ? $_SESSION["signup_message"] : ''),
        'REDIRECT_URL'	=>	$thisApp->redirect_url,
        'HASH'			=>	$hash,
		'username_fieldname' 	=> $TEXT['USERNAME'].'_'.substr($salt, 0, 16),
		'password_fieldname' 	=> $TEXT['PASSWORD'].'_'.substr($salt, 15, 16)
    ];
            
    echo $oTWIG->render(
        "login_form.lte",
        $data
    );
        
    if (isset($_SESSION["signup_message"]))
	{
		unset ($_SESSION["signup_message"]);
	}
    if (isset($_SESSION["result_message"]))
	{
		unset ($_SESSION["result_message"]);
	}
}
