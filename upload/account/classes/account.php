<?php

declare(strict_types=1);

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

class account
{
    use LEPTON_singleton;

    const INTERNAL_NONE_MATCH = "Unknown error [%d]!";
    const INTERNAL_NO_ERROR = "No error!";

    const USERS_NO_GROUP             = 1;
    const USERS_NAME_INVALID_CHARS   = 2;
    const USERS_INVALID_EMAIL        = 3;
    const SIGNUP_NO_EMAIL            = 4;
    const MOD_FORM_INCORRECT_CAPTCHA = 5;
    const USERS_USERNAME_TAKEN       = 6;
    const USERS_EMAIL_TAKEN          = 7;
    const FORGOT_PASS_CANNOT_EMAIL   = 9;
    const MOD_FORM_INCORRECT_CAPTCHA_MISSING = 10;

    public static $instance;

    private array $messages = [];

    protected function __construct()
    {
        $this->initialize();
    }

    protected function initialize(): void
    {
        $MESSAGE = LEPTON_core::getGlobal("MESSAGE");

        $this->messages = [
            0 => $this::INTERNAL_NO_ERROR,
            1 => $MESSAGE['USERS_NO_GROUP'],
            2 => $MESSAGE['USERS_NAME_INVALID_CHARS'].' / '.$MESSAGE['USERS_USERNAME_TOO_SHORT'],
            3 => $MESSAGE['USERS_INVALID_EMAIL'],
            4 => $MESSAGE['SIGNUP_NO_EMAIL'],
            5 => $MESSAGE['MOD_FORM_INCORRECT_CAPTCHA'],
            6 => $MESSAGE['USERS_USERNAME_TAKEN'],
            7 => $MESSAGE['USERS_EMAIL_TAKEN'],
            8 => $MESSAGE['USERS_INVALID_EMAIL'],
            9 => $MESSAGE['FORGOT_PASS_CANNOT_EMAIL'],
            10 => $MESSAGE['MOD_FORM_INCORRECT_CAPTCHA']." [missing]"
        ];
    }


    public function getErrorMesssage(): string
    {
        $errorId = filter_input(INPUT_GET, "err", FILTER_SANITIZE_NUMBER_INT);

        if (is_null($errorId))
        {
            if (isset($_SESSION['account_error_id']))
            {
                $errorId = (int) $_SESSION['account_error_id'];
                unset($_SESSION['account_error_id']);
            } else {
                return "";
            }
        }
        return $this->messages[$errorId] ?? sprintf(self::INTERNAL_NONE_MATCH, $errorId);
    }


    public static function getConstants(): array
    {
        // "static::class" here does the magic
        $reflectionClass = new ReflectionClass(static::class);
        return $reflectionClass->getConstants();
    }
	
	public static function print_error($err_id) 
	{
		$_SESSION['account_error_id'] = $err_id;
		header("Location: ".LEPTON_URL."/account/signup.php?err=".$err_id);
		exit();
	}	

}