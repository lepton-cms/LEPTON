<?php
/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */


echo '<h3>reload all addons</h3>';
// include function files
$file_names = array (
	"get_modul_version",
	"load_language",
	"load_module",
	"load_template",
	"scan_current_dir"
);
LEPTON_handle::register($file_names);
	
/**
 *  reload all addons
 *  Modules first
 */
// first remove addons entrys for modules that don't exist
$res_addons = [];
	$database->execute_query(
	"SELECT `directory` FROM `".TABLE_PREFIX."addons` WHERE `type` = 'module' ",
	true,
	$res_addons,
	true
);
if(!empty($res_addons)) 
{
    foreach ($res_addons as $value)
    {
        if (!file_exists(LEPTON_PATH . '/modules/' . $value['directory']))
        {
            $database->simple_query("DELETE FROM `".TABLE_PREFIX."addons` WHERE `directory` = '".$value['directory']."' ");
        }
    }
}

/**
 *	Now check modules folder with entries in addons
 */
$modules = scan_current_dir(LEPTON_PATH . '/modules');
if (!empty($modules['path']))
{
    foreach ($modules['path'] as &$value)
    {
        $code_version = get_modul_version($value);
        $db_version   = get_modul_version($value, false);
        if (!is_null($db_version) && !is_null($code_version))
        {
            require(LEPTON_PATH . '/modules/' . $value . "/info.php");
            load_module(LEPTON_PATH . '/modules/' . $value);
        }
    }
}

/**
 *  Reload Templates
 *	@notice: we're using the function 'scan_current_dir', so we are not in the need to test for file- or foldernames like ".", ".git" or "index.php".
 */
$templates = scan_current_dir(LEPTON_PATH . '/templates');
if (!empty($templates['path']))
{
    // Delete not existing templates from database
    $database->simple_query("DELETE FROM ".TABLE_PREFIX."addons WHERE type = 'template' ");
    
    // Load all templates
    foreach($templates['path'] as $template_folder)
    {
		require(LEPTON_PATH . '/templates/' . $template_folder . "/info.php");
		load_template($template_folder);
    }
}

/**
 *  Reload Languages
 *	@notice: we're using the function 'scan_current_dir', so we are not in the need to test for file- or foldernames like ".", ".git" or "index.php".
 */
$languages = scan_current_dir(LEPTON_PATH . '/languages/');
if (!empty($languages['filename']))
{
    // Delete  not existing languages from database
    $database->simple_query("DELETE FROM ".TABLE_PREFIX."addons  WHERE type = 'language' ");
    
    // Load all languages
    foreach($languages['filename'] as $lang_file)
    {
		load_language($lang_file);
    }
}
 
echo "<h3>All addons successfully reloaded!</h3>";