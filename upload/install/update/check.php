<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 */

// set error level
 ini_set('display_errors', 1);
 error_reporting(E_ALL|E_STRICT);
 
require_once '../config/config.php';

?>
<!DOCTYPE html>
<html>
<head>
<title>LEPTON Update Script</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<script type='text/javascript' src='<?php echo LEPTON_URL; ?>/modules/lib_fomantic/dist/semantic.min.js' ></script>
<link rel="stylesheet" type="text/css" href="<?php echo LEPTON_URL; ?>/modules/lib_fomantic/dist/semantic.min.css" media="screen,projection" />	
<link href="https://doc.lepton-cms.org/_packinstall/style_300.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="update_form">

	<div class="ui top attached segment">
		<div id="logo">
			<img src="https://doc.lepton-cms.org/_packinstall/img/logo.png" alt="Logo" />
		</div>
		<div id="form_title">
			<h2>LEPTON update script</h2>
		</div>	
	</div>
	
	<div class="ui attached segment">
		<div class="spacer"></div>
		<?php
		/**
		 *  check php version
		 */
		echo("<h3>Check Version</h3>");
		$current_lepton = $database->get_one("SELECT `value` from `".TABLE_PREFIX."settings` where `name`='lepton_version'");
		if (version_compare(PHP_VERSION, '8.1', '<'))
		{ 
			echo "<div class='ui compact negative message'><i class='big announcement icon'></i>
			<h2>No update possible</h2>
			Please update your PHP version to 8.1 or greater <br /><b>current PHP Version is : ".PHP_VERSION."</b> <br />
			</div>";
		} else {	
			echo "<h3 class='good'>Your PHP Version : ".PHP_VERSION." !</h3>";
			echo "<h3 class='good'>Current LEPTON Version : ".$current_lepton." !</h3>";
			echo "<h3 class='good'>Update possible, please push button to start.</h3>";			
			echo "<div class='ui compact info message'><i class='big idea icon'></i>Don't forget to <a href='https://lepton-cms.com/lepador/admintools/backup.php' target='_blank'>backup your files and your database!</a></div>";
			?>
			<div class="spacer"></div>			
			<a href="update/update.php"><button class="ui positive button">Start Update</button></a>
	<?php	}	?>

		<div class="spacer"></div>	
	</div>
	
	<?php	
	// get the footer				
		include 'footer.php';		
	?>	
	
</div> <!-- end id="update_form" -->
</body>
</html>