<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 */


// set error level
 ini_set('display_errors', 1);
 error_reporting(E_ALL|E_STRICT);

// routine [1]: remove not installed standard templates
$lepton3 = $database->get_one("SELECT addon_id FROM ".TABLE_PREFIX."addons WHERE type = 'template' and directory = 'lepton3' ");
if(is_null($lepton3))
{
	LEPTON_handle::delete_obsolete_directories(['/templates/lepton3']);
}

$semantic = $database->get_one("SELECT addon_id FROM ".TABLE_PREFIX."addons WHERE type = 'template' and directory = 'semantic' ");
if(is_null($semantic))
{
	LEPTON_handle::delete_obsolete_directories(['/templates/semantic']);
}

$blank = $database->get_one("SELECT addon_id FROM ".TABLE_PREFIX."addons WHERE type = 'template' and directory = 'blank' ");
if(is_null($blank))
{
	LEPTON_handle::delete_obsolete_directories(['/templates/blank']);
}

// routine [2]: remove pages directory if not /page
if(PAGES_DIRECTORY != '/page')
{
	LEPTON_handle::delete_obsolete_directories(['/page']);
}

// start with update
echo ('<h3>Current process: updating to LEPTON 7.3.0</h3>');


// remove files and directories
echo '<h5>Current process: delete not needed files and directories</h5>';
// [1] directories
$directory_names = [
	'/templates/lepton3/frontend/wrapper'
];
LEPTON_handle::delete_obsolete_directories( $directory_names );


// [2] files
$file_names = [
	'/framework/functions/function.extract_permission.php',
];
LEPTON_handle::delete_obsolete_files( $file_names );
echo "<h5>Delete not needed files and directories: successfull</h5>";

// upgrade updated modules
echo '<h5>Current process: run modules upgrade.php</h5>';

$module_names = [];
$database->execute_query(
	"SELECT directory FROM ".TABLE_PREFIX."addons WHERE type = 'module'",
	true,
	$module_names,
	true	
);

$all_modules = [];
foreach($module_names as $item)
{
	$all_modules[] = $item['directory'];
}

LEPTON_handle::upgrade_modules($all_modules);
echo "<h5>run upgrade.php of all modules: successfull</h5>";


// update release and success message
echo ('<h5>Current process: set new release number</h5>');
	$database->simple_query("UPDATE ".TABLE_PREFIX."settings SET value ='7.3.0' WHERE name = 'lepton_version' ");
echo "<h5>update to LEPTON 7.3.0 successfull!</h5><br />";