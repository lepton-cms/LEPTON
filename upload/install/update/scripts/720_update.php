<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2024 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 */


// set error level
 ini_set('display_errors', 1);
 error_reporting(E_ALL|E_STRICT);

// routine: remove not installed standard templates
$lepton3 = $database->get_one("SELECT addon_id FROM ".TABLE_PREFIX."addons WHERE type = 'template' and directory = 'lepton3' ");
if(is_null($lepton3))
{
	LEPTON_handle::delete_obsolete_directories(['/templates/lepton3']);
}

$semantic = $database->get_one("SELECT addon_id FROM ".TABLE_PREFIX."addons WHERE type = 'template' and directory = 'semantic' ");
if(is_null($semantic))
{
	LEPTON_handle::delete_obsolete_directories(['/templates/semantic']);
}

$blank = $database->get_one("SELECT addon_id FROM ".TABLE_PREFIX."addons WHERE type = 'template' and directory = 'blank' ");
if(is_null($blank))
{
	LEPTON_handle::delete_obsolete_directories(['/templates/blank']);
}

// start with update
echo ('<h3>Current process: updating to LEPTON 7.2.0</h3>');


// decrypt smtp mailer values if exists
if(extension_loaded('openssl') === true)
{
	if(MAILER_ROUTINE == 'smtp')
	{
		echo '<h5>Decrypt SMTP Mailer values in settings table </h5>';
		
		$sNewHost = LEPTON_database::cryptString(MAILER_SMTP_HOST);
		$sNewName = LEPTON_database::cryptString(MAILER_SMTP_USERNAME);
		$sNewPassword = LEPTON_database::cryptString(MAILER_SMTP_PASSWORD);
		
		$database->simple_query("UPDATE ".TABLE_PREFIX."settings SET value ='".$sNewHost."' WHERE name = 'mailer_smtp_host' ");
		$database->simple_query("UPDATE ".TABLE_PREFIX."settings SET value ='".$sNewName."' WHERE name = 'mailer_smtp_username' ");
		$database->simple_query("UPDATE ".TABLE_PREFIX."settings SET value ='".$sNewPassword."' WHERE name = 'mailer_smtp_password' ");
		
		echo "<h5>Decrypt SMTP Mailer values in settings table: successfull</h5>";	
	}
}

// remove talgos BE-Theme from installation
echo '<h5>Current process: remove Talgos BE-Theme from installation</h5>';
$database->simple_query("UPDATE ".TABLE_PREFIX."settings SET value ='lepsem' WHERE name = 'default_theme' ");
$database->simple_query("DELETE FROM ".TABLE_PREFIX."addons WHERE directory = 'talgos' AND type = 'template' ");
LEPTON_handle::delete_obsolete_directories('/templates/talgos');
$message = "<h5>Caution: we removed TALGOS BE-Theme from your installation because it is not compatible with new LEPTON 7.2.0</h5>";
echo(LEPTON_tools::display($message, 'pre','ui red message'));

// alter log table, modify field logged
echo '<h5>Current process: modify field logged in log table</h5>';
$database->simple_query("ALTER TABLE ".TABLE_PREFIX."log CHANGE `logged` `logged` datetime NULL DEFAULT NULL ");
$aAllLogs = [];
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."log",
	true,
	$aAllLogs,
	true
);

foreach($aAllLogs as $newLogged)
{
	if($newLogged['logged'] == '1970-01-01 00:00:00')
	{
		$database->simple_query("UPDATE ".TABLE_PREFIX."log SET `logged` = NULL WHERE id = ".$newLogged['id'] ); 
	}	
}
echo "<h5>Modify field logged in log table: successfull</h5>";

// remove obsolete column
echo '<h5>Current process: remove obsolete column from pages table</h5>';

$fields = [];
$database->describe_table(
    TABLE_PREFIX."pages",
    $fields,
    LEPTON_database::DESCRIBE_ONLY_NAMES
);

if (in_array('admin_users', $fields)) 
{
	// check if column is empty
	$test= [];
	$database->execute_query(
		"SELECT admin_users FROM ".TABLE_PREFIX."pages WHERE admin_users != '' ",
		true,
		$test,
		true
	);

	if(!empty($test))
	{
		echo (LEPTON_tools::display('Please open a <a href="https://forum.lepton-cms.org" target="_blank">thread in the Forum</a> to report this: <b>admin_users</b>!','pre','ui red message'));
	}
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX."pages CHANGE `admin_users` `admin_users` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''"); 
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX."pages DROP COLUMN `admin_users`");

	// check if column is empty
	$test2= [];
	$database->execute_query(
		"SELECT viewing_users FROM ".TABLE_PREFIX."pages WHERE viewing_users != '' ",
		true,
		$test2,
		true
	);

	if(!empty($test2))
	{
		echo (LEPTON_tools::display('Please open a <a href="https://forum.lepton-cms.org" target="_blank">thread in the Forum</a> to report this: <b>viewing_users</b>!','pre','ui red message'));
	}
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX."pages CHANGE `viewing_users` `viewing_users` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''"); 
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX."pages DROP COLUMN `viewing_users`");
}
echo "<h5>Remove obsolete column from pages table: successfull</h5>";

// remove obsolete fields
echo '<h5>Current process: remove obsolete fields from tables</h5>';
$fields = [];
$database->describe_table(
    TABLE_PREFIX."users",
    $fields,
    LEPTON_database::DESCRIBE_ONLY_NAMES
);

if (in_array('group_id', $fields)) 
{
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX."users DROP COLUMN `group_id` ");
}

$fields2 = [];
$database->describe_table(
    TABLE_PREFIX."groups",
    $fields2,
    LEPTON_database::DESCRIBE_ONLY_NAMES
);
if (in_array('template_permissions', $fields)) 
{
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX."groups DROP COLUMN `template_permissions` ");
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX."groups DROP COLUMN `language_permissions` ");
}
echo "<h5>remove obsolete fields from tables: successfull</h5>";

// change field type
echo '<h5>Current process: change field type in tables</h5>';
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."groups` CHANGE `backend_permission` `backend_access` INT(1) NOT NULL DEFAULT 1 ");
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."sections` CHANGE `publ_end` `publ_end` INT(11) NOT NULL DEFAULT 0 ");
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."sections` CHANGE `block` `block` INT(11) NOT NULL DEFAULT 0 ");
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."sections` CHANGE `publ_start` `publ_start` INT(11) NOT NULL DEFAULT 0 ");
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."sections` CHANGE `publ_end` `publ_end` INT(11) NOT NULL DEFAULT 0 ");
echo "<h5>change field type in tables: successfull</h5>";


// remove obsolete row
echo '<h5>Current process: remove obsolete row from settings table</h5>';
$check = $database->get_one("SELECT value FROM ".TABLE_PREFIX."settings WHERE name = 'mediasettings' ");
if(!is_null($check))
{
	$database->simple_query("DELETE FROM ".TABLE_PREFIX."settings WHERE name = 'mediasettings' ");
}
echo "<h5>remove obsolete row from settings table: successfull</h5>";

// remove files and directories
echo '<h5>Current process: delete not needed files and directories</h5>';
// [1] directories
$directory_names = [

];
LEPTON_handle::delete_obsolete_directories( $directory_names );


// [2] files
$file_names = [

];
LEPTON_handle::delete_obsolete_files( $file_names );
echo "<h5>Delete not needed files and directories: successfull</h5>";

// upgrade updated modules
echo '<h5>Current process: run modules upgrade.php</h5>';

$module_names = [];
$database->execute_query(
	"SELECT directory FROM ".TABLE_PREFIX."addons WHERE type = 'module'",
	true,
	$module_names,
	true	
);

$all_modules = [];
foreach($module_names as $item)
{
	$all_modules[] = $item['directory'];
}

LEPTON_handle::upgrade_modules($all_modules);
echo "<h5>run upgrade.php of all modules: successfull</h5>";


// update release and success message
echo ('<h5>Current process: set new release number</h5>');
	$database->simple_query("UPDATE ".TABLE_PREFIX."settings SET value ='7.2.0' WHERE name = 'lepton_version' ");
echo "<h5>update to LEPTON 7.2.0 successfull!</h5><br />";