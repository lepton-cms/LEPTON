<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2024 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 */


// set error level
 ini_set('display_errors', 1);
 error_reporting(E_ALL|E_STRICT);

// routine: remove not installed standard templates
$lepton3 = $database->get_one("SELECT addon_id FROM ".TABLE_PREFIX."addons WHERE type = 'template' and directory = 'lepton3' ");
if($lepton3 === NULL)
{
	LEPTON_handle::delete_obsolete_directories(['/templates/lepton3']);
}

$semantic = $database->get_one("SELECT addon_id FROM ".TABLE_PREFIX."addons WHERE type = 'template' and directory = 'semantic' ");
if($semantic === NULL)
{
	LEPTON_handle::delete_obsolete_directories(['/templates/semantic']);
}

$blank = $database->get_one("SELECT addon_id FROM ".TABLE_PREFIX."addons WHERE type = 'template' and directory = 'blank' ");
if($blank === NULL)
{
	LEPTON_handle::delete_obsolete_directories(['/templates/blank']);
}

// start with update
echo("<h3 class='good'>Current process: updating to LEPTON 7.1.0</h3>");

// reduce XSS risks on modern browsers
echo '<h5>Current process: write .htaccess file if possible</h5>';
if(!file_exists(LEPTON_PATH.'/.htaccess'))
{
// create htaccess file and write content
$content='
# reduce XSS risks on modern browsers, for details see https://content-security-policy.com/
<IfModule mod_headers.c>
    Header always set Referrer-Policy "same-origin"
	Header set X-XSS-Protection "1; mode=block"
	Header set X-Frame-Options "SAMEORIGIN"
	Header set X-Content-Type-Options "nosniff"
	Header set X-Permitted-Cross-Domain-Policies "none"
</IfModule>
';	
file_put_contents(LEPTON_PATH.'/.htaccess',$content);	
	
$message1 ='
<h5>Attention: we added a htaccess file to your root!</h5>
<a href="https://doc.lepton-cms.org/docu/english/tutorials/content-security-policy-(csp).php" target="_blank">For more details please read the docs</a>.
';
$message2 = '<b> Please decide on your own, if you want to keep the file!</b>';
echo(LEPTON_tools::display($message1, 'pre','ui blue message'));
echo(LEPTON_tools::display($message2, 'pre','ui orange message'));
echo "<h5> Task write .htaccess file: done!</h5>";
}
else
{
$message ='
<h5>Attention: you already have a htaccess file in your root!</h5>
<a href="https://doc.lepton-cms.org/docu/english/tutorials/content-security-policy-(csp).php" target="_blank">Please consider to add some content to it, for details please read the docs</a>.
'; 
echo(LEPTON_tools::display($message, 'pre','ui orange message'));
}



echo '<h5>Current process: delete not needed files and directories</h5>';
/*
// [1] directories
$directory_names = [

];
LEPTON_handle::delete_obsolete_directories( $directory_names );
*/

// [2] files
$file_names = [
	'/framework/class.secure.php',
];
LEPTON_handle::delete_obsolete_files( $file_names );
echo "<h5>Delete not needed files and directories: successfull</h5>";

// upgrade updated modules
echo '<h5>Current process: run modules upgrade.php</h5>';

$module_names = [];
$database->execute_query(
	"SELECT directory FROM ".TABLE_PREFIX."addons WHERE type = 'module'",
	true,
	$module_names,
	true	
);

$all_modules = [];
foreach($module_names as $item)
{
	$all_modules[] = $item['directory'];
}

LEPTON_handle::upgrade_modules($all_modules);
echo "<h5>run upgrade.php of all modules: successfull</h5>";


// update release and success message
echo ('<h5>Current process: set new release number</h5>');
	$database->simple_query("UPDATE ".TABLE_PREFIX."settings SET value ='7.1.0' WHERE name = 'lepton_version' ");
echo("<h3 class='good'>Update to LEPTON 7.1.0 successfull!</h3><br />");
