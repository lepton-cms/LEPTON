<?php
/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

/**
 *  reduce XSS risks on modern browsers
 */
 ?>
<div class="ui basic segment">
	<h3 class="ui header">We want to protect your site from XSS attacks</h3>
<?php
if(!file_exists(LEPTON_PATH.'/.htaccess'))
{
// create htaccess file and write content
$content='
# reduce XSS risks on modern browsers, for details see https://content-security-policy.com/
<IfModule mod_headers.c>
    Header always set Referrer-Policy "same-origin"
	Header set X-XSS-Protection "1; mode=block"
	Header set X-Frame-Options "SAMEORIGIN"
	Header set X-Content-Type-Options "nosniff"
	Header set X-Permitted-Cross-Domain-Policies "none"
</IfModule>
';	
file_put_contents(LEPTON_PATH.'/.htaccess',$content);	
	
$message1 ='
<h5>Attention: we added a htaccess file to your root!</h5>
<a href="https://doc.lepton-cms.org/docu/english/tutorials/content-security-policy-(csp).php" target="_blank">For more details please read the docs</a>.
';
$message2 = '<b> Please decide on your own, if you want to keep the file!</b>';
echo(LEPTON_tools::display($message1, 'pre','ui blue message'));
echo(LEPTON_tools::display($message2, 'pre','ui orange message'));
}
else
{
$message ='
<h5>Attention: you already have a htaccess file in your root!</h5>
<a href="https://doc.lepton-cms.org/docu/english/tutorials/content-security-policy-(csp).php" target="_blank">Please consider to add some content to it, for details please read the docs</a>.
'; 
echo(LEPTON_tools::display($message, 'pre','ui orange message'));
}
 ?>		
	<div class="spacer"></div>
	<h3 class='good'>Congratulation, you have successfully installed LEPTON</h3>
</div>
