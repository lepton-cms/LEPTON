<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 */

// set error level
 ini_set('display_errors', 1);
 error_reporting(E_ALL|E_STRICT);
 require_once('../config/config.php');
?>
<!DOCTYPE html>
<html>
<head>
<title>LEPTON Installation</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="<?php echo LEPTON_URL; ?>/modules/lib_fomantic/dist/semantic.min.js" ></script>

<link href="<?php echo LEPTON_URL; ?>/modules/lib_fomantic/dist/semantic.min.css" rel="stylesheet" type="text/css">
<link href="https://doc.lepton-cms.org/_packinstall/style_300.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="update_form">
	<div class="ui top attached segment">
		<div id="logo">
			<img src="https://doc.lepton-cms.org/_packinstall/img/logo.png" alt="Logo" />
		</div>
		<div id="form_title">
			<h2>LEPTON Installation</h2>
		</div>	
	</div>

	<?php
	//	add htacces file
		include('update/secure.php');	
	
	// get the buttons					
		include('update/login.php');
		
	// get the footer				
		include('update/footer.php');
		
	// delete install directory
	LEPTON_handle::delete_obsolete_directories(['/install']);		
	?>	
	
</div> <!-- end id="update_form" -->
</body>
</html>
