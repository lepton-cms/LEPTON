<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		LEPTON project,  W. Studer
 *	@copyright		2010-2025  LEPTON project (initial by Ruud Eisinga)
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */


$files_to_register = array(
	 'modify_template.php'
	,'modify_spamcheck.php'
	,'save.php'
	,'classes/quickform_custom_frontend.php'
);

LEPTON_secure::getInstance()->accessFiles( $files_to_register );


?>
