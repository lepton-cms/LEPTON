<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		LEPTON project,  W. Studer
 *	@copyright		2010-2025  LEPTON project (initial by Ruud Eisinga)
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on the miniform addon
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
 
$module_directory   = 'quickform';
$module_name        = 'QuickForm';
$module_function    = 'page';
$module_version     = '2.1.3';
$module_platform    = '7.x';
$module_author      = 'LEPTON project, W. Studer';
$module_license     = '<a href="http://www.gnu.org/licenses/lgpl.html" target="_blank">GNU Lesser General Public License</a>';
$module_license_terms = '';
$module_description = 'This module allows you to create a quick and simple form without complicated settings using TWIG template engine.';
$module_guid        = '6d1a4304-88f0-4356-a1bf-7b5fcb69cf9f';
