{% autoescape false %}
{# --------------------
 # check within LEPTON documentation (via help button) for decription of the possibilities (english only)
 # -------------------- #}
 
{# --------------------
 # set the default field settings
 # might be overwritten by specific field settings
 #   Key		 	= Default	=> Description
 # ----------------------------------------------
 #  *  field		= Null		=> defines the field name & id. Also used as label if label is not specified.
 #  *  label		= Null		=> define special field label. Empty default.
 #  *  value		= Null		=> define a specific value, mainly for single checkboxes. Empty default.
 #  *  type 		= "text"	=> defines the input field type. Default = "text"
 # (*) inputmode	= "vebatim"	=> defines the keyboard layout for mobile devices. Default = alphanumerical keyboard layout
 # (*) placeholder	= Null		=> set a placeholder text to be shown before user enters data. Not transmitted.
 # (*) required		= "r_"		=> mark field as required.
 # (*) autofocus	= Null		=> Set the autofocus on a field. Only 1 per form is allowed. Overruled by first error found.
 # (*) readonly		= Null		=> Set a field to readonly.
 # (*) disabled		= Null		=> Set a field to disabled.
 #
 # (*) such field has additional settings, mainly the attribute string
 # -------------------- #}
{% set initials = { 
  "field": Null
, "label": Null
, "value": Null
, "type": "text"
, "inputmode": "vebatim", "inputmode_attribute": 'inputmode="###"'
, "placeholder": Null, "placeholder_attribute": 'placeholder="###"'
, "required": "r_", "required_attribute": 'required="required"', "required_markup": '<span class="required">*</span>'
, "autofocus": Null, "autofocus_attribute": 'autofocus="autofocus"'
, "readonly": Null, "readonly_attribute": 'readonly="readonly"'
, "disabled": Null, "disabled_attribute": 'disabled="disabled"'
, "class_attribute": 'class="###"'
} %}

{# --------------------
 # define the form
 # -------------------- #}
<div class="quickform">
	<div class="{{ MESSAGE_CLASS }}">{{ STATUSMESSAGE }}</div>
	<div class="{{ FORM_CLASS }}">
		<h2>Kontakt</h2>
{% if REQUIRED_FIELDS is not empty %}
		<small>Felder mit {{ required_markup }} gekennzeichnet sind Pflichtfelder.</small>
{% endif %}

		<form name="form_{{ SECTION_ID }}" id="form_{{ SECTION_ID }}" method="post" action="{{ URL }}" autocomplete="on">
			<input type="hidden" name="header"		value="Kontakt" />
			<input type="hidden" name="timestamp"	value="{{ TIMESTAMP }}" data-label="Zeitstempel"/>
			<input type="hidden" name="contactid"	value="{{ CONTACT_ID }}" />
			<input type="hidden" name="quickform"	value="{{ SECTION_ID }}" />

{% set settings = initials|merge({"field": "abteilung", "type": "select", "autofocus": true }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="full">
				<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
					<select name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
						tabindex="{{tabindex}}"
						{{ settings.required ? settings.required_attribute }}
						{{ settings.disabled ? settings.disabled_attribute }}
						{{ settings.readonly ? settings.readonly_attribute }}
						{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
						{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
						{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
						data-label="{{ settings.label ?? settings.field|capitalize }}"
					 />
{% for value in ["-- Bitte auswählen --", "Kundendienst", "Sales team", "Sonstiges"] %}
						<option value={{ value }}
								{% if _context[settings.field|upper] == value %}selected="selected"{% endif %} 
						>{{ value }}</option>
{% endfor %}
					</select>
				</label>
			</div>

{% set settings = initials|merge({"field": "firma" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="full">
				<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
					<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
						value="{{ _context[settings.field|upper] }}"
						tabindex="{{tabindex}}"
						{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
						{{ settings.required ? settings.required_attribute }}
						{{ settings.disabled ? settings.disabled_attribute }}
						{{ settings.readonly ? settings.readonly_attribute }}
						{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
						{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
						{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
						data-label="{{ settings.label ?? settings.field|capitalize }}"
					 />
				</label>
			</div>

			<div class="full">
{% set settings = initials|merge({"field": "anrede", "type": "select", "required": Null }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
				<div class="onequarter">
					<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
						<select name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							tabindex="{{tabindex}}"
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.label ?? settings.field|capitalize }}"
						 />
	{% for value in ["-- Bitte auswählen --", "Herr", "Frau", "*"] %}
							<option value={{ value }}
									{% if _context[settings.field|upper] == value %}selected="selected"{% endif %} 
							>{{ value }}</option>
	{% endfor %}
						</select>
					</label>
				</div>

{% set settings = initials|merge({"field": "vorname", "inputmode": "latin-name" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
				<div class="onequarter">
					<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
						<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							value="{{ _context[settings.field|upper] }}"
							tabindex="{{tabindex}}"
							{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.label ?? settings.field|capitalize }}"
						 />
					</label>
				</div>

{% set settings = initials|merge({"field": "nachname", "inputmode": "latin-name" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
				<div class="half pullright">
					<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
						<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							value="{{ _context[settings.field|upper] }}"
							tabindex="{{tabindex}}"
							{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.label ?? settings.field|capitalize }}"
						 />
					</label>
				</div>
			</div>

			<div class="full">
{% set settings = initials|merge({"field": "strasse" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
				<div class="threequarter">
					<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
						<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							value="{{ _context[settings.field|upper] }}"
							tabindex="{{tabindex}}"
							{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.label ?? settings.field|capitalize }}"
						 />
					</label>
				</div>

{% set settings = initials|merge({"field": "hausnummer" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
				<div class="onequarter pullright">
					<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
						<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							value="{{ _context[settings.field|upper] }}"
							tabindex="{{tabindex}}"
							{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.label ?? settings.field|capitalize }}"
						 />
					</label>
				</div>
			</div>

			<div class="full">
{% set settings = initials|merge({"field": "land" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
				<div class="onequarter">
					<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
						<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							value="{{ _context[settings.field|upper] }}"
							tabindex="{{tabindex}}"
							{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.label ?? settings.field|capitalize }}"
							list="countries"
						 />
					</label>
					<datalist id="countries">
						<option value="Deutschland">
						<option value="Österreich">
						<option value="Schweiz">
						<option value="Frankreich">
						<option value="Niederlande">
					</datalist>
				</div>

{% set settings = initials|merge({"field": "plz", "label": "PLZ" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
				<div class="onequarter">
					<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
						<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							value="{{ _context[settings.field|upper] }}"
							tabindex="{{tabindex}}"
							{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.label ?? settings.field|capitalize }}"
						 />
					</label>
				</div>

{% set settings = initials|merge({"field": "stadt", "inputmode": "latin-name" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
				<div class="half pullright ">
					<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
						<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							value="{{ _context[settings.field|upper] }}"
							tabindex="{{tabindex}}"
							{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.label ?? settings.field|capitalize }}"
						 />
					</label>
				</div>
			</div>

			<div class="full">
{% set settings = initials|merge({"field": "telefon nummer", "required": Null, "type": "tel", "inputmode": "tel", "placeholder": "z.B. +00 1234 56789" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
				<div class="half">
					<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
						<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							value="{{ _context[settings.field|upper] }}"
							tabindex="{{tabindex}}"
							{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.label ?? settings.field|capitalize }}"
						 />
					</label>
				</div>

{% set settings = initials|merge({"field": "mobil nummer", "required": Null, "type": "tel", "inputmode": "tel", "placeholder": "z.B. +00 12 345 67 89" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
				<div class="half pullright">
					<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
						<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							value="{{ _context[settings.field|upper] }}"
							tabindex="{{tabindex}}"
							{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.label ?? settings.field|capitalize }}"
						 />
					</label>
				</div>
			</div>

{% set settings = initials|merge({"field": "email", "label": "E-Mail", "type": "email", "inputmode": "email", "placeholder": "z.B. name@domain.tld" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="full">
				<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
					<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
						value="{{ _context[settings.field|upper] }}"
						tabindex="{{tabindex}}"
						{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
						{{ settings.required ? settings.required_attribute }}
						{{ settings.disabled ? settings.disabled_attribute }}
						{{ settings.readonly ? settings.readonly_attribute }}
						{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
						{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
						{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
						data-label="{{ settings.label ?? settings.field|capitalize }}"
					 />
				</label>
			</div>

{% set settings = initials|merge({"field": "nachricht", "label": "Ihre Nachricht" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="full">
				<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
					<textarea cols="80" rows="10"
						name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
						tabindex="{{tabindex}}"
						{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
						{{ settings.required ? settings.required_attribute }}
						{{ settings.disabled ? settings.disabled_attribute }}
						{{ settings.readonly ? settings.readonly_attribute }}
						{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
						{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
						{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
						data-label="{{ settings.label ?? settings.field|capitalize }}"
					>{{ _context[settings.field|upper]|default("") }}</textarea>
				</label>
			</div>

{% set settings = initials|merge({"field": "newsletter", "type": "checkbox", "required": Null, "label": "Welche Newsletter wünschen Sie?" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="full">
				<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span></label>
				<div {{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}>
					<div class="grouping">
{% for key, value in {"news":"Tägliche Nachrichten", "deals":"Spezial Angebote", "fun":"Glücksmomente"} %}
						<input type="hidden"  value="-"		  name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}_{{ key }}" id="{{ settings.field|replace({' ': '_' }) }}_{{ key }}"
								data-label="{{ value }}"
								data-value="Nein"
						 />
						<label>
							<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}_{{ key }}" id="{{ settings.field|replace({' ': '_' }) }}_{{ key }}"
								value="{{ key }}"
								tabindex="{{tabindex}}"
								{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
								{{ settings.required ? settings.required_attribute }}
								{{ settings.disabled ? settings.disabled_attribute }}
								{{ settings.readonly ? settings.readonly_attribute }}
								{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
								{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
								{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
								data-label="{{ value }}"
								data-value="Ja"
							 />
							 <span>{{ value }} {{ settings.required ? settings.required_markup }}</span>
						</label>
{% endfor %}
					</div>
				</div>
			</div>

			<div class="full">&nbsp;</div>


{% set settings = initials|merge({"field": "datenschutz", "value": "Akzeptiert", "type": "checkbox", "label": "Ich habe die Datenschutzerklärung gelesen und stimme der Verarbeitung meiner Daten zu." }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="full">
				<div {{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}>
					<input type="hidden"  value="-"		  name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}" />
					<label>
						<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							value="{{ settings.value }}"
							tabindex="{{tabindex}}"
							{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.field|capitalize }}"
							data-value="{{ settings.label }}"
							{% if _context[settings.field|upper] == settings.value %}checked="checked"{% endif %} 
						 />
						 <span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
					</label>
				</div>
			</div>

{% set settings = initials|merge({"field": "cc_mail", "value": "Ja", "required": Null, "type": "checkbox", "label": "Kopie dieser Nachricht erhalten" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="full">
				<div {{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}>
					<input type="hidden"  value="-"		  name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}" />
					<label>
						<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
							value="{{ settings.value }}"
							tabindex="{{tabindex}}"
							{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
							{{ settings.required ? settings.required_attribute }}
							{{ settings.disabled ? settings.disabled_attribute }}
							{{ settings.readonly ? settings.readonly_attribute }}
							{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
							{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
							{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
							data-label="{{ settings.label ?? settings.field|capitalize }}"
							{% if _context[settings.field|upper] == settings.value %}checked="checked"{% endif %} 
						 />
						 <span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
					</label>
				</div>
			</div>

			<div class="full">&nbsp;</div>

	{% if ENABLED_CAPTCHA == 1 %}
			<div class="{{ CAPTCHA_CLASS }} full">
				<label><span>{{ SPAM_TEXT }} {{ settings.required_markup }}</span>
					<div class="grouping {{ CAPTCHA_ERROR }}">
						{{ CAPTCHA }}
					</div>
				</label>
			</div>

			<div class="full">&nbsp;</div>
	{% endif %}

{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="full">
				<button class="submit"
						name="Submit"
						type="submit"
						tabindex="{{tabindex}}"
				>Senden</button>
			</div> 
		</form>
	</div>
</div>
{% endautoescape %}
