{% autoescape false %}
{# --------------------
 # check within LEPTON documentation (via help button) for decription of the possibilities (english only)
 # -------------------- #}
 
{# --------------------
 # set the default field settings
 # might be overwritten by specific field settings
 #   Key		 	= Default	=> Description
 # ----------------------------------------------
 #  *  field		= Null		=> defines the field name & id. Also used as label if label is not specified.
 #  *  label		= Null		=> define special field label. Empty default.
 #  *  value		= Null		=> define a specific value, mainly for single checkboxes. Empty default.
 #  *  type 		= "text"	=> defines the input field type. Default = "text"
 # (*) inputmode	= "vebatim"	=> defines the keyboard layout for mobile devices. Default = alphanumerical keyboard layout
 # (*) placeholder	= Null		=> set a placeholder text to be shown before user enters data. Not transmitted.
 # (*) required		= "r_"		=> mark field as required.
 # (*) autofocus	= Null		=> Set the autofocus on a field. Only 1 per form is allowed. Overruled by first error found.
 # (*) readonly		= Null		=> Set a field to readonly.
 # (*) disabled		= Null		=> Set a field to disabled.
 #
 # (*) such field has additional settings, mainly the attribute string
 # -------------------- #}
{% set initials = { 
  "field": Null
, "label": Null
, "value": Null
, "type": "text"
, "inputmode": "vebatim", "inputmode_attribute": 'inputmode="###"'
, "placeholder": Null, "placeholder_attribute": 'placeholder="###"'
, "required": "r_", "required_attribute": 'required="required"', "required_markup": '<span class="required">*</span>'
, "autofocus": Null, "autofocus_attribute": 'autofocus="autofocus"'
, "readonly": Null, "readonly_attribute": 'readonly="readonly"'
, "disabled": Null, "disabled_attribute": 'disabled="disabled"'
, "class_attribute": 'class="###"'
} %}

{# --------------------
 # define the form
 # -------------------- #}
<div class="quickform">
	<div class="{{ MESSAGE_CLASS }}">{{ STATUSMESSAGE }}</div>
	<div class="{{ FORM_CLASS }}">
		<h2>Rückruf Kontakt</h2>
{% if REQUIRED_FIELDS is not empty %}
		<small>Felder mit {{ required_markup }} gekennzeichnet sind Pflichtfelder.</small>
{% endif %}

		<form name="form_{{ SECTION_ID }}" id="form_{{ SECTION_ID }}" method="post" action="{{ URL }}" autocomplete="on">
			<input type="hidden" name="header"		value="Rückruf Kontakt" />
			<input type="hidden" name="timestamp"	value="{{ TIMESTAMP }}" data-label="Zeitstempel"/>
			<input type="hidden" name="contactid"	value="{{ CONTACT_ID }}" />
			<input type="hidden" name="quickform"	value="{{ SECTION_ID }}" />

{% set settings = initials|merge({"field": "name", "inputmode": "latin-name", "autofocus": true }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="onethird">
				<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
					<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
						value="{{ _context[settings.field|upper] }}"
						tabindex="{{tabindex}}"
						{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
						{{ settings.required ? settings.required_attribute }}
						{{ settings.disabled ? settings.disabled_attribute }}
						{{ settings.readonly ? settings.readonly_attribute }}
						{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
						{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
						{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
						data-label="{{ settings.label ?? settings.field|capitalize }}"
					 />
				</label>
			</div>

{% set settings = initials|merge({"field": "telefon nummer", "type": "tel", "inputmode": "tel", "placeholder": "z.B. +00 1234 56789" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="twothird pullright">
				<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
					<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
						value="{{ _context[settings.field|upper] }}"
						tabindex="{{tabindex}}"
						{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
						{{ settings.required ? settings.required_attribute }}
						{{ settings.disabled ? settings.disabled_attribute }}
						{{ settings.readonly ? settings.readonly_attribute }}
						{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
						{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
						{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
						data-label="{{ settings.label ?? settings.field|capitalize }}"
					 />
				</label>
			</div>

{% set settings = initials|merge({"field": "kontaktdatum", "type": "date", "required": Null, "label": "Erwünschtes Kontaktdatum" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="onethird">
				<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
					<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
						value="{{ _context[settings.field|upper] }}"
						tabindex="{{tabindex}}"
						{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
						{{ settings.required ? settings.required_attribute }}
						{{ settings.disabled ? settings.disabled_attribute }}
						{{ settings.readonly ? settings.readonly_attribute }}
						{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
						{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
						{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
						data-label="{{ settings.label ?? settings.field|capitalize }}"
{# today or later #}	min="{{ now|date("Y-m-d") }}"
					 />
				</label>
			</div>

{% set settings = initials|merge({"field": "kontaktzeit", "type": "time", "required": Null, "label": "Erwünschte Kontaktzeit" }) %}
{% if _context[ settings.field|upper ~ "_ERROR" ] %}{% set settings = settings|merge({"autofocus": true}) %}
{% elseif IS_ERROR %}{% set settings = settings|merge({"autofocus": NULL}) %}{% endif %}
{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="onethird">
				<label><span>{{ settings.label ?? settings.field|capitalize }} {{ settings.required ? settings.required_markup }}</span>
					<input type="{{ settings.type }}" name="qf_{{ settings.required }}{{ settings.field|replace({' ': '_' }) }}" id="{{ settings.field|replace({' ': '_' }) }}"
						value="{{ _context[settings.field|upper] ?? "00:00" }}"
						tabindex="{{tabindex}}"
						{{ settings.placeholder ? settings.placeholder_attribute|replace({'###': settings.placeholder}) }}
						{{ settings.required ? settings.required_attribute }}
						{{ settings.disabled ? settings.disabled_attribute }}
						{{ settings.readonly ? settings.readonly_attribute }}
						{{ _context[ settings.field|upper ~ "_ERROR" ] ? settings.class_attribute|replace({'###': settings.field|upper ~ "_ERROR"}) }}
						{{ settings.inputmode_attribute|replace({'###': settings.inputmode}) }}
						{% if settings.autofocus %}{{ autofocus_attribute }}set settings = settings|merge[{"autofocus_attribute": Null}]{% endif %}
						data-label="{{ settings.label ?? settings.field|capitalize }}"
{# time limits #}		min="08:00" max="20:00"
					 />
				</label>
			</div>

			<div class="full">&nbsp;</div>

{% set tabindex = ( tabindex | default(0) ) + 1 %}
			<div class="full">
				<button class="submit"
						name="Submit"
						type="submit"
						tabindex="{{tabindex}}"
				>Bitte zurückrufen</button>
			</div> 
		</form>
	</div>
</div>
{% endautoescape %}
