<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		LEPTON project,  W. Studer
 *	@copyright		2010-2025  LEPTON project (initial by Ruud Eisinga)
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// modify database
$database = LEPTON_database::getInstance();
$table = 'mod_quickform';
$release = $database->get_one("SELECT version FROM ".TABLE_PREFIX."addons WHERE directory = 'mod_quickform'");
if ($release < '2.1.0')
{
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX.$table." CHANGE `use_honeypot` `use_honeypot` INT(1) NOT NULL DEFAULT 0 ");
	$database->simple_query("ALTER TABLE ".TABLE_PREFIX.$table." CHANGE `spam_logging` `spam_logging` INT(1) NOT NULL DEFAULT 0 ");
}