<?php

/**
 * @module          Reset PIN
 * @author          cms-lab
 * @copyright       2010-2025 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/reset-pin/license.php
 * @license_terms   please see license
 *
 */

class reset_pin extends LEPTON_abstract
{
	public $all_users = array();
	public $database = 0;
	public $admin = 0;
	public $addon_color = 'blue';
	public $action_url = ADMIN_URL . '/admintools/tool.php?tool=reset_pin';

	public static $instance;

	public function initialize()
	{
		$this->database = LEPTON_database::getInstance();
		$this->admin = LEPTON_admin::getInstance();
		$this->init_tool();
	}

	public function init_tool( $sToolname = '' )
	{

		//get all users
		$this->all_users = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."users " ,
			true,
			$this->all_users,
			true
		);

	}

	public function reset_value()
	{
		if(isset ($_POST['username']) && $_POST['username'] != '') {
			$user_id = intval($_POST['username']);
		}
		elseif(isset ($_POST['display_name']) && $_POST['display_name'] != '') {
			$user_id = intval($_POST['display_name']);
		}
		else
		{
			return array( "STATUS" => false, "MESSAGE_ID" => "MISSING_USER" );
		}

		$this->database->simple_query("UPDATE ".TABLE_PREFIX."users SET pin = '-1' WHERE user_id = ".$user_id." ");
		$this->database->simple_query("UPDATE ".TABLE_PREFIX."users SET pin_set = 0 WHERE user_id = ".$user_id." ");

		// Check if there is a db error, else success
		if($this->database->is_error())
			{ return array( "STATUS" => false, "MESSAGE_ID" => "SAVE_FAILED" ); }
		else
			{ return array( "STATUS" => true, "MESSAGE_ID" => "SAVE_OK" ); }
	}

	/** =========================================================================
	 *
	 * Show an info popup
	 *
	 * @access  public
	 * @param   $modvalues  As optional array containing module specialized values
	 * @param   $bPrompt    True for direct output via echo, false for returning the generated source.
	 * @return  mixed       Depending on the $bPrompt param: boolean or string.
	 */
	public function showmodinfo( $modvalues = null, $bPrompt = true )
	{
		// prepare array with module specific values
		$modvalues = array(
			"BUTTONS"		=> array(
				 "README"		=> array( "AVAILABLE"	=> true
										,"URL"			=> "http://cms-lab.com/_documentation/reset-pin/readme.php"
				)
			)
		);

		// show module info
		$sSource = parent::showmodinfo( $modvalues );

		return $sSource;
	}


} // end of class
?>