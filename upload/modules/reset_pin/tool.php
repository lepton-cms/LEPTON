<?php

/**
 * @module          Reset PIN
 * @author          cms-lab
 * @copyright       2010-2025 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/reset-pin/license.php
 * @license_terms   please see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// get instance of functions file
$oRESET_PIN = reset_pin::getInstance();

$statusMessage = "";

if (( true === isset( $_POST['username'] ))
 || ( true === isset( $_POST['display_name'] )))
{
	if (( true === isset($_SESSION['reset_pin_hash']) 
	 && ( $_SESSION['reset_pin_hash'] == $_POST['hash']) ))
	{
		unset($_SESSION['reset_pin_hash']);
		unset($_POST['hash']);

		$statusMessage = $oRESET_PIN->reset_value();
	} else {
		$statusMessage = array( "STATUS" => false, "MESSAGE_ID" => "SAVE_FAILED" );
	}
}

//  [0] build hash
$sHash = LEPTON_handle::createGUID();
$_SESSION['reset_pin_hash'] = $sHash;

//  [1] set template interface values
$templateValues = array(
	'oRESET_PIN'	=> $oRESET_PIN,
	"hash"			=> $sHash,
	'readme_link'	=> 'https://cms-lab.com/_documentation/reset-pin/readme.php',
	'leptoken'		=> get_leptoken()
);
if ( true === is_array( $statusMessage ))
	{ $templateValues["message"] = $statusMessage; }

/**
 *	get the template-engine.
 */
$oTwig = lib_twig_box::getInstance();
$oTwig->registerModule('reset_pin');

echo $oTwig->render(
	"@reset_pin/tool.lte",	//	template-filename
	$templateValues			//	template-data
);

