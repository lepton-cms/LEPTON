<?php

/**
 * @module          Reset PIN
 * @author          cms-lab
 * @copyright       2010-2025 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/reset-pin/license.php
 * @license_terms   please see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure file

$module_directory     = "reset_pin";
$module_name          = "Reset PIN";
$module_function      = "tool";
$module_version       = "0.2.2";
$module_platform      = "5.x";
$module_author        = "<a href='https://cms-lab.com' target='_blank'>CMS-LAB</a>";
$module_license       = "<a href='https://cms-lab.com/_documentation/reset-pin/license.php' class='admintools_link' target='_blank'>Custom license</a>";
$module_license_terms = "please see license";
$module_description   = "Tool to reset PIN if <a href='https://doc.lepton-cms.org/docu/english/home/core/features/two-factor-authentication.php' class='admintools_link' target='_blank'>TFA</a> failed";
$module_guid		  = "767281a0-096a-40dc-8987-bc132d76578b";

