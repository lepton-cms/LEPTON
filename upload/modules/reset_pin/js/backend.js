/**
 * @module          Reset PIN
 * @author          cms-lab
 * @copyright       2010-2025 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/reset-pin/license.php
 * @license_terms   please see license
 *
 */

$('.ui.dropdown')
  .dropdown()
;


