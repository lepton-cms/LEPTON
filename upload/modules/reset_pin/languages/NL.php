<?php

/**
 *	@module			Reset PIN
 *	@version		please see info.php of this module
 *	@author			cms-lab
 *	@copyright		2010-2025 cms-lab
 *	@link			https://cms-lab.com
 *	@license		custom license: https://cms-lab.com/_documentation/reset-pin/license.php
 *	@license_terms	please see info.php of this module
 *	@platform		please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:20, 25-06-2020
 * translated from..: EN
 * translated to....: NL
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_RESET_PIN	= array(
	"chose_user"		=> "Kies de gebruiker",
	"or"				=> "OF",
	"SAVE_OK"			=> "PIN werd gereset",
	"SAVE_FAILED"		=> "PIN is NIET gereset.",
	"MISSING_USER"		=> "Selecteer eerst een gebruiker!"
);

?>
