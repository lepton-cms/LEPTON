<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          Twig Template Engine
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON  
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$module_directory   = 'lib_twig';
$module_name        = 'Twig Library for LEPTON';
$module_function    = 'library';
$module_version     = '3.20.0.0';
$module_platform    = '7.x';
$module_delete      =  false;
$module_author      = 'sensiolabs.org, LEPTON Team';
$module_license     = 'GNU General Public License for LEPTON Addon, https://twig.symfony.com/license for Twig';
$module_description = 'Twig PHP Template Engine. Please visit <a href="https://twig.symfony.com/" target="_new">https://twig.symfony.com/" for details.</a>';
$module_home        = 'https://lepton-cms.org/';
$module_guid        = '19fb9aba-7f31-4fee-81ea-1db03e83c6cc';
