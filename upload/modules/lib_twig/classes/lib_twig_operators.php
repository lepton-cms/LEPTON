<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          Twig Template Engine
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON  
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

class lib_twig_operators extends \Twig\Extension\AbstractExtension
{
    // initialize
    public function __construct()
    {
    
    }
    
    /**
     *  See: page 40 ff. inside the twig documentation-pdf. 
     *      https://twig.symfony.com/doc/2.x/
     *      https://twig.symfony.com/doc/2.x/advanced.html#operators
     */
    public function getOperators(): array
    {
        return [
            [
                '!' => [
                    'precedence' => 50,
                    'precedence_change' => null,
                    'class' => Twig\Node\Expression\Unary\NotUnary::class
                ],
                '¬' => [
                    'precedence' => 40,
                    'precedence_change' => null,
                    'class' => Twig\Node\Expression\Unary\NotUnary::class
                ]
            ],
            [
                '||' => [
                    'precedence' => 10,
                    'precedence_change' => null,
                    'class' => Twig\Node\Expression\Binary\OrBinary::class,
                    'associativity' => \Twig\ExpressionParser::OPERATOR_LEFT
                ],
                '&&' => [
                    'precedence' => 15,
                    'precedence_change' => null,
                    'class' => Twig\Node\Expression\Binary\AndBinary::class,
                    'associativity' => \Twig\ExpressionParser::OPERATOR_LEFT
                ]
            ]
        ];
    }
}