<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          Twig Template Engine
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON  
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

class lib_twig_functions extends Twig\Extension\AbstractExtension
{

    const LEPTON_INI_FILEPATH = LEPTON_PATH."/config/lepton.ini.php";

    private static array $sConfigData = [];
    
    // Requested by the parent.
    public function __construct() {
        // At this time nothing to do here.    
    }

    /**
     *  See: page 43 ff. inside the twig documentation-pdf. 
     *  https://twig.symfony.com/doc/3.x/advanced.html#functions
     *  @usage: {{ getPasswordPattern() }}
     *  @return object
     */
    public static function getPasswordPattern(): object {
        return new Twig\TwigFunction('getPasswordPattern', function () {
            $add_custom = self::getConfigCustomValue('additional_password_chars');

            $pattern = str_replace("\\", "", LEPTON_core::getInstance()->password_chars.$add_custom);
            $pattern = str_replace("_-", "_\\-", $pattern);
            return '['.$pattern.']{'.AUTH_MIN_PASS_LENGTH.','.AUTH_MAX_PASS_LENGTH.'}';
        });
    }

    /**
     *  See: page 43 ff. inside the twig documentation-pdf. 
     *  https://twig.symfony.com/doc/3.x/advanced.html#functions
     *  @usage: {{ getEmailPattern() }}
     *  @return object
     */
    public static function getEmailPattern(): object {
        return new Twig\TwigFunction('getEmailPattern', function () {
            $add_custom = self::getConfigCustomValue('additional_email_chars');

            $pattern = str_replace("\\", "", LEPTON_core::getInstance()->email_chars.$add_custom);
            $pattern = str_replace("_-", "_\\-", $pattern);
            return '['.$pattern.']{1,}';
        });
    }

    /**
     *  See: page 43 ff. inside the twig documentation-pdf. 
     *  https://twig.symfony.com/doc/3.x/advanced.html#functions
     *  @usage: {{ getDisplayPattern() }}
     *  @return object
     */
    public static function getDisplayPattern(): object {
        return new Twig\TwigFunction('getDisplayPattern', function () {
            $add_custom = self::getConfigCustomValue('additional_usernames_chars');

            $pattern = str_replace("\\", "", LEPTON_core::getInstance()->username_chars.$add_custom);
            $pattern = str_replace("_-", "_\\-", $pattern);

            return '['.$pattern.']{'.AUTH_MIN_LOGIN_LENGTH.','.AUTH_MAX_LOGIN_LENGTH.'}';
        });
    }

    /**
     * 
     * @usage:
     * <span>{{ getPattern('UserNames') }}</span>
     * <span>{{ getPattern('Email') }}</span>
     * <span>{{ getPattern('PassWord') }}</span>
     * 
     * @return object
     */
    public static function getPattern(): object {
        return new Twig\TwigFunction('getPattern', function(string $sPattenType) {
            switch (strtolower($sPattenType))
            {
                case 'usernames':
                    $add_custom = self::getConfigCustomValue('additional_usernames_chars');
                    $pattern = str_replace("\\", "", LEPTON_core::getInstance()->username_chars.$add_custom);
                    break;

                case 'email':
                    $add_custom = self::getConfigCustomValue('additional_email_chars');
                    $pattern = str_replace("\\", "", LEPTON_core::getInstance()->email_chars.$add_custom);
                    break;
                
                case 'password':
                    $add_custom = self::getConfigCustomValue('additional_password_chars');
                    $pattern = str_replace("\\", "", LEPTON_core::getInstance()->password_chars.$add_custom);
                    break;

                default:
                    $pattern = "(unknown pattern!)";
                    break;
            }
            $pattern = str_replace("_-", "_\\-", $pattern);

            return $pattern;
        });
    }
    /**
     *  Internal method: get the value of the "sub-key" of "custom_vars" as a string.
     *
     *  @param string $keyName  A valid key inside the [custom_vars] partition.
     *
     *  @return string The value or an empty string.
     */
    private static function getConfigCustomValue(string $keyName): string
    {
        if (empty(self::$sConfigData))
        {
            self::$sConfigData = parse_ini_string(";".file_get_contents(self::LEPTON_INI_FILEPATH), true);
        }
        return (self::$sConfigData['custom_vars'][$keyName] ?? "");
    }

    /**
     *  check if file exists.
     *  @param string $lookUpPath  A valid path including LEPTON_PATH
     *  @usage (use tilde as a term-connector): {% if !fileExists( (AnyPath ~ AnyFilePart1 ~ '_' ~ AnyFilePart2 ~ '.pdf') ) %}	
     *  @return object.
     */
    public static function fileExists(string $lookUpPath = ''): object {
        return new Twig\TwigFunction('fileExists', function (string $lookUpPath = '') {
            return file_exists($lookUpPath);
        });
    }
}
