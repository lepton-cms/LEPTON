### lib_fomantic
============

Fomantic framework ( a fork of semantic-ui.com) as a library for Content Management Systems [LEPTON CMS][1]<br />
For details please see [Fomantic Homepage][3]

#### Requirements

* [LEPTON CMS][1]

#### Installation

* download latest [lib_fomantic.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### First Steps

Go to https://fomantic-ui.com/ and see what to do

#### Notice

This library delivers default theme only.<br />
More Themes can be added individually. Please visit [Fomantic Homepage][3].

[1]: https://lepton-cms.org "LEPTON CMS"
[2]: https://lepton-cms.com/lepador/libraries/lib_fomantic.php
[3]: https://fomantic-ui.com/ "Fomantic Homepage"
[4]: https://fomantic-ui.com/introduction/getting-started.html "Getting Semantic"