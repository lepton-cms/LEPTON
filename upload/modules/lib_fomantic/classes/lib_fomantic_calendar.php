<?php

/**
 *  @module      	Library Fomantic
 *  @version        see info.php of this module
 *  @author         LEPTON project
 *	@copyright      2010-2025 LEPTON Project
 *  @license        https://opensource.org/licenses/MIT
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

/**
 *  @see    https://fomantic-ui.com/modules/calendar.html#/examples
 */
class lib_fomantic_calendar
{
    use LEPTON_singleton;

    // Own instance for this class!
    public static $instance;
    
    public object|null $oTWIG = null;
    
    public array $defaultEvent = [
        'date'      => '',  // YYYY-MM-DD
        'message'   => '',
        'class'     => 'inverted olive',   // cellclass
        'variation' => 'olive'         // tooltip var
    ];
    
    protected int $displayMonths = 1;
    
    protected array $events = [];
    protected array $disabledDates = [];
    
    public function __construct()
    {
        $this->oTWIG = lib_twig_box::getInstance();
        $this->oTWIG->registerModule("lib_fomantic", "lib_fomantic");
        
        $this->setFixedEvents_Ger();
        $this->setDisabledDates_Ger();
    }
    
    public function addEvent(string|int $sDate, string $sMessage, string $sClass="", string $sVariation = ""): void
    {
        $this->events[] = [
            'date'      => $sDate,     // YYYY-MM-DD
            'message'   => $sMessage,
            'class'     => $sClass,    // cellclass
            'variation' => $sVariation // tooltip var
        ];
    
    }
    
    public function addDisabledDate(string|int $sDate, string $sMessage, string $sClass="", string $sVariation = "", bool $bInverted = false): void
    {
        $this->disabledDates[] = [
            'date'      => $sDate,     // YYYY-MM-DD
            'message'   => $sMessage,
            'class'     => $sClass,    // cellclass
            'variation' => $sVariation,// tooltip var
            'inverted'  => $bInverted
        ];
    
    }
    
    public function generate(): string
    {
        $data = [
            'divID'              => "123456abcde",
            'additionalCSSClass' => "",
            'initialDate'        => date("Y-m-d"),
            'daynames'           => $this->getDayNames(), // "['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag']"
            'monthsnames'        => $this->getMonthNames(),
            'events'             => $this->events,
            'disabledDates'      => $this->disabledDates,
            'displayMonths'      => $this->displayMonths
        ];

        return $this->oTWIG->render(
            '@lib_fomantic/calendar.lte',
            $data
        );
    }
    
    public function setDisplayMonths(int $iNumber): void
    {
        if ($iNumber < 1)
        {
            $iNumber = 1;
        }
        
        if ($iNumber > 6)
        {
            $iNumber = 6;
        }
        $this->displayMonths = $iNumber;
    }
    
    protected function setFixedEvents_Ger(): void
    {
        $year = date("Y"); // Actual year
        
        for ($i = $year-1; $i <= $year+1; $i++)
        {
            // Zeitumstellung
            /*
            $temp = strtotime('-1 week sun april' . $i);
            $this->addEvent(date("Y-m-d", $temp), 'Sommerzeit (+1)', 'blue', 'inverted blue tiny');
            
            $temp = strtotime('-1 week sun november' . $i);
            $this->addEvent(date("Y-m-d", $temp), 'Normalzeit (-1)', 'blue', 'inverted blue tiny');
            */
        }    
    }
    
    protected function setDisabledDates_Ger(): void
    {
        $year = date("Y"); // Actual year
        
        for ($i = $year-1; $i <= $year+1; $i++)
        {
            $this->addDisabledDate($i.'-01-01', 'Neujahr');
            $this->addDisabledDate($i.'-10-03', 'Tag der deutschen Einheit', 'yellow' , 'inverted yellow', true);
            $this->addDisabledDate($i.'-12-24', 'Weihnachten', 'orange' , 'inverted orange', true);
            $this->addDisabledDate($i.'-12-25', '1. Weihnachtsfeiertag', 'orange' , 'inverted orange', true);
            $this->addDisabledDate($i.'-12-26', '2. Weihnachtsfeiertag', 'orange' , 'inverted orange', true);
            $this->addDisabledDate($i.'-12-31', 'Sylvester');

            $temp = strtotime('-1 week sun april' . $i);
            $this->addDisabledDate(date("Y-m-d", $temp), 'Sommerzeit (+1)', 'blue', 'inverted blue tiny');
            
            $temp = strtotime('-1 week sun november' . $i);
            $this->addDisabledDate(date("Y-m-d", $temp), 'Normalzeit (-1)', 'blue', 'inverted blue tiny');

            // Ostern
            $ostern = strtotime("+ " . (easter_days($i)) . " days", mktime(0, 0, 0, 3, 21, $i));
            $this->addDisabledDate(date("Y-m-d", $ostern), 'Ostersonntag', 'orange', 'inverted orange tiny');
            // Ostermontag
            $ostermontag = strtotime("+1 day", $ostern);
            $this->addDisabledDate(date("Y-m-d", $ostermontag), 'Ostermontag', 'orange', 'inverted orange tiny');
        }
    }
    
    protected function getDayNames(): string
    {
        $oTOOL = LEPTON_date::getInstance();
        $aNames = $oTOOL->getWeekdayNames("de_DE", true);
        
        $temp = array_pop($aNames);
        array_unshift($aNames, $temp);
        
        return "['".implode("','", $aNames)."']";
    }
    
    protected function getMonthNames(): string
    {
        $oTOOL = LEPTON_date::getInstance();
        $aNames = $oTOOL->getMonthNames("de_DE", false);
        return "['".implode("','", $aNames)."']";
    }

}
