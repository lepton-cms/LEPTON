<?php

/**
 *  @module      	Library Fomantic
 *  @version        see info.php of this module
 *  @author         LEPTON project
 *	@copyright      2010-2025 LEPTON Project
 *  @license        https://opensource.org/licenses/MIT
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

//remove obsolete files upgrade from 2.8.8,0 to 2.9.2.0
$database = LEPTON_database::getInstance();
$release = $database->get_one("SELECT version FROM ".TABLE_PREFIX."addons WHERE directory = 'lib_fomantic'");
if ($release < '2.9.2') 
{
	LEPTON_handle::delete_obsolete_directories(['/modules/lib_fomantic/dist/themes/default/assets/images']);
	LEPTON_handle::delete_obsolete_directories(['/modules/lib_fomantic/dist/themes/default/assets/fonts']);
	
	rename( LEPTON_PATH.'/modules/lib_fomantic/dist/themes/default/assets/fonts_292',LEPTON_PATH .'/modules/lib_fomantic/dist/themes/default/assets/fonts');	
}
else
{
	LEPTON_handle::delete_obsolete_directories(['/modules/lib_fomantic/dist/themes/default/assets/fonts_292']);	
}
