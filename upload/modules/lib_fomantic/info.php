<?php

/**
 *  @module      	Library Fomantic
 *  @version        see info.php of this module
 *  @author         LEPTON project
 *	@copyright      2010-2025 LEPTON Project
 *  @license        https://opensource.org/licenses/MIT
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file



$module_directory = 'lib_fomantic';
$module_name      = 'Fomantic Library';
$module_function  = 'library';
$module_version   = '2.9.4.0';
$module_platform  = '7.x';
$module_delete	  =  false;
$module_author    = 'cms-lab';
$module_license   = '<a href="http://opensource.org/licenses/MIT" target="_blank">MIT license</a>';
$module_license_terms   = '-';
$module_description = 'This module installs basic files <a href="https://fomantic-ui.com/" target="_blank">Fomantic UI</a>.';
$module_guid      = '316ca2d0-cb0a-4d23-89d0-424a7a51e125';
$module_home      = 'https://lepton-cms.com';
