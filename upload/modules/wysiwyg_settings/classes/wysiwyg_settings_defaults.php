<?php

/**
 * @module          Wysiwyg Settings
 * @author          LEPTON team
 * @copyright       2010-2025 LEPTON team
 * @link            https://lepton-cms.org
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

class wysiwyg_settings_defaults extends LEPTON_abstract implements LEPTON_wysiwyg_interface
{
    public string $default_width   = "100";
    public string $default_height  = "300";
    public string $default_skin    = "none";
    public string $default_toolbar = "none";
    // compatible to wysiwyg_settings
    public string $toolbar         = "none";

    public array $skins           = ["none" => ""];
    public array $toolbars        = ["none" => ""];
    
    public static $instance;

    public function initialize()
    {
        // Nothing to do here now
    }

    // interfaces
    //  [1]
    public function getHeight(): string
    {
        return $this->default_height;
    }

    //  [2]
    public function getWidth(): string
    {
        return $this->default_width;
    }

    //  [3]
    public function getToolbar(): array
    {
        return $this->toolbars[$this->default_toolbar];
    }

    //  [4]
    public function getSkin(): string
    {
        return $this->default_skin ?? "";
    }

}	
