<?php

/**
 * @module          Wysiwyg Settings
 * @author          LEPTON team
 * @copyright       2010-2025 LEPTON team
 * @link            https://lepton-cms.org
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$MOD_WYSIWYG_SETTINGS = [
    'class_missing' => "Can't display preview: editor class is missing!",
	'css'	    	=> "CSS Style",
	'error'	        => "FAILURE",	
	'info'	        => "Addon Info",
	'header1'	   	=> "Settings for",
	'height'	   	=> "Height in px",
	'help'	    	=> "Help page",
	'no_editor'  	=> "<h2>Attention: no Editor!</h2><p>Select from the options an Editor!</p>",
	'save_ok'	    => "Data saved",
	'select'	    => "Please Select",
	'skin'		    => "Skin",
	'toolbar'	    => "Toolbar",
	'width'	    	=> "Width in %",
	'not_available' => "Not available"
];
