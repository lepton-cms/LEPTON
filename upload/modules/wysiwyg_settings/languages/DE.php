<?php

/**
 * @module          Wysiwyg Settings
 * @author          LEPTON team
 * @copyright       2010-2025 LEPTON team
 * @link            https://lepton-cms.org
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$MOD_WYSIWYG_SETTINGS = [
    'class_missing' => "Vorschau kann nicht angezeigt werden: die entsprechende 'editor Klasse' fehlt!",
	'css'	    	=> "CSS Style",
	'error'	        => "FEHLER",	
	'info'	        => "Addon Info",
	'header1'	   	=> "Einstellungen für",
	'height'	   	=> "Höhe in px",	
	'help'	    	=> "Hilfe-Seite",
	'no_editor'  	=> "<h2>Achtung: kein Editor!</h2><p>Bitte wählen Sie in den Optionen einen Editor aus!</p>",
	'save_ok'	    => "Daten erfolgreich gespeichert",
	'select'	    => "Bitte auswählen",
	'skin'		    => "Skin",
	'toolbar'	    => "Toolbar",
	'width'	    	=> "Breite in %",
	'not_available' => "Nicht verfügbar"
];
