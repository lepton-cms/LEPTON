<?php

/**
 * @module          Wysiwyg Settings
 * @author          LEPTON team
 * @copyright       2010-2025 LEPTON team
 * @link            https://lepton-cms.org
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// install table
$table_fields="
	`id`		int(11) NOT NULL AUTO_INCREMENT,
	`skin`		varchar(128) NOT NULL DEFAULT 'none',
	`toolbar`	varchar(128) NOT NULL DEFAULT 'none',
	`content_css` varchar(128) NOT NULL DEFAULT 'none',	
	`width`		int(4) NOT NULL DEFAULT 960,
	`height`	int(4) NOT NULL DEFAULT 500,
	`editor`	varchar(128) NOT NULL DEFAULT 'none',
	PRIMARY KEY (`id`)
";
LEPTON_handle::install_table('mod_wysiwyg_settings', $table_fields);
