<?php

/**
 * Module handles the first entry backend page after new login by user.
 *
 * @module          initial_page
 * @author          LEPTON project 
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$module_directory   = "initial_page";
$module_name        = "Initial Page";
$module_function    = "tool";
$module_version     = "1.5.5";
$module_platform    = "6.x";
$module_delete      = true;
$module_author      = "LEPTON project";
$module_license     = "copyright, all rights reserved";
$module_license_terms = "usage only with written permission, use with LEPTON core is allowed";
$module_description = "This module allows to set up an initial_page in the backend for each user.";
$module_guid        = "237D63F7-4199-48C7-89B2-DF8E2D8AEE5F";
