<?php

/**
 * Module handles the first entry backend page after new login by user.
 *
 * @module          initial_page
 * @author          LEPTON project 
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:07, 25-06-2020
 * translated from..: EN
 * translated to....: NL
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_INITIAL_PAGE	= [
	"label_user"		=> "Gebruiker",
	"label_page"		=> "Pagina",
	"label_default"		=> "Standaard startpagina",
	"label_param"		=> "Optionele params",
	"head_select"		=> "Selecteer een eerste pagina voor de gebruiker(s)",
	"SAVE_OK"			=> "De instellingen worden opgeslagen.",
	"SAVE_FAILED"		=> "Er is een fout opgetreden tijdens het opslaan.<br />Please probeer het opnieuw.<br /><br />Als er weer een fout optreedt, meld dan het probleem <a href='https://forum.lepton-cms.org' target='_blank'>[hier]</a>."
];

