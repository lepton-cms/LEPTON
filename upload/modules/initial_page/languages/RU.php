<?php

/**
 * Module handles the first entry backend page after new login by user.
 *
 * @module          initial_page
 * @author          LEPTON project 
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:08, 25-06-2020
 * translated from..: EN
 * translated to....: RU
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_INITIAL_PAGE	= [
	"label_user"		=> "Пользователь",
	"label_page"		=> "Страница",
	"label_default"		=> "Начальная страница по умолчанию",
	"label_param"		=> "Дополнительные параметры",
	"head_select"		=> "Пожалуйста, выберите одну начальную страницу для пользователя(-ов).",
	"SAVE_OK"			=> "Настройки сохраняются.",
	"SAVE_FAILED"		=> "Во время сохранения произошла ошибка.<br />Попробуйте еще раз.<br /><br />Если ошибка повторится, пожалуйста, сообщите об ошибке <a href='https://forum.lepton-cms.org' target='_blank'>[здесь]</a>."
];

