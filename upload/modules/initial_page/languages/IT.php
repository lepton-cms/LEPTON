<?php

/**
 * Module handles the first entry backend page after new login by user.
 *
 * @module          initial_page
 * @author          LEPTON project 
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:06, 25-06-2020
 * translated from..: EN
 * translated to....: IT
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_INITIAL_PAGE	= [
	"label_user"		=> "Utente",
	"label_page"		=> "Pagina",
	"label_default"		=> "Pagina iniziale predefinita",
	"label_param"		=> "Parametri opzionali",
	"head_select"		=> "Si prega di selezionare una pagina iniziale per l'utente(-s)",
	"SAVE_OK"			=> "Le impostazioni vengono salvate.",
	"SAVE_FAILED"		=> "Si è verificato un errore durante il salvataggio.<br />Provare di nuovo.<br /><br />Se l'errore si verifica di nuovo, si prega di segnalare il problema <a href='https://forum.lepton-cms.org' target='_blank'>[qui]</a>."
];

