<?php

/**
 *
 * @module          initial_page
 * @author          LEPTON project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$oINITIAL_PAGE = initial_page::getInstance();

$statusMessage = "";

if (true === isset($_POST['job']))
{
	if (isset($_SESSION['initial_page_hash']) && ( $_SESSION['initial_page_hash'] == $_POST['hash']) )
	{
		unset($_SESSION['initial_page_hash']);
		unset($_POST['hash']);

		/**
		 *  htmlpurifier
		 *  see: http://htmlpurifier.org/docs
		 */
		$oPurifier = lib_lepton::getToolInstance("htmlpurifier");
        
        // a.1 Using filter_input instead of direct access
        $job = filter_input( INPUT_POST, 'job', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        
        // a.2 using direct comparison instead of a switch
        if ($job === 'save')
        {
            foreach ($_POST['init_page_select'] as $item => $value)
            {
                $temp = explode("_", $item);
                $uid = (int)array_pop($temp);
                $param = htmlspecialchars($oPurifier->purify($_POST['param'][$uid]));

                $oINITIAL_PAGE->update_user(
                    $uid,
                    $value,
                    $param
                );
            }

            $statusMessage = array("STATUS" => true, "MESSAGE_ID" => "SAVE_OK");
        }
	} else {
        $statusMessage = array("STATUS" => false, "MESSAGE_ID" => "SAVE_FAILED");
	}
}

$database = LEPTON_database::getInstance();
// get all active user
$all_users = [];
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."users` WHERE `active` = 1",
	true,
	$all_users,
	true
);

// check for each if backend permissions are available
$aAllUsers = [];
foreach ($all_users as $user)
{
	// a user can be in multiple groups
    $groups_id = explode(",", $user['groups_id']);
    foreach ($groups_id as $group_id)
	{
		// no further check if user has already been set in a previous group
		if  (false === array_key_exists( $user['user_id'], $aAllUsers ))
		{
			// check if user has backend permissions
            $check_backend = intval($database->get_one("SELECT `backend_access` FROM `" . TABLE_PREFIX . "groups` WHERE `group_id` = " . $group_id));
			if (1 === $check_backend )
			{
                $user["backend_access"] = 1;
                $user["temp_info"] = $oINITIAL_PAGE->get_user_info($user['user_id']);
                $user["select"] = $oINITIAL_PAGE->get_backend_pages_select(
                    "init_page_select[user_" . $user['user_id'] . "]",
                    $user["temp_info"]['init_page']
                );

				// set user in array used in tool.lte
				$aAllUsers[ $user['user_id'] ] = $user;
			}
		}
	}
}
unset($all_users);

//  [0] build hash
$sHash = LEPTON_handle::createGUID();

$_SESSION['initial_page_hash'] = $sHash;

//  [1] set template interface values
$templateValues = array(
	"ADMIN_URL"		=> ADMIN_URL,
	"hash"			=> $sHash,
	"oINITIAL_PAGE"	=> $oINITIAL_PAGE,
	"aAllUsers"		=> $aAllUsers
);

if (true === is_array($statusMessage))
{
    $templateValues["message"] = $statusMessage;
}

//  [2] get Twig-Instance
$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule('initial_page');

echo $oTWIG->render(
	"@initial_page/tool.lte",
	$templateValues
);
