<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          menu-link
 * @author          WebsiteBaker Project, LEPTON Project
 * @copyright       2004-2010 WebsiteBaker Project
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// get target_page infos
$menu_link_info = [];
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_menu_link` WHERE `page_id` = ".PAGE_ID." ",
	true,
	$menu_link_info,
	false
);

$target_page_id = $menu_link_info['target_page_id'];
$redirect_type = $menu_link_info['redirect_type'];
$anchor = ($menu_link_info['anchor'] != '0' ? '#'.(string)$menu_link_info['anchor'] : '');
$extern = $menu_link_info['extern'];
// set redirect-type
if($redirect_type == 301)
{
	header('HTTP/1.1 301 Moved Permanently', TRUE, 301);
}

if($target_page_id == -1)	// external link
{
	header('Location: '.$extern);
	exit;
}
else
{
	// get link of internal target-page
	$target_page_link = $database->get_one("SELECT `link` FROM `".TABLE_PREFIX."pages` WHERE `page_id` = ".$target_page_id." ");
	if(!is_null($target_page_link))
	{
		$target_url = LEPTON_URL.PAGES_DIRECTORY.$target_page_link.PAGE_EXTENSION.$anchor;
		header('Location: '.$target_url);
		exit;
	}
}

