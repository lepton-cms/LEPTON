<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          menu-link
 * @author          WebsiteBaker Project, LEPTON Project
 * @copyright       2004-2010 WebsiteBaker Project
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// Headings and text outputs
$MOD_MENU_LINK['TEXT'] = 'Kliknij tu aby przejść do strony';
$MOD_MENU_LINK['EXTERNAL_LINK'] = 'Link zewnętrzny';
$MOD_MENU_LINK['R_TYPE'] = 'Redirect-Type';
$MOD_MENU_LINK['XHTML_EXPLANATION'] = 'Info: Jeśli użyjesz opcje SM2_XHTML_STRICT z show_menu2(), te ustawienie nic nie zmienią!';
$MOD_MENU_LINK['REDIRECT_EXPLANATION'] = 'Info: 301: Żądany zasób otrzymał nowy stały URI i jakiekolwiek przyszłe odniesienia do tego zasobu 
powinno używać jednego ze zwróconych URIs <br /> 302:. 
Żądany zasób znajduje się chwilowo pod innym URI. 
Przekierowania mogą być zmieniane od czasu do czasu, klient powinien 
nadal korzystać z Request-URI w przyszłości <br />(Zobacz <a href="http://www.w3.org/Protocols/rfc2616/rfc2616.html" target="_blank">RFC2616</a>)';
