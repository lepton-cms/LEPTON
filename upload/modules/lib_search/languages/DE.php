<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          lib_search
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$MOD_LIB_SEARCH = array(
	'- unknown -'         => '- unbekannt -',
	'- unknown date -'    => '- Datum unbekannt -',
	'- unknown time -'    => '- Zeit unbekannt -',
	'- unknown user -'    => '- unbekannter Benutzer -',
	'all words'           => 'alle Wörter',
	'any word'            => 'einzelne Wörter',
	'Content locked'      => 'gesperrter Inhalt',        
	'Error creating the directory <b>{{ directory }}</b>.'       => 'Das Verzeichnis <b>{{ directory }}</b> konnte nicht angelegt werden.',
	'exact match'         => 'genaue Wortfolge',
	'LEPTON Search Error' => 'Fehlermeldung der LEPTON Suche',
	'LEPTON Search Message'    => 'Mitteilung der LEPTON Suche',
	'Matching images'     => 'Gefundene Bilder',
	'No matches!'         => 'keine Treffer!',
	'only images'         => 'nur Bilder',
	'Search'              => 'Suche',
	'Search ...'          => 'Suche ...',
	'Submit'              => 'Start',
	'The LEPTON Search is disabled!'     => 'Die LEPTON Suchfunktion ist ausgeschaltet!',
	'This content is reserved for registered users.'        => 'Auf diesen Inhalt k&ouml;nnen nur registrierte Anwender zugreifen.'
);

