<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          lib_search
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$MOD_LIB_SEARCH = array(
    '- unknown -'         => '- unknown -',
    '- unknown date -'    => '- unknown date -',
    '- unknown time -'    => '- unknown time -',
    '- unknown user -'    => '- unknown user -',
    'all words'           => 'all words',
    'any word'            => 'any word',
    'Content locked'      => 'Content locked',        
    'Error creating the directory <b>{{ directory }}</b>.'       => 'Error creating the directory <b>{{ directory }}</b>.',
    'exact match'         => 'exact match',
    'LEPTON Search Error'      => 'LEPTON Search Error',
    'LEPTON Search Message'    => 'LEPTON Search Message',
    'Matching images'     => 'Matching images',
    'No matches!'         => 'No matches!',
    'only images'         => 'only images',
    'Search'              => 'Search',
    'Search ...'          => 'Search ...',
    'Submit'              => 'Submit',
    'The LEPTON Search is disabled!'     => 'The LEPTON Search is disabled!',
    'This content is reserved for registered users.'        => 'This content is reserved for registered users.'
);
