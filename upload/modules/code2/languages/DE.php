<?php

/**
 *  @module         code2
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2010-2025 Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
 
global $MOD_CODE2;

/**
 *	Deutsche Modulbeschreibung
 *
 */
$module_description = 'Dieses Modul erlaubt das ausführen von PHP Befehlen sowie HTML, Javascript und interne Kommentare.<br /><span style="color:#990000;">(Bitte begrenzen Sie den Zugriff auf vertrauenswürdige Personen!)</span>';

$MOD_CODE2 = array (
	'PHP'	=> "PHP",
	'HTML'	=> "HTML",
	'JAVASCRIPT' => "Javascript",
	'INTERNAL'	=> "Interner Kommentar",
	'ADMIN'	=> "Admin Kommentar",
	'SMART'	=> "kleine Box",
	'FULL'	=> "große Box",
	'MODE'	=> "Darstellungs-Modus",
	'TYPE'	=> "Art"
);
