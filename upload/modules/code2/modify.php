<?php

/**
 *  @module         code2
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2010-2025 Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$oCODE2 = code2::getInstance();

/**
 *	Get page content
 */
$content = array();
$database->execute_query(
	"SELECT * from `".TABLE_PREFIX."mod_code2` WHERE `section_id`=".$section_id,
	true,
	$content,
	false
);

$whatis = (int)$content['whatis'];

$mode = ($whatis >= 10) ? (int)($whatis / 10) : 0;
$whatis = $whatis % 10;

$groups = $admin->getValue('groups_id', 'string', 'session',',');

if (($whatis == 4) && (!in_array(1, $groups)))
{
	$content = $content['content'];
	echo '<div class="code2_admin">'.$content.'</div>';
} 
else 
{	
	$content = $content['content'];
	$whatis_types = array('PHP', 'HTML', 'Javascript', 'Internal');
	if (in_array(1, $groups)) 
	{
		$whatis_types[]="Admin";
	}
	$whatisarray = array();
    foreach ($whatis_types as $item)
	{
		$whatisarray[] = $oCODE2->language[strtoupper($item)];
	}
	
	$whatisselect = '';
    for ($i = 0; $i < count($whatisarray); $i++)
	{
   		$select = ($whatis == $i) ? " selected='selected'" : "";
   		$whatisselect .= '<option value="'.$i.'"'.$select.'>'.$whatisarray[$i].'</option>'."\n";
  	}
    
    $modes_names = array('smart', 'full');
    $modes = array();
    foreach($modes_names as $item) 
	{
		$modes[] = $oCODE2->language[strtoupper($item)];
	}
    $mode_options = "";
    $counter = 0;
    foreach($modes as $item) 
	{
    	$mode_options .= "<option value='".$counter."' ".(($counter==$mode)?" selected='selected'":"").">".$item."</option>";
		$counter++;
	}
	
	// Insert vars
	$data = array(
		"PAGE_ID"       => $page_id,
		"SECTION_ID"    => $section_id,
		"LEPTON_URL"    => LEPTON_URL,
		"CONTENT"       => $content,
		"WHATIS"        => $whatis,
		"WHATISSELECT"  => $whatisselect,
		"MODE"          => $mode_options,
		"MODE_"         => $mode,
		"LANGUAGE"      => LANGUAGE,
		"THEME_URL"     => THEME_URL,
		"MOD_CODE2"     => $oCODE2
	);

	if (class_exists('lib_codemirror'))
	{
		$data["codemirror_theme_select"] = (true === $oCODE2->codemirrorSupported)
			? lib_codemirror_interface::getInstance()->buildInterface(
					"code2",
					$section_id,
					"code2_codemirror_callBack",
					lib_codemirror_interface::LCM_DISPLAY_THEME_SELECT | lib_codemirror_interface::LCM_DISPLAY_MODE_SELECT | lib_codemirror_interface::LCM_DISPLAY_SAVE
				)
			: ""
			;
	}
   
    $oTWIG = lib_twig_box::getInstance();	
	$oTWIG->registerModule("code2");
	
	echo $oTWIG->render( 
		"@code2/modify.lte",
		$data
	);
}
