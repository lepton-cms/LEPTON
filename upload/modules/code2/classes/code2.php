<?php

declare(strict_types=1);

/**
 *  @module         code2
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2010-2025 Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

class code2 extends LEPTON_abstract
{
    /**
     *  Is CodeMirror installed?
     *  @type   boolean
     */
    public bool $codemirrorSupported = false;
    
    /**
     *  The own singleton instance.
     *  @type   object|null
     */
    static public $instance;
    
    /**
     *  Called by instance. All we have to do during the initialisation of this class.
     * 
     */
    public function initialize()
    {
        self::$instance->codemirrorSupported = class_exists("lib_codemirror", true);
        if (self::$instance->codemirrorSupported === true)
        {
            /**
             * Class exists - but we also have to look for the installed table inside the current database, too!
             */
            $database = LEPTON_database::getInstance();
            $allTables = $database->list_tables(TABLE_PREFIX);
            self::$instance->codemirrorSupported = in_array("mod_lib_codemirror", $allTables);
        }
    }
    
    /**
     * Handle php source content inside code2, type 'php' 
     *
     * @param string $path A full path to the file.
     * @param string $sourceContent The source without starting "<?php" start tag.
     *
     * @return void
     *
     */
    static function writePHPFile(string $path="", string $sourceContent = ""): void
    {
        $content = "<?php\n\n".$sourceContent."\n";
        file_put_contents($path, $content);
    }
}
