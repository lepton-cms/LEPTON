<?php

declare(strict_types=1);

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          lib_phpmailer
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */



require_once LEPTON_PATH."/modules/lib_phpmailer/library.php";

class lib_phpmailer extends PHPMailer\PHPMailer\PHPMailer
{

	/**
     * @var Singleton The reference to *Singleton* instance of this class
     */
    public static $instance;
	
	/**
	 *	Return the »internal« instance
	 *
	 */
	public static function getInstance()
    {
        if (null === static::$instance)
        {
            static::$instance = new static();
        }
        
        return static::$instance;
    }
}