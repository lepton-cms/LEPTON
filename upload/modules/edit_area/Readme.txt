Copyright 2008 Christophe Dolivet

Homepage:
https://www.cdolivet.com/editarea/

Complete documentation:
https://www.cdolivet.com/editarea/editarea/docs/

All licenses:
https://www.cdolivet.com/editarea/editarea/docs/license.html

Examples:
https://www.cdolivet.com/editarea/editarea/exemples/exemple_full.html