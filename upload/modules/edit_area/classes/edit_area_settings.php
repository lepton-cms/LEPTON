<?php

declare(strict_types=1);

/**
 * @module      edit_area
 * @version     see info.php of this module
 * @author      Christophe Dolivet (EditArea), Christian Sommer (wrapper), LEPTON Project
 * @copyright   2009-2010 Christian Sommer
 * @copyright   2010-2022 LEPTON Project
 * @license         GNU General Public License
 * @license terms   see info.php of this module
 * @platform        see info.php of this module
 *
 */ 

class edit_area_settings extends LEPTON_abstract implements LEPTON_wysiwyg_interface
{
    public $default_width = "100";
    public $default_height = "350";
    public $default_skin = "default";
    public $default_toolbar = "Default";
    // compatible to wysiwyg_settings
    public $toolbar = "";
    
    public $default_syntax = "Php";
    public $syntax_selection = true;
    public $allow_resize = 'both';
    public $allow_toggle = true;
    public $start_highlight = true;
    public $skins = array();
    public $toolbars = array();
    
    public static $instance;

    public function initialize() 
	{
        $this->skins = [
            'default' => 'default'
        ];

        $this->toolbars = [
            'Default' => 'search, fullscreen, |, undo, redo, |, select_font, syntax_selection, |, highlight, reset_highlight, |, help',
            'Simple' => 'fullscreen, |, undo, redo, |, help',
            //	This one is for experimental use only. Use this one for your own studies and development e.g. own plugins, icons, tools, etc.
            'Experimental' => 'search, go_to_line, fullscreen, |, undo, redo, |, select_font, syntax_selection, |, highlight, reset_highlight, |, help'
        ];
        
        // compatible to wysiwyg_settings
        $this->toolbar = $this->default_toolbar;
    }

    // interfaces
    //  [1]
    public function getHeight()
    {
        return $this->default_height;
    }

    //  [2]
    public function getWidth()
    {
        return $this->default_width;
    }

    //  [3]
    public function getToolbar()
    {
        return $this->toolbars[$this->default_toolbar];
    }

    //  [4]
    public function getSkin()
    {
        return $this->default_skin ?? "";
    }

}	

