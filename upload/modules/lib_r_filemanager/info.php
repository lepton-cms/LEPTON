<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released special license.
 * License can be seen in the info.php of this module.
 *
 * @module          lib Responsive Filemanager
 * @author          LEPTON Project, Alberto Peripolli (https://responsivefilemanager.com)
 * @copyright       2010-2025 LEPTON Project, Alberto Peripolli
 * @link            https://lepton-cms.org
 * @license         please see info.php of this module
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) {  
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$module_directory	= 'lib_r_filemanager';
$module_name		= 'Responsive Filemanager';
$module_function	= 'library';
$module_version		= '9.14.0.14';  // update from GitHub master, last commit 31.08.2023 -> https://github.com/trippo/ResponsiveFilemanager
$module_platform	= '7.x';
$module_delete		=  false;
$module_author		= 'Alberto Peripolli, LEPTON team';
$module_license		= 'special license, see <a href="' .LEPTON_URL .'/modules/lib_r_filemanager/license.txt" target="_blank">included license file</a>.';
$module_description	= 'Filemanager for use with LEPTON';
$module_home		= ' https://lepton-cms.org/';
$module_guid		= '071e6320-e081-4d50-a3a7-e84bd8080f2d';

