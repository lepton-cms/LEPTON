<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2025 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// Include admin wrapper script
$update_when_modified = true; // Tells script to update when this page was last updated
require(LEPTON_PATH.'/modules/admin.php');

$fields = [
    "group_id"  => ["type"  => "integer",       "default" => NULL ],
    "page_id"   => ["type"  => "integer",       "default" => NULL ],
    "title"     => ["type"  => "string_chars",  "default" => "" ],
    "active"    => ["type"  => "integer",       "default" => 0 ]
];

$all_values = LEPTON_request::getInstance()->testPostValues($fields);

//  Test for an invalid "group_id" or an invalid "page_id":
if( ( NULL === $all_values["group_id"] ) || ( NULL === $all_values["page_id"] ) )
{
    header("Location: ".ADMIN_URL."/pages/index.php");
	exit( 0 );
}

// We don't accept an empty title. 
if($all_values["title"] == "")
{
	$admin->print_error(
		$MESSAGE["GENERIC_FILL_IN_ALL"],
		LEPTON_URL."/modules/news/modify_group.php?page_id=".$all_values["page_id"]."&section_id=".$all_values["section_id"]."&group_id=".$all_values["group_id"]
	);
}

extract($all_values, EXTR_OVERWRITE);   // values from array $all_values are now available as vars, for example -> $page_id

$fields = array(
	'title' => $title,
	'active' => $active
);

$database->build_and_execute(
	'UPDATE',
	TABLE_PREFIX."mod_news_groups",
	$fields,
	"group_id = ".$group_id
);

// Check if the user uploaded an image or wants to delete one
if(isset($_FILES['image']['tmp_name']) && $_FILES['image']['tmp_name'] != '')
{
	// Get real filename and set new filename
	$filename = $_FILES['image']['name'];
	
	$aTempTerms = explode(".", $filename);
	$sMimeType = strtolower( array_pop($aTempTerms) );
	
	$new_filename = LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/groups/image'.$group_id.'.'.$sMimeType;
	
	$file_temp = explode('.',$filename);
	$file4 = strtolower(array_pop($file_temp));
	
	if(!in_array($file4, LEPTON_core::imageTypesAllowed()))
    {
		$admin->print_error($MESSAGE['GENERIC_FILE_TYPE'].'Not allowed filetype [1]', LEPTON_URL.'/modules/news/modify_group.php?page_id='.$page_id.'&section_id='.$section_id.'&group_id='.$group_id	);
	}
	
	// Make sure the target directory exists
	LEPTON_handle::register("make_thumb");
	LEPTON_core::make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/newspics');
	LEPTON_core::make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/groups');
	
	// Upload image
	move_uploaded_file($_FILES['image']['tmp_name'], $new_filename);
	
	// Check if we need to create a thumb
	$resize = $database->get_one("SELECT resize FROM ".TABLE_PREFIX."mod_news_settings WHERE section_id = ".$section_id );
	
	if($resize != 0)
    {
		// Resize the image
		$thumb_location = LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/groups/thumb'.$group_id.'.'.$sMimeType;
		if(make_thumb($new_filename, $thumb_location, $resize))
        {
			// Delete the current image and replace with the resized version
			unlink($new_filename);
			rename($thumb_location, $new_filename);
		}
	}
}
if(isset($_POST['delete_image']) && $_POST['delete_image'] != '')
{
	
	$aTempList = LEPTON_core::imageTypesAllowed();
	foreach($aTempList as &$sFileType)
	{
	    // Try unlinking image
	    $sTempFilePath = LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/groups/image'.$group_id.'.'.$sFileType;
	    
	    if(file_exists($sTempFilePath))
        {
		    unlink($sTempFilePath);
	    }
	}
}

$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
