<?php

declare(strict_types=1);

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         LEPTON Project
 *  @copyright      2010-2025 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */
 
 
class news_simplepagehead
{
	use LEPTON_singleton;
	
	public string $keywords = 'LEPTON, CMS, Project';
	
  
    public static function get_addon_infos ( $iPageId = PAGE_ID ):array
    {	
		$entries = [
		    'keywords' => "",
		    'title'     => "",
		    'description' => "",
			'tag' => ""	// example: "<link rel='alternate' type='application/rss+xml' title='Your Info - RSS-Feed' href='".LEPTON_URL."/modules/news/rss.php?page_id=1' />"
		];
		
		if( (defined("POST_ID")) && (is_numeric(POST_ID)))
		{
			$database = LEPTON_database::getInstance();
			$database->execute_query(
				"SELECT title, content_short FROM ".TABLE_PREFIX."mod_news_posts WHERE post_id = ".POST_ID,
				true,
				$entries,
				false
			);
			
			// load droplet engine and process
			LEPTON_handle::include_files ('/modules/droplets/droplets.php');
			foreach($entries as &$temp_entry)
			{
				evalDroplets($temp_entry);				
			}
			
			// return htmlspecial chars to tags and remove tags 
			$entries['keywords'] = strip_tags(html_entity_decode(self::getInstance()->keywords,ENT_HTML5));
			$entries['title'] = strip_tags(WEBSITE_TITLE.' - '.html_entity_decode($entries['title'],ENT_HTML5));
			$entries['description'] = strip_tags(html_entity_decode($entries['content_short'],ENT_HTML5));			
			
			// strip quotes and cut length
			$sEntryLength = 180;
			$sLength = $sEntryLength - 40;
			foreach ($entries as $key => &$temp_entry)
			{
				$sValue = str_replace('"', '', $temp_entry); 
				if (strlen($sValue) > $sEntryLength) 
				{
					if(preg_match('/.{0,'.$sEntryLength.'}(?:[.!?:,])/su', $sValue, $match)) 
					{
						$sValue = $match[0];
					}			
					if (strlen($sValue) > $sEntryLength) 
					{
						$pos = strpos($sValue, " ", $sLength);
						if ($pos > 0) 
						{
							$sValue = substr($sValue, 0,  $pos);
						}					
					}
				}
				
				$temp_entry = $sValue;
			}
		
			$entries['post_id'] = POST_ID;
			
			return $entries;
		}
		else
		{
			return $entries;						
		}
    }
	
}