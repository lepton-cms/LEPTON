<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2025 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

global $admin, $page_id, $section_id;

// Include admin wrapper script
$update_when_modified = true; // Tells script to update when this page was last updated
require LEPTON_PATH.'/modules/admin.php';



$aFields = [
    'commenting'        => ['type' => 'string', 'default' => 'none'],
    'posts_per_page'    => ['type' => 'integer', 'default' => 5],
    'use_captcha'       => ['type' => 'integer', 'default' => 0],
    'resize'            => ['type' => 'integer', 'default' => 0]
];

$all_values = LEPTON_request::getInstance()->testPostValues($aFields);

$database->build_and_execute(
	'update',
	TABLE_PREFIX."mod_news_settings",
	$all_values,
	"section_id = ".$section_id
);

if (ENABLED_CAPTCHA == 0)
{
	$database->simple_query("UPDATE ".TABLE_PREFIX."mod_news_settings set use_captcha = 0 WHERE section_id = ".$section_id);
}

// Print admin footer
$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
