<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2025 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// Check if there is a post id
if(!isset($_GET['post_id']) OR !is_numeric($_GET['post_id'])
    OR !isset($_GET['section_id']) OR !is_numeric($_GET['section_id']))
{
	header("Location: ".LEPTON_URL.PAGES_DIRECTORY."");
	exit( 0 );
}

$post_id_temp = intval($_GET['post_id']);

// Query post for page id
$result = [];
$database->execute_query(
    "SELECT post_id,title,section_id,page_id FROM ".TABLE_PREFIX."mod_news_posts WHERE post_id = ".$post_id_temp,
    true,
    $result,
    false
);

if(empty($result))
{
    header("Location: ".LEPTON_URL.PAGES_DIRECTORY."");
	exit( 0 );
}
else
{
	$page_id = $result['page_id'];
	$section_id = $result['section_id'];
	$post_id = $result['post_id'];
	$post_title = $result['title'];
	define('SECTION_ID', $section_id);
	define('POST_ID', $post_id);
	define('POST_TITLE', $post_title);

if (ENABLED_CAPTCHA == 0)
{
	$database->simple_query("UPDATE ".TABLE_PREFIX."mod_news_settings set use_captcha = 0 WHERE section_id = ".$section_id);
}

	// don't allow commenting if its disabled, or if post or group is inactive
	$current_time = time();
	$table_posts = TABLE_PREFIX."mod_news_posts";
	$table_groups = TABLE_PREFIX."mod_news_groups";
	
	$aComments = [];
	$database->execute_query("
		SELECT p.post_id
		FROM $table_posts AS p LEFT OUTER JOIN $table_groups AS g ON p.group_id = g.group_id
		WHERE p.post_id='$post_id' AND p.commenting != 'none' AND p.active = '1' AND ( g.active IS NULL OR g.active = '1' )
		AND (p.published_when = '0' OR p.published_when <= $current_time) AND (p.published_until = 0 OR p.published_until >= $current_time)
	    ",
	    true,
	    $aComments,
	    true
	);
	
	if(empty($aComments))
    {
		header("Location: ".LEPTON_URL.PAGES_DIRECTORY."");
	    exit( 0 );
	}

	// don't allow commenting if ASP enabled and user doesn't comes from the right view.php
	if(ENABLED_ASP && (!isset($_SESSION['comes_from_view']) || $_SESSION['comes_from_view']!= POST_ID))
    {
		header("Location: ".LEPTON_URL.PAGES_DIRECTORY."");
	    exit( 0 );
	}

	// Get page details
	$page = [];
	$database->execute_query(
		"SELECT parent,page_title,menu_title,keywords,description,visibility FROM ".TABLE_PREFIX."pages WHERE page_id = ".$page_id,
		true,
		$page,
		false
	);
	if(empty($page))
    {
		header("Location: ".LEPTON_URL.PAGES_DIRECTORY."");
	    exit( 0 );
	}
    else
    {
		// Required page details
		define('PAGE_CONTENT', LEPTON_PATH.'/modules/news/comment_page.php');
		// Include index (wrapper) file
		require(LEPTON_PATH.'/index.php');
	}
}
