<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2025 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// get time
$current_time = time();

if(ENABLED_ASP == 1)
{
	if (!captcha_control_services::insiteASPTimeRange($current_time))
	{
			header("Location: ".LEPTON_URL);
			exit(0);		
	}	
	if(
		!isset($_POST['comment_'.date('W')]) 
		|| $_POST['comment_'.date('W')] == ''
		|| !isset($_SESSION['comes_from_view'])// user doesn't come from view.php
		|| !isset($_SESSION['comes_from_view_time'])
		|| !isset($_SESSION['submitted_when'])
		|| !isset($_POST['submitted_when']) // faked form
		
		|| $_SESSION['submitted_when'] != $_POST['submitted_when'] // faked form
		|| $_SESSION['submitted_when'] < $current_time - 43200 // form older than 12h	
		|| ($_POST['email'] OR $_POST['url'] OR $_POST['homepage'] OR $_POST['comment'])  /* honeypot-fields */	
			
	 )
	{
		header("Location: ".LEPTON_URL);
		exit(0);	
	}
	
	$_POST['comment'] = $_POST['comment_'.date('W')];
	$_POST['section_id'] = $_GET['section_id'];
	$_POST['page_id'] = $_GET['page_id'];
	$_POST['post_id'] = $_GET['post_id'];
	$_POST['commented_when'] = $_POST['submitted_when'];	
}
else
{
	// check relevant $_POST values
	$_POST['commented_when'] = $current_time;
	$_POST['section_id'] = $_GET['section_id'];
	$_POST['page_id'] = $_GET['page_id'];
	$_POST['post_id'] = $_GET['post_id'];	
}

$oREQUEST = LEPTON_request::getInstance();

$all_names = array (
	'section_id'	=> array ('type' => 'integer', 'default' => "-1"),
	'page_id'		=> array ('type' => 'integer', 'default' => "-1"),
	'post_id'		=> array ('type' => 'integer', 'default' => "-1"),
	'commented_when'	=> array ('type' => 'integer', 'default' => "-1"),
	'commented_by'	=> array ('type' => 'integer', 'default' => "-1"),
	'title'			=> array ('type' => 'string_clean', 'default' => ""),
	'comment' 		=> array ('type' => 'string_clean', 'default' => "")
);

$all_values = $oREQUEST->testPostValues($all_names);

extract($all_values, EXTR_OVERWRITE);   // values from array $all_values are now available as vars, for example -> $page_id

// if ENABLED_CAPTCHA == 0 $use_news_captcha is also = 0, if ENABLED_CAPTCHA == 1 $use_news_captcha is 0 or 1
$use_news_captcha = $database->get_one("SELECT use_captcha FROM ".TABLE_PREFIX."mod_news_settings WHERE section_id = ".$section_id);


// Check if we should show the form or add a comment
if($use_news_captcha == 1)
{
	
	// Check captcha
	if(isset($_SESSION['captcha_retry_news']))
	{
	  unset($_SESSION['captcha_retry_news']);
	}

	if(file_exists(LEPTON_PATH."/modules/news/recaptcha.php")) 
	{
		require_once LEPTON_PATH."/modules/news/recaptcha.php";
		if(isset($_POST['g-recaptcha-response']))
		{
			$captcha_result = news_recaptcha::test_captcha( $_POST['g-recaptcha-response'] );

			if( $captcha_result['success'] == false )
			{
				$_SESSION['captcha_error'] = $MESSAGE['MOD_FORM_INCORRECT_CAPTCHA'];
				$_SESSION['comment_title'] = $title;
				$_SESSION['comment_body'] = $comment;
				header("Location: ".LEPTON_URL."/modules/news/comment.php?post_id=".$post_id."&section_id=".$section_id );
				exit( 0 );
			}
		}
	} 
	else 
	{
		if(isset($_POST['captcha']) AND $_POST['captcha'] != '')
		{
			$sTempKey = "captcha".( $_SESSION['captcha_id'] ?? 0 );
			
			// Check for a mismatch
			if(!isset($_POST['captcha']) || !isset($_SESSION[$sTempKey]) || ($_POST['captcha'] != $_SESSION[$sTempKey] ) )
			{
				$_SESSION['captcha_error'] = $MESSAGE['MOD_FORM_INCORRECT_CAPTCHA'];
				$_SESSION['comment_title'] = $title;
				$_SESSION['comment_body'] = $comment;
				header("Location: ".LEPTON_URL."/modules/news/comment.php?post_id=".$post_id."&section_id=".$section_id."" );
				exit( 0 );
			}
		}
		else
		{
			$_SESSION['captcha_error'] = $MESSAGE['MOD_FORM_INCORRECT_CAPTCHA'];
			$_SESSION['comment_title'] = $title;
			$_SESSION['comment_body'] = $comment;
			header("Location: ".LEPTON_URL."/modules/news/comment.php?post_id=".$post_id."&section_id=".$section_id."" );
			exit( 0 );
		}
	}

	unset($_SESSION['captcha']); 
	unset($_SESSION['comes_from_view']);
	unset($_SESSION['comes_from_view_time']);
	unset($_SESSION['submitted_when']);

}

$oCore = LEPTON_core::getInstance();

// Insert the comment into db
if($oCore->is_authenticated() == true)
{
	$all_values['commented_by'] = $oCore->getValue('user_id', 'integer', 'session');
}

// save checked values 
$database->build_and_execute(
	'INSERT',
	TABLE_PREFIX."mod_news_comments",
	$all_values
);

// Get page link
$page = $database->get_one("SELECT link FROM ".TABLE_PREFIX."mod_news_posts WHERE post_id = ".$post_id );
header('Location: '.$oCore->buildPageLink($page)."?post_id=".$post_id );
exit();
