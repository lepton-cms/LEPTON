<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2025 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// Check that GET values have been supplied
if(isset($_GET['page_id']) && is_numeric($_GET['page_id'])) 
{
	$page_id = intval($_GET['page_id']);
} 
else 
{
	header('Location: /');
	exit(0);
}

if (isset($_GET['group_id']) && is_numeric($_GET['group_id'])) 
{
	$group_id = intval($_GET['group_id']);
} 
else 
{
	$group_id = -1;	// Keep in mind, that $group_id could be 0 (no group)
}

define('GROUP_ID', $group_id);

$oLEPTON = LEPTON_frontend::getInstance();
$oLEPTON->page_id = $page_id;
$oLEPTON->get_page_details();
$oLEPTON->get_website_settings();

// Pre-check: any news on this page?
$aAllModules = [];
$database->execute_query(
    "SELECT module FROM ".TABLE_PREFIX."sections WHERE page_id = ".$page_id." AND module = 'news' ",
    true,
    $aAllModules,
    true
);

if( empty($aAllModules))
{
    die("No news traceable on this page!");
}

$charset=DEFAULT_CHARSET;

ob_start();

// Sending XML header
header("Content-type: text/xml; charset=$charset" );

// Header info
// Required by CSS 2.0
echo '<?xml version="1.0" encoding="'.$charset.'"?>';
?> 
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<title><?php echo PAGE_TITLE; ?></title>
		<link>http://<?php echo $_SERVER['SERVER_NAME']; ?></link>
		<description> <?php echo $oLEPTON->page["description"]; ?></description>
<?php
	echo "<atom:link href='".LEPTON_URL."/modules/news/rss.php?page_id=".$page_id."' rel='alternate' type='application/rss+xml' />";
?>		
		<language><?php echo strtolower(DEFAULT_LANGUAGE); ?></language>
		<copyright><?php $thedate = date('Y'); echo "Copyright ".$thedate." by ".LEPTON_URL." , all rights reserved"; ?></copyright>
		<managingEditor><?php echo SERVER_EMAIL . " (" . MAILER_DEFAULT_SENDERNAME . ")"; ?></managingEditor>
		<webMaster><?php echo SERVER_EMAIL . " (" . MAILER_DEFAULT_SENDERNAME . ")"; ?></webMaster>
		<category><?php echo WEBSITE_TITLE; ?></category>
		<generator>LEPTON CMS, https://lepton-cms.org</generator>
<?php

// Get news items from database
$current_time = TIME();
$time_check_str = "(published_when = '0' OR published_when <= ".$current_time.") AND (published_until = 0 OR published_until >= ".$current_time.")";

//	Query
if ( $group_id > -1 ) 
{
	$query = "SELECT * FROM ".TABLE_PREFIX."mod_news_posts WHERE group_id=".$group_id." AND page_id = ".$page_id." AND active = 1 AND ".$time_check_str." ORDER BY posted_when DESC";
} 
else 
{
	$query = "SELECT * FROM ".TABLE_PREFIX."mod_news_posts WHERE page_id=".$page_id." AND active = 1 AND ".$time_check_str." ORDER BY posted_when DESC";	
}
$result = [];
$database->execute_query(
    $query,
    true,
    $result,
    true
);

// Generating the news items
foreach($result as $item)
{ 
	LEPTON_handle::restoreSpecialChars( $item['content_short'] );
?>
		<item>
			<title><![CDATA[<?php echo stripslashes($item["title"]); ?>]]></title>
			<description><![CDATA[<?php echo stripslashes($item["content_short"]); ?>]]></description>
			<guid><?php echo LEPTON_URL.PAGES_DIRECTORY.$item["link"].PAGE_EXTENSION; ?></guid>
			<link><?php echo LEPTON_URL.PAGES_DIRECTORY.$item["link"].PAGE_EXTENSION; ?></link>
			<pubDate><?php echo date(DATE_RSS,$item["published_when"]); ?></pubDate>
		</item>
<?php } ?>
	</channel>
</rss>

<?php  

$output = ob_get_clean();

$oLEPTON->preprocess($output);

// Load Droplet engine and process
LEPTON_handle::include_files ('/modules/droplets/droplets.php');
evalDroplets($output);

echo $output;
