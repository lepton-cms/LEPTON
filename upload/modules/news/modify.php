<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2025 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


$oNEWS = news::getInstance();

/**
 *	JavaScript
 */
$js_delete_msg = (array_key_exists( 'CONFIRM_DELETE', $oNEWS->language))
	? $oNEWS->language['CONFIRM_DELETE']
	: $TEXT['ARE_YOU_SURE']
	;

/**
 *	Check if more posts exist than posts per page defines
 */
if(isset($_GET['p']) AND is_numeric($_GET['p']) AND $_GET['p'] >= 0) {
	$position = $_GET['p'];
} else {
	$position = 0;
}

/**
 *	Get settings
 */
if (ENABLED_CAPTCHA == 0)
{
	$database->simple_query("UPDATE ".TABLE_PREFIX."mod_news_settings set use_captcha = 0 WHERE section_id = ".$section_id);
}
 
$settings_posts = $database->get_one("SELECT posts_per_page FROM ".TABLE_PREFIX."mod_news_settings WHERE section_id = ".$section_id);

$setting_posts_per_page = isset($settings_posts)
	? $settings_posts
	: ''
	;


//	Timebased activation or deactivation of the news posts. Keep in mind that the database class will return an object-instance each time a query.
$current_time = time();
$database->simple_query("UPDATE ".TABLE_PREFIX."mod_news_posts SET active = 0 WHERE published_until > 0 AND published_until <= ".$current_time);
$database->simple_query("UPDATE ".TABLE_PREFIX."mod_news_posts SET active = 1 WHERE published_when > 0 AND published_when <= ".$current_time." AND published_until > 0 AND published_until >= ".$current_time);

$database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_news_posts WHERE link = '' ");

// Get total number of posts
$count_posts = [];
$database->execute_query(
	"SELECT post_id FROM ".TABLE_PREFIX."mod_news_posts WHERE section_id = ".$section_id,
	true,
	$count_posts,
	true
);

$total_num = count($count_posts);

// Work-out if we need to add limit code to sql
$limit_sql = ($setting_posts_per_page != 0)
	? " LIMIT ".$position.",".$setting_posts_per_page
	: ""
	;
	
// Query posts (for this page)
$all_posts = [];
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."mod_news_posts WHERE section_id = ".$section_id." AND history_post_id = -1 ORDER BY position DESC".$limit_sql,
	true,
	$all_posts,
	true
);
$num_posts = count($all_posts); 
	
// Create previous and next links
if($setting_posts_per_page != 0) 
{
	// Patch, as the JS-redirect makes it nessesary to look for botth "leptoken" AND "amp;leptoken"
	if (array_key_exists('amp;leptoken', $_GET) ) $_GET['leptoken'] = $_GET['amp;leptoken'];
	$leptoken_add = (isset($_GET['leptoken']) ? "&amp;leptoken=".$_GET['leptoken'] : "");
	
	if (strlen( $leptoken_add) == 0) 
	{
		if (isset($_POST['leptoken']) ) $leptoken_add =  "&amp;leptoken=".$_POST['leptoken'];
	}
	$sPathPrefix = ADMIN_URL."/pages/modify.php";
	
	if($position > 0) 
	{
		$pl_prepend = '<a href="'.$sPathPrefix.'?p='.($position-$setting_posts_per_page).'&amp;page_id='.$page_id.$leptoken_add.'">&lt;&lt; ';
		$pl_append = '</a>';
		$previous_link = $pl_prepend.$TEXT['PREVIOUS'].$pl_append;
		$previous_page_link = $pl_prepend.$TEXT['PREVIOUS_PAGE'].$pl_append;
	} 
	else 
	{
		$previous_link = '';
		$previous_page_link = '';
	}
	
	if($position+$setting_posts_per_page >= $total_num) 
	{
		$next_link = '';
		$next_page_link = '';
	} 
	else 
	{
		$nl_prepend = '<a href="'.$sPathPrefix.'?p='.($position+$setting_posts_per_page).'&amp;page_id='.$page_id.$leptoken_add.'"> ';
		$nl_append = ' &gt;&gt;</a>';
		$next_link = $nl_prepend.$TEXT['NEXT'].$nl_append;
		$next_page_link = $nl_prepend.$TEXT['NEXT_PAGE'].$nl_append;
	}
	
	if($position+$setting_posts_per_page > $total_num) 
	{
		$num_of = $position+$num_posts;
	} 
	else 
	{
		$num_of = $position+$setting_posts_per_page;
	}
	
	$out_of = ($position+1).'-'.$num_of.' '.strtolower($TEXT['OUT_OF']).' '.$total_num;
	$of = ($position+1).'-'.$num_of.' '.strtolower($TEXT['OF']).' '.$total_num;
	$display_previous_next_links = '';
} 
else 
{
	$display_previous_next_links = 'none';
}

//delete groups without title
$database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_news_groups WHERE title ='' ");

// Groups
$all_groups = [];
$query_groups = $database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."mod_news_groups WHERE section_id = ".$section_id." ORDER BY position ASC",
	true,
	$all_groups
);
$num_groups = count($all_groups);

// get all group-titles ...
$group_titles = array( '0' => $TEXT['NONE'] );
foreach($all_groups as &$ref) 
{
	$group_titles[ $ref['group_id'] ] = $ref['title'];
}

// Counting the comments
$counted_comments = array( '0' => 0 );
$post_ids = [];

foreach($all_posts as &$ref)
{
	$counted_comments[ $ref['post_id'] ] = 0;
	$post_ids[] = $ref['post_id'];
	
    news::cleanUpString( $ref['title'] );
}

// Patch to avoid unexpected behavior if there is no post_id avaible - News are empty.
if (count($post_ids) == 0) $post_ids[] = -99;

$all_comments = [];
$database->execute_query(
	"SELECT post_id FROM ".TABLE_PREFIX."mod_news_comments WHERE post_id in (".implode(",", $post_ids).")",
	true,
	$all_comments,
	true
);

foreach($all_comments as &$ref) $counted_comments[ $ref['post_id'] ]++;

// Get the correct 'icon'
$current_time = time();
foreach($all_posts as &$ref) {
	$start = $ref['published_when'];
	$end = $ref['published_until'];
	$icon = '';
	if($start<=$current_time && $end==0)
		$icon = LEPTON_URL.'/modules/lib_lepton/backend_images/clock_16.png';
	elseif(($start<=$current_time || $start==0) && $end>=$current_time)
		$icon = LEPTON_URL.'/modules/lib_lepton/backend_images/clock_16.png';
	else
		$icon = LEPTON_URL.'/modules/lib_lepton/backend_images/clock_red_16.png';

	$ref['icon'] = $icon;
}

$js_delete_msg = (array_key_exists( 'CONFIRM_DELETE', $oNEWS->language))
	? $oNEWS->language['CONFIRM_DELETE']
	: $TEXT['ARE_YOU_SURE']
	;
	
foreach($all_posts as &$ref) {
	$ref['js_delete_msg'] = sprintf($js_delete_msg, $ref['title']);
}

$form_values = array(
	'TEXT'			=> $TEXT,
	'LEPTON_URL'	=> LEPTON_URL,
	'THEME_URL'		=> THEME_URL,
	'page_id'		=> $page_id,
	'section_id'	=> $section_id,
	'num_posts'		=> $num_posts,
	'posts'			=> $all_posts,
	'counted_comments' => $counted_comments,
	'display_previous_next_links' => $display_previous_next_links,
	'NEXT_PAGE_LINK'		=> isset($next_page_link) ? $next_page_link : "",
	'PREVIOUS_PAGE_LINK'	=> isset($previous_page_link) ? $previous_page_link : "",
	'OF'					=> isset($of) ? $of : "",
	'num_groups'	=> $num_groups,
	'groups'		=> $all_groups,
	'group_titles'	=> $group_titles,
	'row'	=> 'a',
	'leptoken'  => get_leptoken()
);

if(!isset($_SESSION['last_edit_section']))
{
    $_SESSION['last_edit_section'] = $section_id;
}

$oTWIG = lib_twig_box::getInstance();	
$oTWIG->registerModule("news");

echo $oTWIG->render(
	'@news/modify.lte',
	$form_values
);
