<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2025 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// Get id
if(!isset($_POST['comment_id']) || !isset($_POST['post_id']))
{
	header("Location: ".ADMIN_URL."/pages/index.php");
	exit( 0 );
}

$update_when_modified = true; // Tells script to update when this page was last updated
require(LEPTON_PATH.'/modules/admin.php');

$oREQUEST = LEPTON_request::getInstance();
$all_names = array (
	'section_id'	=> array ('type' => 'integer', 'default' => "-1"),
	'page_id'		=> array ('type' => 'integer', 'default' => "-1"),
	'post_id'		=> array ('type' => 'integer', 'default' => "-1"),
	'comment_id'	=> array ('type' => 'integer', 'default' => "-1"),
	'title'			=> array ('type' => 'string_clean', 'default' => ""),
	'comment' 		=> array ('type' => 'string_clean', 'default' => "")
);

$all_values = $oREQUEST->testPostValues($all_names);
extract($all_values, EXTR_OVERWRITE);   // values from array $all_values are now available as vars, for example -> $page_id


$database->build_and_execute(
	'UPDATE',
	TABLE_PREFIX."mod_news_comments",
	$all_values,
	"comment_id = ".$comment_id
);

if (ENABLED_CAPTCHA == 0)
{
	$database->simple_query("UPDATE ".TABLE_PREFIX."mod_news_settings set use_captcha = 0 WHERE section_id = ".$section_id);
}

$admin->print_success($TEXT['SUCCESS'], LEPTON_URL.'/modules/news/modify_post.php?page_id='.$page_id.'&section_id='.$section_id.'&post_id='.$post_id );
