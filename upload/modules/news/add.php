<?php

/**
 *  @module         news
 *  @version        see info.php of this module
 *  @author         Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos), LEPTON Project
 *  @copyright      2004-2010 Ryan Djurovich, Rob Smith, Dietrich Roland Pehlke, Christian M. Stefan (Stefek), Jurgen Nijhuis (Argos) 
 *  @copyright      2010-2025 LEPTON Project 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 * 
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


global $database, $page_id, $section_id;

$fields = array(
	'page_id'		=> $page_id,
	'section_id'	=> $section_id,
	'commenting'	=> 'none',
	'use_captcha'	=> 0,
	'commenting' 	=> ''
);

$database->build_and_execute(
	'INSERT',
	TABLE_PREFIX."mod_news_settings",
	$fields
);

//  comes from install file -> L* 7.3.0
//  Make news post access files dir
LEPTON_core::make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/newspics'); // create directory for images
	
if(LEPTON_core::make_dir(LEPTON_PATH.PAGES_DIRECTORY.'/posts')) 
{
	// Add a index.php file to prevent directory spoofing
	copy(
		LEPTON_PATH.'/modules/news/index.php',
		LEPTON_PATH.PAGES_DIRECTORY.'/posts/index.php'
	);

    //  Copy the index.php also in the newspics folder inside the media-directory. 
	copy(
		LEPTON_PATH.'/modules/news/index.php',
		LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/index.php'
	);
}
