<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wrapper
 * @author          WebsiteBaker Project
 * @author          LEPTON Project
 * @copyright       2004-2010 WebsiteBaker Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

// Update the mod_wrapper table with the contents
if(isset($_POST['url'])) 
{
	$oREQUEST = LEPTON_request::getInstance();	
	
	$all_names = array (
		'url'	=> array ('type' => 'string_chars', 'default' => ""),
		'height'=> array ('type' => 'integer', 'default' => 400)
	);		

	$all_values = $oREQUEST->testPostValues($all_names);	
	
	$table = TABLE_PREFIX."mod_wrapper";
	$database->build_and_execute(
		'UPDATE', 
		$table, 
		$all_values,
		"section_id = ".$section_id
	);
}

$admin->print_success($MESSAGE['PAGES_SAVED'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);

