<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$module_directory	= 'wysiwyg';
$module_name		= 'WYSIWYG';
$module_function	= 'page';
$module_version		= '4.0.4';
$module_platform	= '7.x';
$module_delete		=  false;
$module_author		= 'LEPTON project, thanks to Thorn for the idea';
$module_license		= 'GNU General Public License';
$module_license_terms	= '-';
$module_description	= 'This module allows you to edit the contents of a page using a graphical editor, including draft and history';
$module_guid		= 'DA07DFA3-7592-4781-89C2-D549DD77B017';
