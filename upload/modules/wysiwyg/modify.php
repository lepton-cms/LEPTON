<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$section_id = ($section_id ?? LEPTON_core::getGlobal("section_id"));

// add own module class
$oWYSIWYG = wysiwyg::getInstance();

if(isset($_POST['show_history']) && $_POST['show_history'] > 0)
{
	$oWYSIWYG->display_history($_POST['show_history']);
}
elseif(isset($_POST['delete_version']) && $_POST['delete_version'] > 0)
{
	$oWYSIWYG->delete_version($_POST['delete_version']);
}
elseif(isset($_POST['get_working_copy']) && $_POST['get_working_copy'] > 0)
{
	$oWYSIWYG->get_working_copy($_POST['get_working_copy']);
}
elseif(isset($_POST['delete_wc']) && $_POST['delete_wc'] > 0)
{
	$oWYSIWYG->delete_working_copy($_POST['delete_wc']);
}
else
{
	/**
	 *	Get section content
	 *
	 */
	$data = []; 
	if(isset($_POST['restore_wc']))
	{
		$section_id = intval($_POST['restore_wc']);
		$database->execute_query(
			"SELECT * FROM `".TABLE_PREFIX."mod_wysiwyg` WHERE `section_id` = ".$section_id,
			true,
			$data,
			false
		);	
		$content = $data['working_content'];
		$comment = $data['working_content_comment'];
	}
	elseif(isset($_POST['restore_version']))
	{
		$version_id = intval($_POST['restore_version']);
		$database->execute_query(
			"SELECT * FROM `".TABLE_PREFIX."mod_wysiwyg_history` WHERE `id` = ".$version_id,
			true,
			$data,
			false
		);	
		$content = $data['content'];
		$comment = $data['comment'];
	}	 
	else
	{
		$database->execute_query(
			"SELECT `content` FROM `".TABLE_PREFIX."mod_wysiwyg` WHERE `section_id`= ".$section_id,
			true,
			$data,
			false
		);
		
		$content = ($data['content'] ?? "Error: no entry found for this section!");
		$comment = '';		
	}


	/**
	 *	Add a \ before the "$" char.
	 */
	$content = str_replace("\$", "\\\$", $content);



	if (isset($preview) && ($preview == true) )
	{
		return false;
	}

	/**
	 *  Get wysiwyg-editor content, incl. js.
	 */
	ob_start();
	show_wysiwyg_editor(
		"content".$section_id,
		"content".$section_id,
		$content,
		null,
		null,
		true
	);
	$wysiwyg_editor_content = ob_get_clean();

	// get current section
	$current_section = [];
	$database->execute_query(
		"SELECT * FROM `".TABLE_PREFIX."mod_wysiwyg` WHERE `section_id`= ".$section_id,
		true,
		$current_section,
		false
	);

	// get all histories of current section
	$all_histories = [];
	$database->execute_query(
		"SELECT * FROM `".TABLE_PREFIX."mod_wysiwyg_history` WHERE `section_id`= ".$section_id." order by id",
		true,
		$all_histories,
		true
	);

	// get current id
	$id = $database->get_one("SELECT MIN(id) FROM `".TABLE_PREFIX."mod_wysiwyg_history` WHERE `section_id`= ".$section_id) ?? 0;
	// get current values
	$current_history = [];
	$database->execute_query(
		"SELECT * FROM `".TABLE_PREFIX."mod_wysiwyg_history` WHERE `id`= ".$id,
		true,
		$current_history,
		false
	);

	//echo(LEPTON_tools::display($_SESSION,'pre','ui blue message'));
	/**
	 *  Collect the values
	 */
	$form_values = [
		'oWG'		=> $oWYSIWYG,
		'page_id'	=> $page_id,
		'section_id'=> $section_id,
		'comment'	=> $comment,
		'wysiwyg_history'=> WYSIWYG_HISTORY,
		'max_wysiwyg_history'=> MAX_WYSIWYG_HISTORY,
		'current_history'=> $current_history,
		'all_histories'	 => $all_histories,
		'current_section'=> $current_section,
		'current_user_id'=> $_SESSION['USER_ID'],
		'wysiwyg_editor' => $wysiwyg_editor_content
	];

	// Get instance of the template engine and render the output
	$oTWIG = lib_twig_box::getInstance();
	$oTWIG->registerModule("wysiwyg");

	echo $oTWIG->render( 
		"@wysiwyg/modify.lte", // template-filename
		$form_values	//	template-data
	);
}
