<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// Create table
$table_fields="
	`section_id`        INT NOT NULL DEFAULT 0,
	`page_id`           INT NOT NULL DEFAULT 0,
	`last_version`      INT NOT NULL DEFAULT 0,
	`hist_autosave`     INT NOT NULL DEFAULT 0,
	`max_history`       INT NOT NULL DEFAULT 0,
	`use_workingcopy`   INT NOT NULL DEFAULT 0,
	`user_id`           INT NOT NULL DEFAULT -1 COMMENT 'User who last saved the content',
	`user_id_working`   INT NOT NULL DEFAULT -1 COMMENT 'User who last saved the working copy',
	`content`           LONGTEXT NOT NULL,
	`content_comment`   TEXT NOT NULL,
	`text`              LONGTEXT NOT NULL,
	`working_content`   LONGTEXT NOT NULL,
	`working_content_comment` TEXT NOT NULL,
	PRIMARY KEY (`section_id`)
";
LEPTON_handle::install_table('mod_wysiwyg', $table_fields);

$table_fields="
		`id`            INT NOT NULL AUTO_INCREMENT,
		`section_id`    INT NOT NULL DEFAULT 0,
		`version`       INT NOT NULL DEFAULT 0,
		`autosaved`     INT NOT NULL DEFAULT 0,
		`date`          INT NOT NULL DEFAULT 0,
		`user_id`       INT NOT NULL DEFAULT -1 COMMENT 'User who last saved this content',
		`user_id_hist`  INT NOT NULL DEFAULT -1 COMMENT 'User who pushed this content to history',
		`comment`       TEXT NOT NULL,
		`content`       LONGTEXT NOT NULL,
		`text`          LONGTEXT NOT NULL,
		PRIMARY KEY (`id`),
		INDEX (`section_id`)
";
LEPTON_handle::install_table('mod_wysiwyg_history', $table_fields);

$aTestSearchEntries = [];
$database->execute_query(
    "SELECT * FROM `".TABLE_PREFIX."search`  WHERE `value` = 'wysiwyg' and name = 'module' ",
    true,
    $aTestSearchEntries,
    true
);

if( empty($aTestSearchEntries) )
{
    // Insert info into the search table
    // Module query info
    $field_info = [];
    $field_info['page_id'] = 'page_id';
    $field_info['title'] = 'page_title';
    $field_info['link'] = 'link';
    $field_info['description'] = 'description';
    $field_info['modified_when'] = 'modified_when';
    $field_info['modified_by'] = 'modified_by';
    $field_info = serialize($field_info);
		
	// Query start
	$query_start_code = "SELECT [TP]pages.page_id, [TP]pages.page_title,	[TP]pages.link, [TP]pages.description, [TP]pages.modified_when, [TP]pages.modified_by	FROM [TP]mod_wysiwyg, [TP]pages WHERE ";
	$query_body_code = " [TP]pages.page_id = [TP]mod_wysiwyg.page_id AND [TP]mod_wysiwyg.text [O] \'[W][STRING][W]\' AND [TP]pages.searching = \'1\'";

	$field_values="
        (NULL,'module', 'wysiwyg', '".$field_info."'),	
        (NULL, 'query_start', '".$query_start_code."', 'wysiwyg'),
        (NULL,'query_body', '".$query_body_code."', 'wysiwyg'),
        (NULL,'query_end', '', 'wysiwyg')
	";

	LEPTON_handle::insert_values('search', $field_values);
}
