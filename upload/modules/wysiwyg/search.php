<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

function wysiwyg_search($func_vars) 
{
	extract($func_vars, EXTR_PREFIX_ALL, 'func');
	static $search_sql = false;
	
	if(function_exists('search_make_sql_part')) 
	{
		if($search_sql === false)
			$search_sql = search_make_sql_part($func_search_url_array, $func_search_match, array('`content`'));
	} 
	else 
	{
		$search_sql = '1=1';
	}  
	
	// how many lines of excerpt we want to have at most
	$max_excerpt_num = $func_default_max_excerpt;
	$divider = ".";
	$result = false;
	
	// we have to get 'content' instead of 'text', because strip_tags() dosen't remove scripting well, scripting will be removed later on automatically
	$content = $func_database->get_one("SELECT `content` FROM `".TABLE_PREFIX."mod_wysiwyg` WHERE `section_id` = ".$func_section_id );

	if($content != "") 
	{
		// using HTMLPurifier in WYSIWYG-sections
		$content = str_replace(["&lt;", "&gt;"],["<", ">"], $content);
		
		$mod_vars = array(
			'page_link' 		=> $func_page_link,
			'page_link_target' 	=> '#'.SEC_ANCHOR.$func_section_id,			
			'page_title' 		=> $func_page_title,
			'page_description' 	=> $func_page_description,
			'page_modified_when'=> $func_page_modified_when,
			'page_modified_by' 	=> $func_page_modified_by,
			'text' 				=> $content.$divider,
			'max_excerpt_num' 	=> $max_excerpt_num
		);
		
		if(print_excerpt2($mod_vars, $func_vars)) 
		{
			$result = true;
		}
	}
	return $result;
}

