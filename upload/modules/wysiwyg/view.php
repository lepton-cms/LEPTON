<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

if( (true === wysiwyg_preview::$iUsePreview) && ($section_id == wysiwyg_preview::$iPreviewID) )
{
    $content = wysiwyg_preview::$sContent;
} 
else 
{
    // Get content
    $temp = $database->get_one("SELECT `content` FROM `".TABLE_PREFIX."mod_wysiwyg` WHERE `section_id` = ".$section_id);
	$content = htmlspecialchars_decode($temp);
}

/**
 *  htmlpurifier
 *  see: http://htmlpurifier.org/docs
 */
$oPURIFIER = lib_lepton::getToolInstance("htmlpurifier");

$content = $oPURIFIER->purify($content);

echo $content;
