<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


$MOD_WYSIWYG = [
	'action'	=> 'Action',
	'autosave_always'	=> 'Publish changes and keep old content (max. '.MAX_WYSIWYG_HISTORY.')',
	'button_copy'	=> 'Drafts',
	'button_history'=> 'History',
	'comment'		=> 'Comment',
	'date'			=> 'Date',
	'docs'			=> 'Documentation',
	'header1'		=> 'No',
	'publish_changes'=> 'Publish changes and replace old content (default)',
	'pushed_by'=> 'Pushed to history by',
	'saved_by'=> 'Saved by',
	'save_ok'		=> 'Data saved successfull!',
	'use_workingcopy'=> 'Save as draft (only one draft per section possible)',
	'version_delete'=> 'Delete version',
	'version_restore'=> 'Restore version',
	'version_view'=> 'Preview version',
	'want_really'	=> "really",	
	'want_delete'	=> "Do you want to delete version from ",
	'want_delete_wc'=> "Do you want to delete draft of section ",
	'wcopy_by'		=> 'Saved as draft by',	
	'wc_delete'		=> 'Delete',
	'wc_preview'	=> 'Preview',	
	'wc_restore'	=> 'Restore',	
	'what_to_do'	=> 'Please choose'
];
