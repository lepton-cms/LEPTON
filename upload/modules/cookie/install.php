<?php

/**
 * @module          Cookie
 * @author          cms-lab
 * @copyright       2010-2025 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookie/license.php
 * @license_terms   see: https://cms-lab.com/_documentation/cookie/license.php
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// mod_cookie
$table_fields="
  `cookie_id` int NOT NULL AUTO_INCREMENT,
  `pop_bg` varchar(16) NOT NULL DEFAULT '#aaa',
  `pop_text` varchar(16) NOT NULL DEFAULT '#fff',
  `but_bg` varchar(16) NOT NULL DEFAULT 'transparent',
  `but_text` varchar(16) NOT NULL DEFAULT '#fff',
  `but_border` varchar(16) NOT NULL DEFAULT '#fff',
  `position` varchar(32) NOT NULL DEFAULT 'bottom-left',
  `layout` varchar(32) NOT NULL DEFAULT 'classic',  
  `type` varchar(32) NOT NULL DEFAULT 'show',
  `overwrite` int(1) NOT NULL DEFAULT 0,
  `message` varchar(512) NOT NULL DEFAULT 'here the message text',
  `dismiss` varchar(128) NOT NULL DEFAULT 'Agree',
  `allow` varchar(128) NOT NULL DEFAULT 'Accept',
  `deny` varchar(128) NOT NULL DEFAULT 'Deny',  
  `link` varchar(64) NOT NULL DEFAULT 'policy link',
  `href` varchar(256) NOT NULL DEFAULT 'https://cms-lab.com/_documentation/cookie.php',
	PRIMARY KEY ( `cookie_id` )
	";
LEPTON_handle::install_table("mod_cookie", $table_fields);

$field_values="
    (NULL, '#aaa', '#fff', 'transparent', '#fff', '#fff', 'bottom-left', 'classic', 'show', 0, 'here the message text', 'Agree', 'Accept', 'Deny','policy link','https://cms-lab.com/_documentation/cookie.php')
";
LEPTON_handle::insert_values("mod_cookie", $field_values);

LEPTON_handle::install_droplets('cookie', 'droplet_site-cookie');
