<?php

/**
 * @module          Cookie
 * @author          cms-lab
 * @copyright       2010-2025 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookie/license.php
 * @license_terms   see: https://cms-lab.com/_documentation/cookie/license.php
 *
 */
 
 /**
 * All cookie javascript files are created by 
 * https://github.com/osano/cookieconsent
 * and are licensed under MIT
 * https://github.com/osano/cookieconsent/blob/dev/LICENSE
 * 
 * The used color-picker is created by
 * https://github.com/taufik-nurrohman/color-picker
 * and is also licensed under MIT: https://opensource.org/licenses/MIT
 * 
 */
  
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure file

$module_directory     = "cookie";
$module_name          = "Cookie";
$module_function      = "tool";
$module_version       = "3.3.4";
$module_platform      = "7.x";
$module_author        = '<a href="https://cms-lab.com" target="_blank">CMS-LAB</a>';
$module_license       = '<a href="https://cms-lab.com/_documentation/cookie/license.php" class="info" target="_blank">Custom license</a>';
$module_license_terms = '<a href="https://cms-lab.com/_documentation/cookie/license.php" class="info" target="_blank">License terms</a>';
$module_description   = "Tool to get users informed about cookies.";
$module_guid		  = "d7a7c31e-a197-45a4-9131-66297f0c0cc8";
