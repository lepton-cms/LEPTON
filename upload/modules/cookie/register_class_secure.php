<?php

/**
 * @module          Cookie
 * @author          cms-lab
 * @copyright       2010-2025 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookie/license.php
 * @license_terms   see: https://cms-lab.com/_documentation/cookie/license.php
 *
 */
 

$files_to_register = array();

LEPTON_secure::getInstance()->accessFiles( $files_to_register );

?>