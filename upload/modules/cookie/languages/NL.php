<?php

/**
 * @module          Cookie
 * @author          cms-lab
 * @copyright       2010-2025 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookie/license.php
 * @license_terms   see: https://cms-lab.com/_documentation/cookie/license.php
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/* ==============================================
 * translated at....: 27-12-2022 by Gsm
 * translated from..: EN
 * translated to....: NL
 * ==============================================
 */

$MOD_COOKIE	= array(

	"allow"					=> "Accoord",
	"allow_label"			=> "Accoord Knop",
	"banner_background"		=> "Bannerkleur",
	"banner_text"			=> "Kleur van de bannertekst",
	"button_background"		=> "Knopkleur",
	"button_border"			=> "Kleur van de knoprand",
	"button_text"			=> "Kleur van de knoptekst",
	"deny"					=> "Geen cookies",
	"deny_label"			=> "Weiger Knop",
	"dismiss"				=> "Accoord",
	"dismiss_label"			=> "Gezien Knop",
	"examples"				=> "Voorbeelden",
	"info"					=> "Addon-informatie",
	"layout"				=> "Indeling",
	"learn_more"			=> "Meer informatie",
	"learn_more_label"		=> "Link naar meer informatie",
	"message"				=> "Deze website maakt gebruik van cookies voor een optimale gebruikerservaring.",
	"message_label"			=> "Informatie",
	"overwrite"				=> "Overschrijven van taalbestanden (alleen voor sites in één taal)",
	"policy_link"			=> "Naar cookie-beleid",
	"policy_name"			=> "Tekst bij link naar meer informatie",
	"position"				=> "Positie",
	"SAVE_FAILED"			=> "Cookie-instellingen zijn niet opgeslagen!",
	"SAVE_OK"				=> "Cookie-instellingen zijn opgeslagen",
	"type"					=> "Type",
	"type_text1"			=> "Vertel gebruikers dat we cookies gebruiken",
	"type_text2"			=> "Laat gebruikers zich afmelden voor cookies (Geavanceerd)",
	"type_text3"			=> "Vraag gebruikers om te kiezen voor cookies (Geavanceerd)",
	"type_text_message1"	=> "Informatie over cookies",
	"type_text_message2"	=> "Bij 'geavanceerde opties' worden cookies ingesteld volgens de voorkeur van de gebruiker!"
);
