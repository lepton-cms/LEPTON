<?php

/**
 * @module          Cookie
 * @author          cms-lab
 * @copyright       2010-2025 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookie/license.php
 * @license_terms   see: https://cms-lab.com/_documentation/cookie/license.php
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 14:22, 25-06-2020
 * translated from..: EN
 * translated to....: FR
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_COOKIE	= array(
	"SAVE_FAILED"			=> "Les paramètres des cookies n'ont pas été sauvegardés !",
	"SAVE_OK"				=> "Paramètres des cookies sauvegardés",
	"allow"					=> "Je suis d'accord",
	"allow_label"			=> "Bouton Autoriser",
	"banner_background"		=> "Couleur de la bannière",
	"banner_text"			=> "Couleur du texte de la bannière",
	"button_background"		=> "Couleur des boutons",
	"button_border"			=> "Bouton Couleur de la bordure",
	"button_text"			=> "Bouton Couleur du texte",
	"deny"					=> "Je nie !",
	"deny_label"			=> "Bouton Nier",
	"dismiss"				=> "J'accepte !",
	"dismiss_label"			=> "Bouton Rejeté",
	"examples"				=> "Exemples",
	"info"					=> "Addon Info",
	"layout"				=> "Mise en page",
	"learn_more"			=> "En savoir plus",
	"learn_more_label"		=> "Pour en savoir plus, cliquez sur le lien",
	"message"				=> "Ce site web utilise des cookies afin de vous garantir la meilleure expérience possible sur notre site web.",
	"message_label"			=> "Informations",
	"overwrite"				=> "Écraser les fichiers de langue (uniquement les sites en langue unique)",
	"policy_link"			=> "Lien politique",
	"policy_name"			=> "Politique",
	"position"				=> "Position",
	"type"					=> "Type",
	"type_text1"			=> "Dites simplement aux utilisateurs que nous utilisons des cookies",
	"type_text2"			=> "Permettre aux utilisateurs de refuser les cookies (Avancé)",
	"type_text3"			=> "Demander aux utilisateurs d'accepter les cookies (Avancé)",
	"type_text_message1"	=> "Lien pour des informations détaillées",
	"type_text_message2"	=> "Dans le cas des 'options avancées', des cookies sont définis en référence à l'action de l'utilisateur !"
);

