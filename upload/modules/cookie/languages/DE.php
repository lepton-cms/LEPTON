<?php

/**
 * @module          Cookie
 * @author          cms-lab
 * @copyright       2010-2025 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookie/license.php
 * @license_terms   see: https://cms-lab.com/_documentation/cookie/license.php
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


$MOD_COOKIE = array(
	'allow'				=> "Ich stimme zu",
	'allow_label'		=> "Button Erlaubnis",	
	'banner_background'	=> "Banner Farbe",
	'banner_text'		=> "Banner Text-Farbe",
	'button_background'	=> "Button Farbe",
	'button_text'		=> "Button Text-Farbe",	
	'button_border'		=> "Button Linien-Farbe",
	'dismiss'			=> "Akzeptiert!",
	'dismiss_label'		=> "Button Notiz",
	'deny'				=> "Nein, keine Cookies!",
	'deny_label'		=> "Button Verweigerung",	
	'examples'			=> "Beispiele",	
	'info'				=> "Addon Info",	
	'layout'			=> "Layout",
	'learn_more'		=> "Weitere Infos",
	'learn_more_label'	=> "Info-link",	
	'message'			=> "Diese Webseite benutzt Cookies. Wenn Sie auf dieser Seite browsen, akzeptieren sie die Nutzung von Cookies!",
	'message_label'		=> "Information",	
	'overwrite'			=> "Sprachdatei überschreiben (nicht bei mehrsprachigen Seiten)",
	'policy_name'		=> "Link zur Datenschutzinfo",
	'policy_link'		=> "Datenschutzerklärung",
	'position'			=> "Position",
	'SAVE_FAILED'		=> "Cookie Einstellungen wurden nicht gespeichert!",		
	'SAVE_OK'			=> "Cookie Einstellungen wurden gespeichert",	
	'type'				=> "Typ",
	'type_text1'		=> "User informieren, dass wir Cookies setzen",
	'type_text2'		=> "User können Cookies verhindern (Erweitert)",
	'type_text3'		=> "User sollen der Nutzung von Cookies zustimmen (Erweitert)",
	'type_text_message1'	=> "Link für weitere Infos",	
	'type_text_message2'	=> "Bei den 'erweiterten Optionen' werden die enstprechenden Cookies automatisch gesetzt!"	
);
