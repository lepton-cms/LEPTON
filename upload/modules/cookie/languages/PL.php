<?php

/**
 * @module          Cookie
 * @author          cms-lab
 * @copyright       2010-2025 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookie/license.php
 * @license_terms   see: https://cms-lab.com/_documentation/cookie/license.php
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 14:25, 25-06-2020
 * translated from..: EN
 * translated to....: PL
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_COOKIE	= array(
	"SAVE_FAILED"			=> "Ustawienia ciasteczek nie zostały zapisane!",
	"SAVE_OK"				=> "Ustawienia plików cookie zapisane",
	"allow"					=> "Zgadzam się",
	"allow_label"			=> "Przycisk Pozwól",
	"banner_background"		=> "Kolor banera",
	"banner_text"			=> "Baner Kolor tekstu",
	"button_background"		=> "Kolor przycisku",
	"button_border"			=> "Przycisk Granica-Kolor",
	"button_text"			=> "Przycisk Text-Color",
	"deny"					=> "Zaprzeczam!",
	"deny_label"			=> "Przycisk Zaprzeczam",
	"dismiss"				=> "Przyjmuję!",
	"dismiss_label"			=> "Przycisk Zwolnić",
	"examples"				=> "Przykłady",
	"info"					=> "Addon Info",
	"layout"				=> "Layout",
	"learn_more"			=> "Dowiedz się więcej",
	"learn_more_label"		=> "Dowiedz się więcej o linku",
	"message"				=> "Niniejsza witryna internetowa wykorzystuje pliki cookie w celu zapewnienia najlepszych doświadczeń na naszej stronie.",
	"message_label"			=> "Informacje",
	"overwrite"				=> "Nadpisywanie plików językowych (tylko strony w jednym języku)",
	"policy_link"			=> "Powiązanie z polityką",
	"policy_name"			=> "Polityka",
	"position"				=> "Pozycja",
	"type"					=> "Typ",
	"type_text1"			=> "Po prostu powiedz użytkownikom, że używamy plików cookie",
	"type_text2"			=> "Pozwól użytkownikom zrezygnować z cookies (zaawansowane)",
	"type_text3"			=> "Poproś użytkowników o wybranie opcji 'cookies' (zaawansowane)",
	"type_text_message1"	=> "Link do szczegółowych informacji",
	"type_text_message2"	=> "W przypadku opcji zaawansowanych ustawiane są pliki cookie odnoszące się do działań użytkownika!"
);

