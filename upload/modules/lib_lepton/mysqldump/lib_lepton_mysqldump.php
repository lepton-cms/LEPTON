<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          lib_lepton
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

require_once dirname(__dir__)."/mysqldump/Mysqldump/Mysqldump.php";

class lib_lepton_mysqldump extends Ifsnop\Mysqldump\Mysqldump
{
    static $instance;

    public static $defaultDir = LEPTON_PATH.DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR."secure".DIRECTORY_SEPARATOR."backup".DIRECTORY_SEPARATOR;
    
    public static function getInstance( $aSettings=array() )
    {
        if (null === static::$instance)
        {
            self::setupOutputDir();
        
            $ini_file_name = LEPTON_PATH."/config/lepton.ini.php";
            
            if( true == file_exists( $ini_file_name ) )
            {
                $config = parse_ini_string(";".file_get_contents($ini_file_name), true );

                if ( isset($config['database']['port']) && $config['database']['port'] !== '3306')
                {
                    $config['database']['host'] .= ';port=' . $config['database']['port'];
                }
                $dsn = "mysql:host=".$config['database']['host'].";dbname=".$config['database']['name'];
                
                if(!is_array($aSettings))
                {
                    $aSettings = [];
                }
                
                static::$instance = new static(
                    $dsn,
                    $config['database']['user'],
                    $config['database']['pass'],
                    $aSettings
                );
            }
        }
        
        return static::$instance;
    }
 
    private static function setupOutputDir()
    {
        if(!file_exists( self::$defaultDir ))
        {
            LEPTON_core::make_dir( self::$defaultDir );
        }
        
        if(!file_exists(self::$defaultDir."index.php"))
        {
            $origin = ADMIN_PATH . "/pages/master_index.php";
            if ( file_exists( $origin ) )
            {
                copy( $origin, self::$defaultDir."index.php" );
            }
        }
        
        if(!file_exists( self::$defaultDir.".htaccess" ))
        {
            copy( __dir__."/__htaccess.txt", self::$defaultDir.".htaccess" );
        }
    }
    
    public function dumpToFile($sFilename = '')
    {
        try
        {
            $this->start( self::$defaultDir.$sFilename );
            return true;
        
        } catch (Exception $e) {
            
            echo LEPTON_tools::display( 'mysqldump-php error: ' . $e->getMessage(), "pre", "ui message red");
            return false;
        }
    }
    
    public function setOutputDir( $sNewDir)
    {
        self::$defaultDir = $sNewDir;
        
        self::setupOutputDir();
    }
}