<?php

declare(strict_types=1);

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          lib_lepton
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

class lib_lepton extends LEPTON_abstract
{
    public static $instance;
    
    public function initialize()
    {
        // nothing to do here yet
    }

    /**
     * For (auto-)loading instances from sub-folders (tools) inside
     * the lib_lepton, e.g. 'pclzip', 'datetools', etc.
     * @code{.php}
     *    $oUpload = lib_lepton::getToolInstance("upload", $_FILES[ $file-id ]);
     * @endcode
     * @param string $sToolName A valid "tool-name" of one of the submodules.
     * @param mixed $sParam2 An optional valid string, array, or object-instance.
     * @param mixed $sParam3 An optional valid string, array, or object-instance.
     *
     * @return mixed    A valid instance (object-reference) or NULL if failed or no name match.
     *
     * @throws Exception
     */
    static public function getToolInstance(string $sToolName = "", mixed $sParam2 = "", mixed $sParam3 = ""): mixed
    {
        $returnValue = NULL;

        switch(strtolower($sToolName))
        {
            //  [1]
            case "datetools":
                // [1.1] $sParam2 has to be a boolean
                $sParam2 = (bool)$sParam2;
                require_once __dir__."/../datetools/lib_lepton_datetools.php";
                $returnValue = 	lib_lepton_datetools::getInstance( $sParam2 );
				$info = '@DEPRECATED_TEMP 20250117: this class will be removed in L* > 7.3.0, use LEPTON_date instead of lib_lepton::getToolInstance("datetools")';
				echo(LEPTON_tools::display($info, 'pre','ui orange message'));
                break;

            //  [2a] release 2.8.2   -> no original source available
            case "pclzip":
                require_once __dir__."/../pclzip/pclzip.lib.php";
                $returnValue = new PclZip( $sParam2 );
                break;

            //  [2b] release 4.0.1 , as an alternative to outdated 2a, for details: https://github.com/Ne-Lexa/php-zip
            case "php-zip":
                // 2b.1 LEPTON_autoloader
                require_once __dir__."/../php-zip/PhpZip_loader.php";
                PhpZip_loader::getInstance()->register();

                // 2b.2 get file and instance
                require_once __dir__."/../php-zip/src/ZipFile.php";
                $returnValue = new PhpZip\ZipFile();
                break;

            //  [3] release 2.1.6 plus additional commits up to 2024-09-11, for details please see https://github.com/verot/class.upload.php
            case "upload": 
                require_once __dir__."/../upload/class.upload.php";
                if($sParam3 == "")
                {
                    $sParam3 = strtolower(LANGUAGE)."_".LANGUAGE;
                }
                $returnValue = new Verot\Upload\Upload( $sParam2, $sParam3 );
                break;

            //  [4] release 2.12, last commit on 20240920, for details please see https://github.com/ifsnop/mysqldump-php
            case "mysqldump":
                require_once dirname(__dir__)."/mysqldump/lib_lepton_mysqldump.php";
                $returnValue = lib_lepton_mysqldump::getInstance( $sParam2 );
                break;

            //  [5] release 4.18.0, for details please see http://htmlpurifier.org, https://github.com/ezyang/htmlpurifier
            case "htmlpurifier":
                require_once dirname(__dir__)."/htmlpurifier/HTMLPurifier.auto.php";
                $config = HTMLPurifier_Config::createDefault();

                //  [5.1] For YouTube and Vimeo embedded videos inside iFrames
                //          see: http://htmlpurifier.org/live/configdoc/plain.html#HTML.SafeIframe
                $config->set('HTML.SafeIframe', true);

                //          see: http://htmlpurifier.org/live/configdoc/plain.html#URI.SafeIframeRegexp
                $config->set('URI.SafeIframeRegexp', '%.*%'); //^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%'); //allow YouTube and Vimeo

                //  [5.2] Caching files
                //          see: http://htmlpurifier.org/live/configdoc/plain.html#Cache.DefinitionImpl
                $config->set('Cache.DefinitionImpl', null); // No local caching during development!

                //  [5.3] Keep ids inside html tags! e.g.: <h1 id="myAnchor">here is top</h1>
                //          see: http://htmlpurifier.org/docs/enduser-id.html
                $config->set('Attr.EnableID', true);

                //  [5.4] Keep "target"
                //          see: http://htmlpurifier.org/live/configdoc/plain.html#HTML.AllowedAttributes
                $config->set('Attr.AllowedFrameTargets', '_blank,_self');

                //  [5.5] add allowed attributes
                $def = $config->getHTMLDefinition(true);
                $def->addAttribute("a", "data-tab", "Text");
                $def->addAttribute("div", "data-tab", "Text");

                $returnValue = new HTMLPurifier($config);
                break;

            //  [6] release 2.0.0 dev plus additional commits up to 2021-09-25, for details please see https://github.com/jeroendesloovere/vcard/tree/2.0.0-dev
            case "vcard":
                    require_once dirname(__dir__)."/vcard/lib_lepton_vcard.php";
                    $returnValue = lib_lepton_vcard::getInstance( $sParam2 );
                    break;
 
            //  [7] release 1.86, for details please see https://github.com/Setasign/FPDF or docs on http://www.fpdf.org/
            case "fpdf":
				if(file_exists(dirname(__dir__)."/fpdf/lib_lepton_fpdf_custom.php"))
				{
					require_once dirname(__dir__)."/fpdf/lib_lepton_fpdf_custom.php";	
				}
				else
				{
	                require_once dirname(__dir__)."/fpdf/lib_lepton_fpdf.php";				
				}

                $returnValue = lib_lepton_fpdf::getInstance();
                break;
				
            default:
                throw new Exception('Unexpected value');
        }

        return $returnValue;
    }
}
