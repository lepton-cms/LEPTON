<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          lib_lepton
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

class PhpZip_loader extends LEPTON_abstract
{
    public static $instance;
    
    public function initialize()
    {

    }
    
	/**
	 * Registers PhpZip as an SPL autoloader.
	 *
	 * @param bool $prepend Whether to prepend the autoloader or not.
	 */
	public static function register($prepend = true)
	{
		spl_autoload_register(array(__CLASS__, 'autoload'), true, $prepend);
	}

	/**
	 * Handles autoloading of classes.
	 *
	 * @param string $class A class name.
	 */
	public static function autoload($class)
	{
	    if (0 !== strpos($class, 'PhpZip'))
	    {
	        return;
	    }
		
		// any namespaces?
		$aTempTerms = explode("\\", $class);
		
		if(1 < count($aTempTerms) )
		{
            array_shift( $aTempTerms ); // remove the \PhpZip\
            $sPath = implode( DIRECTORY_SEPARATOR, $aTempTerms);
            $file = (dirname(__FILE__)).'/src/'.$sPath.".php";
            
            if (is_file($file))
            {
                require_once $file;
            }
        } elseif( 1 === count($aTempTerms) ) {
            $file = (dirname(__FILE__)).'/src/'.$aTempTerms[0].".php";
            if (is_file($file))
            {
                require_once $file;
            }
        }
	}
}