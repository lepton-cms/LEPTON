<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          lib_lepton
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

/**
 *
 *  @code
 *
 *      $oTEST = lib_lepton::getToolInstance("vcard");
 *      echo LEPTON_tools::display( $oTEST );
 *
 *      $oVCard = $oTEST->newVCard();
 *      echo LEPTON_tools::display( $oVCard , "pre", "ui message green");
 *
 *      $oFormatter = $oTEST->newFormatter();
 *      echo LEPTON_tools::display( $oFormatter, "pre", "ui message orange" );
 *
 *  @endcode
 */
 
class lib_lepton_vcard
{
    /**
     *  The reference to *Singleton* instance of this class
     *
     *  @var    object
     *  @access private
     *
     */
    private static $instance;

    /**
     *  Return the "internal" instance of this class
     *
     *  @return object
     *
     */
    public static function getInstance(  ) : object
    {
        if (null === static::$instance) {
            static::$instance = new static();
            spl_autoload_register(array(__CLASS__, 'autoload'), true, false);
        }
        
        return static::$instance;
    }

    /**
     *  Force the user to use "getInstance" method instead of "new".
     */
    protected function __construct()
    {
    
    }

    /**
     *  Handles autoload of classes.
     *
     * @param string $class A class name.
     * @return bool
     */
    public static function autoload(string $class): bool
    {
        if (!str_starts_with($class, 'JeroenDesloovere'))
        {
            return false;
        }
        // any namespaces?
        $aTempTerms = explode("\\", $class);
            
        array_shift( $aTempTerms ); // remove the \JeroenDesloovere\
        array_shift( $aTempTerms ); // remove the \VCard\
        $file = __DIR__."/src/".implode( DIRECTORY_SEPARATOR, $aTempTerms).".php";

        if (is_file($file))
        {
            require_once $file;
            return true;
        }

        return false;

    }

    /**
     *  Get a new instance of VCard
     */
    public function newVCard() : object
    {
        return new \JeroenDesloovere\VCard\VCard();
    }
    
    /**
     * Add new Name 
     * @param string $lastname      required
     * @param string $firstname     required
     * @param string $additional    optional
     * @param string $prefix        optional
     * @param string $suffix        optional
     * @return object
     */
    public function newName( string $lastname, string $firstname, string $additional = "", string $prefix = "", string $suffix="" ) : object
    {
        return new \JeroenDesloovere\VCard\Property\Name( 
                $lastname,
                $firstname,
                $additional = "",
                $prefix,
                $suffix=""
           );
    }
    /**
     *  Get a new instance of Formatter
     */
    public function newFormatter( string $sNameOfFileToExport = 'vcard-export' ) : object
    {
        return new \JeroenDesloovere\VCard\Formatter\Formatter( 
            new \JeroenDesloovere\VCard\Formatter\VcfFormatter(),
            $sNameOfFileToExport
        );
    }
}