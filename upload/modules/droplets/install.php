<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          Droplets
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// create new tables
$table_fields="
	`id` INT NOT NULL auto_increment,
	`name` VARCHAR(32) NOT NULL,
	`code` LONGTEXT NOT NULL ,
	`description` TEXT NOT NULL,
	`modified_when` INT NOT NULL default 0,
	`modified_by` INT NOT NULL default 0,
	`active` INT NOT NULL default 0,
	`admin_edit` INT NOT NULL default 0,
	`admin_view` INT NOT NULL default 0,
	`show_wysiwyg` INT NOT NULL default 0,
	`comments` TEXT NOT NULL,
	PRIMARY KEY ( `id` )
";
LEPTON_handle::install_table('mod_droplets', $table_fields);


// create the new permissions table
$table_fields="
	`id` INT(10) UNSIGNED NOT NULL,
	`edit_perm` VARCHAR(50) NOT NULL,
	`view_perm` VARCHAR(50) NOT NULL,
	PRIMARY KEY ( `id` )
";
LEPTON_handle::install_table('mod_droplets_permissions', $table_fields);


// create the settings table
$table_fields="
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`attribute` VARCHAR(50) NOT NULL DEFAULT '0',
	`value` VARCHAR(50) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
";
LEPTON_handle::install_table('mod_droplets_settings', $table_fields);


//	insert settings
$field_values="
	(1, 'Manage_backups', '1'),
	(2, 'Import_droplets', '1'),
	(3, 'Delete_droplets', '1'),
	(4, 'Add_droplets', '1'),
	(5, 'Export_droplets', '1'),
	(6, 'Modify_droplets', '1'),
	(7, 'Manage_perms', '1')
";
LEPTON_handle::insert_values('mod_droplets_settings', $field_values);


// install default droplets
$zip_names = array(
	'droplet_check-css',
	'droplet_EditThisPage',
	'droplet_EmailFilter',
	'droplet_LoginBox',
	'droplet_Lorem',
	'droplet_year'
);
LEPTON_handle::install_droplets('droplets',$zip_names);
