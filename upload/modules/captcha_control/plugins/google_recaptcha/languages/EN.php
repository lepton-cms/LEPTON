<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the captcha module
 * translation file, whereas this plugin translation has a higher priority and may 
 * overwrite any translation defined in the captcha main.
 * However, this translation file here should contain only translations used in this plugin
 * if not already defined in main captcha translation file or divers from there.
 */
 
$MOD_PLUGIN = array(
	// the description of current plugin
	'GOOGLE_RECAPTCHA'		=> 'Google Recaptcha',

	// CCL = captcha_control label
	'CCL_SITEKEY'			=> 'Google site key',
	'CCL_SECUREKEY'			=> 'Google secure key',
	'CCL_CAPTCHALABEL'		=> 'Defined Label for Captcha',
	'CCL_VERSION'			=> 'Google reCAPTCHA version',
	'CCL_ITEM_V3'			=> 'reCAPTCHA Version 3',
	'CCL_ITEM_V2_CHECKBOX'	=> 'reCAPTCHA Version 2 - Checkbox',
	'CCL_ITEM_V2_INVISIBLE'	=> 'reCAPTCHA Version 2 - Invisible',
	'CCL_THEME'				=> 'Theme',
	'CCL_ITEM_V2_DARK'		=> 'dark',
	'CCL_ITEM_V2_LIGHT'		=> 'light (* default )',
	'CCL_SIZE'				=> 'Size',
	'CCL_ITEM_V2_COMPACT'	=> 'compact',
	'CCL_ITEM_V2_NORMAL'	=> 'normal (* default )',
	'CCL_GOOGLE_REGISTER'	=> 'Google reCAPTCHA Registration Settings',
	'CCL_GOOGLE_CONFIG'		=> 'Google reCAPTCHA Configuration Settings'
);

?>
