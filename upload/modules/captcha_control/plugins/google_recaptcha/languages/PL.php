<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the captcha module
 * translation file, whereas this plugin translation has a higher priority and may 
 * overwrite any translation defined in the captcha main.
 * However, this translation file here should contain only translations used in this plugin
 * if not already defined in main captcha translation file or divers from there.
 */
 
/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 13:02, 28-06-2020
 * translated from..: EN
 * translated to....: PL
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_PLUGIN	= array(
	"CCL_CAPTCHALABEL"			=> "Zdefiniowana etykieta dla Captcha",
	"CCL_GOOGLE_CONFIG"			=> "Ustawienia konfiguracyjne Google reCAPTCHA",
	"CCL_GOOGLE_REGISTER"		=> "Ustawienia rejestracji Google reCAPTCHA",
	"CCL_ITEM_V2_CHECKBOX"		=> "reCAPTCHA Wersja 2 - pole wyboru",
	"CCL_ITEM_V2_COMPACT"		=> "zwarty",
	"CCL_ITEM_V2_DARK"			=> "ciemny",
	"CCL_ITEM_V2_INVISIBLE"		=> "reCAPTCHA Wersja 2 - niewidoczna",
	"CCL_ITEM_V2_LIGHT"			=> "światło (* wartość domyślna )",
	"CCL_ITEM_V2_NORMAL"		=> "normalny (* wartość domyślna )",
	"CCL_ITEM_V3"				=> "reCAPTCHA Wersja 3",
	"CCL_SECUREKEY"				=> "Bezpieczny klucz Google",
	"CCL_SITEKEY"				=> "Klucz witryny Google",
	"CCL_SIZE"					=> "Wielkość",
	"CCL_THEME"					=> "Temat",
	"CCL_VERSION"				=> "Wersja Google reCAPTCHA",
	"GOOGLE_RECAPTCHA"			=> "Google Recaptcha"
);

?>
