<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the captcha module
 * translation file, whereas this plugin translation has a higher priority and may 
 * overwrite any translation defined in the captcha main.
 * However, this translation file here should contain only translations used in this plugin
 * if not already defined in main captcha translation file or divers from there.
 */
 
/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 13:02, 28-06-2020
 * translated from..: EN
 * translated to....: RU
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_PLUGIN	= array(
	"CCL_ADD_LANGUAGE"			=> "Добавить новый язык со следующим сохранением",
	"CCL_ANSWER"				=> "Ответы",
	"CCL_CASE_SENSITIVE"		=> "Чувствительный к регистру ответ",
	"CCL_DEL_LANGUAGE"			=> "Чтобы удалить языковой набор вопросов, удалите все вопросы и ответы на них на этом языке.",
	"CCL_ITEM_IMAGE"			=> "Выходной сигнал Captcha в виде стилизованного пролета",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Только Captcha",
	"CCL_ITEM_NOT_SENSITIVE"	=> "Нет, проверка ответов не чувствительна к регистру.",
	"CCL_ITEM_SENSITIVE"		=> "Да, проверка ответов чувствительна к регистру.",
	"CCL_NBR_QUESTIONS"			=> "Количество вопросов для выбора (1-10)",
	"CCL_QA"					=> "Вопросы и ответы",
	"CCL_QUESTION"				=> "Вопросы",
	"DEFAULT_ANSWER"			=> "0",
	"DEFAULT_QUESTION"			=> "1 минус 1 ?",
	"ENTER_RESULT"				=> "Ответьте на вопрос",
	"TEXT"						=> "Текст-Капча"
);

?>
