<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the captcha module
 * translation file, whereas this plugin translation has a higher priority and may 
 * overwrite any translation defined in the captcha main.
 * However, this translation file here should contain only translations used in this plugin
 * if not already defined in main captcha translation file or divers from there.
 */
 
$MOD_PLUGIN = array(
	// the description of current plugin
	'TEXT'					=> 'Text-CAPTCHA',

	// CCL = captcha_control Label
	"CCL_ITEM_IMAGE"		=> 'Captcha output as styled span',
	"CCL_ITEM_IMAGE_IFRAME"	=> 'Captcha only',
	"CCL_QA"				=> 'Questions & Answers',
	"CCL_NBR_QUESTIONS"		=> 'Number of questions for choosing (1-10)',
	"CCL_QUESTION"			=> 'Questions',
	"CCL_ANSWER"			=> 'Answers',
	"CCL_ADD_LANGUAGE"		=> 'Add new language with next save',
	"CCL_DEL_LANGUAGE"		=> 'To delete a language question set, remove all questions & answers of that language.',
	"CCL_CASE_SENSITIVE"	=> 'Answer check case sensitive',
	"CCL_ITEM_SENSITIVE"	=> 'Yes, answer check is case sensitive',
	"CCL_ITEM_NOT_SENSITIVE"=> 'No, answer check is not case sensitive',

	// captcha calculation settings
	'ENTER_RESULT'			=> 'Answer the question',

	// used in case no valid Q&A are defined
	'DEFAULT_QUESTION'		=> '1 minus 1 is ?',
	'DEFAULT_ANSWER'		=> '0'
);

?>
