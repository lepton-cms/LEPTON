<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the captcha module
 * translation file, whereas this plugin translation has a higher priority and may 
 * overwrite any translation defined in the captcha main.
 * However, this translation file here should contain only translations used in this plugin
 * if not already defined in main captcha translation file or divers from there.
 */
 
/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 13:02, 28-06-2020
 * translated from..: EN
 * translated to....: PL
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_PLUGIN	= array(
	"CCL_ADD_LANGUAGE"			=> "Dodaj nowy język z następnym zapisem",
	"CCL_ANSWER"				=> "Odpowiedzi",
	"CCL_CASE_SENSITIVE"		=> "Wrażliwość odpowiedzi na wielkość liter",
	"CCL_DEL_LANGUAGE"			=> "Aby usunąć zestaw pytań językowych, należy usunąć wszystkie pytania i odpowiedzi w tym języku.",
	"CCL_ITEM_IMAGE"			=> "Wyjście Captcha jako stylizowana rozpiętość",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Tylko Captcha",
	"CCL_ITEM_NOT_SENSITIVE"	=> "Nie, kontrola poprawności odpowiedzi nie ma znaczenia.",
	"CCL_ITEM_SENSITIVE"		=> "Tak, sprawdzanie odpowiedzi ma znaczenie dla poszczególnych przypadków",
	"CCL_NBR_QUESTIONS"			=> "Liczba pytań do wyboru (1-10)",
	"CCL_QA"					=> "Pytania i odpowiedzi",
	"CCL_QUESTION"				=> "Pytania",
	"DEFAULT_ANSWER"			=> "0",
	"DEFAULT_QUESTION"			=> "1 minus 1 jest ?",
	"ENTER_RESULT"				=> "Odpowiedz na pytanie",
	"TEXT"						=> "Tekst-CAPTCHA"
);

?>
