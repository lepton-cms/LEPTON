<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 

$files_to_register = array(
	 'image.php'
	 ,'reload.php'
);

LEPTON_secure::getInstance()->accessFiles( $files_to_register );
