<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// includes
require_once LEPTON_PATH . "/modules/captcha_control/functions/function.generate_captcha.php";


if ( false === isset($_SESSION['captcha_time']))
{
	header("Location: ../index.php");
	die();
}

// Captcha
$section_id = $_SESSION[ 'captcha_id' ];

// get a captcha
$_SESSION['captcha' . $section_id] = '';
$captcha = generate_captcha( "calc" );
$_SESSION['captcha' . $section_id] = $captcha[ "RESULT" ];
$_SESSION['captcharequest' . $section_id] = $captcha;

// create reload-image
$reload		= ImageCreateFromPNG( __DIR__ . "/images/reload_120_30.png" ); // reload-overlay

$image		= imagecreate( 120, 30 );

$white		= imagecolorallocate( $image, 0xFF, 0xFF, 0xFF );
$gray		= imagecolorallocate( $image, 0xC0, 0xC0, 0xC0 );
$darkgray	= imagecolorallocate( $image, 0x30, 0x30, 0x30 );

for ( $i = 0; $i < 30; $i++ )
{
	$x1		= mt_rand(0,120);
	$y1		= mt_rand(0,30);
	$x2		= mt_rand(0,120);
	$y2		= mt_rand(0,30);
	imageline( $image, $x1, $y1, $x2, $y2, $gray );  
}

$x = 10;
$l = strlen( $captcha[ "CAPTCHA" ] );
for ( $i = 0; $i < $l; $i++ )
{
	$fnt	= mt_rand(3,5);
	$x		= $x + mt_rand( 12 , 20 );
	$y		= mt_rand( 7 , 12 ); 
	imagestring( $image, $fnt, $x, $y, substr( $captcha[ "CAPTCHA" ], $i, 1 ), $darkgray ); 
}

imagealphablending( $reload, TRUE );
imagesavealpha( $reload, TRUE );

// overlay
imagecopy( $reload, $image, 0,0,0,0, 120, 30 );
imagedestroy( $image );
$image = $reload;

header("Expires: Mon, 1 Jan 1990 05:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, proxy-revalidate");
header("Pragma: no-cache");
header("Content-type: image/png");

imagepng( $image );
imagedestroy( $image );

