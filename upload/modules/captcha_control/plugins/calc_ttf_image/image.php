<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


if ( false === isset($_SESSION['captcha_time']))
{
	header("Location: ../index.php");
	die();
}


// includes
require_once LEPTON_PATH . "/modules/captcha_control/functions/function.create_ttf_image.php";


// Captcha
// check section & page
$section_id = $_SESSION[ 'captcha_id' ];
$page_id = 0;
if ( array_key_exists( "PAGE_ID", $_SESSION ))
	{ $page_id = $_SESSION['PAGE_ID']; }

// get a captcha
$_SESSION['captcha' . $section_id] = '';
$captcha = generate_captcha( "calc" );
$_SESSION['captcha' . $section_id] = $captcha[ "RESULT" ];
$_SESSION['captcharequest' . $section_id] = $captcha;

// create Captcha image
$image = create_ttf_image( $page_id, $section_id, $captcha, __DIR__ );

// create reload image
$reload = ImageCreateFromPNG(__DIR__ . "/images/reload_140_40.png" ); // reload-overlay

imagealphablending($reload, TRUE);
imagesavealpha($reload, TRUE);

// overlay
imagecopy($reload, $image, 0,0,0,0, 140,40);
imagedestroy($image);
$image = $reload;

// define header
header("Expires: Mon, 1 Jan 1990 05:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, proxy-revalidate");
header("Pragma: no-cache");
header("Content-type: image/png");

// prepare output
ob_start();
imagepng($image);
header("Content-Length: ".ob_get_length()); 
ob_end_flush();
imagedestroy($image);


