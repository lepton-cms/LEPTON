<?php

declare(strict_types=1);

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */

class captcha_control_services
{
    const ASP_SESSION_MIN_AGE = 20;
    const ASP_VIEW_MIN_AGE = 10;
    const ASP_INPUT_MIN_AGE = 5;
    
    protected bool $keepCurrentCaptcha = false;

    public static $instance;

    public static function getInstance()
    {
        if (null === static::$instance)
        {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * @return bool
     */
    public function isKeepCurrentCaptcha(): bool
    {
        return $this->keepCurrentCaptcha;
    }

    /**
     * @param bool $keepCurrentCaptcha
     */
    public function setKeepCurrentCaptcha(bool $keepCurrentCaptcha): void
    {
        $this->keepCurrentCaptcha = $keepCurrentCaptcha;
    }
    
    /**
     *
     *  (($_SESSION['SESSION_STARTED'] + ASP_SESSION_MIN_AGE) > $current_time)  // session too young
     *  current time is too short from the allowed min time -- less than 5 sec(?).(?)
     *
     *      - currentTimestamp is less than allowed -> true
     *      - currentTimestamp is insite the given range from [start .. (start + max)]
     *
     *
     *
     */
     public static function insiteASPTimeRange(int $currentTimestamp): bool
     {
        // 1 too fast -> session min. age.
        $sessionStarted = intval($_SESSION['SESSION_STARTED'] ?? 0); 
        $asp_session_min_age = intval(defined("ASP_SESSION_MIN_AGE") ? ASP_SESSION_MIN_AGE : 20);

        $range_start = $sessionStarted + $asp_session_min_age;;
        if ($currentTimestamp <= $range_start)
        {
            return false;
        }
        
        // 2 user has answered too fast -> robot! (script)
        $asp_view_min_age = intval(defined("ASP_VIEW_MIN_AGE") ? ASP_VIEW_MIN_AGE : 10);
        
        if ($_SESSION['comes_from_view_time'] > ($currentTimestamp - $asp_view_min_age))
        {
            return false;
        }
        
        // 2.1
        if (!isset($_SESSION['submitted_when']))
        {
            $_SESSION['submitted_when'] = $_SESSION['comes_from_view_time'];
        }
        
        // 3 has submitted too fast ... robort/script
        $asp_input_min_age = intval(defined("ASP_INPUT_MIN_AGE") ? ASP_INPUT_MIN_AGE : 5);
        if (($_SESSION['submitted_when'] > ($currentTimestamp - $asp_input_min_age)) 
            &&  (!isset($_SESSION['captcha_retry_news'])))
        {
            return false;
        }
        
        return true;
     }

}