<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$module_directory 	= 'captcha_control';
$module_name        = 'Captcha and Advanced-Spam-Protection (ASP) Control';
$module_function    = 'tool';
$module_version     = '2.6.4';
$module_platform    = '7.x';
$module_delete      =  false;
$module_author      = 'W. Studer, LEPTON Project';
$module_license     = 'GNU General Public License';
$module_license_terms   = 'GNU General Public License';
$module_description     = 'Admin-Tool to control CAPTCHA and ASP';
$module_guid            = 'c29c5f1a-a72a-4137-b5cd-62982809bd38';
