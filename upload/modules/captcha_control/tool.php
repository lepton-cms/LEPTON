<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// get classes instance 
$oCAPTCHA = captcha_control_captcha::getInstance();
$oREQUEST = LEPTON_request::getInstance();
$database = LEPTON_database::getInstance();

// load current captcha settings
$data = [];
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX . "mod_captcha_control`",
	true,
	$data,
	false
);
		
// check if data was submitted
$message = [];
if (( isset( $_POST['save'])) || ( isset( $_POST['run'])))
{

	if($_POST['enabled_captcha'] == 0)
	{
		$_POST['enabled_asp'] = 0;
	}
	// add default fields
	$input_fields = [
		'enabled_captcha'		=> [ 'type' => 'int', 'default' => 0 ],
		'enabled_asp'			=> [ 'type' => 'int', 'default' => 0 ],
		'captcha_type'			=> [ 'type' => 'string_clean', 'default' => 'calc_text' ],
		'asp_session_min_age'	=> [ 'type' => 'int', 'default' => 20 ],
		'asp_view_min_age'		=> [ 'type' => 'int', 'default' => 10 ],
		'asp_input_min_age'		=> [ 'type' => 'int', 'default' => 5 ],
		'ct_text'				=> [ 'type' => 'string_clean', 'default' => 'calc_text' ]
	];

	// set validation
	$valid_fields = $oREQUEST->testPostValues($input_fields);

	// check hash: with invalid hash no save and no info ...
	if (( true === isset( $_SESSION['captcha_control_hash'] )) && ( $_SESSION['captcha_control_hash'] == $_POST[ "hash" ] ))
	{	
		unset( $_SESSION[ "captcha_control_hash" ] );
		unset( $_POST[ "hash" ] );

		// update database default settings
		$database->build_and_execute(
			"UPDATE",
			TABLE_PREFIX . "mod_captcha_control",
			$valid_fields,
			""
		);

		// update plugin specific fields
		$oCAPTCHA->captcha_control_save( $valid_fields );

		// reload current captcha settings
		$data = [];
		$database->execute_query(
			"SELECT * FROM `" . TABLE_PREFIX . "mod_captcha_control`",
			true,
			$data,
			false
		);

		// show status message
	$message = [ "status"=>true, "text"=>$oCAPTCHA->language[ "SAVE_DONE" ] ];
	}
}
elseif ( isset( $_POST['validate']))
{
	// add default fields
	$input_fields = [
		'hash'		=> [ 'type' => 'string_clean', 'default' => '' ],
		'captcha'	=> [ 'type' => 'string_clean', 'default' => '' ]
	];

	// set validation
	$valid_fields = $oREQUEST->testPostValues($input_fields);

	// check hash: with invalid hash no save and no info ...
	if (( true === isset( $_SESSION['captcha_control_hash'] )) || ( $_SESSION['captcha_control_hash'] == $_POST[ "hash" ] ))
	{
		unset( $_SESSION[ "captcha_control_hash" ] );
		unset( $_POST[ "hash" ] );

		//validate the captcha
		$message = [ "status"=>true, "text"=>$oCAPTCHA->language[ "VERIFICATION_SUCCEED" ] ];
		if ( false === $oCAPTCHA->test_captcha( false, 0 ))
		{
			$message = [ "status"=>false, "text"=>$oCAPTCHA->language[ "VERIFICATION_FAILED" ] ];
		}
	}
}

// build hash
$sHash = LEPTON_handle::createGUID();
$_SESSION['captcha_control_hash'] = $sHash;

// build field values for twig template
$fieldvalues = [
	"oCAPTCHA_CONTROL"	=> $oCAPTCHA,
	"hash"				=> $sHash,
	"DATA"				=> $data,
	"GENERIC"			=> $oCAPTCHA->get_generic_config( $data ),
	"PLUGIN"			=> $oCAPTCHA->captcha_control( $data ),
	"USEABLE_PLUGINS"	=> $oCAPTCHA->get_useable_plugins( $data ),
	"MESSAGE"			=> $message
];

// choose backend template
$template = "tool.lte";

// test handling
if (( isset( $_POST['run'])) || ( isset( $_POST['test'])) || ( isset( $_POST['validate'])))
{
	// use test template for backend captcha test
	$template = "test.lte";

	// get plugin defaults of current plugin
	$defaults = $oCAPTCHA->get_plugin_defaults( $data );

	// get testing fields
	$input_fields = [
		'action'		=> [ 'type' => 'string_clean', 'default' => "" ],
		'text_attr'		=> [ 'type' => 'string_clean', 'default' => $defaults[ "TEXT_ATTR" ] ],
		'image_attr'	=> [ 'type' => 'string_clean', 'default' => $defaults[ "IMAGE_ATTR" ] ],
		'input_attr'	=> [ 'type' => 'string_clean', 'default' => $defaults[ "INPUT_ATTR" ] ]
	];

	// set validation
	$valid_fields = $oREQUEST->testPostValues($input_fields);

	// get all captcha variations
	$fieldvalues[ "CAPTCHA" ] = $oCAPTCHA->build_captcha( $valid_fields );
	// return input fields
	$fieldvalues[ "TEST" ] = $valid_fields;
	// overwrite hash to avoid save on return
	$fieldvalues[ "sHash" ] = 0;
}

// get Twig-Instance
$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule('captcha_control');

// register plugin specific template path
if ( true === array_key_exists( "TEMPLATEPATH", $fieldvalues[ "PLUGIN" ] ))
{
	$oTWIG->registerPath( $fieldvalues[ "PLUGIN" ][ "TEMPLATEPATH" ], "captcha_plugin");
}

echo $oTWIG->render(
	"@captcha_control/" . $template,
	$fieldvalues
);
