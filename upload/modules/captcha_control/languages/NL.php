<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 12:06, 28-06-2020
 * translated from..: EN
 * translated to....: NL
 * translated using.: www.DeepL.com/Translator
 * jan 2023 met de hand aangepast
 * ==============================================
 */

$module_description	= "Admin-Tool om CAPTCHA en ASP instellingen aan te passen";

$MOD_CAPTCHA_CONTROL	= array(
	"ASP_CONF"				=> "Geavanceerde Spambeveiligings Instellingen",
	"ASP_EXP"				=> "ASP probeert te bepalen of een formulierinvoer afkomstig is van een mens of van een spam-bot.",
	"ASP_TEXT"				=> "Activeer ASP (indien beschikbaar)",
	"CAPTCHA_CONF"			=> "CAPTCHA-configuratie",
	"CAPTCHA_DEPRECATED"	=> "Houd er rekening mee dat deze CAPTCHA functie is gemarkeerd als verouderd en t.z.t. wordt verwijderd.<br />Gebruik het niet langer en schakel over naar een andere captcha!",
	"CAPTCHA_EXP"			=> "CAPTCHA instellingen voor modules bevinden zich in de respectievelijke module-instellingen",
	"CAPTCHA_SPEECH"		=> "CAPTCHA spraak toevoegen",
	"CAPTCHA_SPEECH_PITCH"	=> "Toonhoogte voor de stem",
	"CAPTCHA_SPEECH_RATE"	=> "Spreek sneltheid (hoger = sneller)",
	"CAPTCHA_TYPE"			=> "Type CAPTCHA",
	"DEPRECATED"			=> "Verouderd !",
	"DISABLED"				=> "Uitschakelen",
	"ENABLED"				=> "Ingeschakeld",
	"GENERIC_CONF"			=> "Algemene instellingen voor alle captcha's",
	"HEADING"				=> "Captcha en ASP-instellingen",
	"HOWTO"					=> "Hier kunt u het gedrag van 'CAPTCHA' en 'Advanced Spam Protection' (ASP) wijzigen. Om ASP met een bepaalde module te laten werken, moet deze speciale module worden aangepast om gebruik te kunnen maken van ASP.",
	"NO_CAPTCHA_CONF"		=> "Geen CAPTCHA specifieke instellingen beschikbaar.",
	"OUTPUT"				=> "CAPTCHA gegenereerde output",
	"PARAMETER"				=> "Gebruikte parameters",
	"PLEASE_SAVE"			=> "Sla eerst op na de CAPTCHA type wijziging en pas dan de CAPTCHA instellingen aan (indien beschikbaar).",
	"RELOAD_TYPE"			=> "CAPTCHA herladen met",
	"RELOAD_TYPE_IFRAME"	=> "Klassiek iframe (indien ondersteund door CAPTCHA)",
	"RELOAD_TYPE_JS"		=> "JavaScript (JQUERY in frontend vereist)",
	"RESULT"				=> "CAPTCHA-resultaat",
	"SAVE_DONE"				=> "De configuratie wordt opgeslagen.",
	"TEST"					=> "Test",
	"TEST_ACTION"			=> "Test beschikbare acties",
	"TEST_HEADER"			=> "Uitproberen",
	"TEST_IMAGE_ATTR"		=> "[IMAGE_ATTR] &lt;img> / &lt;span> attributen voor de afbeelding",
	"TEST_INPUT_ATTR"		=> "[INPUT_ATTR] &lt;input> attributen voor antwoorden invoer",
	"TEST_INTRO"			=> "Merk op dat alle onderstaande attribuut instellingen niet worden opgeslagen, maar alleen om u het effect na de set te laten zien.<br />Gebruik ze in uw eigen modules of sjablonen!",
	"TEST_TEXT_ATTR"		=> "[TEXT_ATTR] &lt;span> attributen voor vraagstelling",
	"USE_CAPTCHA"	=> "Activeer CAPTCHA",
	"VERIFICATION_FAILED"	=> "CAPTCHA Verificatie mislukt. Probeer het nog eens!",
	"VERIFICATION_SUCCEED"	=> "CAPTCHA Verificatie geslaagd.",

	"ADD"						=> "plus",
	"CCL_ACTION"				=> "Actie",
	"CCL_ITEM_ALL"				=> "Volledige uitvoer met iFrame voor herlading",
	"CCL_ITEM_ALL_JS"			=> "Volledige uitvoer met JS voor herladen",
	"CCL_ITEM_DATA"				=> "Captcha-uitvoerinformatie als array",
	"CCL_ITEM_IMAGE"			=> "Uitgang Captcha beeld",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Uitgang Captcha als iFrame",
	"CCL_ITEM_INPUT"			=> "Uitgang Captcha Alleen invoerveld",
	"CCL_ITEM_JS"				=> "Uitgang JQUERY JS Code voor herladen",
	"CCL_ITEM_TEXT"				=> "Uitvoer Captcha Invoeraanvraag alleen tekst",
	"DIVIDE"					=> "delen door",
	"ENTER_RESULT"				=> "Vul het resultaat in",
	"MULTIPLY"					=> "maal",
	"SUBTRACT"					=> "min"
);

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the plugin related
 * translation file, whereas the plugin translation has a higher priority and may 
 * overwrite any translation defined here in.
 * However, this translation file here should contain only translations used in all 
 * or various plugins or inside the captcha module itself.
 */
