<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 12:07, 28-06-2020
 * translated from..: EN
 * translated to....: PL
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$module_description	= "Admin-Tool do kontroli CAPTCHA i ASP";

$MOD_CAPTCHA_CONTROL	= array(
	"ASP_CONF"				=> "Zaawansowana Konfiguracja Ochrony Spamu",
	"ASP_EXP"				=> "ASP próbuje ustalić, czy formularz wejściowy pochodzi od człowieka, czy od spam-bota.",
	"ASP_TEXT"				=> "Uruchomić ASP (jeśli jest dostępny)",
	"CAPTCHA_CONF"			=> "Konfiguracja CAPTCHA",
	"CAPTCHA_DEPRECATED"	=> "Proszę zauważyć, że ta CAPTCHA jest oznaczona jako przestarzała i może zostać usunięta w przyszłych wydaniach.<br />Proszę nie używać jej dłużej i przełączyć się na inną captcha!",
	"CAPTCHA_EXP"			=> "Ustawienia CAPTCHA dla modułów znajdują się w odpowiednich ustawieniach modułu",
	"CAPTCHA_SPEECH"		=> "Dodaj CAPTCHA do mowy",
	"CAPTCHA_SPEECH_PITCH"	=> "Skok prędkości dla głosu",
	"CAPTCHA_SPEECH_RATE"	=> "Szybkość mówienia (wyższa = szybsza)",
	"CAPTCHA_TYPE"			=> "Rodzaj CAPTCHA",
	"DEPRECATED"			=> "Deprecated!",
	"DISABLED"				=> "Wyłączone",
	"ENABLED"				=> "Włączony",
	"GENERIC_CONF"			=> "Konfiguracja ogólna dla wszystkich captchas",
	"HEADING"				=> "Kontrola Captcha i ASP",
	"HOWTO"					=> "Tutaj można kontrolować zachowanie 'CAPTCHA' i 'Advanced Spam Protection' (ASP). Aby ASP działał z danym modułem, ten specjalny moduł musi być przystosowany do korzystania z ASP.",
	"NO_CAPTCHA_CONF"		=> "Brak dostępnej specyficznej konfiguracji CAPTCHA.",
	"OUTPUT"				=> "Wygenerowana produkcja CAPTCHA",
	"PARAMETER"				=> "Stosowane parametry",
	"PLEASE_SAVE"			=> "Proszę najpierw zapisać po zmianie typu CAPTCHA, a następnie zmodyfikować konfigurację CAPTCHA (jeśli jest dostępna).",
	"RELOAD_TYPE"			=> "przeładunek CAPTCHA z",
	"RELOAD_TYPE_IFRAME"	=> "Klasyczne iframe (jeśli wspierane przez CAPTCHA)",
	"RELOAD_TYPE_JS"		=> "JavaScript (wymagany JQUERY z przodu)",
	"RESULT"				=> "Wynik CAPTCHA",
	"SAVE_DONE"				=> "Konfiguracja zostaje zapisana.",
	"TEST"					=> "Test",
	"TEST_ACTION"			=> "Przetestuj dostępne akcje",
	"TEST_HEADER"			=> "Plac zabaw",
	"TEST_IMAGE_ATTR"		=> "[IMAGE_ATTR] &lt;img> / &lt;span> atrybuty dla obrazu",
	"TEST_INPUT_ATTR"		=> "[INPUT_ATTR] &lt;input> atrybuty wprowadzania odpowiedzi",
	"TEST_INTRO"			=> "Zwróć uwagę, że żadne z poniższych ustawień atrybutów nie są zapisywane, a jedynie po to, aby pokazać Ci efekt po set.<br />Użyj ich we własnych modułach lub szablonach!",
	"TEST_TEXT_ATTR"		=> "[TEXT_ATTR] &lt;span> atrybuty dla zapytania ofertowego",
	"USE_CAPTCHA"	=> "Uruchomić CAPTCHA",
	"VERIFICATION_FAILED"	=> "Weryfikacja CAPTCHA zakończyła się niepowodzeniem. Proszę spróbować jeszcze raz!",
	"VERIFICATION_SUCCEED"	=> "Weryfikacja CAPTCHA zakończona sukcesem.",

	"ADD"						=> "plus",
	"CCL_ACTION"				=> "Działanie",
	"CCL_ITEM_ALL"				=> "Wyjście kompletne z iFrame do przeładowania",
	"CCL_ITEM_ALL_JS"			=> "Wyjście kompletne z JS do przeładowania",
	"CCL_ITEM_DATA"				=> "Wyjście Captcha Info jako tablica",
	"CCL_ITEM_IMAGE"			=> "Wyjście Obraz Captcha",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Captcha wyjściowa jako iFrame",
	"CCL_ITEM_INPUT"			=> "Wyjście Captcha Tylko pole wejściowe",
	"CCL_ITEM_JS"				=> "Wyjście JQUERY JS Kod do ponownego załadowania",
	"CCL_ITEM_TEXT"				=> "Wyjście Captcha Tylko tekst żądania wejściowego",
	"DIVIDE"					=> "podzielić przez",
	"ENTER_RESULT"				=> "Wypełnij wynik",
	"MULTIPLY"					=> "razy",
	"SUBTRACT"					=> "minus"
);

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the plugin related
 * translation file, whereas the plugin translation has a higher priority and may 
 * overwrite any translation defined here in.
 * However, this translation file here should contain only translations used in all 
 * or various plugins or inside the captcha module itself.
 */
