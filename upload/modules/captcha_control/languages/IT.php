<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 12:05, 28-06-2020
 * translated from..: EN
 * translated to....: IT
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$module_description	= "Admin-Tool per controllare CAPTCHA e ASP";

$MOD_CAPTCHA_CONTROL	= array(
	"ASP_CONF"				=> "Configurazione avanzata della protezione antispam",
	"ASP_EXP"				=> "ASP cerca di determinare se un form-input è stato originato da un umano o da un bot spam.",
	"ASP_TEXT"				=> "Attivare ASP (se disponibile)",
	"CAPTCHA_CONF"			=> "Configurazione CAPTCHA",
	"CAPTCHA_DEPRECATED"	=> "Si prega di notare che questo CAPTCHA è contrassegnato come deprecato e potrebbe essere rimosso in future release.<br />Si prega di non utilizzarlo più e di passare ad un altro CAPTCHA!",
	"CAPTCHA_EXP"			=> "Le impostazioni CAPTCHA per i moduli si trovano nelle rispettive impostazioni del modulo",
	"CAPTCHA_SPEECH"		=> "Aggiungi CAPTCHA al discorso",
	"CAPTCHA_SPEECH_PITCH"	=> "Velocità di passo per la voce",
	"CAPTCHA_SPEECH_RATE"	=> "Velocità da parlare (più alta = più veloce)",
	"CAPTCHA_TYPE"			=> "Tipo di CAPTCHA",
	"DEPRECATED"			=> "Deprecato!",
	"DISABLED"				=> "Disabili",
	"ENABLED"				=> "Abilitato",
	"GENERIC_CONF"			=> "Configurazione generica per tutti i captchas",
	"HEADING"				=> "Controllo Captcha e ASP",
	"HOWTO"					=> "Qui è possibile controllare il comportamento di 'CAPTCHA' e 'Advanced Spam Protection' (ASP). Per far funzionare l'ASP con un dato modulo, questo modulo speciale deve essere adattato per utilizzare l'ASP.",
	"NO_CAPTCHA_CONF"		=> "Nessuna configurazione specifica CAPTCHA disponibile.",
	"OUTPUT"				=> "Uscita generata da CAPTCHA",
	"PARAMETER"				=> "Parametri utilizzati",
	"PLEASE_SAVE"			=> "Si prega di salvare prima dopo la modifica del tipo di CAPTCHA, quindi modificare la configurazione CAPTCHA successivamente (se disponibile).",
	"RELOAD_TYPE"			=> "CAPTCHA ricaricare con",
	"RELOAD_TYPE_IFRAME"	=> "Iframe classico (se supportato da CAPTCHA)",
	"RELOAD_TYPE_JS"		=> "JavaScript (JQUERY in frontend richiesto)",
	"RESULT"				=> "Risultato CAPTCHA",
	"SAVE_DONE"				=> "La configurazione viene salvata.",
	"TEST"					=> "Prova",
	"TEST_ACTION"			=> "Testare le azioni disponibili",
	"TEST_HEADER"			=> "Parco giochi",
	"TEST_IMAGE_ATTR"		=> "[IMAGE_ATTR] [IMMAGINE_ATTR] attributi per l'immagine",
	"TEST_INPUT_ATTR"		=> "[INPUT_ATTR] [INPUT_ATTR] [input> attributi per l'immissione delle risposte",
	"TEST_INTRO"			=> "Si noti che le impostazioni degli attributi qui sotto non vengono salvate, ma solo per mostrare l'effetto dopo set.<br />Utilizzatele nei vostri moduli o modelli!",
	"TEST_TEXT_ATTR"		=> "[TEXT_ATTR] [TEXT_ATTR] [span> attributi per la richiesta di domande",
	"USE_CAPTCHA"	=> "Attivare CAPTCHA",
	"VERIFICATION_FAILED"	=> "Verifica CAPTCHA fallita. Si prega di riprovare!",
	"VERIFICATION_SUCCEED"	=> "CAPTCHA Verifica riuscita.",

	"ADD"						=> "aggiungere",
	"CCL_ACTION"				=> "Azione",
	"CCL_ITEM_ALL"				=> "Uscita completa con iFrame per il ricarico",
	"CCL_ITEM_ALL_JS"			=> "Uscita completa con JS per il ricarico",
	"CCL_ITEM_DATA"				=> "Uscita Captcha Info come array",
	"CCL_ITEM_IMAGE"			=> "Immagine Captcha in uscita",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Uscita Captcha come iFrame",
	"CCL_ITEM_INPUT"			=> "Captcha di uscita Solo campo di ingresso",
	"CCL_ITEM_JS"				=> "Uscita JQUERY JS Codice per la ricarica",
	"CCL_ITEM_TEXT"				=> "Uscita Captcha Testo di richiesta in ingresso solo testo",
	"DIVIDE"					=> "dividere per",
	"ENTER_RESULT"				=> "Compilare il risultato",
	"MULTIPLY"					=> "moltiplicare",
	"SUBTRACT"					=> "sottrarre"
);

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the plugin related
 * translation file, whereas the plugin translation has a higher priority and may 
 * overwrite any translation defined here in.
 * However, this translation file here should contain only translations used in all 
 * or various plugins or inside the captcha module itself.
 */

?>
