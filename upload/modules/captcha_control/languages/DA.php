<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 12:00, 28-06-2020
 * translated from..: EN
 * translated to....: DA
 * translated using.: translate.google.com
 * ==============================================
 */

$module_description	= "Admin-værktøj til at kontrollere CAPTCHA og ASP";

$MOD_CAPTCHA_CONTROL	= array(
	"ASP_CONF"				=> "Avanceret konfiguration af spambeskyttelse",
	"ASP_EXP"				=> "ASP forsøger at bestemme, om et form-input stammer fra et menneske eller en spam-bot.",
	"ASP_TEXT"				=> "Aktivér ASP (hvis tilgængelig)",
	"CAPTCHA_CONF"			=> "CAPTCHA konfigurering",
	"CAPTCHA_DEPRECATED"	=> "Bemærk, at denne CAPTCHA er markeret som forældet og muligvis fjernes i fremtidige udgivelser. # BR # Brug ikke længere den og skift til en anden captcha!",
	"CAPTCHA_EXP"			=> "CAPTCHA-indstillinger for moduler findes i de respektive modulindstillinger",
	"CAPTCHA_SPEECH"		=> "Føj CAPTCHA til talen",
	"CAPTCHA_SPEECH_PITCH"	=> "Hastigheds pitch for stemmen",
	"CAPTCHA_SPEECH_RATE"	=> "Hastighed, der skal tales (højere = hurtigere)",
	"CAPTCHA_TYPE"			=> "Type af CAPTCHA",
	"DEPRECATED"			=> "Udskrevet!",
	"DISABLED"				=> "Deaktiveret",
	"ENABLED"				=> "Aktiveret",
	"GENERIC_CONF"			=> "Generisk konfiguration til alle captchas",
	"HEADING"				=> "Captcha- og ASP-kontrol",
	"HOWTO"					=> "Her kan du kontrollere opførelsen af 'CAPTCHA' og 'Advanced Spam Protection' (ASP). For at få ASP-arbejde med et givet modul skal dette specielle modul tilpasses til at gøre brug af ASP.",
	"NO_CAPTCHA_CONF"		=> "Ingen CAPTCHA-specifik konfiguration tilgængelig.",
	"OUTPUT"				=> "CAPTCHA genereret output",
	"PARAMETER"				=> "Brugte parametre",
	"PLEASE_SAVE"			=> "Gem først efter ændring af CAPTCHA-typen, og rediger derefter CAPTCHA-konfigurationen næste (hvis tilgængelig).",
	"RELOAD_TYPE"			=> "CAPTCHA genindlæses med",
	"RELOAD_TYPE_IFRAME"	=> "Klassisk iframe (hvis understøttet af CAPTCHA)",
	"RELOAD_TYPE_JS"		=> "JavaScript (JQUERY i frontend krævet)",
	"RESULT"				=> "CAPTCHA-resultat",
	"SAVE_DONE"				=> "Konfiguration er gemt.",
	"TEST"					=> "Test",
	"TEST_ACTION"			=> "Test tilgængelige handlinger",
	"TEST_HEADER"			=> "Legeplads",
	"TEST_IMAGE_ATTR"		=> "[IMAGE_ATTR] &lt;img> / &lt;span> attributter til billedet",
	"TEST_INPUT_ATTR"		=> "[INPUT_ATTR] &lt;input> attributter til indtastning af svar",
	"TEST_INTRO"			=> "Bemærk, at nedenstående attributindstillinger ikke gemmes, men bare for at vise effekten efter indstillingen. # BR # Brug dem i dine egne moduler eller skabeloner!",
	"TEST_TEXT_ATTR"		=> "[TEXT_ATTR]  &lt;span> attributter til spørgsmål om anmodning",
	"USE_CAPTCHA"	=> "Aktivér CAPTCHA",
	"VERIFICATION_FAILED"	=> "CAPTCHA-verifikation mislykkedes. Prøv igen",
	"VERIFICATION_SUCCEED"	=> "CAPTCHA Bekræftelse succesfuld.",

	"ADD"						=> "tilføj",
	"CCL_ACTION"				=> "Handling",
	"CCL_ITEM_ALL"				=> "Komplet output med iFrame til genindlæsning",
	"CCL_ITEM_ALL_JS"			=> "Komplet output med JS til genindlæsning",
	"CCL_ITEM_DATA"				=> "Output Captcha Info som matrix",
	"CCL_ITEM_IMAGE"			=> "Output Captcha-billede",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Output Captcha som iFrame",
	"CCL_ITEM_INPUT"			=> "Output Captcha Input felt",
	"CCL_ITEM_JS"				=> "Output JQUERY JS-kode til genindlæsning",
	"CCL_ITEM_TEXT"				=> "Output Captcha Input-anmodningstekst",
	"DIVIDE"					=> "divider med",
	"ENTER_RESULT"				=> "Udfyld resultatet",
	"MULTIPLY"					=> "formere sig",
	"SUBTRACT"					=> "trække fra"
);

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the plugin related
 * translation file, whereas the plugin translation has a higher priority and may 
 * overwrite any translation defined here in.
 * However, this translation file here should contain only translations used in all 
 * or various plugins or inside the captcha module itself.
 */
