<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


//This script is called via ajax from all_js.lte

// init output
$json_return = [ "status" => false ];

// do input validation
$oREQUEST = LEPTON_request::getInstance();

$input_fields = [
	"action"		=> [ "type" => "string_clean", "default" => "" ],
	"section_id"	=> [ "type" => "integer+", "default" => 0 ]
];
$valid_fields = $oREQUEST->testPostValues($input_fields);

// process only if section_id is available
if ( $valid_fields[ "section_id" ] >= 0 )
{
	switch( $valid_fields[ "action" ] )
	{
		case "title":
			$json_return[ "title" ] = $_SESSION[ "captcharequest" . $valid_fields[ "section_id" ] ][ "SPEECH"];
			$json_return[ "status" ]  = true;
			break;

		case "reload":
			// get captcha instance
			$oCAPTCHA = captcha_control::getInstance();

			// recalculate
			$params = array( "SECTION_ID" => $valid_fields[ "section_id" ] );
			$json_return[ "image" ] = $oCAPTCHA->calculate_captcha( $params );

			// reset title
			$json_return[ "title" ] = $_SESSION[ "captcharequest" . $valid_fields[ "section_id" ] ][ "SPEECH"];
			$json_return[ "status" ]  = true;
			break;
	}
}

// return
echo json_encode( $json_return );

