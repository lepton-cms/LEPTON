<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// Get lists of fonts and choose a random one
function get_font( $dir )
{
	$lookups = array();

	// check on input DIR
	if ( false === empty( $dir ))
    {
        $lookups[] = $dir . "/fonts/";
    }

	// Check on LEPTON-CMS (since L* V)
	$lookups[] = LEPTON_PATH . "/modules/lib_lepton/fonts/";

	// loop on path till first found, than return list of content
	$fonts = array();
	foreach( $lookups as $path )
	{
		if ( is_dir( $path ))
		{
			$fonts = glob( $path . "*.ttf" );
			break;
		}
	}

	// prepare return
	$font = "";
	if ( count( $fonts ) > 1 )
    {
        $font = $fonts[ array_rand( $fonts ) ];
    }
	elseif ( count( $fonts ) == 1 )
	{
	    $font = $fonts[ 0 ];
	}

	return $font;
}
