<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha_control
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2010-2025 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		https://gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// Get lists of backgrounds and choose a random one
function get_background( $page_id, $section_id, $dir )
{
	global $database;

	$lookups = array();

	// check on the actual frontend template.
	$template = "";
	if ( $page_id > 0 )
	{
		$template = $database->get_one("SELECT `template`"
											. " from `".TABLE_PREFIX."pages`"
											. " where `page_id`= '" . $_SESSION['PAGE_ID'] . "'");
		if ( false === empty( $template ))
			{ $lookups[] = LEPTON_PATH . "/templates/" . $template . "/frontend/captcha_control/backgrounds/"; }
	}
	
	// check on default template
	if ( $template != DEFAULT_TEMPLATE )
		{ $lookups[] = LEPTON_PATH . "/templates/" . DEFAULT_TEMPLATE . "/frontend/captcha_control/backgrounds/"; }

	// check on input DIR
	if ( false === empty( $dir ))
		{ $lookups[] = $dir . "/backgrounds/"; }

	// check on module DIR
	$lookups[] = LEPTON_PATH . "/modules/captcha_control/functions/backgrounds/";

	// loop on path till first found, than return list of content
	$backgrounds = array();
	foreach( $lookups as $path )
	{
		if ( is_dir( $path ))
		{
			$backgrounds = glob( $path . "*.png" );
			break;
		}
	}

	// prepare return
	$background = "";
	if ( count( $backgrounds ) > 1 )
		{ $background = $backgrounds[ array_rand( $backgrounds ) ]; }
	elseif ( count( $backgrounds ) == 1 )
		{ $background = $backgrounds[ 0 ]; }

	return $background;
}

