<?php

/**
 *  @module         TinyMCE
 *  @version        see info.php of this module
 *  @authors        erpe, Dietrich Roland Pehlke (Aldus)
 *  @copyright      2010-2025 erpe, Dietrich Roland Pehlke (Aldus)
 *  @license        GPLv2+ License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *  Please note: TINYMCE is distributed under the <a href="https://github.com/tinymce/tinymce/blob/develop/LICENSE.TXT">MIT License</a> 
 *
 *
 */
 
class tinymce_settings_custom extends tinymce_settings
{
    public $default_skin    = "oxide-dark";
    public $default_height  = "250";
    public $default_toolbar = "Simple";
    // public $default_content_css = "document";

    /**
     * Own example text
     * @var string
     */
    public $content = "This text comes from the tinymce_settings_custom-class as a property\n";
    
    public function initialize() 
    {
        parent::initialize();
        // add own definition to current settings
        $this->toolbars["Smart or not"] = "undo redo | image mailto | pagelink droplets";

        $this->content .= "<p><img style='margin: 0 auto;display: block;' src='".LEPTON_URL."/modules/tinymce/icon.png' /></p>";
    }
}	
