<?php

/**
 *  @module         TinyMCE
 *  @version        see info.php of this module
 *  @authors        erpe, Dietrich Roland Pehlke (Aldus)
 *  @copyright      2010-2025 erpe, Dietrich Roland Pehlke (Aldus)
 *  @license        GPLv2+ License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *  Please note: TINYMCE is distributed under the <a href="https://github.com/tinymce/tinymce/blob/main/LICENSE.md">GPLv2+ License</a> 
 *	Download: https://www.tiny.cloud/get-tiny/   or https://www.tiny.cloud/get-tiny/self-hosted/
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$module_directory     = 'tinymce';
$module_name          = 'TinyMCE';
$module_function      = 'WYSIWYG';
$module_version       = '7.7.1.0';
$module_platform      = '7.x';
$module_author        = 'erpe, Aldus';
$module_home          = 'https://lepton-cms.org';
$module_guid          = '0ad7e8dd-2f6b-4525-b4bf-db326b0f5ae8';
$module_license       = 'GNU General Public License, TINYMCE is GPLv2+ license';
$module_license_terms  = '-';
$module_description   = '<a href="https://www.tiny.cloud" target="_blank">Current TinyMCE </a>allows you to edit the content<br />of a page and see media image folder.
						<br />Please keep in mind that there are <a href="'.LEPTON_URL.'/modules/tinymce/tinymce/TinyMCE-kbd-shortcuts-rev1701.pdf" target="_blank"><b>shortcuts</b></a> to use tinymce';
