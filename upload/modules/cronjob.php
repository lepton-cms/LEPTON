<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */ 

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$oLCJ = LEPTON_cronjob::getInstance();

// check internal calls
if(isset($_POST['ikey']) && $_POST['ikey'] == $oLCJ->cj_key)
{
	LEPTON_handle::include_files($oLCJ->cj_path.$oLCJ->cj_file);
}
elseif(isset($_GET['key']) && $_GET['key'] == $oLCJ->cj_key)
{
	$_POST['ekey'] = $oLCJ->cj_key;
	LEPTON_handle::include_files($oLCJ->cj_path.$oLCJ->cj_file);
}
unset($_POST['ikey'],$_POST['ekey']);
unset($_GET['key']);