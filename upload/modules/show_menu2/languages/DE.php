<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          show_menu2
 * @author          Brofield, LEPTON Project
 * @copyright       2006-2010 Brofield
 * @copyright       2010-2025 LEPTON Project
 * @link            https://doc.lepton-cms.org/sm2/
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

 

// Deutsche Modulbeschreibung
$module_description = 'Dieses Modul integriert die erweiterte Menüfunktion <code>show_menu2()</code> in LEPTON. Detailierte Informationen gibt es in der  <a href="https://doc.lepton-cms.org/documentation/sm2/" target="_blank">Dokumentation</a> oder in der <a href="' .LEPTON_URL .'/modules/show_menu2/README.de.txt" target="_blank">readme</a> Datei.';
