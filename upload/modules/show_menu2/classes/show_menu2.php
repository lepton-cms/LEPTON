<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          show_menu2
 * @author          Brofield, LEPTON Project
 * @copyright       2006-2010 Brofield
 * @copyright       2010-2025 LEPTON Project
 * @link            https://doc.lepton-cms.org/sm2/
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */

class show_menu2 extends LEPTON_abstract
{
    //  Own instance of this class
    static $instance = NULL;
    
    //  Public 
    public int $aMenu       = 1;
    public int $aStart      = SM2_ROOT;
    public int $aMaxLevel   = SM2_ALL;
    public int $aOptions    = SM2_ALL|SM2_PRETTY|SM2_BUFFER;
    public string $aItemOpen   = '<li><a href="[url]" class="[class]" target="[target]">[menu_title]</a>';
    public string $aItemClose  = '</li>';
    public string $aMenuOpen   = '<ul>';
    public string $aMenuClose  = '</ul>';
    public string $aTopItemOpen    = "";
    public string $aTopMenuOpen    = "";
            
    //  Called by first time by getInstance()
    public function initialize()
    {
    
    }
    
    public function getMenu()
    {
        return show_menu2(
            $this->aMenu,
            $this->aStart,
            $this->aMaxLevel,
            $this->aOptions,
            $this->aItemOpen,
            $this->aItemClose,
            $this->aMenuOpen,
            $this->aMenuClose,
            $this->aTopItemOpen,
            $this->aTopMenuOpen
        );
    }
}
