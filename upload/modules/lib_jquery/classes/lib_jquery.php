<?php

declare(strict_types=1);

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see info.php of this module
 *
 */



class lib_jquery extends LEPTON_abstract {

	public static $instance;

	public function initialize()
	{
		
	}
	
	public static function SetJQueryLanguage($widget = 'datepicker')
	{
		$sTempLookup = LEPTON_PATH.'/modules/lib_jquery/jquery-ui/i18n/'.$widget.'-';
		if(file_exists($sTempLookup.strtolower(DEFAULT_LANGUAGE).'.js'))
		{
			return(strtolower(DEFAULT_LANGUAGE));
		}
		else
		{
			return 'en';
		}
		
	}

}
