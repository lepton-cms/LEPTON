<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */ 

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// Get page id
if(isset($_GET['page_id']))
{
	$page_id = LEPTON_core::getValue('page_id','integer','get');
} 
elseif(isset($_POST['page_id']))
{
	$page_id = LEPTON_core::getValue('page_id','integer','post');
}
else 
{
	header("Location: index.php");
	exit(0);
}

// Get section id if there is one
if(isset($_GET['section_id']))
{
	$section_id = LEPTON_core::getValue('section_id','integer','get');
} 
elseif(isset($_POST['section_id']))
{
	$section_id = LEPTON_core::getValue('section_id','integer','post');
} 
else 
{	// @CHECK 20240225 -> unclear what this should do???
	// Check if we should redirect the user if there is no section id
	if(!isset($section_required))
	{
		$section_id = 0;
	} 
	else 
	{
		header("Location: $section_required");
		exit(0);
	}
}

// Create js back link
$js_back = 'javascript: history.go(-1);';

// Create new LEPTON_admin object, header will be set here, see database->is_error
$admin = LEPTON_admin::getInstance('Pages', 'pages_modify');

// Get perms
$sAdminGroup = $database->get_one("SELECT `admin_groups` FROM `".TABLE_PREFIX."pages` WHERE `page_id`= ".$page_id);

$admin_groups = explode(',', $sAdminGroup);
$in_group = false;

foreach($admin->getValue('groups_id', 'string', 'session',',') as $sCurrentGroupId)
{
    if (in_array($sCurrentGroupId, $admin_groups))
    {
        $in_group = true;
    }
}

if($in_group == false)
{
    $admin->print_error($MESSAGE['PAGES_INSUFFICIENT_PERMISSIONS']);
}

// some additional security checks: check if the section_id belongs to the page_id at all
if ($section_id != 0)
{
	$sections = [];
	$res_sec = $database->execute_query(
		"SELECT `module` FROM `".TABLE_PREFIX."sections` WHERE `page_id` = ".$page_id." AND `section_id` = ".$section_id,
		true,
		$sections,
		true
	);

	if (empty($sections))
	{
		$admin->print_error($MESSAGE['PAGES_NOT_FOUND']);
	}

    $bHasAdminPrivilegs = LEPTON_admin::userHasAdminRights();
    
    if(false === $bHasAdminPrivilegs)
    {
        /**	****************************
         *	[1] Check module permissions
         *	As we can get more than one section/module on
         *	this page we will have to check them all! NOT only the first one!
         */
        $tested_modules = [];
        $failed = 0;
        foreach($sections as $sec)
        {
            if(!in_array($sec['module'], $tested_modules))
            {    
                $tested_modules[] = $sec['module'];
                        
                if (!$admin->get_permission($sec['module'], 'module'))
                {
                    $failed++;
                }
            }
        }

        /**
         *	All used modules "failed" - so we have no permission to the page at all!
         */
        if($failed == count($tested_modules))
        {
            $admin->print_error($MESSAGE['PAGES_INSUFFICIENT_PERMISSIONS']." [679]");
        }
    }
}

// Work-out if the developer wants us to update the timestamp for when the page was last modified
if(isset($update_when_modified) && $update_when_modified == true)
{
	$fields = array(
		'modified_when'	=> time(),
		'modified_by'	=> $admin->getValue('user_id', 'integer', 'session')
	);
	
	$result = $database->build_and_execute(
		'update',
		TABLE_PREFIX.'pages',
		$fields,
		'`page_id`= '.$page_id
	);
}
