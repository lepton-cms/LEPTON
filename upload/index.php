<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Org. e.V.
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */ 


define('DEBUG', true);
define("FRONTEND", true);

// Include config file
$config_file = dirname(__FILE__).'/config/config.php';

if(file_exists($config_file))
{
    require_once($config_file);

} 
else 
{
    /**
     *    No config file, so we run the installer
     *
     *    Anmerkung:  HTTP/1.1 verlangt einen absoluten URI inklusive dem Schema,
     *    Hostnamen und absoluten Pfad als Argument von Location:, manche, aber nicht alle
     *    Clients akzeptieren jedoch auch relative URIs.
     */
    $host       = $_SERVER['HTTP_HOST'];
    $uri        = rtrim(dirname($_SERVER['SCRIPT_NAME']), '/\\');
    $file       = 'install/index.php';
    $target_url = 'http://'.$host.$uri.'/'.$file;
    header('Location: '.$target_url);
    exit();    // make sure that the code below will not be executed
}

// Create new frontend object
$oLEPTON = LEPTON_frontend::getInstance();

// Figure out which page to display
$oLEPTON->page_select();

// Collect info about the currently viewed page
// and check permissions
$oLEPTON->get_page_details();

// Collect general website settings
$oLEPTON->get_website_settings();

// Get page-content in buffer for Droplets
ob_start();
    require LEPTON_PATH . '/templates/' . TEMPLATE . '/index.php';
    $templateOutput = ob_get_clean();

// Replace all [LEPTONlinkxxx] with real, internal links
$oLEPTON->preprocess($templateOutput);

// Load Droplet engine and process
LEPTON_handle::include_files ('/modules/droplets/droplets.php');
evalDroplets($templateOutput);

// CSRF protection - add tokens to internal links
if ($oLEPTON->is_authenticated())
{
	LEPTON_core::getInstance()->getProtectedFunctions($templateOutput, $oLEPTON);
}

echo $templateOutput;
