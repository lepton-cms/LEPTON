<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


ob_start();
global $TEXT;

header( "Cache-Control: no-cache, must-revalidate" );
header( "Pragma: no-cache" );
header( "Content-Type: text/html; charset:utf-8;" );

if (LEPTON_core::userHasAdminRights() == false)
{
    echo "<div class='ui negative  message'>You're not allowed to use this function!</div>";
    exit;
}

ob_clean();

// Send the test-mail
$mail = LEPTON_mailer::getInstance();
//$mail->SMTPDebug = 3;
$mail->CharSet = DEFAULT_CHARSET;	
$mail->setFrom(SERVER_EMAIL, 'System');
$mail->addAddress(SERVER_EMAIL, 'System');
$mail->Subject = 'LEPTON MAILER';
$mail->msgHTML($TEXT['MAILER_TESTMAIL_TEXT']);

if (!$mail->send()) 
{
    echo "<div class='ui negative  message'>".$TEXT['MAILER_TESTMAIL_FAILED']."<br /> ".$mail->ErrorInfo."<br /></div>";
}
else 
{
    echo "<div class='ui positive message'>".$TEXT['MAILER_TESTMAIL_SUCCESS']."</div>";
}
