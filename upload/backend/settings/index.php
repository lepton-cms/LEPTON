<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 * 
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// get classes
$oTWIG = lib_twig_box::getInstance();
$admin = LEPTON_admin::getInstance();

if ($admin->userHasAdminRights() == false)
{
    // prevent users to access url directly
    if(!in_array('settings',$_SESSION['SYSTEM_PERMISSIONS']))  
    {
	header("Location: ".ADMIN_URL."");
	exit(0);
    }
}

// enable custom files
if (LEPTON_handle::require_alternative('settings/index.php'))
{
    return 0;
}
	
// check if current user is admin
$curr_user_is_admin = ( in_array( 1, $admin->getValue('groups_id', 'string', 'session',',') ) );

$all_settings = [];
$database->execute_query(
	"SELECT `name`,`value` from `".TABLE_PREFIX."settings`",
	true,
	$all_settings,
	true
);

$settings = [];
foreach($all_settings as &$ref)
{
    $settings[ $ref['name'] ] = $ref['value'];
}

//	get an instance from LEPTON_basics as we "call" this more than once
$oDATE = LEPTON_date::getInstance();
	
$page_values = [
	'FORM_NAME'	=> 'settings',	
	'ACTION_URL'	=> ADMIN_URL.'/settings/save.php',
	'leptoken'      => get_leptoken(),
	'error_levels'	=> LEPTON_basics::get_errorlevels(),
	'timezones'     => $oDATE->get_timezones(),
	'date_formats'	=> $oDATE->get_dateformats(),	
	'time_formats'	=> $oDATE->get_timeformats(),
    'languages'     => [],
    'editors'       => [],
    'templates'     => [],
    'themes'        => [],
    'groups'        => []
];

//	[2.0] db fields of settings
foreach ($settings as $key => $value)
{
    $page_values[strtoupper($key)] = $value;
}

//	[2.1] Languages
$database->execute_query(
	"SELECT `name`,`directory` FROM `" . TABLE_PREFIX . "addons` WHERE `type` = 'language' ORDER BY `name`",
	true,
	$page_values['languages'],
	true
);
	
//	[2.2] installed editors
$database->execute_query(
	"SELECT `name`,`directory` FROM `" . TABLE_PREFIX . "addons` WHERE `type` = 'module' AND `function`='wysiwyg' ORDER BY `name`",
	true,
	$page_values['editors'],
	true
);

//	[2.3.1] template list
$database->execute_query(
	"SELECT `name`,`directory` FROM `" . TABLE_PREFIX . "addons` WHERE `type` = 'template' AND `function` != 'theme' ORDER BY `name`",
	true,
	$page_values['templates'],
	true
);

//	[2.3.2] backend theme list
$database->execute_query(
	"SELECT `name`,`directory` FROM `" . TABLE_PREFIX . "addons` WHERE `type` = 'template' AND `function` = 'theme' ORDER BY `name`",
	true,
	$page_values['themes'],
	true
);

//	[2.4.0] search table
$temp_search_values = [];
$database->execute_query(
	"SELECT `name`, `value` FROM `".TABLE_PREFIX."search` WHERE `extra` = '' ",
	true,
	$temp_search_values,
	true
);

$search_settings = [];
foreach($temp_search_values as $ref)
{
	$search_settings[ $ref['name'] ] = $ref['value'];
}

$page_values['search'] = $search_settings;
	
//	[2.5.0] groups
$database->execute_query(
	"SELECT `group_id`,`name` FROM `" . TABLE_PREFIX . "groups` WHERE `group_id` > 1 ORDER BY `name`",
	true,
	$page_values['groups'],
	true
);

//	[2.6.0] decrypt mailer values
$bCheckModul = extension_loaded('openssl');
if($bCheckModul === true)
{	
	if($page_values['MAILER_SMTP_HOST'] != '')
	{
		$tempResult = LEPTON_database::decryptstring($page_values['MAILER_SMTP_HOST']);
		if (!is_bool($tempResult))
		{
			$page_values['MAILER_SMTP_HOST'] = $tempResult;
		}		
	}
	
	if($page_values['MAILER_SMTP_USERNAME'] != '')
	{
		$tempResult = LEPTON_database::decryptstring($page_values['MAILER_SMTP_USERNAME']);
		if (!is_bool($tempResult))
		{
			$page_values['MAILER_SMTP_USERNAME'] = $tempResult;
		}		
	}	

	if($page_values['MAILER_SMTP_PASSWORD'] != '')
	{
		$tempResult = LEPTON_database::decryptstring($page_values['MAILER_SMTP_PASSWORD']);
		if (!is_bool($tempResult))
		{
			$page_values['MAILER_SMTP_PASSWORD'] = $tempResult;
		}		
	}
}

echo $oTWIG->render(
	"@theme/settings.lte",
	$page_values
);

$admin->print_footer();
