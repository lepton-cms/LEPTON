<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// Initial page
if ((class_exists("initial_page", true)) && (isset($_SESSION['USER_ID'])) && (!isset($_GET['CallByIcon'])))
{
    $oInitPage = initial_page::getInstance();
    $oInitPage->execute($_SESSION['USER_ID'], $_SERVER['SCRIPT_NAME']);
}

$database = LEPTON_database::getInstance();
$admin = LEPTON_admin::getInstance();
$oTWIG = lib_twig_box::getInstance();

//	Preload the theme languages
// @DEPRECATED_TEMP 20250117 -> obsolete??
//LEPTON_basics::getInstance();

// get some infos and current release no
$is_uptodate = true;
$info = "";
$version = "";
$value = parse_ini_file(LEPTON_PATH . '/config/lepton.ini.php');
if ($value['pass'] != 'usbw')
{	
	$info = "https://lepton-cms.org/version_info.txt";
	$version = file_get_contents("https://lepton-cms.org/version.txt", false);
	// check version for updates if possible
	if ($version !== false)
	{
		$is_uptodate = (version_compare( $version, LEPTON_VERSION,  ">" )) ? false : true;
	}
}

// check if openssl is installed
if (defined( 'OPENSSL_VERSION_NUMBER'))
{
	$openssl_version = OPENSSL_VERSION_NUMBER;
}
else
{
	$openssl_version = 'not installed';
}


// enable custom files
if(LEPTON_handle::require_alternative('start/index.php')) return 0;

// get pages and sections info
$pages = [];
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."pages` ORDER BY `modified_when` DESC ",
	true,
	$pages,
	true
);

$last_pmodified = date("d.m.Y - H:i", $pages[0]['modified_when']);
$page_link_fe = LEPTON_URL.PAGES_DIRECTORY.$pages[0]['link'].PAGE_EXTENSION;
$page_link_be = ADMIN_URL.'/pages/modify.php?page_id='.$pages[0]['page_id'];

$count_pages = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."pages`");
$count_section = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."sections` ");
$count_sections = ($count_section -1);

// get addons info
$count_modules = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."addons` WHERE `type`='module' ");
$count_templates = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."addons` WHERE `type`='template' ");
$count_languages = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."addons` WHERE `type`='language' ");

// get users and groups info
$count_users = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."users` ");
$count_groups = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."groups` ");

$install = 0;	
if ( file_exists(LEPTON_PATH.'/install/')) 
{
	$install = 1;
}

$page_values = [
	'count_sections'=> $count_sections,
	'count_pages'	=> $count_pages,
	'count_modules' => $count_modules,
	'count_templates'=> $count_templates,
	'count_languages'=> $count_languages,
	'count_users' 	=> $count_users,
	'count_groups' 	=> $count_groups,	
	'last_pmodified'=> $last_pmodified,
	'lepton_link' 	=> 'https://lepton-cms.org',
	'page_link_fe' 	=> $page_link_fe,
	'page_link_be' 	=> $page_link_be,	
	'current_release'=> $version,
	'is_uptodate' 	=> $is_uptodate,
	'version_info' 	=> $info,
	'php_version'	=> PHP_VERSION,
	'openssl_version'	=> $openssl_version,
	'install'		=> $install,
	'THEME'			=> $THEME
];

echo $oTWIG->render(
	"@theme/start.lte",
	$page_values
);

$admin->print_footer();
