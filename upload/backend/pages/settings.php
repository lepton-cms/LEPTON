<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// Get page id
if(!isset($_GET['page_id']) OR !is_numeric($_GET['page_id']))
{
	header("Location: index.php");
	exit(0);
} 
else 
{
	$page_id = intval($_GET['page_id']);
}

// enable custom files
if(LEPTON_handle::require_alternative('pages/settings.php')) return 0;

$admin = LEPTON_admin::getInstance('Pages', 'pages_settings');

// Get perms and page_details
$aCurrentPageInfo = [];
$database->execute_query(
	'SELECT * FROM `'.TABLE_PREFIX.'pages` WHERE `page_id` = '.$page_id,
	true,
	$aCurrentPageInfo,
	false
);

if (empty($aCurrentPageInfo))
{
	$admin->print_error($MESSAGE['PAGES_NOT_FOUND']);
}

$admin_groups = explode(',', $aCurrentPageInfo['admin_groups']);
$in_group = false;

foreach ($admin->getValue('groups_id', 'string', 'session',',') as $sCurrentGroupId)
{
    if (in_array($sCurrentGroupId, $admin_groups))
    {
        $in_group = true;
    }
}

if($in_group == false)
{
    $admin->print_error($MESSAGE['PAGES_INSUFFICIENT_PERMISSIONS']);
}

// Get display name of person who last modified the page
$user = LEPTON_admin::get_user_details($aCurrentPageInfo['modified_by']);

// Convert the unix ts for modified_when to human a readable form
$modified_ts = ($aCurrentPageInfo['modified_when'] != 0)
	? date(TIME_FORMAT.', '.DATE_FORMAT, $aCurrentPageInfo['modified_when'])
	: 'Unknown'	;


$lepton_core_all_groups = [];
$database->execute_query(
	'SELECT * FROM `'.TABLE_PREFIX.'groups`',
	true,
	$lepton_core_all_groups
);


//	Get all pages as (array-) tree
LEPTON_handle::register('page_tree');

//	Storage for all infos in an array
$all_pages = [];

//	Determinate what fields/keys we want to get in our 'page_tree'-array
$fields = ['page_id','page_title','menu_title','parent','position','visibility','link', 'admin_groups'];

//	Get the tree here
page_tree( 0, $all_pages, $fields );

// Get all installed languages
$all_languages = [];
$database->execute_query(
	'SELECT `directory`,`name` FROM `'.TABLE_PREFIX.'addons` WHERE `type` = "language" ORDER BY `name`',
	true,
	$all_languages
);

// Get all installed templates
$all_templates = [];
$database->execute_query(
	'SELECT `directory`,`name`,`function` FROM `'.TABLE_PREFIX.'addons` WHERE `type` = "template" AND (`function` = "template" OR `function`="") order by `name`',
	true,
	$all_templates
);

// Try to get the correct Menu
$temp = ($aCurrentPageInfo['template'] == "") ? DEFAULT_TEMPLATE : $aCurrentPageInfo['template'];
$temp_path = LEPTON_PATH."/templates/".$temp."/info.php";
require_once $temp_path;
$all_menus = (isset($menu) ? $menu : [] );

$page_values = [
	'PAGE_ID' => $page_id,
	'PAGE_TITLE' => $aCurrentPageInfo['page_title'],
	'MENU_TITLE' => $aCurrentPageInfo['menu_title'],
	'PAGE_LINK' => substr($aCurrentPageInfo['link'],strripos($aCurrentPageInfo['link'],'/')+1),
	'PAGE_EXTENSION' => PAGE_EXTENSION,
	'PAGE_LANGUAGE' => $aCurrentPageInfo['language'],
	'LANGUAGE' => LANGUAGE,
	'DESCRIPTION' => $aCurrentPageInfo['description'],
	'KEYWORDS' => $aCurrentPageInfo['keywords'],
    'PAGE_CODE' => $aCurrentPageInfo['page_code'],
	'MODIFIED_BY' => $user['display_name'],
	'MODIFIED_BY_USERNAME' => $user['username'],
	'MODIFIED_WHEN' => $modified_ts,
	'ADMIN_URL' => ADMIN_URL,
	'LEPTON_URL' => LEPTON_URL,
	'LEPTON_PATH' => LEPTON_PATH,
	'THEME_URL' => THEME_URL,
	//	Additions
	'all_groups' => $lepton_core_all_groups,
	'all_pages'	=> $all_pages,
	'all_languages' => $all_languages,
	'all_templates' => $all_templates,
	'all_menus'		=> $all_menus,
	'leptoken'		=> get_leptoken(),
	'PAGE_PARENT'	=> $aCurrentPageInfo['parent'],
	'page_values'	=> $aCurrentPageInfo,
	'PAGE_LANGUAGES'	=> PAGE_LANGUAGES,
    'permissions'           => 
		[
            'p_page_modify' => ($admin->getUserPermission("pages_modify") ? 1 : 0),
            'p_page_delete' => ($admin->getUserPermission("pages_delete") ? 1 : 0),
            'p_page_add'    => ($admin->getUserPermission("pages_add")    ? 1 : 0),
            'p_pages_settings'  => ($admin->getUserPermission('pages_settings') ? 1 : 0)
        ],
     'user_groups'       => LEPTON_core::getValue("groups_id", "string", "session", ",")
];

$oTWIG = lib_twig_box::getInstance();

echo $oTWIG->render(
	'@theme/pages_settings.lte',
	$page_values
);

$admin->print_footer();
