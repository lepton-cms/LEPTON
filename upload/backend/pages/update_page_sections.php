<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

header('Content-Type: application/javascript');

// [1.1]
$posted_sections = filter_input(INPUT_POST, "sections", FILTER_SANITIZE_SPECIAL_CHARS);
if (is_null($posted_sections))
{
    die("E1");
}

// [1.2]
$posted_sig = filter_input(INPUT_POST, "sig", FILTER_SANITIZE_SPECIAL_CHARS);
if (is_null($posted_sig))
{
    die("E0");
} else {
    $secHash = $_SESSION['section_reorder_hash'];
    unset($_SESSION['section_reorder_hash']);
    if ($secHash !== $posted_sig)
    {
        die("E4");
    } else {
        //  [4] Hash
        LEPTON_handle::register("random_string");
        $sSignature = random_string(36);
        //  [4.1] Hash in session
        $_SESSION['section_reorder_hash'] = $sSignature;
    }
}

// [2]
$sSectionList = trim($posted_sections);
if ($sSectionList == "")
{
    die("E2");
}

$aSections = explode(",", $sSectionList);
$aNewList = [];

foreach ($aSections as $ref)
{
    if ($ref != "")
    {
        $aNewList[] = intval($ref);
    }
}

if (empty($aNewList))
{
    return "Error [2]: no Items in list.";
}

// [3]
$database = LEPTON_database::getInstance();
$position = 1;
foreach ($aNewList as $section_id)
{
    $fields = ['position' => $position++];
    
    $database->build_and_execute(
        'update',
        TABLE_PREFIX."sections",
        $fields,
        "`section_id`=".$section_id
    );
}

$MESSAGE = LEPTON_core::getGlobal("MESSAGE");
echo $MESSAGE['PAGES_SECTIONS_REORDERED'].json_encode( $aNewList ).".\n".$sSignature;
