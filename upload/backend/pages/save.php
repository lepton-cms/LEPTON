<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure file


// Get page & section id
if(!isset($_POST['page_id']) OR !is_numeric($_POST['page_id'])) 
{
	header("Location: index.php");
	exit(0);
} 
else 
{
	$page_id = intval($_POST['page_id']);
}

if(!isset($_POST['section_id']) OR !is_numeric($_POST['section_id'])) 
{
	header("Location: index.php");
	exit(0);
}
else 
{
	$section_id = intval($_POST['section_id']);
}


$admin = LEPTON_admin::getInstance('Pages', 'pages_modify');
$database = LEPTON_database::getInstance();

// Get perms
$aCurrentPageInfo = [];
$database->execute_query(
	"SELECT `admin_groups` FROM `".TABLE_PREFIX."pages` WHERE `page_id` = ".$page_id,
	true,
	$aCurrentPageInfo,
	true
);

$admin_groups = explode(',', $aCurrentPageInfo['admin_groups']);
$in_group = false;

foreach($admin->getValue('groups_id', 'string', 'session',',') as $sCurrentGroupId)
{
    if (in_array($sCurrentGroupId, $admin_groups))
    {
        $in_group = true;
    }
}

if($in_group == false)
{
    $admin->print_error($MESSAGE['PAGES_INSUFFICIENT_PERMISSIONS']);
}

// Get page module
$module = $database->get_one("SELECT `module` FROM `".TABLE_PREFIX."sections` WHERE `page_id`=".$page_id." AND `section_id`=".$section_id." ");

if(!$module)
{
	$admin->print_error( $database->is_error() ? $database->get_error() : $MESSAGE['PAGES_NOT_FOUND']);
}

// Update the pages table
$database->simple_query("'UPDATE `".TABLE_PREFIX."pages` SET `modified_when` = ".time().", `modified_by` = ".$admin->getValue('user_id', 'integer', 'session')." WHERE `page_id` = ".$page_id);

LEPTON_handle::include_files ('/modules/'.$module.'/save.php');

$admin->print_success($MESSAGE['PAGES_SAVED'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);

