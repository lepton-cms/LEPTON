<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// Get page id
if(!isset($_GET['page_id']) OR !is_numeric($_GET['page_id'])) 
{
	header("Location: index.php");
	exit(0);
} 
else 
{
	$page_id = $_GET['page_id'];
}

$admin = LEPTON_admin::getInstance('Pages', 'pages_delete');

// Find out more about the page
$aCurrentPageInfo = [];
$database->execute_query(
    "SELECT * FROM `".TABLE_PREFIX."pages` WHERE `page_id` = ".$page_id,
    true,
    $aCurrentPageInfo,
    false
);

if(empty($aCurrentPageInfo))
{
	$admin->print_error($MESSAGE['PAGES_NOT_FOUND']);
}

$admin_groups = explode(',', $aCurrentPageInfo['admin_groups']);

$in_group = false;
foreach($admin->getValue('groups_id', 'string', 'session',',') as $sCurrentGroupId)
{
    if (in_array($sCurrentGroupId, $admin_groups))
    {
        $in_group = true;
    }
}

if($in_group == false)
{
    $admin->print_error($MESSAGE['PAGES_INSUFFICIENT_PERMISSIONS']);
}

//  [1] Function to change all child pages visibility to deleted
function restore_subs($parent = 0) 
{    
    $database = LEPTON_database::getInstance();
	// Query pages
	$aSubpages = [];
	$database->execute_query(
	    "SELECT `page_id` FROM `".TABLE_PREFIX."pages` WHERE `parent` = '".$parent."' ORDER BY position ASC",
	    true,
	    $aSubpages,
	    true
	);

    foreach($aSubpages as $page)
    {
        // Update the page visibility to 'deleted'
        $database->simple_query("UPDATE `".TABLE_PREFIX."pages` SET `visibility` = 'public' WHERE page_id = '".$page['page_id']."'");
        
        // Run function for all sub-pages ... See [1]
        restore_subs($page['page_id']);
    }
}
		
$visibility = $aCurrentPageInfo['visibility'];

if(PAGE_TRASH)
{
	if($visibility == 'deleted')
	{	
        // Update the page visibility to 'public'
        $database->simple_query("UPDATE `".TABLE_PREFIX."pages` SET `visibility` = 'public' WHERE page_id = ".$page_id);
		
        // Run trash subs for this page
        restore_subs($page_id);
	}
}

$admin->print_success($MESSAGE['PAGES_RESTORED']);
