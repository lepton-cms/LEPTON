<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// get classes
$oTWIG = lib_twig_box::getInstance();
$admin = LEPTON_admin::getInstance();

$bUserHasAdminRights = $admin->userHasAdminRights();

// prevent users to access url directly
if ($bUserHasAdminRights == false)
{
    if (!in_array('pages', ( $_SESSION['SYSTEM_PERMISSIONS'] ?? [] ) ) )
    {
        header("Location: ".ADMIN_URL."");
        exit(0);
    }

	// get all permissions
	$aModulePermissions = LEPTON_core::getValue('module_permissions','array','session');
	$aMemberGroups = LEPTON_core::getValue('groups_id','string_clean','session',',');
} else {
    $aMemberGroups = [1];
}



// enable custom files
if (LEPTON_handle::require_alternative('pages/index.php')) return 0;

// get classes
$oTWIG = lib_twig_box::getInstance();
$admin = LEPTON_admin::getInstance();

// Get all groups (incl. 1 == Administrators
$all_groups = [];
$database->execute_query(
    "SELECT * FROM `".TABLE_PREFIX."groups`",
    true,
    $all_groups,
    true
);
 
// Get all page-modules
$all_page_modules = [];
$database->execute_query(
    "SELECT * FROM `".TABLE_PREFIX."addons` WHERE `type` = 'module' AND `function` = 'page' order by `name`",
    true,
    $all_page_modules,
    true
);

if ($bUserHasAdminRights == false)
{
    // only modules with access!
    $temp_addons = [];
    foreach($all_page_modules as $permitted)
    {
	    if (in_array($permitted['directory'],$aModulePermissions))
	    {
		    $temp_addons[] = $permitted;
	    }
    }

    $all_page_modules =$temp_addons;
}

//  [1] Get all pages as (array-) tree
LEPTON_handle::register('page_tree');

//  [1.1] Storage for all infos in an array
$all_pages = [];

//  [1.2] Determinate what fields/keys we want to get in our 'page_tree'-array
$fields = ['page_id','page_title','menu_title','parent','position','visibility','link', 'admin_groups'];

//  [1.3] Get the tree here
page_tree(0, $all_pages, $fields);

$page_values = [
    'all_groups'        => $all_groups,
    'all_page_modules'  => $all_page_modules,
    'leptoken'          => get_leptoken(),
    'MANAGE_SECTIONS'   => MANAGE_SECTIONS,
    'all_pages'         => $all_pages,
	'aMemberGroups'      => $aMemberGroups ,
    'preselect_page'    => (isset($_GET['page_id']) ? intval($_GET['page_id']) : 0 ),
    // used for the pagetree (backend)
    'permissions'       => [
            'p_page_modify' => ($admin->getUserPermission("pages_modify") ? 1 : 0) ,
            'p_page_delete' => ($admin->getUserPermission("pages_delete") ? 1 : 0) ,
            'p_page_add'    => ($admin->getUserPermission("pages_add")    ? 1 : 0) ,
            'p_pages_settings'  => ($admin->getUserPermission('pages_settings') ? 1 : 0)
        ],
    'user_groups'       => $admin->getValue("groups_id", "array", "session", ",")
];

echo $oTWIG->render(
    "@theme/pages_add.lte",
    $page_values
);

$admin->print_footer();
