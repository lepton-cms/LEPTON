<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$admin = LEPTON_admin::getInstance('Pages', 'pages_add');
$database = LEPTON_database::getInstance();
global $MESSAGE;

$functions = [
	"save_filename",
	"get_parent_titles",
	"root_parent",
	"get_page_trail"
];
LEPTON_handle::register($functions);

// Get values
$title = LEPTON_core::getValue('title');

// Make sure that postet modulename contains only lowercases, numbers and "-" or "_".
$module = LEPTON_core::getValue('type','string_secure','post');

$parent =  LEPTON_core::getValue('parent','integer','post');
$visibility = LEPTON_core::getValue('visibility','string_secure','post');
if (!in_array($visibility, array('public', 'private', 'registered', 'hidden', 'none')))
{
    $visibility = 'public';
}

$viewing_groups = $admin->getValue('viewing_groups');
$viewing_groups[] = 1;
$viewing_groups = implode(",", $viewing_groups);

$admin_groups = $admin->getValue('admin_groups');
// add Admin group
$admin_groups[] = 1;


if ($parent != 0) 
{
	if (!$admin->get_page_permission($parent,'admin'))
    {
        $admin->print_error($MESSAGE['PAGES_INSUFFICIENT_PERMISSIONS']." [1]");
    }
} 
elseif (!$admin->get_permission('pages_add_l0','system'))
{
	$admin->print_error($MESSAGE['PAGES_INSUFFICIENT_PERMISSIONS']." [2]");
}

// Validate data
if (is_null($title) || str_starts_with($title, '.'))
{
	$admin->print_error($MESSAGE['PAGES_BLANK_PAGE_TITLE']);
}

// Check to see if page created has needed permissions
if(!in_array(1, $admin->getValue('groups_id', 'string', 'session',',')))
{
	$admin_perm_ok = false;
	foreach ($admin_groups as $adm_group)
    {
		if (in_array($adm_group, $admin->getValue('groups_id', 'string', 'session',',')))
        {
			$admin_perm_ok = true;
		}
	}
	
	if ($admin_perm_ok == false)
    {
		$admin->print_error($MESSAGE['PAGES_INSUFFICIENT_PERMISSIONS']." [3]");
	}
}

$admin_groups = implode(',', $admin_groups);

// Work-out what the link and page filename should be
if($parent == '0')
{
	$link = '/'.save_filename($title);
	// rename menu titles: index to prevent clashes with core file /pages/index.php
	if($link == '/index')
    {
		$link .= '_0';
		$filename = LEPTON_PATH .PAGES_DIRECTORY .'/' .save_filename($title) .'_0' .PAGE_EXTENSION;
	} 
	else 
	{
		$filename = LEPTON_PATH.PAGES_DIRECTORY.'/'.save_filename($title).PAGE_EXTENSION;
	}
} 
else 
{
	$parent_section = '';
	$parent_titles = array_reverse(get_parent_titles($parent));
	
	foreach($parent_titles as $parent_title)
    {
		$parent_section .= save_filename($parent_title).'/';
	}
	
	if($parent_section == '/') { $parent_section = ''; }
	
	$link = '/'.$parent_section.save_filename($title);
	$filename = LEPTON_PATH.PAGES_DIRECTORY.'/'.$parent_section.save_filename($title).PAGE_EXTENSION;
	LEPTON_core::make_dir(LEPTON_PATH.PAGES_DIRECTORY.'/'.$parent_section);

	$source = ADMIN_PATH."/pages/master_index.php";
	copy($source, LEPTON_PATH.PAGES_DIRECTORY.'/'.$parent_section."/index.php");
}

// Check if a page with same page filename exists
$get_same_page = $database->get_one("SELECT page_id FROM ".TABLE_PREFIX."pages WHERE link = '".$link."' ");
if (!is_null($get_same_page) || file_exists(LEPTON_PATH.PAGES_DIRECTORY.$link.PAGE_EXTENSION))
{
    $admin->print_error($MESSAGE['PAGES_PAGE_EXISTS']." [1]");
}

// Include the ordering class, use LEPTON_order
$order = LEPTON_order::getInstance(TABLE_PREFIX.'pages', 'position', 'page_id', 'parent');

// First clean order
$order->clean($parent);

// Get new order
$position = $order->get_new($parent);

// Work-out if the page parent (if selected) has a separate template or language to the default
$fetch_parent = [];
$database->execute_query(
	"SELECT `template`, `language` FROM `".TABLE_PREFIX."pages` WHERE `page_id` = ".$parent,
	true,
	$fetch_parent,
	false
);

if (!empty($fetch_parent))
{
	$template = $fetch_parent['template'];
	$language = $fetch_parent['language'];
} 
else 
{
	$template = '';
	$language = DEFAULT_LANGUAGE;
}

// Insert page into pages table
$fields = [
	'parent' 		=> $parent,
	'target'		=> "_top",
	'page_title'	=> $title,
	'menu_title'	=> $title,
	'template'		=> $template,
	'visibility'	=> $visibility,
	'position'		=> $position,
	'menu'			=> 1,
	'language'		=> $language,
	'searching'		=> 1,
	'modified_when'	=> time(),
	'modified_by'	=> LEPTON_core::getValue('USER_ID','integer','session'),
	'admin_groups'	=> $admin_groups,
	'viewing_groups' => $viewing_groups,
	'link'			=> '',
	'description'	=> '',
	'keywords'		=> '',
	'page_trail'	=> ''
];

$table = TABLE_PREFIX.'pages';
$database->build_and_execute(
	'insert',
	$table,
	$fields
);

// Get the page id
$page_id = $database->get_one("SELECT LAST_INSERT_ID() FROM ".$table);

// Update page with new level and link
$fields = [
	'root_parent'	=> root_parent($page_id),
	'level'			=> LEPTON_core::level_count($page_id),
	'link'			=> $link,
	'page_trail'	=> get_page_trail($page_id)
];

$database->build_and_execute(
	'update',
	$table,
	$fields,
	'page_id = '.$page_id
);

// Create a new file in the /pages dir
LEPTON_core::create_access_file($filename, $page_id, $level);

// add new section at position 1 to the new page
$position = 1;

// Add new record into the sections table
$fields = [
	'page_id'	=> $page_id,
	'position'	=> $position,
	'module'	=> $module,
	'name'		=> 'no name',
	'block'		=> 1
];

$sec_table = TABLE_PREFIX.'sections';
$database->build_and_execute(
	'insert',
	$sec_table,
	$fields
);

// Get the section id
$section_id = $database->get_one("SELECT LAST_INSERT_ID() FROM ".$sec_table);

LEPTON_handle::include_files ('/modules/'.$module.'/add.php');

$admin->print_success($MESSAGE['PAGES_ADDED'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
