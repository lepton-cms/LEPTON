<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 *
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// enable custom files
if(LEPTON_handle::require_alternative('pages/index.php')) return 0;

// get classes
$admin = LEPTON_admin::getInstance();
$oTWIG = lib_twig_box::getInstance();


//	Get all groups (incl. 1 == Administrators
$all_groups = [];
$database->execute_query(
    "SELECT * FROM `".TABLE_PREFIX."groups`",
    true,
    $all_groups,
    true
);
 

//  Get all page-modules
$all_page_modules = [];
$database->execute_query(
    "SELECT * FROM `".TABLE_PREFIX."addons` WHERE `type` = 'module' AND `function` = 'page' order by `name`",
    true,
    $all_page_modules,
    true
);

//  Get all pages as (array-) tree
LEPTON_handle::register('page_tree');

//  Storage for all infos in an array
$all_pages = [];

//  Determinate what fields/keys we want to get in our 'page_tree'-array
$fields = ['page_id','page_title','menu_title','parent','position','visibility','link','admin_groups'];

//  Get the tree here
page_tree( 0, $all_pages, $fields );

$page_values = [
    'all_groups'        => $all_groups,
    'all_page_modules'  => $all_page_modules,
    'all_pages'         => $all_pages,
    'open_tree'         => 1,
    'leptoken'          => get_leptoken(),
    'MANAGE_SECTIONS'   => MANAGE_SECTIONS,
    'permissions'       => [
            'p_page_modify' => ($admin->getUserPermission("pages_modify") ? 1 : 0) ,
            'p_page_delete' => ($admin->getUserPermission("pages_delete") ? 1 : 0) ,
            'p_page_add'    => ($admin->getUserPermission("pages_add")    ? 1 : 0) ,
            'p_pages_settings'  => ($admin->getUserPermission('pages_settings') ? 1 : 0)
        ],
    'user_groups'       => LEPTON_core::getValue("groups_id", "string", "session", ",")
];

echo $oTWIG->render(
    "@theme/pages_overview.lte",
    $page_values
);

$admin->print_footer();
