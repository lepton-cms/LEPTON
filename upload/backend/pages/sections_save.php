<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// Make sure people are allowed to access this page
if (MANAGE_SECTIONS != 'true') 
{
	header('Location: '.ADMIN_URL.'/pages/index.php');
	exit(0);
}

// Get page id
if (!isset($_POST['page_id']) || !is_numeric($_POST['page_id'])) 
{
	header("Location: index.php");
	exit(0);
} 
else 
{
	$page_id = $_POST['page_id'];
}

// Create new LEPTON_admin object
$admin = LEPTON_admin::getInstance('Pages', 'pages_modify');

$oDateTools = LEPTON_date::getInstance();

// Get perms
$sAdminGroup = $database->get_one("SELECT `admin_groups` FROM `".TABLE_PREFIX."pages` WHERE `page_id`= ".$page_id);

$admin_groups = explode(',', $sAdminGroup);
$in_group = false;
$gotAdminRights = false;

foreach ($admin->getValue('groups_id', 'string', 'session',',') as $sCurrentGroupId)
{
    if (in_array($sCurrentGroupId, $admin_groups))
    {
        $in_group = true;
    }
    
    if ($sCurrentGroupId == 1)
    {
        $gotAdminRights = true;
    }
}

if ($in_group == false)
{
    $admin->print_error($MESSAGE['PAGES_INSUFFICIENT_PERMISSIONS']);
}

// Get page details
$aCurrentPageInfo = [];
$database->execute_query(
	"SELECT count(*) FROM `".TABLE_PREFIX."pages` WHERE `page_id` = ".$page_id,
	true,
	$aCurrentPageInfo,
	false
);

if (empty($aCurrentPageInfo) ) 
{
	$admin->print_header();
	$admin->print_error($MESSAGE['PAGES_NOT_FOUND']);
}

// Set module permissions
$module_permissions = $_SESSION['MODULE_PERMISSIONS'];

// Loop through sections
$all_sections = []; 
$database->execute_query(
	"SELECT `section_id`,`module`,`position` FROM `".TABLE_PREFIX."sections` WHERE `page_id`= ".$page_id." ORDER BY `position` ASC",
	true,
	$all_sections,
	true
);

foreach ($all_sections as $section)
{
    // echo LEPTON_tools::display_dev(  is_numeric(array_search($section['module'], $module_permissions)), "pre", "ui message", true  );
	
	if (($gotAdminRights) || (is_numeric(array_search($section['module'], $module_permissions))))
	{
		// Update the section record with properties
		$section_id = $section['section_id'];
		
		$fields = [
			'publ_start'	=> 0,
			'publ_end'		=> 0
		];

		//	[1]	Blocks
		if (isset($_POST['block'.$section_id]) AND $_POST['block'.$section_id] != '')
		{
			$fields['block'] = addslashes($_POST['block'.$section_id]);
		}

		//	[2]	Start date
		if (true === isset($_POST['start_date'.$section_id]) )
		{
			$start_date = trim($_POST['start_date'.$section_id]);
			if (($start_date != ""))
			{
				$fields['publ_start'] = $oDateTools->calendarToTimestamp( $start_date );
			}
		}

		//	[3] End date
		if (true === isset($_POST['end_date'.$section_id]))
		{
			$end_date = trim($_POST['end_date'.$section_id]);
			if ($end_date != "")
			{
				$fields['publ_end'] = $oDateTools->calendarToTimestamp( $end_date );
			}
		}
		
		//	[4]	Name of the section
		if (isset($_POST['section_name'][$section_id]))
		{
			$fields['name'] = htmlspecialchars(strip_tags($_POST['section_name'][$section_id], ""));
		}
		
		$database->build_and_execute(
			'update',
			TABLE_PREFIX."sections",
			$fields,
			"`section_id` = ".$section_id
		);
	}
}
	
$admin->print_success($MESSAGE['PAGES_SECTIONS_PROPERTIES_SAVED'], ADMIN_URL.'/pages/sections.php?page_id='.$page_id);

