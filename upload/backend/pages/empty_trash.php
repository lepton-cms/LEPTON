<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


$admin = LEPTON_admin::getInstance('Pages', 'pages');

// Get page list from database
$get_pages = [];
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."pages` WHERE `visibility` = 'deleted' ORDER BY `level` DESC",
	true,
	$get_pages,
	true
);

// Insert values into main page list
if(!empty($get_pages))
{
	foreach($get_pages as $page) 
	{
		// Delete page subs
		$sub_pages = [];
		LEPTON_core::get_subs($page['page_id'], $sub_pages);
		
		foreach($sub_pages AS $sub_page_id) 
		{
			LEPTON_core::delete_page($sub_page_id);
		}	
		// Delete page
		LEPTON_core::delete_page($page['page_id']);
	}
}

$admin->print_success($TEXT['TRASH_EMPTIED']);

