<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


header('Content-Type: application/javascript');

if(!isset($_POST['pages'])) die("E1");

$sPageList = trim( $_POST['pages'] );

if( $sPageList == "" ) die("E2");

$aPages = explode(",", $sPageList);
$aNewList = [];

foreach($aPages as $ref)
{
    if($ref != "") $aNewList[] = intval($ref);
}

if( empty($aNewList))
{
    return "Error [1]: no Items in list.";
}

// get the (page-)level from the first item of the list
$current_level = $database->get_one("SELECT `level` FROM `".TABLE_PREFIX."pages` WHERE `page_id`=".$aNewList[0]);

$position = 1;
foreach($aNewList as $page_id)
{
    $fields =[ 'position'  => $position++ ];
    
    $database->build_and_execute(
        'update',
        TABLE_PREFIX."pages",
        $fields,
        "`page_id`=".$page_id." AND `level`=".$current_level
    );
}

echo "Pageorder has been successfully changed: ".json_encode( $aNewList ).".\n";
