<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// Get page id
if(!isset($_POST['page_id']) OR !is_numeric($_POST['page_id']))
{
    header("Location: index.php");
    exit(0);
} 
else 
{
    $page_id = $_POST['page_id'];
}

$admin = LEPTON_admin::getInstance('Pages', 'pages_settings');

$functions = [
    "get_parent_titles",
    "get_page_trail",
    "level_count",
    "save_filename",
    "root_parent"
];
LEPTON_handle::register($functions);

$_POST["page_title"] = strip_tags($_POST["page_title"] ?? "");
$_POST["menu_title"] = strip_tags($_POST["menu_title"] ?? "");

//  Get values
$aFields = [
    "page_link"     => [ "type" => "str", "default" => "" ],
    "page_title"    => [ "type" => "string_chars", "default" => "" ],
    "menu_title"    => [ "type" => "string_chars", "default" => "" ],
    "description"   => [ "type" => "string_chars", "default" => "" ],
    "keywords"      => [ "type" => "string_chars", "default" => "" ],
    "page_code"     => [ "type" => "string_chars", "default" => "" ],
    "parent"        => [ "type" => "int", "default" => 0 ],
    "visibility"    => [ "type" => "str", "default" => "hidden" ],  // ! keep in mind
    "page_template" => [ "type" => "str", "default" => "" ],   // empty string === default template
    "target"        => [ "type" => "str", "default" => "_self" ],
    "admin_groups"  => [ "type" => "array", "default" => [] ],  // array!
    "viewing_groups" => [ "type" => "array", "default" => [] ],  // array!,
    "searching"     => [ "type" => "int", "default" => 0 ],         // 
    "language"      => [ "type" => "str", "default" => DEFAULT_LANGUAGE],
    "menu"          => [ "type" => "int", "default" => 1 ]
];

$aAllPosts = LEPTON_request::getInstance()->testPostValues($aFields);
extract( $aAllPosts, EXTR_OVERWRITE );

// Post-Validate some values
if ($page_title == '' || substr($page_title,0,1)== '.')
{
    $admin->print_error($MESSAGE['PAGES_BLANK_PAGE_TITLE']);
}

// Remove entities inside the page title!
$page_title = str_replace(["&amp;amp;","&amp;"], "&", $page_title);

if ($menu_title == '' || substr($menu_title,0,1)=='.')
{
    $admin->print_error($MESSAGE['PAGES_BLANK_MENU_TITLE']);
}

// Remove entities inside the menu title!
$menu_title = str_replace(["&amp;amp;","&amp;"], "&", $menu_title);

// [1] Sprecial entities
LEPTON_handle::restoreSpecialChars($menu_title);
LEPTON_handle::restoreSpecialChars($page_title);

// Remove entities inside the menu description!
$description = str_replace(["&amp;amp;","&amp;"], "&", $description);
LEPTON_handle::restoreSpecialChars($description);

// Remove entities inside the menu keywords!
$keywords = str_replace(["&amp;amp;","&amp;"], "&", $keywords);
LEPTON_handle::restoreSpecialChars($keywords);

if ($page_template === '-1')
{
    $page_template = "";
}

// Get existing perms
$aCurrentPageInfo = [];
$results = $database->execute_query(
    'SELECT `parent`,`link`,`position`,`admin_groups` FROM `'.TABLE_PREFIX.'pages` WHERE `page_id`='.$page_id,
    true,
    $aCurrentPageInfo,
    false
);

$old_parent     = $aCurrentPageInfo['parent'];
$old_link       = $aCurrentPageInfo['link'];
$old_position   = $aCurrentPageInfo['position'];
$old_admin_groups = explode(',', $aCurrentPageInfo['admin_groups']);
$in_group = false;

foreach ($admin->getValue('groups_id', 'string', 'session',',') as $sCurrentGroupId)
{
    if (in_array($sCurrentGroupId, $old_admin_groups))
    {
        $in_group = true;
    }
}

if ($in_group == false)
{
    $admin->print_error($MESSAGE['PAGES_INSUFFICIENT_PERMISSIONS']);
}

$viewing_groups[] = 1;
sort($viewing_groups);
$viewing_groups = implode(",", $viewing_groups);

// Setup admin groups
$admin_groups[] = 1;
sort($admin_groups);
$admin_groups = implode(',', $admin_groups);

// If needed, get new order
if ($parent != $old_parent)
{
    // Include ordering class
    $order = LEPTON_order::getInstance(TABLE_PREFIX.'pages', 'position', 'page_id', 'parent');
    // Get new order
    $position = $order->get_new($parent);
    // Clean new order
    $order->clean($parent);

    $database->simple_query("UPDATE `".TABLE_PREFIX."pages` SET `parent`=".$parent." WHERE `page_id`=".$page_id);
} 
else 
{
    $position = $old_position;
}

// Work out level and root parent
if ($parent!='0')
{
    $level = LEPTON_core::level_count($parent)+1;
    $root_parent = root_parent($parent);
}
else 
{
    $level = '0';
    $root_parent = '0';
}

// Work-out what the link should be
if ($parent == '0')
{
    $link = '/'.save_filename($page_link);

    // rename menu titles: index to prevent clashes with core file /pages/index.php
    if ($link == '/index')
    {
        $link .= '_' .$page_id;
        $filename = LEPTON_PATH.PAGES_DIRECTORY.'/'.save_filename($page_link).'_'.$page_id.PAGE_EXTENSION;
    } 
    else 
    {
        $filename = LEPTON_PATH.PAGES_DIRECTORY.'/'.save_filename($page_link).PAGE_EXTENSION;
    }
}
else 
{
    $parent_section = '';
    $parent_titles = array_reverse(get_parent_titles($parent));

    foreach ($parent_titles AS $parent_title)
    {
        $parent_section .= save_filename($parent_title).'/';
    }

    if ($parent_section == '/')
    {
        $parent_section = '';
    }

    $link = '/'.$parent_section.save_filename($page_link);
    $filename = LEPTON_PATH.PAGES_DIRECTORY.'/'.$parent_section.save_filename($page_link).PAGE_EXTENSION;
}

// [3] Check if a page with same page filename exists
$get_same_page = [];
$database->execute_query(
    'SELECT `page_id`,`page_title` FROM `'.TABLE_PREFIX.'pages` WHERE `link` = "'.$link.'" AND `page_id` != '.$page_id,
    true,
    $get_same_page,
    false
);

if (!empty($get_same_page))
{
    $admin->print_error($MESSAGE['PAGES_PAGE_EXISTS']);
}

// [4] Update page settings in the pages table
$fields = [
    'parent'         => $parent,
    'page_title'     => $page_title,
    'menu_title'     => $menu_title,
    'menu'           => $menu,
    'level'          => $level,
    'page_trail'     => get_page_trail($page_id),
    'root_parent'    => $root_parent,
    'link'           => $link,
    'template'       => $page_template,
    'target'         => $target,
    'description'    => $description,
    'keywords'       => $keywords,
    'position'       => $position,
    'visibility'     => $visibility,
    'searching'      => $searching,
    'admin_groups'   => $admin_groups,
    'viewing_groups' => $viewing_groups
];

// [4.1] The following values are only submitted when PAGE_LANGUAGES are enabled! 
if (false !== PAGE_LANGUAGES)
{
    $fields['language']  = $language;
    $fields['page_code'] = $page_code;
}

$database->build_and_execute(
    'update',
    TABLE_PREFIX.'pages',
    $fields,
    "`page_id` = ".$page_id
);


// Clean old order if needed
if ($parent != $old_parent)
{
    $order->clean($old_parent);
}

// Create a new file in the /pages dir if title changed
if (!is_writable(LEPTON_PATH.PAGES_DIRECTORY.'/'))
{
    $admin->print_error($MESSAGE['PAGES_CANNOT_CREATE_ACCESS_FILE']." [023]");
} 
else 
{
    $old_filename = LEPTON_PATH.PAGES_DIRECTORY.$old_link.PAGE_EXTENSION;
    // First check if we need to create a new file
    if (($old_link != $link) || (!file_exists($old_filename)))
    {
        // Delete old file
        $old_filename = LEPTON_PATH.PAGES_DIRECTORY.$old_link.PAGE_EXTENSION;
        if (file_exists($old_filename))
        {
            unlink($old_filename);
        }
        // Create access file
        LEPTON_core::create_access_file($filename,$page_id,$level);

        // Move a directory for this page
        if (is_dir(LEPTON_PATH.PAGES_DIRECTORY.$link.'/'))
        {
            LEPTON_handle::delete_obsolete_directories( [ PAGES_DIRECTORY.$link.'/' ] );
        }

        if (file_exists(LEPTON_PATH.PAGES_DIRECTORY.$old_link.'/') && is_dir(LEPTON_PATH.PAGES_DIRECTORY.$old_link.'/'))
        {
            rename(LEPTON_PATH.PAGES_DIRECTORY.$old_link.'/', LEPTON_PATH.PAGES_DIRECTORY.$link.'/');
        }
        // Update any pages that had the old link with the new one
        $old_link_len = strlen($old_link);

        $query_subs = [];
        $database->execute_query(
            "SELECT page_id,link,level FROM ".TABLE_PREFIX."pages WHERE link LIKE '%$old_link/%' ORDER BY LEVEL ASC",
            true,
            $query_subs,
            true
        );

        if (!empty($query_subs))
        {
            foreach($query_subs as $sub)
            {
                // Double-check to see if it contains old link
                if (substr($sub['link'], 0, $old_link_len) == $old_link)
                {
                    // Get new link
                    $replace_this = $old_link;
                    $old_sub_link_len =strlen($sub['link']);
                    $new_sub_link = $link.'/'.substr($sub['link'],$old_link_len+1,$old_sub_link_len);
                    // Work out level
                    $new_sub_level = LEPTON_core::level_count($sub['page_id']);
                    // Update level and link
                    $database->simple_query("UPDATE ".TABLE_PREFIX."pages SET link = '".$new_sub_link."', level = '".$new_sub_level."' WHERE page_id = '".$sub['page_id']."' LIMIT 1");
                    // Re-write the access file for this page
                    $old_subpage_file = LEPTON_PATH.PAGES_DIRECTORY.$new_sub_link.PAGE_EXTENSION;

                    if (file_exists($old_subpage_file))
                    {
                        unlink($old_subpage_file);
                    }
                    LEPTON_core::create_access_file(LEPTON_PATH.PAGES_DIRECTORY.$new_sub_link.PAGE_EXTENSION, $sub['page_id'], $new_sub_level);
                }
            }
        }
    }
}

// Function to fix page trail of subs
function fix_page_trail($parent,$root_parent)
{
    // Get objects and vars from outside this function
    global $admin, $database, $TEXT, $MESSAGE;

    // Get page list from database
    $all_pages = [];
    $database->execute_query(
        "SELECT `page_id` FROM `".TABLE_PREFIX."pages` WHERE `parent` = '".$parent."'",
        true,
        $all_pages,
        true
    );

    // Insert values into main page list
    foreach ($all_pages as &$page)
    {
        // Fix page trail
        $database->simple_query("UPDATE `".TABLE_PREFIX."pages` SET ".($root_parent != 0 ?"`root_parent` = '$root_parent', ":"")." page_trail = '".get_page_trail($page['page_id'])."' WHERE `page_id` = '".$page['page_id']."'");
        // Run this query on subs
        fix_page_trail($page['page_id'],$root_parent);
    }
}

// Fix sub-pages page trail
fix_page_trail($page_id,$root_parent);

$leptoken = get_leptoken();
$pagetree_url = ADMIN_URL.'/pages/index.php';
$target_url = ADMIN_URL.'/pages/settings.php?page_id='.$page_id."&leptoken=".$leptoken;

$admin->print_success($MESSAGE['PAGES_SAVED_SETTINGS'], $target_url );

