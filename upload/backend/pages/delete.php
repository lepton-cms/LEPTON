<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure file

// Get page id
if(!isset($_GET['page_id']) OR !is_numeric($_GET['page_id'])) 
{
	header("Location: index.php");
	exit(0);
} 
else 
{
	$page_id = $_GET['page_id'];
}

$admin = LEPTON_admin::getInstance('Pages', 'pages_delete');

// Get perms
if (!$admin->get_page_permission($page_id,'admin')) 
{
	$admin->print_error($MESSAGE['PAGES_INSUFFICIENT_PERMISSIONS']);
}

$database = LEPTON_database::getInstance();

// Find out more about the page
$results_array = [];
$database->execute_query(
	"SELECT `visibility` FROM `".TABLE_PREFIX."pages` WHERE `page_id` = ".$page_id,
	true,
	$results_array,
	false
);

if( empty($results_array)) 
{
	$admin->print_error($MESSAGE['PAGES_NOT_FOUND']);
}

$visibility = $results_array['visibility'];

// Check if we should delete it or just set the visibility to 'deleted'
if(PAGE_TRASH != 'disabled' AND $visibility != 'deleted') 
{
	// Page trash is enabled and page has not yet been deleted, Function to change all child pages visibility to deleted
	function trash_subs($parent = 0) 
	{	
		$database = LEPTON_database::getInstance();
		// Query pages
		$temp_pages = [];
		$database->execute_query(
			"SELECT `page_id` FROM `".TABLE_PREFIX."pages` WHERE `parent` = '".$parent."' ORDER BY `position` ASC",
			true,
			$temp_pages
		);
		
		// Loop through pages
		foreach($temp_pages as $page) 
		{		
			// Update the page visibility to 'deleted'
			$database->simple_query("UPDATE `".TABLE_PREFIX."pages` SET `visibility` = 'deleted' WHERE page_id = '".$page['page_id']."' LIMIT 1");

			// Run this function again for all sub-pages
			trash_subs($page['page_id']);
		}
	}
	
	// Update the page visibility to 'deleted'
	$database->simple_query("UPDATE `".TABLE_PREFIX."pages` SET `visibility` = 'deleted' WHERE `page_id` = ".$page_id." LIMIT 1");
	
	// Run trash subs for this page
	trash_subs($page_id);
} 
else 
{
	// Really dump the page, Delete page subs
	$sub_pages = [];
	LEPTON_core::get_subs($page_id, $sub_pages);
	
	foreach($sub_pages AS $sub_page_id) 
	{
		LEPTON_core::delete_page($sub_page_id);
	}
	// Delete page
	LEPTON_core::delete_page($page_id);
}	

$admin->print_success($MESSAGE['PAGES_DELETED'], ADMIN_URL."/pages/overview.php");
