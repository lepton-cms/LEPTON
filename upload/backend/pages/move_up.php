<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$MESSAGE = LEPTON_core::getGlobal("MESSAGE");
$TEXT = LEPTON_core::getGlobal("TEXT");

// Get id
if (isset($_GET['page_id']) AND is_numeric($_GET['page_id']))
{
	if (isset($_GET['section_id']) AND is_numeric($_GET['section_id']))
	{
		$page_id = $_GET['page_id'];
		$id = $_GET['section_id'];
		$id_field = 'section_id';
		$common_field = 'page_id';
		$table = TABLE_PREFIX.'sections';
	} 
	else 
	{
		$id = $_GET['page_id'];
		$id_field = 'page_id';
		$common_field = 'parent';
		$table = TABLE_PREFIX.'pages';
	}
} 
else 
{
	header("Location: index.php");
	exit(0);
}

$admin = LEPTON_admin::getInstance('Pages', 'pages_settings');

// Create new order object and reorder
$order = LEPTON_order::getInstance($table, 'position', $id_field, $common_field);

if ($id_field == 'page_id')
{
	if ($order->move_up($id))
	{
		$admin->print_success($MESSAGE['PAGES_REORDERED']);
	} 
	else 
	{
		$admin->print_error($MESSAGE['PAGES_CANNOT_REORDER']);
	}
} 
else 
{
	if ($order->move_up($id))
	{
		if (file_exists(THEME_PATH.'/backend/backend/pages/sections.php'))
		{
			$admin->print_success($TEXT['SUCCESS'], THEME_URL.'/backend/backend/pages/sections.php?page_id='.$page_id);
			die();
		}
		
		$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/sections.php?page_id='.$page_id);
	} 
	else 
	{
		if (file_exists(THEME_PATH.'/backend/backend/pages/sections.php'))
		{
			$admin->print_error($TEXT['ERROR'], THEME_URL.'/backend/backend/pages/sections.php?page_id='.$page_id);
			die();
		}
		
		$admin->print_error($TEXT['ERROR'], ADMIN_URL.'/pages/sections.php?page_id='.$page_id);
	}
}

$admin->print_footer();
