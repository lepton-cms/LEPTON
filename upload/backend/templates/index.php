<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure file


// enable custom files
if(LEPTON_handle::require_alternative('templates/index.php')) return 0;


// get twig instance
$admin = LEPTON_admin::getInstance();
$oTWIG = lib_twig_box::getInstance();


//	Get all templates
$all_templates = [];
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."addons` WHERE `type` = 'template' order by `name`",
	true,
	$all_templates,
	true
);

// Build secure-hash for the js-calls
LEPTON_handle::register('random_string');

$hash = array(
	'h_name' => random_string(16),
	'h_value' => random_string(24)
);

$_SESSION['backend_templates_h'] = $hash['h_name'];
$_SESSION['backend_templates_v'] = $hash['h_value'];

// Collecting all page values here
$page_values = array(
	'all_templates'	=> $all_templates,
	'hash'	=> $hash,
	'ACTION_URL'  => ADMIN_URL."/templates/",
	'RELOAD_URL'	=> ADMIN_URL."/addons/reload.php"
);

echo $oTWIG->render(
	"@theme/templates.lte",
	$page_values
);

$admin->print_footer();
