<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


global $MESSAGE;

// Check if user selected template
if (!isset($_POST['template_id']) or $_POST['template_id'] == "")
{
	header("Location: index.php");
	exit(0);
} 
else 
{
	$template_id = intval($_POST['template_id']);
}

// Extra protection
if (trim($template_id) == '')
{
	header("Location: index.php");
	exit(0);
}

$database = LEPTON_database::getInstance();

LEPTON_handle::register("rm_full_dir");

$template_info = [];
$database->execute_query(
    "SELECT * FROM `".TABLE_PREFIX."addons` WHERE `addon_id`=".$template_id." AND `type`='template' ",
    true,
    $template_info,
    false
);

//  check for an entry - if not: exit.
if( empty($template_info))
{
    header("Location: index.php");
	exit(0);
}

$file = $template_info['directory'];

$admin = LEPTON_admin::getInstance('Addons', 'templates_uninstall');

// Check if the template exists
if (!is_dir(LEPTON_PATH . '/templates/' . $file)) 
{
	$admin->print_error($MESSAGE['GENERIC_NOT_INSTALLED']);
}

function replace_all($aStr = "", $aArray = [])
{		
	foreach ($aArray as $k=>$v)
	{
        $aStr = str_replace("{{" . $k . "}}", $v, $aStr);
	}
	return $aStr;
}

// Check if the template is the standard-template or still in use
if (!isset($MESSAGE['GENERIC_CANNOT_UNINSTALL_IS_DEFAULT_TEMPLATE']) )
{
    $MESSAGE['GENERIC_CANNOT_UNINSTALL_IS_DEFAULT_TEMPLATE'] = "Can't uninstall this template <b>{{name}}</b> because it's the standard-template!";
}

// check whether the template is used as default theme
if($file == DEFAULT_THEME) 
{
	$temp = array ('name' => $file );
	$msg = replace_all( str_replace("template", "theme", $MESSAGE['GENERIC_CANNOT_UNINSTALL_IS_DEFAULT_TEMPLATE']), $temp );
	$admin->print_error( $msg );
}

if ($file == DEFAULT_TEMPLATE) 
{
	$temp = [ 'name' => $file ];
	$msg = replace_all( $MESSAGE['GENERIC_CANNOT_UNINSTALL_IS_DEFAULT_TEMPLATE'], $temp );
	$admin->print_error( $msg );

} 
else 
{
	
	// Check if the template is still in use by a page ...
	$aAllPages = [];
	$database->execute_query(
	    "SELECT `page_id`, `page_title` FROM `".TABLE_PREFIX."pages` WHERE `template`='".$file."' order by `page_title`",
	    true,
	    $aAllPages,
	    true
	);
	
	if (!empty($aAllPages))		// Template is still in use, so we're collecting the page-titles
	{
		// The base-message template-string for the top of the message
		if (!isset($MESSAGE['GENERIC_CANNOT_UNINSTALL_IN_USE_TMPL'] ) ) 
		{
			$add = count($aAllPages) == 1 ? "this page" : "these pages";
			$msg_template_str  = "<br /><br />{{type}} <b>{{type_name}}</b> could not be uninstalled because it is still in use by {{pages}}";
			$msg_template_str .= ":<br /><i>click for editing.</i><br /><br />";
		} 
		else 
		{
			$msg_template_str = $MESSAGE['GENERIC_CANNOT_UNINSTALL_IN_USE_TMPL'];
			$temp = explode(";",$MESSAGE['GENERIC_CANNOT_UNINSTALL_IN_USE_TMPL_PAGES']);
			$add = count($aAllPages) == 1 ? $temp[0] : $temp[1];
		}

		// The template-string for displaying the Page-Titles ... in this case as a link
		$page_template_str = "- <b><a href='../pages/settings.php?page_id={{id}}'>{{title}}</a></b><br />";
		
		$values = [ 'type' => 'Template', 'type_name' => $file, 'pages' => $add ];
		$msg = replace_all ( $msg_template_str,  $values );
		
		$page_names = "";
		
		foreach($aAllPages as $data)
		{
			$page_info = [
				'id'	=> $data['page_id'], 
				'title' => $data['page_title']
			];
			
			$page_names .= replace_all($page_template_str, $page_info);
		}
		
		$admin->print_error($MESSAGE['GENERIC_CANNOT_UNINSTALL_IN_USE'].$msg.$page_names);
	}
}

// Check if we have permissions on the directory
if(!is_writable(LEPTON_PATH.'/templates/'.$file)) 
{
	$admin->print_error($MESSAGE['GENERIC_CANNOT_UNINSTALL'].LEPTON_PATH.'/templates/'.$file);
}

// Try to delete the template dir
if(!rm_full_dir(LEPTON_PATH.'/templates/'.$file)) 
{
	$admin->print_error($MESSAGE['GENERIC_CANNOT_UNINSTALL']);
} 
else 
{
	// Remove entry from DB
	$database->simple_query("DELETE FROM `".TABLE_PREFIX."addons` WHERE `directory` = '".$file."' AND type = 'template'");
}

// Update pages that use this template with default template
$database->simple_query("UPDATE `".TABLE_PREFIX."pages` SET template = '".DEFAULT_TEMPLATE."' WHERE `template` = '$file'");

// Update the template-permissions inside groups
$aAllGroups = [];
$database->execute_query(
    "SELECT `group_id`,`template_permissions` FROM `".TABLE_PREFIX."groups` WHERE `template_permissions` LIKE '%".$file."%'",
    true,
    $aAllGroups,
    true
);

foreach($aAllGroups as $group) {
    $aTemp = explode(",", $group['template_permissions']);
    $aNew = array();
    foreach($aTemp as $sTempName)
    {
        if($sTempName != $file) $aNew[] = $sTempName;
    }
	
    $aUpdateFields = array(
        'template_permissions'  => implode(",", $aNew)
    );
	
    $database->build_and_execute(
        'update',
        TABLE_PREFIX."groups",
        $aUpdateFields,
        "`group_id`=".$group['group_id']
    );
}

$admin->print_success($MESSAGE['GENERIC_UNINSTALLED']);

