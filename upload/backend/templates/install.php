<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function        cleanup
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

global $MESSAGE;

// Check if user uploaded a file
if (!isset($_FILES['userfile']))
{
	header("Location: index.php");
	exit(0);
}

$admin = LEPTON_admin::getInstance('Addons', 'templates_install');

$functions = [
	"directory_list",
	"load_template",
	"preCheckAddon",
	"rename_recursive_dirs",
	"versionCompare"
];
LEPTON_handle::register($functions);

// Set temp vars
$temp_dir = LEPTON_PATH . '/temp/';
$temp_file = $temp_dir . $_FILES['userfile']['name'];
$temp_unzip = LEPTON_PATH . '/temp/unzip/';

// Try to upload the file to the temp dir
if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $temp_file))
{
	$admin->print_error($MESSAGE['GENERIC_CANNOT_UPLOAD']);
}

// Include the PclZip class file
LEPTON_handle::include_files("/modules/lib_lepton/pclzip/pclzip.lib.php");

// Remove any vars with name "template_directory" and "theme_directory"
unset($template_directory);
unset($theme_directory);

// Setup the PclZip object
$archive = new PclZip($temp_file);

// Unzip the files to the temp unzip folder
$list = $archive->extract(PCLZIP_OPT_PATH, $temp_unzip);

// Check for GitHub zip archive
if (!file_exists($temp_unzip."info.php")) 
{
	$temp_dirs = [];
	directory_list($temp_unzip, false, 0, $temp_dirs);
	foreach($temp_dirs as &$temp_path)
	{
		if (file_exists($temp_path."/info.php")) 
		{
			$temp_unzip = $temp_path."/";
			break;
		}
	}
}

// Check if uploaded file is a valid Add-On zip file
if (!($list && file_exists($temp_unzip . 'index.php'))) 
{
	$admin->print_error($MESSAGE['GENERIC_INVALID_ADDON_FILE']);
}

// Include the templates info file
require($temp_unzip.'info.php');

// Perform Add-on requirement checks before proceeding
preCheckAddon($temp_file);

// Check if the file is valid
if(!isset($template_directory)) 
{
	if(file_exists($temp_file)) 
	{ 
		unlink($temp_file);
	} 
	$admin->print_error($MESSAGE['GENERIC_INVALID']);
}

// Check if this template is already installed and in this case compare versions
$new_template_version = $template_version;

if (is_dir(LEPTON_PATH . '/templates/' . $template_directory))
{
    if (file_exists(LEPTON_PATH . '/templates/' . $template_directory . '/info.php'))
	{
		require_once LEPTON_PATH.'/templates/'.$template_directory.'/info.php';
		
		$temp_error = false;
		$temp_msg = "";

		// Version to be installed is older than currently installed version
		if (versionCompare($template_version, $new_template_version, '>='))
        {
			$temp_error = true;
			$temp_msg = $MESSAGE['GENERIC_ALREADY_INSTALLED'];
		}
		
		// Additional tests for required vars
		if(	(!isset($template_license))		||
			(!isset($template_directory))	||
			(!isset($template_author))		||
			(!isset($template_version))		||
			(!isset($template_function))	
		) 
		{
			$temp_error = true;
			$temp_msg = $MESSAGE["TEMPLATES_MISSING_PARTS_NOTICE"];
		}

        if (true === $temp_error)
        {
            if (file_exists($temp_file))
            {
                unlink($temp_file);
            }
			
			$admin->print_error( $temp_msg );
		}
	}
	
	$success_message = $MESSAGE['GENERIC_UPGRADED'];
} 
else 
{
	$success_message = $MESSAGE['GENERIC_INSTALLED'];
}

// Check if template dir is writable
if (!is_writable(LEPTON_PATH . '/templates/'))
{
	if (file_exists($temp_file)) 
	{
        unlink($temp_file);
    }
	
	$admin->print_error($MESSAGE['TEMPLATES_BAD_PERMISSIONS']);
}

// Set template dir
$template_dir = LEPTON_PATH . '/templates/' . $template_directory;

// Make sure the template dir exists, and chmod if needed
if (!file_exists($template_dir))
{
	LEPTON_core::make_dir($template_dir);
} 
else 
{
	LEPTON_core::change_mode($template_dir);
}

rename_recursive_dirs($temp_unzip, $template_dir);

// Delete the temp zip file
if (file_exists($temp_file))
{
	unlink($temp_file); 
}

// Chmod all the uploaded files
$dir = dir($template_dir);
while (false !== $entry = $dir->read())
{
	// Skip pointers
	if (!str_starts_with($entry, '.') && $entry != '.svn' && !is_dir($template_dir.'/'.$entry)) 
	{
		LEPTON_core::change_mode($template_dir.'/'.$entry);
	}
}

// Load template info into DB
load_template($template_directory);

$admin->print_success($success_message);
