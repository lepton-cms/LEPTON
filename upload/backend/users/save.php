<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


$admin = LEPTON_admin::getInstance();

// Check if user id is a valid number and not 1
if(!isset($_POST['user_id']) OR !is_numeric($_POST['user_id']) OR $_POST['user_id'] == 1) 
{
	header("Location: index.php");
	exit(0);
} 
else 
{
	$user_id = $_POST['user_id'];
}

// check if job save or delete
if (isset($_POST['job'])) 
{
	switch (strtolower($_POST['job']))
	{	
		case "delete":
			 //	Test for user statusflags == 32 
			$result = [];
			$database->execute_query(
				"SELECT `statusflags` FROM `".TABLE_PREFIX."users` WHERE `user_id`= ".$user_id,
				true,
				$result,
				false
			);

			if ($result['statusflags'] == 32) 
			{
				$admin->print_error($THEME['CANNOT_DELETE']);
			} 
			else 
			{
				$database->simple_query("DELETE FROM `".TABLE_PREFIX."users` WHERE `user_id` = ".$user_id." LIMIT 1");
				
				$admin->print_success($MESSAGE['USERS_DELETED'], ADMIN_URL.'/users/index.php');			
			}
			break;
		
		case "save":
	
			// Gather details entered
			$groups_id = (isset($_POST['groups'])) ? implode(",", $_POST['groups'] ) : '';
			$active = LEPTON_core::getValue('active','integer','post');
			$username_fieldname = LEPTON_core::getValue('username_fieldname','username','post');
			$username = LEPTON_core::getValue($username_fieldname);
			
			if (is_null($username)) 
			{
				$admin->print_error( $MESSAGE['USERS_NAME_INVALID_CHARS'], 'index.php' );
			}
			

			$password = LEPTON_core::getValue('password','password','post');
			$password2 = LEPTON_core::getValue('password2','password','post');
			$display_name = LEPTON_core::getValue('display_name','username','post');

			$email = LEPTON_core::getValue('email','email','post');
			if(is_null($email))
			{
					$admin->print_error($MESSAGE['USERS_INVALID_EMAIL'],'index.php');

			}
			
			$home_folder = LEPTON_core::getValue('home_folder');

			// Check values
			if ($groups_id == "") 
			{
				$admin->print_error($MESSAGE['USERS_NO_GROUP'],'index.php');
			}

			if (strlen( $username ) < AUTH_MIN_LOGIN_LENGTH) 
			{
				$admin->print_error( $MESSAGE['USERS_USERNAME_TOO_SHORT']." [1]", 'index.php');
			}


			if (($password != "") && ($password2 != "")) 
			{
				if (strlen($password) < AUTH_MIN_PASS_LENGTH) 
				{
					$admin->print_error($MESSAGE['USERS_PASSWORD_TOO_SHORT']." [2]",'index.php');
				}
				
				if ($password != $password2) 
				{
					$admin->print_error($MESSAGE['USERS_PASSWORD_MISMATCH'],'index.php');
				}
			}

			if ($email == "")
			{
				 //	e-mail must be present
				$admin->print_error($MESSAGE['SIGNUP_NO_EMAIL'],'index.php');
			}

			 //	Check if the email already exists
			$results = $database->get_one("SELECT user_id FROM ".TABLE_PREFIX."users WHERE `email` = '".$email."' AND user_id <> ".$user_id);
			
			if (!is_null($results))
			{
				$admin->print_error($MESSAGE['USERS_EMAIL_TAKEN'],'index.php');
			}

			 // Update the database
			$fields = [
				'groups_id'		=> $groups_id,
				'active'		=> $active,
				'display_name'	=> $display_name,
				'home_folder'	=> $home_folder,
				'email'	=> $email
			];

			if( $password2 != "") $fields['password'] = password_hash( $password , PASSWORD_DEFAULT);	

			// Prevent from renaming user to "admin"
			if ($username != 'admin') $fields[ 'username' ] = $username;

			$query = $database->build_and_execute(
				'UPDATE',
				TABLE_PREFIX."users",
				$fields,
				"user_id = ".$user_id
			);

			$admin->print_success($MESSAGE['USERS_SAVED']);
			break;
		
		default: 
			die('no job declared');
			break;
	}		
}
