<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


$admin = LEPTON_admin::getInstance();

// Create a back link
$js_back = ADMIN_URL.'/users/index.php';

// Get details entered
$groups_id = null;

if ( isset( $_POST['groups'] ) )
{
  $groups_id = implode(",", array_map('addslashes', ($_POST['groups']))); //should check permissions
  $groups_id = trim($groups_id, ','); // there will be an additional ',' when "Please Choose" was selected, too
}

$active = LEPTON_core::getValue('active','integer','post');

$username_fieldname = LEPTON_core::getValue('username_fieldname');
$username = LEPTON_core::getValue($username_fieldname,'username','post');

$password = LEPTON_core::getValue('password','password','post');
$password2 = LEPTON_core::getValue('password2','password','post');
$display_name = LEPTON_core::getValue('display_name','username','post');
$email = LEPTON_core::getValue('email','email','post');

if(is_null($email))
{
	$admin->print_error($MESSAGE['USERS_INVALID_EMAIL']."[2]", $js_back);
}

$home_folder = LEPTON_core::getValue('home_folder');

// Check values
if($groups_id == '') 
{
	$admin->print_error($MESSAGE['USERS_NO_GROUP'], $js_back);
}

if(is_null($username)) 
{
	$admin->print_error( $MESSAGE['USERS_NAME_INVALID_CHARS'], $js_back);
}

if (strlen( $username ) < AUTH_MIN_LOGIN_LENGTH ) 
{
	$admin->print_error( $MESSAGE['USERS_USERNAME_TOO_SHORT'], $js_back);
}

if(is_null($password)) 
{
	$admin->print_error( $MESSAGE['USERS_PASSWORD_MISMATCH'], $js_back);
}

if(strlen($password) < AUTH_MIN_PASS_LENGTH) 
{
	$admin->print_error($MESSAGE['USERS_PASSWORD_TOO_SHORT'], $js_back);
}

if($password != $password2) 
{
	$admin->print_error($MESSAGE['USERS_PASSWORD_MISMATCH'], $js_back);
}

//  [b.1]   Check if username already exists
$results = $database->get_one("SELECT `user_id` FROM `".TABLE_PREFIX."users` WHERE `username` = '".$username."' ");
if(!is_null($results))
{
	$admin->print_error($MESSAGE['USERS_USERNAME_TAKEN'], $js_back);
}

//  [b.2]   Check if the email already exists
$results = $database->get_one("SELECT user_id FROM ".TABLE_PREFIX."users WHERE email = '".$email."' ");
if(!is_null($results))
{
	$admin->print_error($MESSAGE['USERS_EMAIL_TAKEN'],'index.php');
}

// supplied password
$crypt_password = password_hash( $password, PASSWORD_DEFAULT);

// Insert the user-data into the database
$fields = [
//	'group_id'	=> $group_id, @DEPRECATED_TEMP -> 20240329 obsolete if groupd_id is removed from users table
	'groups_id'	=> $groups_id,
	'active'	=> $active,
	'username'	=> $username,
	'password'	=> $crypt_password,
	'email'		=> $email,
	'display_name'	=> $display_name,
	'home_folder'	=> $home_folder,	
	'timezone_string'	=> DEFAULT_TIMEZONE_STRING,
	'language'	=> DEFAULT_LANGUAGE
];

$database->build_and_execute( 
	'INSERT', 
	TABLE_PREFIX.'users', 
	$fields
);

$admin->print_success($MESSAGE['USERS_ADDED']);
