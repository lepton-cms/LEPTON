<?php

declare(strict_types=1);

global $MESSAGE, $TEXT;

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$admin = LEPTON_admin::getInstance('Addons', 'modules_uninstall');
$database = LEPTON_database::getInstance();

if (!isset($_POST['module_id'])) 
{
	header("Location: index.php");
	exit(0);
} 
else 
{
	$m_id = intval($_POST['module_id']);
	
	if ($m_id == 0) 
	{
		header("Location: index.php");
		exit(0);
	}
}

$module = [];
$database->execute_query(
	"SELECT `name`,`directory`,`addon_id` FROM `".TABLE_PREFIX."addons` WHERE `addon_id` = ".$m_id." AND `type`= 'module'",
	true,
	$module,
	false
);

if (empty($module)) 
{
	header("Location: index.php");
	exit(0);
}

$sModuleToDelete = $module['directory'];

// Check if the module exists
if (!is_dir(LEPTON_PATH.'/modules/'.$sModuleToDelete))
{
	$admin->print_error($MESSAGE['GENERIC_NOT_INSTALLED']);
}

if (!function_exists("replace_all")) 
{
    function replace_all(string $aStr = "", array &$aArray = []): string
    {
        foreach($aArray as $k=>$v)
        {
            $aStr = str_replace("{{".$k."}}", (string)$v, $aStr);
        }
        return $aStr;
    }
}

$aAllPagesWithThisModule = [];
$database->execute_query(
    "SELECT `section_id`,`page_id` FROM `".TABLE_PREFIX."sections` WHERE `module`='".$sModuleToDelete."'",
    true,
    $aAllPagesWithThisModule,
    true
);

//	If the module is in use we've to warn the user.
if (!empty($aAllPagesWithThisModule))
{
	//	Try to get unique page_ids if e.g. the modul is used in more than one section on a page
	$page_ids = [];
	foreach( $aAllPagesWithThisModule as $data )
	{
		if (!in_array($data['page_id'], $page_ids))
		{
		    $page_ids[] = $data['page_id'];
		}
	}
	
	if (!array_key_exists("GENERIC_CANNOT_UNINSTALL_IN_USE_TMPL", $MESSAGE)) 
	{
		$add = count( $page_ids ) == 1 ? "this page" : "these pages";
		$msg_template_str  = "<br /><br />{{type}} <b>{{type_name}}</b> could not be uninstalled because it is still in use on {{pages}}";
		$msg_template_str .= ":<br /><i>click for editing.</i><br /><br />";
	} 
	else 
	{
		$msg_template_str = $MESSAGE['GENERIC_CANNOT_UNINSTALL_IN_USE_TMPL'];
		$temp = explode(";",$MESSAGE['GENERIC_CANNOT_UNINSTALL_IN_USE_TMPL_PAGES']);
		$add = count( $page_ids ) == 1 ? $temp[0] : $temp[1];
	}

	//	The template-string for displaying the Page-Titles ... in this case as a link
	$leptoken_str = (isset($_GET['leptoken'])) ? "&leptoken=".$_GET['leptoken'] : "";
	$page_template_str = "- <b><a href='../pages/sections.php?page_id={{id}}".$leptoken_str."'>{{title}}</a></b><br />";

    $values = [
        'type' => 'Modul',
        'type_name' => $sModuleToDelete,
        'pages' => $add
    ];
    $msg = replace_all($msg_template_str, $values);
		
	$page_names = "";

    foreach ($page_ids as $temp_id)
    {
		$temp_title = $database->get_one("SELECT `page_title` from `".TABLE_PREFIX."pages` where `page_id`=".$temp_id);
		
		$page_info = array(
			'id'	=> $temp_id, 
			'title' => $temp_title
		);

        $page_names .= replace_all($page_template_str, $page_info);
	}
	
	$admin->print_error(str_replace ($TEXT['FILE'], "Modul", $MESSAGE['GENERIC_CANNOT_UNINSTALL_IN_USE']).$msg.$page_names);
}


//	Test for the standard wysiwyg-editor ...
if ((defined('WYSIWYG_EDITOR')) && ($sModuleToDelete == WYSIWYG_EDITOR))
{
    $admin->print_error("The module <b>".WYSIWYG_EDITOR."</b> is the current standard wysiwyg editor and cannot be uninstalled unless you change the settings!");
}

// Check if we have permissions on the directory
if (!is_writable(LEPTON_PATH . '/modules/' . $sModuleToDelete))
{
    $admin->print_error($MESSAGE['GENERIC_CANNOT_UNINSTALL']." [12]");
}

// Run the modules uninstall script if there is one
if(file_exists(LEPTON_PATH.'/modules/'.$sModuleToDelete.'/uninstall.php')) 
{
	$temp_css = LEPTON_PATH.'/modules/'.$sModuleToDelete.'/backend.css';
	if (file_exists($temp_css)) 
	{
		echo "\n<link href=\"". (LEPTON_URL.'/modules/'.$sModuleToDelete.'/backend.css') ." rel=\"stylesheet\" type=\"text/css\" media=\"screen, projection\" />\n";
	} 
	else 
	{
		$temp_css = LEPTON_PATH.'/modules/'.$sModuleToDelete.'/css/backend.css';
		if (file_exists($temp_css)) 
		{
			echo "\n<link href=\"". (LEPTON_URL.'/modules/'.$sModuleToDelete.'/css/backend.css') ." rel=\"stylesheet\" type=\"text/css\" media=\"screen, projection\" />\n";
		}
	}
	
	require(LEPTON_PATH.'/modules/'.$sModuleToDelete.'/uninstall.php');
}

// Try to delete the module dir
LEPTON_handle::register('rm_full_dir');
if(!rm_full_dir(LEPTON_PATH.'/modules/'.$sModuleToDelete)) 
{
	$admin->print_error($MESSAGE['GENERIC_CANNOT_UNINSTALL']." [14]");
} 
else 
{
	// Remove entry from DB
	$database->simple_query("DELETE FROM ".TABLE_PREFIX."addons WHERE directory = '".$sModuleToDelete."' AND type = 'module'");
}

// remove module permissions
$stmt = [];
$database->execute_query( 
	"SELECT * FROM ".TABLE_PREFIX."groups WHERE group_id != 1 ",
	true,
	$stmt,
	true
);

if (!empty($stmt))
{
    foreach ($stmt as $row)
    {
        $gid = $row['group_id'];
        // get current value
        $modules = explode(',', $row['module_permissions'] );
        // remove uninstalled module
        if (in_array($sModuleToDelete, $modules))
        {
            $i = array_search( $sModuleToDelete, $modules );
            array_splice( $modules, $i, 1 );
            $modules = array_unique($modules);
            asort($modules);

            $database->simple_query("UPDATE `".TABLE_PREFIX."groups` SET `module_permissions`= '".
                implode(',', $modules)."' WHERE `group_id`= ".$gid.";");
        }
    }
}

$admin->print_success($MESSAGE['GENERIC_UNINSTALLED']);

