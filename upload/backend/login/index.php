<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

LEPTON_handle::register("get_page_headers");

//	we want different hashes for the two fields
$salt = sha1(microtime());

$thisApp = LEPTON_login::getInstance(
    [
        'USERNAME_FIELDNAME' => $TEXT['USERNAME']."_".substr($salt, 0, 7),
        'PASSWORD_FIELDNAME' => $TEXT['PASSWORD']."_".substr($salt, -7),
        'LOGIN_URL' => ADMIN_URL."/login/index.php",
        'DEFAULT_URL' => ADMIN_URL."/start/index.php",
        'WARNING_URL' => THEME_URL."/templates/warning.html",
        'REDIRECT_URL' => ADMIN_URL."/start/index.php",
        'FORGOTTEN_URL' => ADMIN_URL."/login/forgot/index.php",
        'TEMPLATE_DIR' => THEME_PATH."/templates",
        'TEMPLATE_FILE' => "login.lte",
        'GET_PAGE_HEADERS' => get_page_headers('backend', false),
        'PAGE_ID' => ''
    ]
);
