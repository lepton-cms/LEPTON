<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// reset value for next login
$database = LEPTON_database::getInstance();
$iTempUserID = $_SESSION['USER_ID'] ?? 0;
$pin_set = $database->get_one("SELECT pin_set FROM ".TABLE_PREFIX."users WHERE user_id = ".$iTempUserID);

if ($pin_set == 2) 
{
	$database->simple_query("UPDATE ".TABLE_PREFIX."users SET `pin_set` = 1 WHERE user_id = ".$iTempUserID);
}	

// delete most critical session variables manually
$_SESSION['USER_ID'] = null;
$_SESSION['GROUPS_ID'] = null;
$_SESSION['USERNAME'] = null;
$_SESSION['PAGE_PERMISSIONS'] = null;
$_SESSION['SYSTEM_PERMISSIONS'] = null;

// delete session cookie and session itself
LEPTON_session::deleteCookieSession(session_name());

// redirect to admin login
die(header('Location: '.ADMIN_URL.'/login/index.php') ?? 0);
