<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// Check if user selected language
if(!isset($_POST['code']) OR $_POST['code'] == "") 
{
	header("Location: index.php");
	exit(0);
}

// Extra protection
if(trim($_POST['code']) == '') 
{
	header("Location: index.php");
	exit(0);
}

$admin = LEPTON_admin::getInstance('Addons', 'languages_uninstall');


// Check if the language exists
if(!file_exists(LEPTON_PATH.'/languages/'.$_POST['code'].'.php')) 
{
	$admin->print_error($MESSAGE['GENERIC_NOT_INSTALLED']);
}

// Check if the language is in use
if( ($_POST['code'] == DEFAULT_LANGUAGE) OR ($_POST['code'] == LANGUAGE ) )
{
	$admin->print_error($MESSAGE['GENERIC_CANNOT_UNINSTALL_IN_USE']." [L1]");
} 
else 
{	
	$query_users = $database->get_one("SELECT `user_id` FROM `".TABLE_PREFIX."users` WHERE `language` = '".addslashes($_POST['code'])."' LIMIT 1");
	
	if(!is_null($query_users))
	{
		$admin->print_error($MESSAGE['GENERIC_CANNOT_UNINSTALL_IN_USE']." [L2]");
	}
}

// Try to delete the language code
if(!unlink(LEPTON_PATH.'/languages/'.$_POST['code'].'.php')) 
{
	$admin->print_error($MESSAGE['GENERIC_CANNOT_UNINSTALL']." [L3]");
} 
else 
{
	// Remove entry from DB
	$database->simple_query("DELETE FROM `".TABLE_PREFIX."addons` WHERE `directory` = '".$_POST['code']."' AND `type` = 'language'");
}

$admin->print_success($MESSAGE['GENERIC_UNINSTALLED']);

