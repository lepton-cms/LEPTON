<?php

/**
 *  @template       LEPSem
 *  @version        see info.php of this template
 *  @author         cms-lab
 *  @copyright      2010-2025 cms-lab
 *  @license        GNU General Public License
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

//	Template/Theme Description
$template_description 	= 'Enhanced backend theme for LEPTON CMS.';

$THEME = [
	'ADMIN_ONLY' 		=> 'Settings for administrator only',
	'NO_SHOW_THUMBS' 	=> 'Hide thumbnails',
	'TEXT_HEADER' 		=> 'Set maximum imagesize for a folder</b><br><small><i>(resizing on new uploads only)</i></small>',
	'ADDON_PERMISSIONS' => 'Addon Permissions',	
	'ADDON_RELOAD' 		=> 'All addon-infos are reloaded from database.',	
	'ADD_LEVEL_0' 		=> 'add level 0',
	'BACKEND_ACCESS' => 'Backend Access',
	'UPLOAD' 			=> 'upload',
	'CANNOT_DELETE'		=> 'Cannot delete User - User got statusflags 32.',	
	'COPY'				=> 'Copy',
	'CREATE' 			=> 'create',	
	'DASHBOARD'			=> 'Installation Overview',
	'DISPLAY'			=> 'Display',
	'EDIT'				=> 'Edit',
	'OVERVIEW'			=> 'Pages Overview',
	'SITE_INFOS' 		=> 'Site-Statistics',
	'HELP_LINKS' 		=> 'Helpful Links',		
	'PAGE' 				=> 'Count pages',
	'PAGE_ID' 			=> 'ID',		
	'PAGE_DETAILS' 		=> 'Page Details',
	'PAGE_PERMISSION' 	=> 'Page Permissions',		
	'SECTIONS' 			=> 'Count sections',
	'MAILER_SMTP_SECURE' => 'SMTP Secure',
	'MAILER_SMTP_PORT'	=> 'SMTP Port',		
	'MODIFIED_WHEN'		=> 'Last update',
	'NEED_CURRENT_PASSWORD'	=> 'confirm changes with current password',		
	'LINK_FE' 			=> 'Link frontend',
	'LINK_BE' 			=> 'Link backend',
	'UPDATE' 			=> 'A later LEPTON version is released! Current Version: ',
	'LINK_HOME' 		=> 'For details please see ',
	'HOMEPAGE' 			=> 'LEPTON Homepage',
	'MODULES' 			=> 'Installed Modules',
	'LANGUAGES' 		=> 'Installed Languages',
	'TEMPLATES' 		=> 'Installed Templates',
	'USERS' 			=> 'Registered Users',
	'PW_INFO' 			=> 'Please let fields empty if you do not want to change password.',
	'GROUPS' 			=> 'Registered Groups',
	'ACCESS'            => 'Preferences access',
	'ENABLE_ACCESS'     => 'Enable Access',
	'WYSIWYG_HISTORY' 	=> 'WYSIWYG History'
];
