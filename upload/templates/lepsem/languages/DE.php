<?php

/**
 *  @template       LEPSem
 *  @version        see info.php of this template
 *  @author         cms-lab
 *  @copyright      2010-2025 cms-lab
 *  @license        GNU General Public License
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

//	Template/Theme Description
$template_description 	= 'Ein erweitertes Backend-Theme für LEPTON CMS.';

$THEME = [
	'ADMIN_ONLY' 		=> 'Diese Optionen nur Administratoren zugänglich machen!',
	'NO_SHOW_THUMBS' 	=> 'Vorschaubilder verstecken',
	'TEXT_HEADER' 		=> 'Maximale Bildergröße für Ordner festlegen</b><br><small><i>(Änderung nur beim Hochladen)</i></small>',
	'ADDON_PERMISSIONS' => 'Zugriffsrechte für Addons',
	'ADDON_RELOAD' 		=> 'Alle Addon-Infos werdern neu aus der Datenbank geladen.',
	'ADD_LEVEL_0' 		=> 'auch Ebene 0',
	'BACKEND_ACCESS' 	=> 'Backend Zugang',	
	'UPLOAD' 			=> 'hochladen',
	'CANNOT_DELETE'		=> 'Löschen nicht möglich - User hat statusflags 32.',		
	'COPY'				=> 'Kopieren',
	'CREATE' 			=> 'erstellen',		
	'DASHBOARD'			=> 'Überblick über die Installation',
	'DISPLAY'			=> 'Vorschau',
	'EDIT'				=> 'Editieren',
	'OVERVIEW'			=> 'Übersicht Seiten',	
	'SITE_INFOS' 		=> 'Seiten-Statistik',
	'HELP_LINKS' 		=> 'Hilfreiche Links',			
	'PAGE' 				=> 'Anzahl Seiten',
	'PAGE_ID' 			=> 'ID',	
	'PAGE_DETAILS' 		=> 'Seiten Details',
	'PAGE_PERMISSION' 	=> 'Seiten Berechtigung',	
	'SECTIONS' 			=> 'Anzahl Sektionen',
	'MAILER_SMTP_SECURE' => 'SMTP Sicherheit',
	'MAILER_SMTP_PORT'	=> 'SMTP Port',		
	'MODIFIED_WHEN'		=> 'Letzte Änderung',
	'NEED_CURRENT_PASSWORD'	=> 'Änderungen mit aktuellem Passwort bestätigen',	
	'LINK_FE' 			=> 'Link Frontend',
	'LINK_BE' 			=> 'Link Backend',
	'UPDATE' 			=> 'Eine neuere LEPTON Version ist verfügbar! Aktuelle Version: ',
	'LINK_HOME' 		=> 'Weitere Informationen auf der ',
	'HOMEPAGE' 			=> 'LEPTON Homepage',
	'MODULES' 			=> 'Installierte Module',
	'LANGUAGES' 		=> 'Installierte Sprachen',
	'TEMPLATES' 		=> 'Installierte Templates',
	'USERS' 			=> 'Eingerichtete User',
	'PW_INFO' 			=> 'Bitte Felder leer lassen, wenn das Passwort nicht geändert werden soll.',	
	'GROUPS' 			=> 'Eingerichtete Gruppen',
    'ACCESS'            => 'Zugriff auf eigene Einstellungen',
	'ENABLE_ACCESS'     => 'Zugriff erlauben',
	'WYSIWYG_HISTORY' 	=> 'WYSIWYG Historie'
];
