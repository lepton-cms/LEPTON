{#
/**
 *  @template       LEPSem
 *  @version        see info.php of this template
 *  @author         cms-lab
 *  @copyright      2010-2025 cms-lab
 *  @license        GNU General Public License
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 #}
{% autoescape false %}
<div class="ui teal segment"> <!-- start outer teal segment -->
	<div class="ui two column doubling grid container"> <!-- start header grid -->

			<div class="column">
				<h2 class="ui teal header">
					<i class="teal user icon"></i>
					<div class="content">{{ TEXT.MANAGE_USERS }}</div>
				</h2>
			</div>			
			<div class="column">		
				<h3 class="ui teal right floated header">
					<div class="content">{{ HEADING.ADD_USER }} / {{ HEADING.MODIFY_USER }}</div>
				</h3>
			</div>

	</div> <!-- end header grid -->

	<div class="ui basic segment"> <!-- start inner teal segment -->	

		<div class="ui two column doubling grid container"> <!-- start content-->	

			{% include "@theme/users_tree.lte" %} <!-- include users tree-->

			<div class="twelve wide column"> <!-- start add users -->
				<form name="{{ FORM_NAME }}" id="{{ FORM_NAME }}" action="{{ ACTION_URL }}" method="post">
					<input type="hidden" name="user_id" value="{{ users.users_id }}" />
					<input type="hidden" name="username_fieldname" value="{{ username_fieldname }}" />
					<input type="hidden" name="job" id="users_form_job" value="save" />					
					
					<div class="ui teal message">
					<b>{{ TEXT.USER_INFO1 }}</b><br>
					{{ TEXT.USER_INFO2 }} {{ AUTH_MIN_LOGIN_LENGTH }} / {{ AUTH_MAX_LOGIN_LENGTH }}<br>
					{{ TEXT.USER_INFO3 }} {{ AUTH_MIN_PASS_LENGTH }} / {{ AUTH_MAX_PASS_LENGTH }}
					</div>				
					<div class="ui two column doubling grid container"> <!-- start level 1-->
					
					<!-- start row users -->
						<div class="column">
							<div class="ui form">
								<div class="required field" data-variation="orange" data-title="{{ TEXT.VALID_CHARS }}"  data-content="{{ getPattern('UserNames') }}">
									<label>{{ TEXT.USERNAME }}:</label>
									<input {{ readonly_and_disabled }} required type="text" name="{{ username_fieldname }}" pattern="{{ getDisplayPattern() }}" value="{{ users.usersname }}" />
								</div>
							</div>							
						</div>	
						
						<div class="column">			
							<div class="ui form">
								<div class="required field" data-variation="orange" data-title="{{ TEXT.VALID_CHARS }}"  data-content="{{ getPattern('UserNames') }}">
									<label>{{ TEXT.DISPLAY_NAME }}:</label>
									<input {{ readonly_and_disabled }} required type="text" name="display_name" pattern="{{ getDisplayPattern() }}" value="{{ users.display_name }}" />
								</div>
							</div>					
						</div>					

						<div class="column">
							<div class="ui form">
								<div class="required field" data-variation="orange" data-title="{{ TEXT.VALID_CHARS }}"  data-content="{{ getPattern('Email') }}">
									<label>{{ TEXT.EMAIL }}:</label>
									<input {{ readonly_and_disabled }} required type="email" name="email" pattern="{{ getEmailPattern() }}" value="{{ users.email }}" />
								</div>
							</div>							
						</div>
						<div class="column">			
							<div class="ui form">
								<div class="field">
									<label>{{ TEXT.HOME_FOLDER }}:</label>
									<select {{ readonly_and_disabled }} name="home_folder">
										<option value="">{{ TEXT.NONE }}</option>
										{% for folder in media_dirs %}
										<option value="{{ folder }}" {%  if (folder == users.home_folder) %}selected='selected'{% endif %}>{{ folder }}</option>
										{% endfor %}
									</select>
								</div>
							</div>					
						</div>									

						<div class="column">		
							<div class="ui form">
								<div class="field">
									<label>{{ TEXT.GROUP }}:</label>
									<select {{ readonly_and_disabled }} name="groups[]" multiple="multiple" id="user_groups">
									{% for group in all_groups %}		
										<option value="{{ group.group_id }}" {% if (group.group_id in groups_ids) %}selected="selected"{% endif %}>{{ group.name }}</option>
									{% endfor %}
									</select>
								</div>
							</div>					
						</div>
							
						<div class="column">							
							<div class="ui segment custom_segment">
								<div class="ui checkbox">
									<input type="hidden" name="active" value='0' />
									<input {{ readonly_and_disabled }} type="checkbox" name="active" id="active" value="1" {% if users.active == 1 %}checked="checked"{% endif %} />
									<label>{{ TEXT.ACTIVE }}</label>
								</div>
							</div>							
						</div>				
	
						

						<div class="column">			
							<div class="ui form">
								<div class="field" data-variation="orange" data-title="{{ TEXT.VALID_CHARS }}"  data-content="{{ getPattern('PassWord') }}">
									<label>{{ TEXT.PASSWORD }}:</label>
									<input {{ readonly_and_disabled }} type="password" name="password" pattern="{{ getPasswordPattern() }}" value="{{ users.password }}" />
								</div>
							</div>					
						</div>	
							
						<div class="column">			
							<div class="ui form">
								<div class="field" data-variation="orange" data-title="{{ TEXT.VALID_CHARS }}"  data-content="{{ getPattern('PassWord') }}">
									<label>{{ TEXT.RETYPE_PASSWORD }}:</label>
									<input {{ readonly_and_disabled }} type="password" name="password2" pattern="{{ getPasswordPattern() }}" value="{{ users.password }}" />
								</div>
							</div>					
						</div>
						<div class="ui basic segment">
							<div class="ui teal message">{{ THEME.PW_INFO }}</div>
							<div class="ui three column stackable grid"> <!-- start submit -->
                    {% if (p_user_modify == true) %}
								<div class="column">							
									<button class="right floated positive ui button lepsem_submit" type="submit" >{{ TEXT.SAVE }}</button>
								</div>
                    {% endif %}
					{% if (p_user_delete == true) %}
								<div class="column">
									<button class="negative ui button lepsem_submit" type="submit" name="delete" id="users_form_delete" onclick="prepare_delete();">{{ TEXT.DELETE }}</button>
								</div>
                    {% endif %}
                    {% if (p_user_modify == true) %}
								<div class="column">			
									<button class="negative ui button lepsem_submit" type="reset" name="reset">{{ TEXT.RESET }}</button>					
								</div>	
                    {% endif %}
							</div> <!-- end submit-->
							<div class="spacer4"></div>
						</div>						
					
					</div> <!-- end level 1 -->

				</form>
			
			</div> <!-- end add users -->
		
		</div> <!-- end content -->	
	
	
	
	</div> <!-- end inner teal segment1 -->

</div> <!-- end outer teal segment -->

<script type="text/javascript">
$('.field')
  .popup({
    inline: true
  })
;

	function prepare_delete() 
	{
		
		if(confirm( "{{ MESSAGE.USERS_CONFIRM_DELETE }}")) {
			var form_ref = document.getElementById("{{ FORM_NAME }}");
			var ref = document.getElementById("users_form_job");
			
			if (ref) {
				ref.value = "delete";
			
				if (form_ref) form_ref.submit();
				
				return true;
			}
		}
		return false;
	}
	
	function clear_users_form() 
	{
		
		//	Set the job to 'save'
		var ref = document.getElementById("users_form_job");
		if (ref) ref.value = "save";
		
		// Set the user id to -1
		ref = document.getElementById("user_id");
		if (ref) ref.value = -1;
		
		// Hide the delete button. 
		var del_button = document.getElementById("users_form_delete");
		if (del_button) {
	    	del_button.style.visibility = "hidden";
		}
		return true;
	}
	
	lepsem_reset_form();
</script>
{% endautoescape %}