<?php

/**
 *  @template       LEPSem
 *  @version        see info.php of this template
 *  @author         cms-lab
 *  @copyright      2010-2025 cms-lab
 *  @license        GNU General Public License
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


$mod_headers = array(
	'backend' => array(
        'css' => array(
		array(
			'media'  => 'all',
			'file'  => 'modules/lib_fomantic/dist/semantic.min.css'
			),
		array(
			'media'  => 'all',
			'file'  => 'modules/lib_jquery/jquery-ui/jquery-ui.min.css'
			),	
		array(
			'media'  => 'all',
			'file'  => 'templates/lepsem/backend/backend/custom.css'
			),
 		),
		
		'js' => array(
			'modules/lib_jquery/jquery-core/jquery-core.min.js',
			'modules/lib_jquery/jquery-core/jquery-migrate.min.js',
			'modules/lib_jquery/external/jquery.tablednd.min.js',
			'modules/lib_jquery/jquery-ui/jquery-ui.min.js',
			'modules/lib_fomantic/dist/semantic.min.js'
		)
	)
);

/**
 *  Keep in mind that EN is the default language of the "datepicker"
 *  and there is NO "datepicker-en.js" file inside the jq-ui framework at all,
 *  we are only in the need to load this one if there is a different language here
 *  as en - or the matching file for e.g. "NO", or "XN" doesn't exist.
 */
$sTempDatePickerLang = lib_jquery::SetJQueryLanguage('datepicker');
if ($sTempDatePickerLang != "en")
{
    $mod_headers["backend"]["js"][] = 'modules/lib_jquery/jquery-ui/i18n/datepicker-'.$sTempDatePickerLang.'.js';
}
