<?php

/**
 *	@template       LEPTON Start
 *	@version        see info.php of this template
 *	@author         cms-lab
 *	@copyright		2010-2025 CMS-LAB
 *	@license        https://creativecommons.org/licenses/by/3.0/
 *	@license terms  see info.php of this template
 *	@platform       see info.php of this template
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// TEMPLATE CODE STARTS BELOW
?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo DEFAULT_CHARSET; ?>" />	
	<title><?php echo WEBSITE_TITLE.' | '.PAGE_TITLE; ?></title>  
	<meta name="description" content="<?php echo DESCRIPTION; ?>" />
	<meta name="keywords" content="<?php echo KEYWORDS; ?>" />
	<?php get_page_headers();	?>  
	<link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_DIR; ?>/css/template.css" media="screen,projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_DIR; ?>/css/print.css" media="print" />
	<!-- Begin Cookie Consent plugin by Silktide - https://silktide.com/cookieconsent -->
	[[site-cookie]]
	<!-- End Cookie Consent plugin -->		
</head>
<body>
<div id="site">
	<div id="head">
		<a href="<?php echo LEPTON_URL;?>"><img class="head_img" src="<?php echo TEMPLATE_DIR;?>/img/1.jpg" width="900" height="180" alt="Head" /></a>
	</div>
	<div id="headtitle">
		<?php echo WEBSITE_HEADER; ?>
	</div>

	<!-- Left Column -->
	<div id="side">	
		<div id="navi1">
		<!-- Main-Navigation on the left) -->
    	<?php
			show_menu2(1, SM2_ROOT, SM2_ROOT+2, SM2_TRIM|SM2_PRETTY|SM2_XHTML_STRICT); 
    	?>
		</div>
		
		<!-- OPTIONAL: display frontend search -->
		[[LEPTON_SearchBox]]
		<!-- END frontend search -->

		<!-- OPTIONAL: display frontend login -->
		<div id="login">
		[[LoginBox]]
		</div>

		<div id="frontedit">
			[[editthispage]]
		</div>

	</div>    <!-- END left column -->   

	<!-- Content -->
	
	<div id="cont">
		<?php page_content(1); ?>
	</div>
		<br style="clear: both;" />
	<div id="foot">
	<?php 
		show_menu2(2, SM2_ROOT, SM2_ALL, SM2_TRIM|SM2_PRETTY|SM2_XHTML_STRICT);
	?>
</div>

<!-- Block Bottom -->
	<div id="basic">
		<div id="links">
			<?php echo WEBSITE_FOOTER; ?>
		</div>
		<div id="design">
			<a href='https://cms-lab.com'>Design by CMS-LAB</a>
		</div>
	</div>
</div>
<?php
	get_page_footers();
?>
</body>
</html>
