<?php

/**
 *  @template       Semantic
 *  @version        see info.php of this template
 *  @author         cms-lab
 *  @copyright      2010-2025 CMS-LAB
 *  @license        https://creativecommons.org/licenses/by/3.0/
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$database = LEPTON_database::getInstance();
$oLEPTON = LEPTON_frontend::getInstance();
$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerPath( dirname(__FILE__)."/templates/"  );

// load language
$languages = [];
$database->execute_query(
	"SELECT directory, name FROM ".TABLE_PREFIX."addons WHERE type = 'language'",
	true,
	$languages,
	true
 );
 
$language = [];
foreach($languages as $data)
{
	$language[] =[
		'LANG_CODE' 	=>	$data['directory'],
		'LANG_NAME'		=>	$data['name'],
		'LANG_SELECTED'	=> (LANGUAGE == $data['directory']) ? " selected='selected'" : ""
	];
}


/**	****************
 *	default timezone
 *
 */
$timezone_table = LEPTON_date::get_timezones();
$timezone = [];
foreach ($timezone_table as $title)
{
	$timezone[] = [
		'TIMEZONE_NAME' => $title,
		'TIMEZONE_SELECTED' => ($oLEPTON->getValue('timezone_string', 'string', 'session') == $title) ? ' selected="selected"' : ''
	];
}

/**	***********
 *	date format
 */

$date_format = [];
$user_time = true;
$DATE_FORMATS =  LEPTON_date::get_dateformats();
foreach($DATE_FORMATS AS $format => $title) 
{
	$format = str_replace('|', ' ', $format); // Add's white-spaces (not able to be stored in array key)
	
	$value = ($format != 'system_default') ? $format : "";

	if(DATE_FORMAT == $format AND !isset($_SESSION['USE_DEFAULT_DATE_FORMAT'])) {
		$sel = "selected='selected'";
	} elseif($format == 'system_default' AND isset($_SESSION['USE_DEFAULT_DATE_FORMAT'])) {
		$sel = "selected='selected'";
	} else {
		$sel = '';	
	}			
	$date_format[] = array(
		'DATE_FORMAT_VALUE'	=>	$value,
		'DATE_FORMAT_TITLE'	=>	$title,
		'DATE_FORMAT_SELECTED' => $sel
	);
}

/**	***********
 *	time format
 */
$time_format = [];

$TIME_FORMATS =  LEPTON_date::get_timeformats();
foreach($TIME_FORMATS AS $format => $title) 
{
	$format = str_replace('|', ' ', $format); // Add's white-spaces (not able to be stored in array key)

	$value = ($format != 'system_default') ? $format : "";

	if(TIME_FORMAT == $format AND !isset($_SESSION['USE_DEFAULT_TIME_FORMAT'])) {
		$sel = "selected='selected'";	
	} elseif($format == 'system_default' AND isset($_SESSION['USE_DEFAULT_TIME_FORMAT'])) {
		$sel = "selected='selected'";
	} else {
		$sel = '';
	}			
	$time_format[] = array(
		'TIME_FORMAT_VALUE'	=>	$value,
		'TIME_FORMAT_TITLE'	=>	$title,
		'TIME_FORMAT_SELECTED' => $sel
	);
}

/**
 *	Build an access-preferences-from secure hash
 */
if(!function_exists("random_string")) require_once( LEPTON_PATH."/framework/functions/function.random_string.php");
$hash = sha1( microtime().$_SERVER['HTTP_USER_AGENT'].random_string( 32 ) );
$_SESSION['wb_apf_hash'] = $hash;

/**
 *	Delete any "result_message" if there is one.
 */
if( true === isset($_SESSION['result_message']) ) unset($_SESSION['result_message']);

$data = array(
	'TEMPLATE_DIR' 				=>	TEMPLATE_DIR,
	'PREFERENCES_URL'			=>	PREFERENCES_URL,
	'LOGOUT_URL'				=>	LOGOUT_URL,
	'DISPLAY_NAME'				=>	$oLEPTON->getValue('display_name', 'string', 'session'),
	'GET_EMAIL'					=>	$oLEPTON->getValue('email', 'email', 'session'),
	'USER_ID'					=>	(isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : '-1'),
	'r_time'					=>	TIME(),
	'HASH'						=>	$hash,
	'RESULT_MESSAGE'			=> (isset($_SESSION['result_message'])) ? $_SESSION['result_message'] : "",
	'AUTH_MIN_LOGIN_LENGTH'		=> AUTH_MIN_LOGIN_LENGTH,
	'language'	=> $language,
	'timezone'	=> $timezone,
	'date_format' => $date_format,
	'time_format' => $time_format	
);

echo $oTWIG->render("preferences_form.lte", $data);		
