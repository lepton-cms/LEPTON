<?php

/**
 *  @template       Semantic
 *  @version        see info.php of this template
 *  @author         cms-lab
 *  @copyright      2010-2025 CMS-LAB
 *  @license        https://creativecommons.org/licenses/by/3.0/
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// OBLIGATORY VARIABLES
$template_directory     = 'semantic';
$template_name          = 'Semantic-Frontend';
$template_function      = 'template';
$template_delete  		=  true;
$template_version       = '3.0.11';
$template_platform      = '7.0';
$template_author        = 'CMS-LAB';
$template_license       = 'https://creativecommons.org/licenses/by/3.0/';
$template_license_terms = 'you have to keep the frontend-backlink to cms-lab untouched';
$template_description   = 'This template bases on <a href="https://fomantic-ui.com" target="_blank">Fomantic</a>';
$template_guid          = 'd4365367-db03-4f3c-8304-42e88abc008f';

// OPTIONAL VARIABLES FOR ADDITIONAL MENUES AND BLOCKS
$menu[ 1 ]  = 'Main';
$menu[ 2 ]  = 'Foot';
$menu[ 3 ]  = 'Pseudomenu';
$block[ 1 ] = 'Content1';
$block[ 2 ] = 'Content2';
$block[ 3 ] = 'Content3';
$block[ 4 ] = 'News';
