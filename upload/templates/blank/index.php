<?php

/**
 *  @template       blank
 *  @version        see info.php of this template
 *  @author         erpe
 * @copyright       2010-2025 erpe 
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file



// TEMPLATE CODE STARTS BELOW
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo DEFAULT_CHARSET; ?>" />
	<?php get_page_headers(); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_DIR; ?>/template.css" media="screen,projection" />
	<title><?php echo WEBSITE_TITLE.' | '.PAGE_TITLE; ?></title>  
	<meta name="description" content="<?php echo DESCRIPTION; ?>" />
	<meta name="keywords" content="<?php echo KEYWORDS; ?>" />
</head>
<body>
<?php
	/**
	 *	TEMPLATE CODE STARTS BELOW
	 *	output only the page content, nothing else
	 */
	page_content();
?>

</body>
</html>