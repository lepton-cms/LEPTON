<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file



/**
 * Constants used in field 'status-flags' of table 'users'
 */
const USERS_DELETED             =  1; // User marked as deleted
const USERS_ACTIVE              =  2; // User is activated
const USERS_CAN_SETTINGS        =  4; // User can change own settings
const USERS_CAN_SELF_DELETE     =  8; // User can delete himself
const USERS_PROFILE_ALLOWED     = 16; // User can create a profile page
const USERS_PROFILE_AVAIL       = 32; // User has fulfilled profile and can not be deleted via core
const USERS_DEFAULT_SETTINGS    = USERS_ACTIVE | USERS_CAN_SETTINGS;  // provided as status-flags = 6 in users table

// Get an instance from class database
$database = LEPTON_database::getInstance();

// Get website settings (title, keywords, description, header, and footer) to define constants
$storage = [];
$database->execute_query( 
	"SELECT name, value FROM ". TABLE_PREFIX."settings ORDER BY name ", 
	true, 
	$storage 
);

foreach ( $storage as $row )
{
	if ( preg_match( '/^0[0-7]{1,3}$/', $row[ 'value' ] ) === 1 )
	{
		$value = $row[ 'value' ];
	}
	elseif ( preg_match( '/^[0-9]+$/S', $row[ 'value' ] ) === 1 )
	{
		$value = intval( $row[ 'value' ] );
	}
	elseif ( $row[ 'value' ] == 'false' )
	{
		$value = false;
	}
	elseif ( $row[ 'value' ] == 'true' )
	{
		$value = true;
	}
	else
	{
		$value = $row[ 'value' ];
	}

	// decode html tags
	if( ($row['name'] == "website_header") || ($row['name'] == "website_footer") )
	{
		LEPTON_handle::restoreSpecialChars( $value ); 
	}
	
	// create constant from each settings value
	$temp_name = strtoupper( $row[ 'name' ] );
	if ( !defined( $temp_name ) )
	{
		define( $temp_name, $value );
	}
}
unset( $row );


if ( !defined( 'LEPTON_INSTALL_PROCESS' ) )
{
	// get CAPTCHA and ASP settings
	$setting = [];
	$database->execute_query( 
		"SELECT * FROM ".TABLE_PREFIX."mod_captcha_control ", 
		true, 
		$setting, 
		false 
	);
	
	if ( empty($setting))
	{
		die( "CAPTCHA-Settings not found" );
	}
	
	define( 'ENABLED_CAPTCHA', $setting[ 'enabled_captcha' ]);
	define( 'ENABLED_ASP', $setting[ 'enabled_asp' ]);
	define( 'CAPTCHA_TYPE', $setting[ 'captcha_type' ] );
	
	if(ENABLED_ASP == 1)
	{
		define( 'ASP_SESSION_MIN_AGE', (int) $setting[ 'asp_session_min_age' ] );
		define( 'ASP_VIEW_MIN_AGE', (int) $setting[ 'asp_view_min_age' ] );
		define( 'ASP_INPUT_MIN_AGE', (int) $setting[ 'asp_input_min_age' ] );
	}	
	unset( $setting );


	// Setting the correct default timezone to avoid "date" conflicts and warnings
    $timezone_string = ($_SESSION['TIMEZONE_STRING'] ?? DEFAULT_TIMEZONE_STRING);
    date_default_timezone_set($timezone_string);
 
    // Get users date format
    define('DATE_FORMAT', ($_SESSION['DATE_FORMAT'] ?? DEFAULT_DATE_FORMAT));

    // Get users time format
    define('TIME_FORMAT', ($_SESSION['TIME_FORMAT'] ?? DEFAULT_TIME_FORMAT));

    // Set Theme dir
    define('THEME_URL', LEPTON_URL . '/templates/' . DEFAULT_THEME);
    define('THEME_PATH', LEPTON_PATH . '/templates/' . DEFAULT_THEME);

    // Set Module dir
    define('MODULE_URL', LEPTON_URL . '/modules');
    define('MODULE_PATH', LEPTON_PATH . '/modules');

    // Set Secure dir
    define('SECURE_URL', LEPTON_URL . '/temp/secure');
    define('SECURE_PATH', LEPTON_PATH . '/temp/secure');

} 
else 
{
   // Set Theme dir
    define('THEME_URL', LEPTON_URL . '/templates/lepsem');
    define('THEME_PATH', LEPTON_PATH . '/templates/lepsem');
}
