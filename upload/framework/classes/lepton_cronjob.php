<?php

declare(strict_types=1);

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// @ADD_cronjob 20230727, include cronjob file for external call
class LEPTON_cronjob
{
	public $cj_array = []; 	// content of cronjob.ini file
	public string $cj_path = LEPTON_PATH;
	public string $cj_file = '';
	public string $cj_key = '';
	public string $cj_interval = '-1';
	public int $cj_time = -1;


	public object|null $oTwig = null;
	public object|null $database = null;
	public object|null $mailer = null;
	public static $instance;
	
	// Return the instance of this class.
    public static function getInstance()
    {
        if (null === static::$instance)
        {
            static::$instance = new static();
            static::$instance->initialize();
        }
        return static::$instance;
    }
	
	public function initialize()
	{
		$this->database = LEPTON_database::getInstance();
		$this->oTwig = lib_twig_box::getInstance();
		$this->mailer = LEPTON_mailer::getInstance();
		$this->cj_time = time();
	
		$cronjob_ini = LEPTON_PATH."/config/cronjob.ini.php";
		$this->cj_array = parse_ini_file($cronjob_ini, true);
		
		// set values
		$this->cj_path = $this->cj_array['file_info']['path'];
		$this->cj_file = $this->cj_array['file_info']['file'];
		$this->cj_key = $this->cj_array['file_info']['key'];
		$this->cj_interval = $this->cj_array['file_info']['interval'];
		
		if(!isset($_SESSION['last_cronjob']))
		{
			$_SESSION['last_cronjob'] = $_SESSION['SESSION_STARTED'] - ($this->cj_interval +1);
		}		
		
		if(strlen($this->cj_key) < AUTH_MIN_PASS_LENGTH)
		{
			die (LEPTON_tools::display('NOT allowed: key must have a min of chars ("AUTH_MIN_PASS_LENGTH") in cronjob.ini.php','pre','ui red message'));
		}
		
		if(!file_exists(LEPTON_PATH.$this->cj_path.$this->cj_file) )
		{
			die (LEPTON_tools::display('You have to specify a correct file in cronjob.ini.php to use this class!','pre','ui red message'));
		}		
	}

	
	public function send_mail( $id = -99 )
	{
		global $TEXT;
		
		$sFrom = SERVER_EMAIL;
		$sSendTo = SERVER_EMAIL;
		$sSubject = 'cronjob_info';
		$sMessage = "

		";	

		if (!$this->mailer->sendmail( $sFrom, $sSendTo, $sSubject, $sMessage)) 
		{		
			$message = "Failure send mail";	
			echo(LEPTON_tools::display($message,'pre','ui red message'));	
		}			
	}
}
