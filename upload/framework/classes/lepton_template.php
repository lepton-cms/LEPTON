<?php

declare(strict_types=1);

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

/**
 *  This is only an abstract class for LEPTON specific classes and inside modules.
 *  Keep in mind that this one looks similar to the LEPTON_abstract class but it's slightly different.
 *
 */
abstract class LEPTON_template
{
    /**
     *  Array with the language(-array) of the child-object.
     *  @type   array
     *
     */
    public $language = array();

    /**
     *  Array with the names of all parents (desc. order)
     *  @type   array
     *
     */    
    public $parents = array();

    /**
     *  The template directory from the info.php of the child.
     *  @type   string
     *
     */    
    public $template_directory = "";
    
    /**
     *  The template name from the info.php of the child.
     *  @type   string
     *
     */
    public $template_name = "";
    
    /**
     *  The template function from the info.php of the child.
     *  @type   string
     *
     */
    public $template_function = "";
    
    /**
     *  The template version from the info.php of the child.
     *  @type   string
     *
     */
    public $template_version = "";

    /**
     *  The template platform from the info.php of the child.
     *  @type   string
     *
     */
    public $template_platform = "";

    /**
     *  The template author from the info.php of the child.
     *  @type   string
     *
     */
    public $template_author = "";

    /**
     *  The template license from the info.php of the child.
     *  @type   string
     *
     */
    public $template_license = "";

    /**
     *  The template license terms from the info.php of the child.
     *  @type   string
     *
     */
    public $template_license_terms = "";

    /**
     *  The template description from the info.php of the child.
     *  @type   string
     *
     */
    public $template_description = "";

    /**
     *  The template guid from the info.php of the child.
     *  @type   string
     *
     */
    public $template_guid = "";

    /**
     *  The template block(-s).
     *  @type   array
     *
     */
    public $block = [];
    
    /**
     *  The template menu(-s).
     *  @type   array
     *
     */
    public $menu = [];
    
    /**
     *  @var    object  The reference to the *Singleton* instance of this class.
     *  @notice         Keep in mind that a child-object has to have his own one!
     */
    public static $instance;

    /**
     *  Return the instance of this class.
     *
     */
    public static function getInstance()
    {
        if (null === static::$instance)
        {
            static::$instance = new static();
            static::$instance->getParents();
            static::$instance->getTemplateInfo();
            static::$instance->getLanguageFile();
            static::$instance->initialize();
        }
        return static::$instance;
    }
 
    /**
     *  Try to get all parents form the current instance as a simple linear list.
     */
    protected function getParents()
    {
        // First the class itself
        static::$instance->parents[] = get_class(static::$instance);
        
        // Now the parents        
        $aTempParents = class_parents( static::$instance, true );
        foreach($aTempParents as $sParentname)
        {
            static::$instance->parents[] = $sParentname;
        }
    }
   
    /**
     *  Try to read the module specific info.php from the module-Directory
     *  and update the current class-properties.
     *
     */
    protected function getTemplateInfo()
    {
        
        foreach(static::$instance->parents as $sModuleDirectory)
        {
            //  strip namespace
            $aTemp = explode("\\", $sModuleDirectory);
            $sModuleDirectory = array_pop($aTemp);
            
            $sLookUpPath = __DIR__."/../../templates/".$sModuleDirectory."/info.php";
            if( file_exists($sLookUpPath) )
            {
                require $sLookUpPath;
                // [1.1]
                if(isset($template_name))
                {
                    static::$instance->template_name = $template_name;
                }
                // [1.2]
                if(isset($template_directory))
                {
                    static::$instance->template_directory = $template_directory;
                }
                // [1.3]
                if(isset($template_function))
                {
                    static::$instance->template_function = $template_function;
                }
                // [1.4]
                if(isset($template_version))
                {
                    static::$instance->template_version = $template_version;
                }
                // [1.5]
                if(isset($template_platform))
                {
                    static::$instance->template_platform = $template_platform;
                }
                // [1.6]
                if(isset($template_author))
                {
                    static::$instance->template_author = $template_author;
                }
                // [1.7]
                if(isset($template_license))
                {
                    static::$instance->template_license = $template_license;
                }
                // [1.8]
                if(isset($template_license_terms))
                {
                    static::$instance->template_license_terms = $template_license_terms;
                }
                // [1.9]
                if(isset($template_description))
                {
                    static::$instance->template_description = $template_description;
                }
                // [1.10]
                if(isset($template_guid))
                {
                    static::$instance->template_guid = $template_guid;
                }
                // [1.11]
                if(isset($block))
                {
                    static::$instance->block = $block;
                }
                // [1.12]
                if(isset($menu))
                {
                    static::$instance->menu = $menu;
                }
                
                break;
            }
        }
    }
    
    /**
     *  Try to get a module-spezific language file.
     */
    protected function getLanguageFile()
    {
        if (defined("LEPTON_PATH"))
        {
            $aLookUpFilenames = [
                LANGUAGE."_add.php",
                LANGUAGE."_custom.php",
                LANGUAGE.".php",
                "EN_custom.php",
                "EN.php"
            ];
            
            foreach (static::$instance->parents as $sClassName)
            {
                //  Strip the namespace
                $aTemp = explode("\\", $sClassName);
                $sClassName = array_pop($aTemp);

                $lookUpPath = LEPTON_PATH."/templates/".$sClassName."/languages/";
                
                $bFoundFile = false;
                
                foreach ($aLookUpFilenames as $sTempFilename)
                {
                    if (true === file_exists($lookUpPath.$sTempFilename))
                    {
                        if (isset($template_description))
                        {
                            unset($template_description);
                        }
                        require $lookUpPath.$sTempFilename;
                        $bFoundFile = true;
                        break;
                    }
                }
                
                if (false === $bFoundFile)
                {
                    continue;
                }
                
                $tempName = (static::$instance->template_function == "theme" 
                    ? "THEME" 
                    : "TEMPLATE_".strtoupper($sClassName)
                );
                
                if (isset(${$tempName}))
                {
                    static::$instance->language = ${$tempName};
                    
                    /**
                     *  Overwrite the $template_description with the current language one.
                     */
                    if (isset($template_description))
                    {
                        $this->template_description = $template_description;
                    }
                    break;
                }
            }
        }
    }

    /**
     *  Abstact declarations - to be overwrite by the child-instance.
     */
    abstract protected function initialize();
}