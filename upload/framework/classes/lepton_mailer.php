<?php

declare(strict_types=1);

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
require_once (LEPTON_PATH . "/modules/lib_phpmailer/library.php");

class LEPTON_mailer extends PHPMailer\PHPMailer\PHPMailer
{

	/**
	 *	@var Singleton The reference to *Singleton* instance of this class
	 */
	public static $instance;
	
	/**
	 *	Return the »internal«
	 *
	 *	@param	array	Optional params
	 */
	public static function getInstance( &$settings=array() )
	{
		if (null === static::$instance)
		{
			static::$instance = new static();
			static::$instance->__construct();
		}

		// clear all "old" addresses
		static::$instance->clearAllRecipients(); // seems not to work by getInstance()
		
		return static::$instance;
	}
	
	/**
	 *	Constructor of the class
	 */
	public function __construct()
	{
		// set method to send out emails
		if ( MAILER_ROUTINE == "smtp" && strlen( MAILER_SMTP_HOST ) > 5 )
		{
			$bCheckModul = extension_loaded('openssl');
			// use SMTP for all outgoing mails send
			$this->IsSMTP();
			$this->SMTPAuth = true; // default if smtp
			$this->SMTPSecure = MAILER_SMTP_SECURE;
			$this->Port = MAILER_SMTP_PORT; 			
			
			// check if decrypting possible
			if ( $bCheckModul === true)
			{
				// decrypt values
				$this->Host = LEPTON_database::decryptstring(MAILER_SMTP_HOST);
				$this->Username = LEPTON_database::decryptstring(MAILER_SMTP_USERNAME);
				$this->Password = LEPTON_database::decryptstring(MAILER_SMTP_PASSWORD);
			}
			else
			{
				// use unencrypted values
				$this->Host = MAILER_SMTP_HOST;
				$this->Username = MAILER_SMTP_USERNAME;
				$this->Password = MAILER_SMTP_PASSWORD;				
			}
		}
		else
		{
			// use PHP mail() function for outgoing mails send by Website Baker
			$this->IsMail();
		}
		
		// set language file for PHPMailer error messages
		if ( defined( "LANGUAGE" ) )
		{
		    $this->SetLanguage( strtolower( LANGUAGE ), "language" ); // english default (also used if file is missing)
        }
        
		// set default charset
		$this->CharSet =  defined( 'DEFAULT_CHARSET' ) ? DEFAULT_CHARSET : 'utf-8';
		
		// set default sender name
		if ( $this->FromName == 'Root User' )
		{
			$this->FromName = isset( $_SESSION[ 'DISPLAY_NAME' ] ) 
				? $_SESSION[ 'DISPLAY_NAME' ] 
				: MAILER_DEFAULT_SENDERNAME
				;
		}
		
		/* 
			some mail provider (lets say mail.com) reject mails send out by foreign mail 
			relays but using the providers domain in the from mail address (e.g. myname@mail.com)
		*/
		$this->From = SERVER_EMAIL; // FROM MAIL: (server mail)
		
		// set default mail formats
		$this->IsHTML( true );
		$this->WordWrap = 80;
		$this->Timeout  = 30;
	}

	/**
	 *	Simple sendMail method
	 *
	 *	@param	string	Sender from xxx (e-mail)
	 *	@param	string	Address Send to (e-mail)
	 *	@param	string	A subject string
	 *	@param	string	The message(-text) to be send
	 *	@return boolean	True if success, otherwise false.
	 *
	 */
	public function sendmail( $sFrom="", $sSendTo="", $sSubject="", $sMessage="")
	{
		$this->From = $sFrom;    
		$this->AddAddress( $sSendTo );   
		$this->Subject = $sSubject;	// SUBJECT
		$this->Body = $sMessage;	// CONTENT (HTML)
		$this->AltBody = $sMessage;	// CONTENT (PLAINTEXT)
		$this->CharSet="UTF-8";		// force text to be utf-8

		return $this->send();
	}
}
