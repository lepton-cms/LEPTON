<?php

declare(strict_types=1);

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
class LEPTON_tools
{
    /**
     *  To use "var_dump" instead of "print_r" inside the "display"-method.
     *
     *  @property bool	$use_var_dump For the use of 'var_dump'.
     */
    static public bool $use_var_dump = false;

    /**
     *  Method to change the var_dump_mode
     *
     *  @param  boolean    $bUseVarDump True, to use "var_dump" instead of "print_r" for the "display"-method, false if not. Default is "true".
     *  @see    __self__::display
     *
     */
    static public function use_var_dump(bool $bUseVarDump=true): void
    {
        self::$use_var_dump = $bUseVarDump;
    }

    /**
     *  Static method to return the result of a "print_r" call for a given object/address.
     *
     * @param mixed       $something_to_display Any (e.g. mostly an object instance, or e.g. an array)
     * @param string      $tag                  Optional a "tag" (-name). Default is "pre".
     * @param string|null $css_class            Optional a class name for the tag.
     * @param bool|null   $useVarDump           Optional overwrite internal setting. Must be a boolean.
     *
     * @return string
     *
     *  example given:  
     *  @code{.php}
     *      LEPTON_tools::display( $result_array, "code", "example_class" )  
     *  @endcode
     *      will return:  
     *  @code{.xml}
     *
     *      <code class="example_class">  
     *          array( [1] => "whatever");  
     *      </code>  
     *
     *  @endcode
     *
     */
    static function display(
        mixed       $something_to_display ="",
        string      $tag="pre",
        string|null $css_class = null,
        bool|null   $useVarDump = null
    ): string
    {
        if (is_null($something_to_display))
        {
            $something_to_display = "The value is NULL!";
        }
        
        // [0] useVarDump?
        $useVarDumpParam = is_bool($useVarDump)
            ? (bool)$useVarDump
            : false
            ;
            
        $sReturnVal = "\n<".$tag.(null === $css_class ? "" : " class='".$css_class."'").">\n";
        ob_start();
            ((true === self::$use_var_dump) || (true === $useVarDumpParam))
            ? var_dump($something_to_display)
            : print_r($something_to_display)
            ;
        $sReturnVal .= ob_get_clean();
        $sReturnVal .= "\n</".$tag.">\n";

        return $sReturnVal;
    }

    /**
     * Static method to return the result of a "print_r" call for a given object/address like above
     * but add additional called file and line to the output.
     *
     * @param mixed       $something_to_display Any (e.g. mostly an object instance, or e.g. an array)
     * @param string      $tag                  Optional a "tag" (-name). Default is "pre".
     * @param string|null $css_class            Optional a class name for the tag.
     * @param bool|null   $useVarDump           Optional overwrite internal setting. Must be a boolean.
     *
     * @return string
     *
     *  example given:..
     *  @code{.php}
     *      LEPTON_tools::display_dev( $result_array, "code", "example_class" )  
     *  @endcode
     *      will return:  
     *  @code{.xml}
     *
     *      <code class="example_class">  
     *          Location: ~modules/whatever/ajax/CallMe.php ->Line: 208  
     *          array( [1] => "whatever");  
     *      </code>  
     *
     *  @endcode
     *
     */
    static function display_dev(
        mixed       $something_to_display = "",
        string      $tag = "pre",
        string|null $css_class = null,
        bool|null   $useVarDump = null
    ): string
    {
        if (is_null($something_to_display))
        {
            $something_to_display = "The value is NULL!";
        }
        // [0] useVarDump?
        $useVarDumpParam = is_bool($useVarDump)
            ? (bool)$useVarDump
            : false
            ;
        
        // [1] get 'caller'
        $backtrace = debug_backtrace();
        $sOriginInfo = "<none>";
        if (isset($backtrace[0]['file']))
        {
            $sFormated = "<b>Location: %s ->Line %s</b>\n<br>";
            $sOriginInfo = sprintf(
                    $sFormated,
                    str_replace(LEPTON_PATH, "~", $backtrace[0]['file']),
                    $backtrace[0]['line']
                );
            
        }
        // [2] start return string
        $s = "\n<".$tag.(null === $css_class ? "" : " class='".$css_class."'").">\n";
        $s .= $sOriginInfo;
        ob_start();
            ((true === self::$use_var_dump) || (true === $useVarDumpParam))
            ? var_dump($something_to_display)
            : print_r($something_to_display)
            ;
        $s .= ob_get_clean();
        $s .= "\n</".$tag.">\n";

        return $s;
    }

    /**
     *  Method to convert a base64 string into an image file
     * 
     *  @param  string  $base64_string Any valid base64 string
     *  @param  string  $output_file Any local path
     *
     *  example given:
     *  @code{.php}
     *      LEPTON_tools::base64ToImage($base64_string, $output_file);
     *  @endcode
     *
     *
     */	
    // create signature image
    static function base64ToImage(string $base64_string, string $output_file): string
    {
        $file = fopen($output_file, "wb"); // write binary!

        $data = explode(',', $base64_string);

        fwrite($file, base64_decode($data[1]));
        fclose($file);

        return $output_file;
    }
}
