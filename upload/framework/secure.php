<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 */


if (!defined('LEPTON_PATH') && !defined('LEPTON_INSTALL_PROCESS'))
{
    require_once __DIR__."/functions/function.lepton_autoloader.php";
    spl_autoload_register( "lepton_autoloader", true);

    //  1.2 Get an instance of the class secure
    $oSecure = LEPTON_secure::getInstance();

    //  1.3 Is the script called inside a module directory - and if so: is there a file named "register_class_secure"?      
    $temp_path = (dirname($_SERVER['SCRIPT_FILENAME'])) . "/register_class_secure.php";
    if (file_exists($temp_path))
    {
        require_once $temp_path;
    }

    //  2.0 Testing the filename
    //  @notice: $_SERVER['SCRIPT_NAME'] holds the path to the script witch include this file!
    $allowed = $oSecure->testFile($_SERVER['SCRIPT_NAME']);

    //  2.1 All failed - we look for some special ones
    if (!$allowed)
    {
        $admin_dir = $oSecure->getAdminDir();

        if ((str_contains($_SERVER['SCRIPT_NAME'], $admin_dir.'/media/index.php')) || (str_contains($_SERVER['SCRIPT_NAME'], $admin_dir.'/preferences/index.php')))
        {
            // special: do absolute nothing!
        }
        elseif ((str_contains($_SERVER['SCRIPT_NAME'], $admin_dir . '/index.php')))
        {
            // special: call start page of admins directory
            $leptoken = isset($_GET['leptoken']) ? "?leptoken=" . $_GET['leptoken'] : "";
            header("Location: ../".$admin_dir.'/start/index.php'.$leptoken);
            exit();
        }
        elseif (str_contains($_SERVER['SCRIPT_NAME'], '/index.php'))
        {
            // call the main page
            header("Location: ../index.php");
            exit();
        }
        else
        {
            if (!headers_sent())
            {
                // set header to 403
                header($_SERVER['SERVER_PROTOCOL'] . " 403 Forbidden");
            }
            // stop program execution
            exit('<p><b>ACCESS DENIED! [L3]</b> - Invalid call of <i>'.$_SERVER['SCRIPT_NAME'].'</i></p>');
        }
    }

    //  3.0 At last - all ok - get the config.php (and process the initialize.php)
    $config_path =  dirname(dirname(__FILE__))."/config/config.php";
    require_once $config_path;
}

if (defined("FRONTEND") && (FRONTEND == true))
{
    //strip droplets and script tags
    if (!function_exists('lep_sec_formdata'))
    {
        function lep_sec_formdata(array &$arr): void
        {
            foreach ($arr as $key => $value)
            {
                if (is_array($value))
                {
                    lep_sec_formdata($value);
                }
                else
                {
                    // remove <script> tags
                    $value     = str_replace(
                            ['<script', '</script'],
                            ['&lt;script', '&lt;/script' ],
                            $value);
                    $value  = preg_replace('#(\&lt;script.+?)>#i', '$1&gt;', $value);
                    $value  = preg_replace('#(\&lt;\/script)>#i', '$1&gt;', $value);

                    $arr[$key] = str_replace(
                            ['[', ']'],
                            ['&#91;', '&#93;'],
                            $value);
                }
            }
        }
    }

    // secure form input
    if (isset($_SESSION) && !defined('LEP_SEC_FORMDATA'))
    {
        if (!empty($_GET))
        {
            lep_sec_formdata($_GET);
        }
        if (!empty($_POST))
        {
            lep_sec_formdata($_POST);
        }
        if (!empty($_REQUEST))
        {
            lep_sec_formdata($_REQUEST);
        }

        // make sure function is only called once
        define('LEP_SEC_FORMDATA', true);
    }
}