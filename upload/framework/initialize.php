<?php

 /**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Org. e.V.
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */ 

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE; 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE; 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

if (!function_exists("lepton_autoloader"))
{
	require_once __DIR__."/functions/function.lepton_autoloader.php";
	spl_autoload_register("lepton_autoloader", true);
}

// load constants
LEPTON_handle::include_files("/framework/constants.php");

// Get an instance from class database
$database = LEPTON_database::getInstance();

LEPTON_handle::register("get_leptoken");

 
//	set error-reporting
if (is_numeric(ER_LEVEL))
{
    error_reporting( ER_LEVEL );
    if (ER_LEVEL != 0)
    {
        ini_set('display_errors', 1);
    }
}

// Start a session
if (!defined('SESSION_STARTED'))
{
    // set name of session cookie
    session_name(APP_NAME . 'sessionid');

    //$cookie_settings = session_get_cookie_params();
	$cookie_settings = LEPTON_session::get_cookie_defaults();
	
    // create the session
    session_start();

    //  Initialize the session cookie with the defaults
    LEPTON_session::set_cookie( session_name(), session_id(), $cookie_settings );

    unset($cookie_settings);

    // create constant so this part is not running again
    define( 'SESSION_STARTED', true );

    // save in session
    $_SESSION['LSH'] = password_hash( LEPTON_GUID, PASSWORD_DEFAULT);
}


//	Update the session cookie to the defaults
if (true === isset($_COOKIE[APP_NAME.'sessionid']))
{
	LEPTON_session::set_cookie( session_name(), $_COOKIE[ APP_NAME . 'sessionid' ], [] );
}

if (defined('ENABLED_ASP') && !isset($_SESSION['SESSION_STARTED']))
{
	$_SESSION[ 'SESSION_STARTED' ] = time();
}

// logout if not properly initialized
if (!defined("LEPTON_INSTALL_PROCESS"))
{
	
    if ((!isset($_SESSION['LSH'])) || (!password_verify(LEPTON_GUID, $_SESSION['LSH'])))
	{	
		$_SESSION = [];
		LEPTON_session::set_cookie( session_name(), "", array( "expires"=> time() - 1 ) );
		session_destroy();
		header('Location: '.ADMIN_URL.'/login/index.php');
		die();
	}
}


if(!defined('FRONTEND'))
{
	define('FRONTEND', false);
}
if(FRONTEND == true)
{
	// Get users language
	if (isset($_GET['lang']) && ($_GET['lang'] != '') && (!is_numeric($_GET['lang'])) && (strlen($_GET['lang']) == 2))
	{
		// needed for frontend and easymultilanguage function without being logged in
		$iTempPageId = $page_id ?? ((defined("PAGE_ID") ? PAGE_ID : 0));

		if (0 != $iTempPageId)
		{
			$sTempLang = $database->get_one("SELECT `language` FROM `".TABLE_PREFIX."pages` WHERE `page_id` = ".$iTempPageId);
			if (NULL != $sTempLang)
			{
				define( 'LANGUAGE', $sTempLang );
			} 
			else 
			{
				// more or less a theoretical case
				define('LANGUAGE', DEFAULT_LANGUAGE);
			}
		} 
		else 
		{	// needed for FE being logged in
			if (isset($_SESSION['USER_ID']))
			{
				$sTempLang = $database->get_one("SELECT `language` FROM `".TABLE_PREFIX."users` WHERE `user_id`=" . $_SESSION['USER_ID']);
				if (NULL != $sTempLang)
				{
					define('LANGUAGE', $sTempLang);
				} 
				else 
				{
					define('LANGUAGE', strtoupper($_GET['lang']));
				}
			} 
			else 
			{
				define('LANGUAGE', strtoupper($_GET['lang']));
			}
		}

		$_SESSION['LANGUAGE'] = LANGUAGE;
	}
	else
	{
		if(isset($page_id))
		{
			$page_language = $database->get_one("SELECT language FROM ".TABLE_PREFIX."pages WHERE page_id = ".$page_id);
			define('LANGUAGE',$page_language);
		}
		else
		{
			define('LANGUAGE',DEFAULT_LANGUAGE);
		}
	}
}
else
{	// this part is for backend only
    if (isset($_SESSION['LANGUAGE']) && ($_SESSION['LANGUAGE'] != ''))
	{
        $iTempPageId = $page_id ?? ((defined("PAGE_ID") ? PAGE_ID : 0));

        if (0 != $iTempPageId)
        {
            $sTempLang = $database->get_one("SELECT `language` FROM `" . TABLE_PREFIX . "pages` WHERE `page_id`=" . $iTempPageId);
            if (NULL != $sTempLang)
			{
                define('LANGUAGE', $sTempLang);
			} 
			else 
			{
				// more or less a theoretical case
                define('LANGUAGE', $_SESSION['LANGUAGE']);
			}
		} 
		else 
		{
            if (isset($_SESSION['USER_ID']))
			{
                $sTempLang = $database->get_one("SELECT `language` FROM `" . TABLE_PREFIX . "users` WHERE `user_id`='" . $_SESSION['USER_ID'] . "'");
                if (NULL != $sTempLang)
				{
                    define('LANGUAGE', $sTempLang);
				} else {
                    define('LANGUAGE', $_SESSION['LANGUAGE']);
				}
			} 
			else 
			{
                define('LANGUAGE', $_SESSION['LANGUAGE']);
			}
		}
	}
	else
	{
        define('LANGUAGE', DEFAULT_LANGUAGE);
	}
}


// Load Language file
if (!file_exists(LEPTON_PATH.'/languages/'.LANGUAGE.'.php'))
{
	exit('Error loading language file '.LANGUAGE.', please check configuration');
}
else
{
	$aLanguageFile = [
		LANGUAGE.'_add.php',
		LANGUAGE.'_custom.php',
		LANGUAGE.'.php'
	];
	
	foreach ($aLanguageFile as $sFile)
	{
		$sLanguagePath = LEPTON_PATH.'/languages/';
		if (file_exists($sLanguagePath.$sFile))
		{
			require_once $sLanguagePath.$sFile;
			break;
		}
	}
}

