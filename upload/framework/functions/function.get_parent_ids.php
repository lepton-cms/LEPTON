<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

declare(strict_types = 1);

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/**
 * Function to get all parent page id's
 *
 * @param  int $iParentId - page ID
 * @return array :0 if page hasn't children, parent id if not
 *
 */

// Function to get all parent page id's
function get_parent_ids( int $iParentId ): array
{
	LEPTON_handle::register("is_parent");
    $ids[] = $iParentId;
    $iTempParent = is_parent( $iParentId );
    if ( $iTempParent !== 0 )
    {
        $parent_ids = get_parent_ids( $iTempParent );
        $ids        = array_merge( $ids, $parent_ids );
    }
    return $ids;
}
