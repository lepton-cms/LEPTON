<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *  Try to geht the current leptoken.
 *
 *  @return string
 */
function get_leptoken(): string 
{
    //  [1] -- first $_GET
    $leptoken = filter_input(
        INPUT_GET,
        "leptoken",
        FILTER_VALIDATE_REGEXP,
        [
             "options" => [
                "regexp"    => "~^[a-zA-Z0-9\.]{32}$~",
                "default"   => NULL
             ]
        ]
    );
    //  [2] -- not found - look for the $_POST
    if( NULL === $leptoken )
    {
        $leptoken = filter_input(
            INPUT_POST,
            "leptoken",
            FILTER_VALIDATE_REGEXP,
            [
                 "options" => [
                    "regexp"    => "~^[a-zA-Z0-9\.]{32}$~",
                    "default"   => NULL
                 ]
            ]
        );
    }
    //  [3] --- not found -- look for a diff. name
    if( NULL === $leptoken )
    {
        $leptoken = filter_input(
            INPUT_GET,
            "amp;leptoken",
            FILTER_VALIDATE_REGEXP,
            [
                 "options" => [
                    "regexp"    => "~^[a-zA-Z0-9\.]{32}$~",
                    "default"   => NULL
                 ]
            ]
        );
    }
    
    return ( $leptoken ?? "" );
}
