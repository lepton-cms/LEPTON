<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *  Function called by parent, default by the wysiwyg-module.
 *
 *  @param  string  $name    The name of the textarea to watch
 *  @param  string  $id      The "id" - some other modules handel this param differ
 *  @param  string  $content The source to edit.
 *  @param  string  $width   Optional the width
 *  @param  string  $height  Optional the height of the editor
 *  @param  bool    $prompt  Optional true: direct output to the browser, false: return the renerated string.
 *
 *  @return string
 */
function display_wysiwyg_editor(
    string $name,
    string $id,
    string $content,
    int|null $width = null,
    int|null $height = null,
    bool   $prompt = false
): string
{

		// using constant in buffering causes sometimes errors!
		$classname = WYSIWYG_EDITOR;
		if($classname == 'none')
		{
			$result = '<textarea name="'.$name.'" id="'.$id.'" style="width: '.$width.'; height: '.$height.';">'.$content.'</textarea>';
		}
		else
		{
			ob_start();	
			$classname::show_wysiwyg_editor($name, $id, $content, $width, $height, true);	
			$result = ob_get_clean();	
		}

    
    if ($prompt == true)
    {
        echo $result;
        return "";
    }
    else
    {
        return $result;
    }
}