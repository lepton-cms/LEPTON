<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *  
 *
 *  @param string $sContent     The content string.
 *  @param array $aTermsToMark  Linear array with the terms to highlight
 *
 *  @return string  The parsed result string
 *
 */
function search_highlight(string $sContent='', array $aTermsToMark=array()): string
{
	if ($sContent === "")
	{
		return "";
	}
			
	array_walk($aTermsToMark,
		function( &$v )
		{
			$v = preg_quote($v, '~');
		}
	);
	$search_string = implode("|", $aTermsToMark);
	$string = $search_string;//str_replace($string_ul_umlaut, $string_ul_regex, $search_string);
	/* 
		the highlighting
		match $string, but not inside <style>...</style>, <script>...</script>, <!--...--> or HTML-Tags
		Also droplet tags are now excluded from highlighting.
		split $string into pieces - "cut away" styles, scripts, comments, HTML-tags and eMail-addresses
		we have to cut <pre> and <code> as well.
		for HTML-Tags use <(?:[^<]|<.*>)*> which will match strings like <input ... value="<b>value</b>" >
	*/
	$matches = preg_split("~(\[\[.*\]\]|<style.*</style>|<script.*</script>|<pre.*</pre>|<code.*</code>|<!--.*-->|<(?:[^<]|<.*>)*>|\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,8}\b)~iUs",$sContent,-1,(PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY));
	if (is_array($matches) && $matches != array())
	{
		$sContent = "";
		foreach ($matches as $match)
		{ 
			if ($match[0] != "<" && !preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,8}$/i', $match) && !preg_match('~\[\[.*\]\]~', $match))
			{
				$match = str_replace(
					array('&lt;', '&gt;', '&amp;', '&quot;', '&#039;', '&nbsp;'),
					array('<', '>', '&', '"', '\'', "\xC2\xA0"),
					$match
				);
				
				$match = preg_replace(
					'~('.$string.')~ui', 
					'_span class=_highlight__$1_/span_',
					$match
				);
				
				$match = str_replace(
					array('&', '<', '>', '"', '\'', "\xC2\xA0"),
					array('&amp;', '&lt;', '&gt;', '&quot;', '&#039;', '&nbsp;'),
					$match
				);
			   
				$match = str_replace(
					array('_span class=_highlight__', '_/span_'),
					array('<span class="highlight">', '</span>'),
					$match
				);
			}
			$sContent .= $match;
		}
	}

	return $sContent;
}
