<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/**
 * This function displays a button to toggle between CSS files (invoked from edit_css.php)
 *
 * @param string $mod_dir       The module directory
 * @param string $base_css_file The current css file
 * @return bool
 */
function toggle_css_file(string $mod_dir, string $base_css_file = 'frontend.css' ): bool
{
    global $page_id, $section_id, $TEXT;

    LEPTON_handle::register("mod_file_exists");

    // check if the required edit_module_css.php file exists
    if (!file_exists(LEPTON_PATH . '/modules/edit_module_files.php'))
    {
        return false;
    }

    // do sanity check of specified css file
    if (!in_array ($base_css_file, ['frontend.css', 'css/frontend.css', 'backend.css', 'css/backend.css']))
    {
        return false;
    }

    // display button to toggle between the two CSS files: frontend.css, backend.css
    switch ($base_css_file)
    {
        case 'frontend.css':
            $toggle_file = 'backend.css';
            break;

        case 'backend.css':
            $toggle_file = 'frontend.css';
            break;

        case 'css/frontend.css':
            $toggle_file = 'css/backend.css';
            break;

        case 'css/backend.css':
            $toggle_file = 'css/frontend.css';
            break;

        default:
            return false;
    }

    // Aldus: another patch for the css-paths.
    $toggle_file_label = str_replace("css/", "", $toggle_file);

    if (mod_file_exists($mod_dir, $toggle_file))
    {
        $oTWIG = lib_twig_box::getInstance();

        $oTWIG->loader->prependPath( LEPTON_PATH."/templates/".DEFAULT_THEME."/templates/" );

        $fields = array(
            'LEPTON_URL'    => LEPTON_URL,
            'page_id'       => $page_id,
            'section_id'    => $section_id,
            'mod_dir'       => $mod_dir,
            'edit_file'     => $toggle_file_label,
            'label_submit'  => ucfirst($toggle_file_label)
        );

        echo $oTWIG->render(
            'edit_module_css_form.lte',
            $fields
        );

        return true;
    }
    else
    {
        return false;
    }
}
