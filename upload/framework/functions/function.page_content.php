<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


function page_content(int $block = 1)
{
    // Get outside objects
    global $TEXT, $MENU, $HEADING, $MESSAGE, $section_id;
    $database = LEPTON_database::getInstance();
    $oLEPTON = LEPTON_frontend::getInstance();

    // [0.1]
    if ($oLEPTON->page_access_denied === true)
    {
        echo $MESSAGE['FRONTEND_SORRY_NO_VIEWING_PERMISSIONS'];
        return;
    }
    
    // [0.2]
    if ($oLEPTON->page_no_active_sections === true)
    {
        echo $MESSAGE['FRONTEND_SORRY_NO_ACTIVE_SECTIONS'];
        return;
    }

    // [0.3]
    $sFrontendMessage = LEPTON_core::getValue("page_content", "string", "session") ?? "";
    if (str_starts_with($sFrontendMessage, "@LEPTON_FE_"))
    {
        unset($_SESSION['PAGE_CONTENT']);
		echo LEPTON_frontend::displayMessage();
        return;
    }
     
    // [1] Include page content
    if (!defined('PAGE_CONTENT') || $block != 1)
    {
        $page_id = intval( $oLEPTON->page_id );

        // [1.0.1] Set session variable to save page_id only if PAGE_CONTENT is empty
        $_SESSION[ 'PAGE_ID' ] = !isset( $_SESSION[ 'PAGE_ID' ] ) ? $page_id : $_SESSION[ 'PAGE_ID' ];

        // [1.0.2] Set to new value if page_id changed and not 0
        if (($page_id != 0 ) && ($_SESSION[ 'PAGE_ID' ] <> $page_id))
        {
            $_SESSION[ 'PAGE_ID' ] = $page_id;
        }

        // [1.2] First get all sections for this page
        $aSections = [];
        $database->execute_query( 
            "SELECT section_id, module, publ_start, publ_end from ".TABLE_PREFIX."sections WHERE page_id = ".$page_id."  AND block = '".$block."' ORDER BY position",
            true,
            $aSections,
            true
        );            

        // [2] We have get valid sections! Loop through the sections and include their module files.
        foreach ($aSections as $section)
        {
            // skip this section if it is out of publication-date
            $now = time();
            if ( !( ( $now <= $section[ 'publ_end' ] || $section[ 'publ_end' ] == 0 ) && ( $now >= $section[ 'publ_start' ] || $section[ 'publ_start' ] == 0 ) ) )
            {
                continue;
            }
            $section_id = $section[ 'section_id' ];
            $module     = $section[ 'module' ];
            
            // make a anchor for every section.
            if ( defined( 'SEC_ANCHOR' ) && SEC_ANCHOR != '' )
            {
                echo '<a class="section_anchor" id="' . SEC_ANCHOR . $section_id . '"></a>';
            }
            // check if module exists
            if ( file_exists( LEPTON_PATH . '/modules/' . $module . '/view.php' ) )
            {
                // fetch content -- this is where to place possible output-filters (before highlighting)
                ob_start();
                require LEPTON_PATH . '/modules/' . $module . '/view.php';
                $content = ob_get_clean();
            }
            else
            {
                continue;
            }
            
            // [2] highlights search-results
            LEPTON_handle::register("search_highlight");
            $sSearchResult  = (LEPTON_request::getRequest("searchresult")   ?? NULL);
            $sString        = (LEPTON_request::getRequest("sstring")        ?? "");
            $sNoHighlight   = (LEPTON_request::getRequest("nohighlight")    ?? NULL);

            if (!is_null($sSearchResult) && is_null($sNoHighlight) && $sString != "" )
            {
                $arr_string = explode( " ", $sString );
                if ( intval($sSearchResult) == 2 ) // exact match
                {
                    $arr_string[ 0 ] = str_replace( "_", " ", $arr_string[ 0 ] );
                }
                echo search_highlight( $content, $arr_string );
            }
            else
            {
                echo $content;
            }
        }
    }
    else
    {
        require PAGE_CONTENT;
    }
}
