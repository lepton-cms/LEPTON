<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *	This function returns a recursive list of all subdirectories from a given directory
 *
 *	@access	public
 *	@param	string	$directory from this dir the recursion will start.
 *	@param	bool	$show_hidden (optional): if set to TRUE also hidden dirs (.dir) will be shown.
 *	@param	int		$recursion_deep (optional): An optional integer to test the recursions-deep at all.
 *	@param	array	$aList (optional): A simple storage list for the recursion.
 *	@param	string	$ignore (optional): This is the part of the "path" to be "ignored"
 *
 *	@return  array
 *
 *	@example:
 *    assume you are looking for directories like:
 *		/srv/www/htdocs/lepton/upload/media/a/b/c/
 *		/srv/www/htdocs/lepton/upload/media/a/b/d/
 *
 *      $aEmptyList = [];
 *      $skipPathComponent = LEPTON_PATH.MEDIA_DIRECTORY.DIRECTORY_SEPARATOR;
 *      $aAll = directory_list( LEPTON_PATH.MEDIA_DIRECTORY, false, 0, $aEmptyList, $skipPathComponent);
 *
 *      $aAll will contain Indicated array list like:
 *
 *      [0] => a
 *      [1] => a/b
 *      [2] => a/b/c
 *      [3] => a/b/d
 *
 */
function directory_list(string $directory, bool $show_hidden = false, int $recursion_deep = 0, array &$aList = [], string &$ignore = "")
{
    if (is_dir($directory))
    {
        // Open the directory
        $dir = dir( $directory );
        if (!is_null($dir))
        {
            while (false !== ($entry = $dir->read()))
            {
                // Skip hidden files
                if ($entry[0] == '.' && $show_hidden === false)
                {
                    continue;	
                }

                $temp_dir = $directory."/".$entry;
                if (is_dir($temp_dir))
                {
                    // Add dir and contents to list
                    $aList[] = str_replace( $ignore, "", $temp_dir );
                    directory_list($temp_dir, $show_hidden, $recursion_deep + 1, $aList, $ignore);
                }
            }
            $dir->close();
        }
    }
    if ($recursion_deep == 0)
    {
        natcasesort( $aList );
    }
    return $aList;
}
