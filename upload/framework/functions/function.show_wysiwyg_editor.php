<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/**
 *  Function to display textarea if no wysiwyg editor is set.
 *
 *  @param  string  $name		db field
 *  @param  string  $id			id of textarea
 *  @param  string  $content	content of db field
 *  @param  string  $width		width of textarea
 *  @param  string  $height		height of textarea 
 *  @param  bool	$prompt		Optional flag for direct output or return
 *
 *  @return	string
 *
 */
 
function show_wysiwyg_editor( $name, $id, $content, $width, $height, $prompt=true )
{
	$info = '@DEPRECATED_TEMP 20250211: this function will be removed in L* > 7.3.0, use display_wysiwyg_editor() instead';
	echo(LEPTON_tools::display($info, 'pre','ui orange message'));
	$sHTML = '<textarea name="'.$name.'" id="'.$id.'" style="width: '.$width.'; height: '.$height.';">'.$content.'</textarea>';

	if( true === $prompt)
	{
		echo $sHTML;
	} 
	else 
	{
		return $sHTML;
	}
}
