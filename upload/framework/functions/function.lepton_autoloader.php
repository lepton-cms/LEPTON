<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
/**
 * Here is no secure header because installation script fails then.
 */

/**
 *	LEPTON CMS autoloader 
 *
 * @param string $aClassName A valid class name.
 *
 *  e.g.
 *      LEPTON_handle::
 *      looking for class file inside framework/classes/
 *          here lepton_handle.php
 *
 *      TEMPLATE_aldus_sketchbook::
 *          looking inside templates/aldus_sketchbook/classes/
 *
 *      TEMPLATE_aldus_sketchbook_interface::
 *          looking inside templates/aldus_sketchbook/classes/ too!
 *
 *      display_code::getPaths()
 *          looking inside modules/display_code/classes/
 *          or:
 *          looking inside modules/display-code/classes/
 *
 */

function lepton_autoloader(string $aClassName ): bool
{
    $lepton_path = dirname(__DIR__, 2);

    $terms = explode("_", $aClassName);

    if ($terms[0] === 'LEPTON')
    {
        // We are looking inside the LEPTON-CMS framework directory:
        $path = $lepton_path."/framework/classes/".strtolower($aClassName).".php";
        if (file_exists($path))
        {
            require_once $path;
            return true;
        }
    } 
    else 
    {
        // Any module or template specific CLASS
        $aMainFolders = ["modules", "templates"];
        
        //  [1] Any namespaces given?
        $aNTest = explode("\\", $aClassName);
        if (count($aNTest) > 1)
        {
            $sPartOne = array_shift($aNTest);
            $sSubPart = implode("/", $aNTest);
            foreach ($aMainFolders as $sMainDir)
            {
                $path = $lepton_path."/".$sMainDir."/".$sPartOne."/classes/".$sSubPart.".php";

                if (file_exists($path))
                {
                    require $path;
                    return true;
                } 
                else 
                {
                    $path = $lepton_path."/".$sMainDir."/".$sPartOne."/".$sSubPart.".php";
                    if (file_exists($path))
                    {
                        require $path;
                        return true;
                    }
                }
            }
        }
        
        //  [2] Non given, nor found
        $aMainFolders = ["modules", "templates"];

        foreach ($aMainFolders as $sMainDir)
        {
            $path = $lepton_path."/".$sMainDir."/".$aClassName."/classes/".$aClassName.".php";

            if (file_exists($path))
            {
                require_once $path;
                return true;
            } 
            else 
            {
                $n = count($terms);
                $look_up = $terms[0];

                for ($i = 0; $i < $n; $i++)
                {
                    $temp_dir = $look_up . ($i > 0 ? "_" . $terms[$i] : "");

                    $path = $lepton_path."/".$sMainDir."/".$temp_dir."/classes/".$aClassName.".php";
                    if (file_exists($path))
                    {
                        require_once $path ;
                        return true;
                    }
                    elseif ($i > 0)
                    {
                        $temp_dir = $look_up."-".$terms[$i];
                        $path = $lepton_path."/".$sMainDir."/".$temp_dir."/classes/".$aClassName.".php";
                        if (file_exists($path))
                        {
                            require_once $path;
                            return true;
                        }
                    }

                    $look_up = $temp_dir;
                }
            }
        }

        //  [3] account
        $path = $lepton_path."/".$aClassName."/classes/".$aClassName.".php";
        if (file_exists($path))
        {
            require_once $path;
            return true;
        } else {
            $look_up = $terms[0];

            $path = $lepton_path."/".$look_up."/classes/".$aClassName.".php";
            if (file_exists($path))
            {
                require_once $path;
                return true;
            }

        }
    }
    return false;
}
