<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *	Remove all files or directories given as arguments to the function.
 *	Typical use of this function is to delete temp files inside the the temp-directory
 *	during any module-, or template- or language-installation.
 * 
 *	@usage	cleanup( $file1, $dir1, $file, ...)
 *
 *	@params	any	Any file, directory
 *	@return bool	true
 *
 *	@notice	Be aware not to call "cleanup(LEPTON_PATH."/temp");" the temp-folder itself;
 *			the temp-folder itself will be deleted!
 *
 */


LEPTON_handle::register("rm_full_dir");

function cleanup() : bool
{
	if ( 0 == func_num_args() )
	{
	    return true;
	}
	
	$all_args = func_get_args();
	
	foreach($all_args as &$file) {
		if (true === file_exists($file))
		{
			if (true === is_dir($file))
			{
				rm_full_dir( $file );
			} else {
				unlink( $file );
			}
		}
	}
	
	return true;
}
