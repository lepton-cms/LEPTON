<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


	/**
	 *  Load template information from the info.php of a given template into the DB.
	 *
	 *  @param  string  Any valid directory
	 *
	 *  @notice  Keep in mind, that the variable-check is here the same as in
	 *      File: admins/templates/install.php.
	 *      The reason is, that it could be possible to call this function from
	 *      another script/codeblock direct. So in thees circumstances where would no
	 *      test at all, and the possibility to entry wrong data is given.
	 *
	 */
	function load_template( $directory )
	{
		global $MESSAGE;
		$database = LEPTON_database::getInstance();
		
		if (is_dir(LEPTON_PATH.'/templates/'.$directory) && file_exists(LEPTON_PATH.'/templates/'.$directory.'/info.php'))
		{
			global $template_license, $template_directory, $template_author, $template_version, $template_function, $template_description, $template_platform, $template_name, $template_guid;			
			require LEPTON_PATH.'/templates/'.$directory.'/info.php';

			if(!isset($template_delete))
			{
				$template_delete = true;
			}
		
			// Check that it doesn't already exist
			$sqlwhere = "`type` = 'template' AND `directory` = '" . $template_directory . "'";
			$sql      = "SELECT COUNT(*) FROM `" . TABLE_PREFIX . "addons` WHERE " . $sqlwhere;
			if ( $database->get_one( $sql ) )
			{
				$sql_job = "update";
			}
			else
			{
				$sql_job = "insert";
				$sqlwhere = "";
			}
			
			$fields = array(
				'directory'	=> $template_directory,
				'name'		=> $template_name,
				'description'	=> $template_description,
				'type' 		=> 'template',
				'function'	=> strtolower( $template_function ),
				'to_delete'	=> intval( $template_delete ),				
				'version'	=> $template_version,
				'platform'	=> $template_platform,
				'author' 	=> $template_author,
				'license'	=> $template_license
			);
			
			if (isset($template_guid))
			{
			    $fields['guid'] = $template_guid;
            }
            
			$database->build_and_execute(
				$sql_job,
				TABLE_PREFIX . "addons",
				$fields,
				$sqlwhere
			);
		}
	}
