<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *  Load language information from a given language-file into the current DB
 *
 *  @param  string  Any valid path.
 *
 */
function load_language( $file )
{
	global $MESSAGE;
	$database = LEPTON_database::getInstance();
	
	if ( file_exists( LEPTON_PATH.'/languages/'.$file ) && preg_match( '#^([A-Z]{2}[_custom]{0,1}.php)#', basename( $file ) ) )
	{
		$language_license	= null;
		$language_code		= null;
		$language_version	= null;
		$language_delete	= true;
		$language_guid		= null;
		$language_name		= null;
		$language_author	= null;
		$language_platform	= null;
		
		require LEPTON_PATH.'/languages/'.$file;

		if (   is_null($language_license)
			|| is_null($language_code )
			|| is_null($language_version)
			|| is_null($language_guid) 
		)
		{
			echo(LEPTON_tools::display($MESSAGE[ "LANG_MISSING_PARTS_NOTICE" ]. $language_name, 'pre','ui red message'));
		}
		
		// Check that it doesn't already exist
		$sqlwhere = '`type` = \'language\' AND `directory` = \'' . $language_directory . '\'';
		$sql      = 'SELECT COUNT(*) FROM `' . TABLE_PREFIX . 'addons` WHERE ' . $sqlwhere;
		if ( $database->get_one( $sql ) )
		{
			$sql_job = "update";
		}
		else
		{
			$sql_job = "insert";
			$sqlwhere = '';
		}

		$fields = array(
			'directory'	=> $language_directory,
			'name'		=> $language_name,
			'type'		=> 'language',
			'version'	=> $language_version,
			'platform'	=> $language_platform,
			'author'	=> addslashes( $language_author ),
			'license'	=> addslashes( $language_license ),
			'to_delete'	=> intval( $language_delete ),
			'guid'		=> $language_guid,
			'description' => ""
		);

		$database->build_and_execute(
			$sql_job,
			TABLE_PREFIX . "addons",
			$fields,
			$sqlwhere
		);
	}
}
