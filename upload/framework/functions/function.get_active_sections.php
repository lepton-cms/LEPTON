<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *	Get the active sections of the current page
 *
 *	@param	int		Current page_id
 *	@param	str		Optional block-name
 *	@param	bool	Backend? Default is false
 *	@return	array	Linear array within all ids of active section
 */
function get_active_sections( $page_id, $block = null, $backend = false )
{
    $database = LEPTON_database::getInstance();

    $lep_active_sections = [];

    // First get all sections for this page	
    $aSections = [];
    $database->execute_query(
        "SELECT * FROM `".TABLE_PREFIX."sections` WHERE `page_id` = '". $page_id ."' ORDER BY `block`, `position`",
        true,
        $aSections,
        true
    );

    if (empty($aSections))
    {
        return NULL;
    }

    $now = time();
    foreach ($aSections as $section)
    {
        // skip this section if it is out of publication-date
        if ( ($backend === false) && !(($now <= $section['publ_end'] || $section['publ_end'] == 0) && ($now >= $section['publ_start'] || $section['publ_start'] == 0)))
        {
            continue;
        }
        $lep_active_sections[$section['block']][] = $section;
    }

    $pages_seen[$page_id] = true;

    if ( $block )
    {
        return ( isset($lep_active_sections[$block] ) )
            ? $lep_active_sections[$block]
            : NULL
            ;
    }

    $all = [];
    foreach ($lep_active_sections as $block => $values)
    {
        foreach ($values as $value)
        {
            $all[] = $value;
        }
    }

    // add all parameters to array to get frontend.css for snippets work like modules!
    $aSnippets = [];
    $database->execute_query(
        "SELECT * FROM `".TABLE_PREFIX."addons` WHERE `function` = 'snippet' ",
        true,
        $aSnippets,
        true
    );
    foreach ($aSnippets as $temp_snippets)
    {
        $all[] = [
            'module'        => $temp_snippets['directory'],
            'section_id'    => -1
        ];
    }

    return $all;

}
