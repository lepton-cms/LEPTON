<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/**
 *  Function to list all files in a given directory.
 *
 *  @param  string  $directory   Directory to list
 *  @param  array   $skip        Optional array with directories to skip, e.g. '.svn' or '.git'
 *  @param  bool    $show_hidden Optional bool to list also hidden files, e.g. ".htaccess". Default is 'false'.
 *  @param  string  $file_type   Optional pattern for file types, e.g. 'png' or '(jpg|jpeg|gif)'.
 *  @param  string  $strip       Optional string to strip from the full file path, e.g. LEPTON_PATH.
 *  @param  boolean $recursive   Optional flag for recursion (looking also in subdirectories)
 *
 *  @return  array  Sorted array within the files.
 *
 *  @example    file_list(LEPTON_PATH.'/modules/captcha_control/functions/backgrounds', [], false, "png", LEPTON_PATH);
 *
 *              - Will return a list within all found .png files inside the folder functions/backgrounds,
 *                without the LEPTON_PATH like "/modules/captcha_control/functions/backgrounds/bg_10.png".
 *
 */
function file_list(
    string $directory,
    array $skip = [],
    bool $show_hidden = false,
    string $file_type = "",
    string $strip = "",
    bool $recursive = false
): array
{
    $result_list = [];

    if (is_dir($directory))
    {
        $use_skip = !empty($skip);

        $dir = dir($directory);
        while (false !== ($entry = $dir->read()))
        {
            // Skip hidden files
            if (($entry[0] == '.') && (false === $show_hidden))
            {
                continue;
            }
            // Check if we to skip anything else
            if ((true === $use_skip) && (in_array($entry, $skip)))
            {
                continue;
            }

            if (is_file($directory.'/'.$entry))
            {
                // Add file to list
                $temp_file = $directory.'/'.$entry;
                if ($strip != "")
                {
                    $temp_file = str_replace($strip, "", $temp_file);
                }
                
                if ($file_type === "")
                {
                    $result_list[] = $temp_file;
                } else {
                    if (preg_match('/\.'.$file_type.'$/i', $entry))
                    {
                        $result_list[] = $temp_file;
                    }
                }
            } 
			else 
			{
                if (true === $recursive)
                {
                    $aTemp = file_list($directory.'/'.$entry, $skip, $show_hidden, $file_type, $strip, $recursive);
                    if (!empty($aTemp))
                    {
                        $result_list = array_merge($result_list, $aTemp);
                    }
                }
            }
        }
        $dir->close();
    }
    natcasesort($result_list);
    return $result_list;
}
