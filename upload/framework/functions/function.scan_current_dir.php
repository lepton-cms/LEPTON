<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *	Scan a given directory for dirs and files.
 *
 *	Used inside e.g. 'admins/addons/reload.php'
 *
 *	@param 	string	$root Optional path to be scanned; default is the current working directory (getcwd()).
 *	@param	string	$file_type Optional pattern for file types, e.g. 'png' or '(jpg|jpeg|gif)'. Default is an empty string for all file(-types).
 *
 *	@return	array	Returns an assoc. array with the keys 'path' and 'filename'.
 *	
 */
function scan_current_dir(string $root = '', string $file_type = '' ): array
{
    $FILE = array(
        'path'	=> array(),
        'filename' => array()
    );

    if ($root == '')
    {
        $root = getcwd();
    }
    
    if (false !== ($handle = dir($root)))
    {
        // Loop through the files and dirs an add to list
        while (false !== ($file = $handle->read()))
		{
            if (($file[0] != '.') && ($file != 'index.php'))
            {
                if (is_dir( $root . '/' . $file)) 
				{
                    $FILE[ 'path' ][] = $file;
                } 
				else 
				{
                    if ($file_type === '') 
					{
                        $FILE[ 'filename' ][] = $file;
                    } 
					else 
					{
                        if (preg_match('/\.'.$file_type.'$/i', $file)) 
						{
                            $FILE[ 'filename' ][] = $file;
                        }
                    }
                }
            }
        }
        $handle->close();
    }
    
    natcasesort( $FILE[ 'path' ] );
    natcasesort( $FILE[ 'filename' ] );
    $FILE['path'] = array_merge([], $FILE['path']);
    $FILE['filename'] = array_merge([], $FILE['filename']);
    return $FILE;
}
