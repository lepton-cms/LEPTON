<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *  Function to generate a random-string.
 *  Random-Strings are often used inside LEPTON-CMS, e.g. Captcha, Modul-Form, User-Account, Forgot-Password, etc.
 *
 *  @param	int     $iNumOfChars    Number of chars to generate. Default is 8 (chars).
 *
 *  @param	string  $aType          Type, default is 'alphanum'.
 *                  Possible values are:
 *                  'alphanum'      = Generates an alphanumeric string with chars and numbers.
 *                  'alpha'         = Generates only chars.
 *                  'chars'         = Also generates only chars.
 *                  'hex'           = Hexadecimal.
 *                  'hex-lower'     = Hexadecimal in lower cases.
 *                  'lower'         = Only lower cases are used.
 *                  'num'           = Generates only numbers.
 *                  'pass'          = Generates an alphanumeric string within some special chars e.g. '&', '$' or '|'.
 *                  '<anyString>'   = Generates a shuffled string with these chars.
 *
 *
 *  @return string  A shuffled string within the chars.
 *
 *  @examples       random_string()
 *                      - Will generate something like 'abC2puwm' (8 chars).
 *
 *                  random_string(5)
 *                      - The same, but only 5 chars, e.g. 'abc56' or '2wsd4'.
 *
 *                  random_string(8, 'num')
 *                      - Will result in a random number-string, e.g. '0898124'
 *
 *                  random_string(8, 'Aldus')
 *                      - Will generate a shuffled-string with theese chars, e.g: 'sAdludsA'.
 *
 *                  random_string(8, 'hex-lower')
 *                      - Will generate a shuffled-string with hex-decimalchars like 'afb2c22e7'.
 *
 */
function random_string(int $iNumOfChars = 8, string $aType="alphanum"): string
{
    switch(strtolower($aType))
    {
        case 'alphanum':
            $salt = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            break;

        case 'alpha':
        case 'chars':
            $salt = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;

        case 'hex':
            $salt = 'ABCDEF0123456789';
            break;

        case 'hex-lower':
            $salt = 'abcdef0123456789';
            break;

        case 'lower':
            $salt = 'abcefghijklmnopqrstuvwxyz';
            break;

        case 'num':
            $salt = '1234567890';
            break;

        case 'pass':
            $salt = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.";
            break;

        default:
            $salt = (is_array($aType)) ? implode("", $aType) : (string) $aType ;
            break;
    }

    $max = strlen($salt);
    if ($iNumOfChars > $max)
    {
        do
        {
            $salt .= $salt;
        } while (strlen($salt) < $iNumOfChars);
    }

    return substr(str_shuffle($salt), 0, $iNumOfChars);
}
