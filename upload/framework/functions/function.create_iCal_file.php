<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *  Function to create and save an iCal file from given data
 *  Detailed information: https://datatracker.ietf.org/doc/html/rfc5545
 * 
 *  @param  string  $title		The name of the event
 *  @param  string  $location	The location of the event
 *  @param  string  $startdate	The startdate, format = yyyy-mm-dd
 *  @param  string  $starttime  The starttime, format = h:m
 *  @param  string  $enddate	The enddate, format = yyyy-mm-dd
 *  @param  string  $endtime	The endtime, format = h:m
 *  @param  string  $description	The description of the event
 *  @param  string  $reminder	The reminder in minutes, that the user will get, format = '60', means 60 minutes in before event starts
 *  @param  string  $reminder_text	The info, the remider should diplay, for example 'Erinnerung'
 *  @param  string  $filename	The complete path and filename of the file
 *
 *  @return string
 */
 
function create_iCal_file(
	string $title,
	string $location = 'Anywhere',
	string $startdate = '2025-01-01',
	string $starttime = '10:30:00',
	string $enddate = '2025-01-01',
	string $endtime = '12:00:00',
	string $description = 'Description',
	string $reminder = '60',
	string $reminder_text = 'Reminder',	
	string $filename = LEPTON_PATH.MEDIA_DIRECTORY.'/event.ics'
): int
{

	// format data
	$startdate = str_replace('-','',$startdate);
	$enddate = str_replace('-','',$enddate);
	$starttime = str_replace(':','',$starttime).'00';
	$endtime = str_replace(':','',$endtime).'00';


	// start iCal function
	$eol = "\r\n";
	$sICalContent =	
		'BEGIN:VCALENDAR'.$eol.
		'CHARSET:'.strtoupper(DEFAULT_CHARSET).$eol.
		'VERSION:2.0'.$eol.
		'TZNAME:UTC'.$eol.
		'TZID:'.DEFAULT_TIMEZONE_STRING.$eol.
		'X-LIC-LOCATION:'.DEFAULT_TIMEZONE_STRING.$eol.
		'PRODID:LEPTON-CMS,https://lepton-cms.org, '.LANGUAGE.$eol.
		'CALSCALE:GREGORIAN'.$eol.
		'BEGIN:VEVENT'.$eol.
		'CLASS:PUBLIC'.$eol.
		'UID:'.LEPTON_URL.'_'.time().$eol.		
		'DTSTART:'.$startdate.'T'.$starttime.$eol.
		'DTEND:'.$enddate.'T'.$endtime.$eol.
		'LOCATION:'.$location.$eol.
		'DTSTAMP:'.date('Ymd',time()).'T'.date('his',time()).$eol.
		'SUMMARY:'.$title.$eol.
		'URL;VALUE=URI:'.LEPTON_URL.$eol.
		'DESCRIPTION:'.$description.$eol.
		'BEGIN:VALARM'.$eol.
		'ACTION:DISPLAY'.$eol.
		'TRIGGER:-PT'.$reminder.'M'.$eol.
		'DESCRIPTION:'.$reminder_text.$eol.
		'END:VALARM'.$eol.
		'END:VEVENT'.$eol.
		'END:VCALENDAR';
	$result = file_put_contents($filename,$sICalContent);	

	if($result === false)
	{
		echo(LEPTON_tools::display('Something went wrong, ics file not written!','pre','ui red message'));
	}
	return $result;
}