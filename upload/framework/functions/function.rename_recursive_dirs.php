<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *	Recursive function to rename all subdirectories and contents
 *  (PHP-function "rename" did not work on all windows installations)
 *
 *	@param	string	$dirsource	A valid path to the source directory
 *	@param	string	$dirdest	A valid path to the destination directory
 *	@param	int		$deep Counter for the recursion-deep. Default is 0.
 *	@return	bool
 *
 */
function rename_recursive_dirs(string $dirsource, string $dirdest, int $deep=0): bool
{
	LEPTON_handle::register("rm_full_dir");
	
    if (true === is_dir($dirsource))
    {
        if (false === is_dir($dirdest))
        {
            LEPTON_core::make_dir($dirdest);
        }
        
        $dir = dir($dirsource);
        while ( $file = $dir->read() )
        {
            if( $file[0] != "." )
            {
                if( !is_dir($dirsource."/".$file) )
                {
                    copy ($dirsource."/".$file, $dirdest."/".$file);
                    LEPTON_core::change_mode($dirdest."/".$file);
                } 
				else 
				{
                    LEPTON_core::make_dir($dirdest."/".$file);
                    rename_recursive_dirs($dirsource."/".$file, $dirdest.'/'.$file, $deep+1);
                }
            }
        }
        $dir->close();
    }
    if ($deep == 0)
    {
        rm_full_dir( $dirsource );
    }
    return true;
}
