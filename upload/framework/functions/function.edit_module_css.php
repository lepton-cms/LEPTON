<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/** 
 *  This function displays the "Edit CSS" button in modify.php
 *
 */ 
function edit_module_css( string $mod_dir ) : bool
{
	global $page_id, $section_id, $TEXT;
	LEPTON_handle::register("mod_file_exists");
	
	// check if the required edit_module_css.php file exists
	if ( !file_exists( LEPTON_PATH . '/modules/edit_module_files.php' ) )
	{
		return false;
	}
			
	// check if frontend.css or backend.css exist
	$frontend_css = mod_file_exists( $mod_dir, 'frontend.css' );
	$backend_css  = mod_file_exists( $mod_dir, 'backend.css' );
	
	// output the "edit CSS" form
	if ( $frontend_css || $backend_css )
	{
		$oTWIG = lib_twig_box::getInstance();
		
		$oTWIG->loader->prependPath( LEPTON_PATH."/templates/".DEFAULT_THEME."/templates/" );
		
		$fields = array(
			'LEPTON_URL'    => LEPTON_URL,
			'page_id'       => $page_id,
			'section_id'    => $section_id,
			'mod_dir'       => $mod_dir,
			'edit_file'     => ( $frontend_css ) ? 'frontend.css' : 'backend.css',
			'label_submit'  => $TEXT['CAP_EDIT_CSS']
		);
		
		echo $oTWIG->render(
			'edit_module_css_form.lte',
			$fields
		);
		
		return true;
	} 
	else 
	{
		return false;
	}
}
