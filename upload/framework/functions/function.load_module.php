<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2025 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


/**
 *	Load module information from the info.php of a given module into the current DB.
 *
 *	@param	string	$directory  Any valid directory(-path)
 *	@param	bool	$install    Call the install-script of the module? Default: false
 *
 */
function load_module( $directory, $install = false )
{
    global $MESSAGE;
	$database = LEPTON_database::getInstance();

    if (is_dir( LEPTON_PATH.'/modules/'.$directory) && file_exists(LEPTON_PATH.'/modules/'.$directory .'/info.php'))
    {
        require LEPTON_PATH.'/modules/'.$directory.'/info.php';
		
		$module_function = strtolower( $module_function );
		if(!isset($module_delete))
		{
			$module_delete = true;
		}	
	
		// Check that it doesn't already exist
		$sqlwhere = " type = 'module' AND directory = '".$module_directory."' ";
		if ( $database->get_one("SELECT COUNT(*) FROM ".TABLE_PREFIX."addons WHERE ".$sqlwhere) > 0 )
		{
			$sql_job = "update";
		}
		else
		{
			$sql_job = "insert";
			$sqlwhere = '';
		}
		
		$fields = array(
			'directory' => $module_directory,
			'name'		=> $module_name,
			'description' => $module_description,
			'type'		=> 'module',
			'function'	=> strtolower( $module_function ),
			'to_delete'	=> intval( $module_delete ),
			'version'	=> $module_version,
			'platform'	=> $module_platform,
			'author'	=> $module_author,
			'license'	=> $module_license				
		);

		if (isset($module_guid))
		{
			$fields['guid'] = $module_guid;		
		}
	
		$database->build_and_execute(
			$sql_job,
			TABLE_PREFIX . "addons",
			$fields,
			$sqlwhere
		);

		
		// Run installation script
		if (($install === true) && (file_exists(LEPTON_PATH.'/modules/'.$directory.'/install.php')))
		{
			require LEPTON_PATH.'/modules/'.$directory.'/install.php';
		}
    }
}
